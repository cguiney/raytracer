package raytracer

func NormalAt(obj Object, wp Point, x Intersection) Vector {
	op := WorldToObject(obj, wp)
	on := obj.NormalAt(op, x)
	wn := NormalToWorld(obj, on)
	return wn
}

func WorldToObject(object Object, p Point) Point {
	if object.Parent() != nil {
		p = WorldToObject(object.Parent(), p)
	}

	return p.transform(object.Transformation().Invert())
}

func NormalToWorld(object Object, normal Vector) Vector {
	normal = normal.transform(object.Transformation().Invert().Transpose())
	normal = normal.Normalize()

	if object.Parent() != nil {
		normal = NormalToWorld(object.Parent(), normal)
	}

	return normal
}
