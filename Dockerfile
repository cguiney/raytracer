FROM golang:1.15

ENV GOBIN /usr/local/bin

ADD . /go/src/raytracer

WORKDIR /go/src/raytracer

RUN go install ./cmd/...