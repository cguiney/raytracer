package raytracer

import "math"

type MicrofacetDistribution interface {
	D(Vector) Scalar
	G(Vector, Vector) Scalar
	Sample(ctx RenderCtx, wo Vector) Vector
	Pdf(wo, wh Vector) Scalar
}

type MicrofacetReflection struct {
	r       Color
	dist    MicrofacetDistribution
	fresnel Fresnel
}

func NewMicrofacetReflection(r Color, dist MicrofacetDistribution, fresnel Fresnel) MicrofacetReflection {
	return MicrofacetReflection{
		r:       r,
		dist:    dist,
		fresnel: fresnel,
	}
}

func (m MicrofacetReflection) Type() BxDFType {
	return BSDF_REFLECTION | BSDF_GLOSSY
}

func (m MicrofacetReflection) Eval(wo, wi Vector) Color {
	var (
		cosThetaO = costheta(wo).Abs()
		cosThetaI = costheta(wi).Abs()

		wh = wo.Add(wi)
	)

	if cosThetaO.Eq(0) || cosThetaI.Eq(0) {
		return Black
	}

	if wh.X().Eq(0) && wh.Y().Eq(0) && wh.Z().Eq(0) {
		return Black
	}

	wh = wh.Normalize()

	f := m.fresnel.Eval(wi.Dot(wh.Forward(NewVector(0, 1, 0))))

	var (
		d = m.dist.D(wh)
		g = m.dist.G(wo, wi)
	)

	return m.r.Scale(d * g).Mul(f).Scale(1 / (4 * cosThetaI * cosThetaO))

}

func (m MicrofacetReflection) Sample(ctx RenderCtx, wo Vector) (s SurfaceSample) {
	if wo.Y() == 0 {
		return
	}

	wh := m.dist.Sample(ctx, wo)
	if wo.Dot(wh) < 0 {
		return
	}

	s.Direction = reflect(wo, wh)

	if !sameHemisphere(wo, s.Direction) {
		return
	}

	s.Color = m.Eval(wo, s.Direction)
	s.PDF = m.dist.Pdf(wo, wh) / (4 * wo.Dot(wh))

	return
}

func (m MicrofacetReflection) Pdf(wo, wi Vector) Scalar {
	if !sameHemisphere(wo, wi) {
		return 0
	}

	wh := wo.Add(wi).Normalize()

	dpdf := m.dist.Pdf(wo, wh)
	denom := 4 * wo.Dot(wh)
	pdf := dpdf / denom

	if pdf < 0 {
		// panic("negative pdf")
		return 0
	}
	return pdf
}

type MicrofacetTransmission struct {
	transparency Color
	dist         MicrofacetDistribution
	fres         Fresnel

	etaA, etaB Scalar
}

func (m MicrofacetTransmission) Eval(wo, wi Vector) Color {
	if sameHemisphere(wo, wi) {
		return Black
	}

	cosThetaO := costheta(wo)
	cosThetaI := costheta(wi)

	if cosThetaO == 0 || cosThetaI == 0 {
		return Black
	}

	var eta Scalar

	if cosThetaO > 0 {
		eta = m.etaB / m.etaA
	} else {
		eta = m.etaA / m.etaB
	}

	wh := wo.Add(wi.Mul(eta)).Normalize()
	if wh.Y() < 0 {
		wh = wh.Negate()
	}

	fres := m.fres.Eval(wo.Dot(wh))
	d := m.dist.D(wh)
	g := m.dist.G(wo, wi)
	f := White.Sub(fres)

	sqrtDenom := wo.Dot(wh) + eta*wi.Dot(wh)
	// factor := Scalar(1) / eta
	factor := Scalar(1)

	/*
		if material.mode == Radiance { factor = 1/eta }
	*/

	term := Scalar.Abs(
		(d * g * eta * eta * wi.Dot(wh).Abs() * wo.Dot(wh).Abs() * factor * factor) /
			(cosThetaI * cosThetaO * sqrtDenom * sqrtDenom))

	res := f.Mul(m.transparency).Scale(term)

	return res
}

func (m MicrofacetTransmission) Type() BxDFType {
	return BSDF_GLOSSY | BSDF_TRANSMISSION
}

func (m MicrofacetTransmission) Sample(ctx RenderCtx, wo Vector) SurfaceSample {
	if wo.Y() == 0 {
		return SurfaceSample{}
	}

	wh := m.dist.Sample(ctx, wo)
	if wo.Dot(wh) < 0 {
		return SurfaceSample{}
	}

	var eta Scalar
	if costheta(wo) > 0 {
		eta = m.etaA / m.etaB
	} else {
		eta = m.etaB / m.etaA
	}

	wi, ok := refract(wo, wh, eta)
	if !ok {
		return SurfaceSample{}
	}

	return SurfaceSample{
		Direction: wi,
		Color:     m.Eval(wo, wi),
		PDF:       m.Pdf(wo, wi),
	}
}

func (m MicrofacetTransmission) Pdf(wo, wi Vector) Scalar {
	if sameHemisphere(wo, wi) {
		return 0
	}

	var eta Scalar
	if costheta(wo) > 0 {
		eta = m.etaB / m.etaA
	} else {
		eta = m.etaA / m.etaB
	}

	wh := wi.Mul(eta).Add(wo).Normalize()

	sqrtDenom := wo.Dot(wh) + eta*wi.Dot(wh)
	dwhDwi := Scalar.Abs(eta*eta*wi.Dot(wh)) / (sqrtDenom * sqrtDenom)

	return m.dist.Pdf(wo, wh) * dwhDwi
}

func roughnessToAlpha(roughness Scalar) Scalar {
	roughness = max(roughness, 1e-3)
	x := roughness.Log()
	return 1.62142 + 0.819955*x + 0.1734*x*x + 0.0171201*x*x*x +
		0.000640711*x*x*x*x
}

func NewTrowbridgeReitzDist(roughness Scalar) TrowbridgeReitzDistribution {
	return TrowbridgeReitzDistribution{
		alphaX: roughness,
		alphaZ: roughness,
	}
}

type TrowbridgeReitzDistribution struct {
	alphaX, alphaZ Scalar
}

// http://jcgt.org/published/0007/04/01/paper.pdf
func (t TrowbridgeReitzDistribution) SampleGGX(ctx RenderCtx, wo Vector) Vector {
	var (
		u = [...]Scalar{
			ctx.RandomScalar(),
			ctx.RandomScalar(),
		}

		phi = 2 * math.Pi * u[1]

		vh    = NewVector(wo.X()*t.alphaX, wo.Y(), wo.Z()*t.alphaZ).Normalize()
		lensq = vh.X().Pow2() + vh.Z().Pow2()
	)

	var (
		T1 Vector
		T2 Vector
	)

	if lensq > 0 {
		T1 = NewVector(-vh.Z(), 0, vh.X()).Mul(1 / lensq.Sqrt())
	} else {
		T1 = NewVector(1, 0, 0)
	}

	T2 = vh.Cross(T1)

	var (
		r  = Scalar.Sqrt(u[0])
		t1 = r * Scalar.Cos(phi)
		t2 = r * Scalar.Sin(phi)
		s  = 0.5 * (1.0 + vh.Y())
	)

	t2 = (1.0-s)*Scalar.Sqrt(Scalar.Pow2(1.0-t1)) + (s * t2)

	var (
		nh = T1.Mul(t1).Add(T2.Mul(t2)).Add(vh.Mul(Scalar.Sqrt(max(0, 1.0-(t1*t1)-(t2*t2)))))
		ne = NewVector(t.alphaX*nh.X(), max(0, nh.Y()), t.alphaZ*nh.Z())
	)

	return ne
}

func (t TrowbridgeReitzDistribution) Sample(ctx RenderCtx, wo Vector) Vector {
	/*
		flip := wo.Y() < 0

		if flip {
			wo = wo.Negate()
		}

		s := t.SampleGGX(ctx, wo)

		if flip {
			s = s.Negate()
		}

		return s
	*/
	return t.sampleLegacy(ctx, wo)
}

func (t TrowbridgeReitzDistribution) sampleLegacy(ctx RenderCtx, wo Vector) Vector {
	var (
		u = [...]Scalar{
			ctx.RandomScalar(),
			ctx.RandomScalar(),
		}

		cosTheta Scalar
		phi      = 2 * math.Pi * u[1]
	)

	if t.alphaX.Eq(t.alphaZ) {
		tanTheta2 := Scalar.Pow2(t.alphaX) * u[0] / (1 - u[0])
		cosTheta = 1 / Scalar.Sqrt(tanTheta2+1)
	} else {
		phi = (t.alphaZ / t.alphaX * (2*math.Pi*u[1] + 0.5*math.Pi).Tan()).Atan()
		if u[1] > 0.5 {
			phi += math.Pi
		}

		var (
			sinphi  = phi.Sin()
			cosphi  = phi.Cos()
			alphaX2 = t.alphaX.Pow2()
			alphaY2 = t.alphaZ.Pow2()

			alpha2 = 1 / (cosphi*cosphi/alphaX2 + sinphi*sinphi/alphaY2)

			tanTheta2 = alpha2 * u[0] / (1 - u[0])
		)

		cosTheta = 1 / (tanTheta2 + 1).Sqrt()
	}

	var (
		sinTheta = max(0, Scalar.Sqrt(Scalar.Pow2(1-cosTheta)))
		wh       = sphericalDirection(sinTheta, cosTheta, phi)
	)
	if !sameHemisphere(wo, wh) {
		wh = wh.Negate()
	}

	return wh
}

func sphericalDirection(sinTheta, cosTheta, phi Scalar) Vector {
	return NewVector(
		sinTheta*phi.Cos(),
		cosTheta,
		sinTheta*phi.Sin(),
	)
}

func (t TrowbridgeReitzDistribution) D(wh Vector) Scalar {
	var tan2theta = sin2theta(wh) / cos2theta(wh)

	if tan2theta.IsInf() {
		return 0
	}

	var (
		cos4theta = cos2theta(wh).Pow2()
		e         = (cos2phi(wh)/t.alphaX.Pow2() + sin2phi(wh)/t.alphaZ.Pow2()) * tan2theta
	)

	return 1 / (math.Pi * t.alphaX * t.alphaZ * cos4theta * Scalar.Pow2(1+e))
}

func (t TrowbridgeReitzDistribution) G(wo, wi Vector) Scalar {
	return 1 / (1 + t.Lambda(wo) + t.Lambda(wi))
}

func (t TrowbridgeReitzDistribution) Lambda(w Vector) Scalar {
	var absTanTheta = Scalar.Abs(tantheta(w))
	if absTanTheta.Eq(0) {
		return 0
	}

	var (
		alpha           = Scalar.Sqrt((cos2phi(w) * t.alphaX.Pow2()) + (sin2phi(w) * t.alphaZ.Pow2()))
		alpha2tan2theta = Scalar.Pow2(alpha * absTanTheta)
	)

	return (-1 + Scalar.Sqrt(1+alpha2tan2theta)) / 2
}

func (t TrowbridgeReitzDistribution) Pdf(wo, wh Vector) Scalar {
	pdf := t.D(wh) * costheta(wh).Abs()
	if pdf < 0 {
		panic("negative pdf")
	}
	return pdf
	// var g1 = 1 / (1 + t.Lambda(wo))
	// return t.D(wh) * g1 * Scalar.Abs(wo.Dot(wh)) * Scalar.Abs(costheta(wh))
}
