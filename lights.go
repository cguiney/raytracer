package raytracer

import "math"

var (
	_ Light = PointLight{}
	_ Light = LightObject{}
)

type Light interface {
	IsDeltaDistribution() bool
	SampleAt(ctx RenderCtx, interaction Interaction) (LightSample, bool)
	PdfAt(RenderCtx, Interaction, Vector) Scalar
}

type LightSample struct {
	Point
	Normal Vector
	SurfaceSample
}

type PointLight struct {
	pos       Point
	intensity Color
	transformation
}

func NewPointLight(p Point, c Color) PointLight {
	t := newTransformation()
	t.Translate(NewTranslation(p.X(), p.Y(), p.Z()))
	return PointLight{
		pos:            p,
		intensity:      c,
		transformation: t,
	}
}

func (li PointLight) IsDeltaDistribution() bool {
	return true
}

func (li PointLight) Position() Point {
	return li.pos
}

func (li PointLight) Intensity() Color {
	return li.intensity
}

func (li PointLight) PdfAt(RenderCtx, Interaction, Vector) Scalar {
	return 0
}

func (li PointLight) SampleAt(ctx RenderCtx, interaction Interaction) (LightSample, bool) {
	var (
		path  = li.pos.PathFrom(interaction.point)
		color = li.intensity.Scale(1 / path.Magnitude()) // li.intensity
	)
	return LightSample{
		Point: li.pos,
		SurfaceSample: SurfaceSample{
			Color:     color,
			Direction: path.Normalize(),
			PDF:       1,
		}}, !color.Eq(Black)
}

// Wraps an object to create a diffuse area light
type LightObject struct {
	Object
	samples int
}

func (lo LightObject) IsDeltaDistribution() bool {
	return false
}

func (lo LightObject) radiance(normal, lightv Vector) Color {
	ndo := lightv.Dot(normal)
	if ndo <= 0 {
		return Black
	}

	return lo.Material().Emission()
}

func (lo LightObject) SampleAt(ctx RenderCtx, interaction Interaction) (LightSample, bool) {
	var (
		s        = SampleAt(ctx, lo.Object, interaction)
		radiance = lo.radiance(s.Normal, s.Direction)
	)

	return LightSample{
		Point:  s.Point,
		Normal: s.Normal,
		SurfaceSample: SurfaceSample{
			Color:     radiance,
			Direction: s.Direction,
			PDF:       s.PDF,
		},
	}, !radiance.Eq(Black) && s.PDF > 0
}

type InfiniteAreaLight struct {
	center Point
	radius Scalar
}

type DistantLight struct {
	transformation
	direction Vector
	radiance  Color
	distance  Scalar
}

func NewDistantLight(direction Vector, radiance Color) *DistantLight {
	return &DistantLight{
		transformation: newTransformation(),
		direction:      direction.Normalize(),
		radiance:       radiance,
	}
}

func (d *DistantLight) IsDeltaDistribution() bool {
	return true
}

func (d *DistantLight) SampleAt(ctx RenderCtx, interaction Interaction) (LightSample, bool) {
	return LightSample{
		Point: interaction.point.TravelTo(d.direction).Mul(d.distance),
		SurfaceSample: SurfaceSample{
			Color:     d.radiance,
			Direction: d.direction,
			PDF:       1,
		},
	}, true
}

func (d *DistantLight) Optimize(w *World) {
	d.distance = 2 * w.box.Sphere().Raidus()
}

func (d *DistantLight) PdfAt(RenderCtx, Interaction, Vector) Scalar {
	return 1
}

func Lighting(m *MaterialProperties, object Object, li PointLight, pos Point, eye, normal Vector, shadowed bool, uv UV) Color {
	var color Color

	si := SurfaceInteraction{
		Interaction: Interaction{
			point: pos,
		},
	}

	if m.Pattern() != nil {
		color = PatternOnObject(m.Pattern(), object, si)
	} else {
		color = m.ColorMap().At(si)
	}

	// combine surface color w/ light color/intensity
	effectiveColor := color.Mul(li.Intensity())

	// find direction of the light source
	lightv := li.Position().PathFrom(pos).Normalize()

	// compute ambient contribution
	ambient := effectiveColor.Mul(m.Ambient())

	// if it's in the shadow, we can omit computing the diffuse and specular components
	if shadowed {
		return ambient
	}

	// cosine of angle between the light vector and normal vector
	// negative number means the light is on the other side of the surface

	lightDotNormal := lightv.Dot(normal)

	var (
		diffuse  Color
		specular Color
	)

	if lightDotNormal >= 0 {
		// diffuse = effectiveColor.Mul(material.DiffuseMap().At(uv.U, uv.V).Scale(lightDotNormal))
		diffuse = effectiveColor.Mul(m.Diffuse().Scale(lightDotNormal))

		reflectv := lightv.Negate().Reflect(normal)

		// cosine of angle between the reflection vector and eye vector
		// negative number means the  light reflects away from the eye
		reflectDotEye := reflectv.Dot(eye)

		if reflectDotEye > 0 {
			// compute specular contribution
			factor := Scalar(math.Pow(float64(reflectDotEye), float64(m.Shininess())))
			specular = li.Intensity().Mul(m.Specular().Scale(factor))
		}
	}

	finalColor := ambient.Add(diffuse).Add(specular)
	return finalColor
}

func powerHeuristic(nf, npdf, ng, gpdf Scalar) Scalar {
	var (
		f = nf * npdf
		g = ng * gpdf
	)

	return f.Pow2() / (f.Pow2() + g.Pow2())
}
