package raytracer

import "math"

type Pattern interface {
	Transformer
	At(SurfaceInteraction) Color
}

type Stripe struct {
	a, b Color
	transformation
}

func NewStripe(a, b Color) *Stripe {
	return &Stripe{
		a:              a,
		b:              b,
		transformation: newTransformation(),
	}
}

func (s *Stripe) At(si SurfaceInteraction) Color {
	if int(math.Floor(float64(si.point.X())))%2 == 0 {
		return s.a
	}
	return s.b
}

type Gradient struct {
	from Color
	to   Color

	transformation
}

func NewGradient(from, to Color) *Gradient {
	return &Gradient{
		from:           from,
		to:             to,
		transformation: newTransformation(),
	}
}

func (g *Gradient) At(si SurfaceInteraction) Color {

	var (
		p        = si.point
		dist     = g.to.Sub(g.from)
		fraction = p.X() - Scalar(math.Floor(float64(p.X())))
	)
	return g.from.Add(dist.Scale(fraction))
}

type Ring struct {
	a, b Color
	transformation
}

func NewRing(a, b Color) *Ring {
	return &Ring{
		a:              a,
		b:              b,
		transformation: newTransformation(),
	}
}

func (r *Ring) At(si SurfaceInteraction) Color {
	var (
		p    = si.point
		x    = math.Pow(float64(p.X()), 2)
		z    = math.Pow(float64(p.Z()), 2)
		test = int(math.Floor(math.Sqrt(x + z)))
	)
	if test%2 == 0 {
		return r.a
	}
	return r.b
}

type Checkers struct {
	a, b Color
	transformation
}

func NewCheckers(a, b Color) *Checkers {
	return &Checkers{
		a:              a,
		b:              b,
		transformation: newTransformation(),
	}
}

func (c *Checkers) At(si SurfaceInteraction) Color {
	var (
		p = si.point
		x = math.Floor(float64(p.X()))
		y = math.Floor(float64(p.Y()))
		z = math.Floor(float64(p.Z()))
	)

	if int(x+y+z)%2 == 0 {
		return c.a
	}

	return c.b
}

// PatternOnObject gives the color for a given texture as it should appear
// on the provided object at the provided world point.
func PatternOnObject(pat Pattern, object Object, si SurfaceInteraction) Color {
	si.point = WorldToObject(object, si.point)
	si.point = si.point.transform(pat.Transformation().Invert())
	return pat.At(si)
}
