package raytracer

import (
	"math"
	"testing"
)

func TestWorld(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		w := NewWorld()

		if w.Light() != (PointLight{}) {
			t.Error("expected world to start with no light")
		}

		if len(w.Objects()) != 0 {
			t.Error("expected world to start with no objects")
		}
	})

	t.Run("intersection", func(t *testing.T) {
		w := DefaultWorld()
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))

		xs := Intersections{}
		w.Intersect(r, &xs)

		if len(xs) != 4 {
			t.Errorf("incorrect number of intersections. have=%d want=%d", len(xs), 4)
		}

		times := []Scalar{4, 4.5, 5.5, 6}

		for i, x := range xs {
			if !x.Time().Eq(times[i]) {
				t.Errorf("incorrect intersection time for hit %d. have=%f want=%f",
					i, x.Time(), times[i])
			}
		}
	})

	t.Run("shading intersection", func(t *testing.T) {
		world := DefaultWorld()
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))
		shape := world.Objects()[0]
		x := NewIntersection(shape, 4)
		state := NewSurfaceInteraction(x, r)

		w := NewWhittedIntegrator(world, NewCamera(100, 100, math.Pi))
		if want, have := NewColor(0.38066, 0.47583, 0.2855), w.shade(DefaultContext(), state); !have.Eq(want) {
			t.Errorf("incorrect color for hit. have=%v want=%v", have, want)
		}
	})

	t.Run("shading intersection from inside", func(t *testing.T) {
		w := DefaultWorld()
		w.SetLight(NewPointLight(NewPoint(0, 0.25, 0), NewColor(1, 1, 1)))
		r := NewRay(NewPoint(0, 0, 0), NewVector(0, 0, 1))
		shape := w.Objects()[1]
		x := NewIntersection(shape, 0.5)
		state := NewSurfaceInteraction(x, r)

		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, math.Pi))
		if want, have := NewColor(0.90498, 0.90498, 0.90498), integrator.shade(DefaultContext(), state); !have.Eq(want) {
			t.Errorf("incorrect color for hit. have=%v want=%v", have, want)
		}
	})

	t.Run("shading intersection in shadow", func(t *testing.T) {
		var (
			s1 = NewSphere()
			s2 = func() *Sphere {
				s := NewSphere()
				s.Translate(NewTranslation(0, 0, 10))
				return s
			}()
		)

		w := NewWorld()
		w.SetLight(NewPointLight(NewPoint(0, 0, -10), NewColor(1, 1, 1)))
		w.AddObject(s1)
		w.AddObject(s2)

		r := NewRay(NewPoint(0, 0, 5), NewVector(0, 0, 1))
		x := NewIntersection(s1, 4)

		state := NewSurfaceInteraction(x, r)
		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, math.Pi))
		if want, have := NewColor(0.1, 0.1, 0.1), integrator.shade(DefaultContext(), state); !have.Eq(want) {
			t.Errorf("incorrect color for his.  have=%v want=%v", have, want)
		}
	})

	t.Run("shading hit should offset the point", func(t *testing.T) {
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))
		shape := func() *Sphere {
			s := NewSphere()
			s.Translate(NewTranslation(0, 0, 1))
			return s
		}()

		x := NewIntersection(shape, 5)

		state := NewSurfaceInteraction(x, r)

		if have := state.over.Z(); !(have < -epsilon/2) {
			t.Errorf("overpoint is too high. have=%f want < %f", have, -epsilon/2)
		}

		if have := state.over.Z(); !(state.point.Z() > have) {
			t.Errorf("overpoint is too low. have=%f want >= %f", have, state.point.Z())
		}
	})

	t.Run("color at", func(t *testing.T) {
		type colorTest struct {
			name  string
			world func() *World
			r     Ray
			want  Color
		}

		tests := []colorTest{
			{
				name:  "ray misses",
				world: DefaultWorld,
				r:     NewRay(NewPoint(0, 0, -5), NewVector(0, 1, 0)),
				want:  NewColor(0, 0, 0),
			},
			{
				name:  "ray hits",
				world: DefaultWorld,
				r:     NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1)),
				want:  NewColor(0.38066, 0.47583, 0.2855),
			},
			{
				name: "intersection behind ray",
				world: func() *World {
					w := DefaultWorld()
					w.objects[0] = func() *Sphere {
						m := defaultMaterial
						m.SetAmbient(NewGray(1))

						s := NewSphere()
						s.SetMaterial(&m)
						return s
					}()
					w.objects[1] = func() *Sphere {
						m := NewMaterialProperties()
						m.SetAmbient(White)

						s := NewSphere()
						s.Scale(NewScale(0.5, 0.5, 0.5))
						s.SetMaterial(m)

						return s
					}()
					return w
				},
				r:    NewRay(NewPoint(0, 0, 0.75), NewVector(0, 0, -1)),
				want: NewColor(1, 1, 1),
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				w := tt.world()
				integrator := NewWhittedIntegrator(w, NewCamera(100, 100, math.Pi))
				if have := integrator.ColorAt(DefaultContext(), tt.r); !have.Eq(tt.want) {
					t.Errorf("incorrect color for ray. have=%v want=%v", have, tt.want)
				}
			})
		}
	})

	t.Run("shadowing", func(t *testing.T) {
		type shadowTest struct {
			name string
			w    *World
			p    Point
			want bool
		}

		tests := []shadowTest{
			{
				name: "no shadow when nothing is collinear with point and light",
				w:    DefaultWorld(),
				p:    NewPoint(0, 10, 0),
				want: false,
			},
			{
				name: "shadow when an object is between point and light",
				w:    DefaultWorld(),
				p:    NewPoint(10, -10, 10),
				want: true,
			},
			{
				name: "no shadow when object is behind light",
				w:    DefaultWorld(),
				p:    NewPoint(-20, 20, -20),
				want: false,
			},
			{
				name: "no shadow when an object is behind point",
				w:    DefaultWorld(),
				p:    NewPoint(-2, 2, -2),
				want: false,
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				integrator := NewWhittedIntegrator(tt.w, NewCamera(100, 100, math.Pi))
				if have := integrator.isShadowed(tt.w.lights[0].(PointLight), tt.p); have != tt.want {
					t.Errorf("incorrect result of shadowing. have=%t want=%t", have, tt.want)
				}
			})
		}
	})

	t.Run("reflection of non reflective surface", func(t *testing.T) {
		w := DefaultWorld()
		r := NewRay(NewPoint(0, 0, 0), NewVector(0, 0, 1))

		m := NewMaterialProperties()
		m.SetAmbient(White)

		shape := w.Objects()[1]
		shape.(MutableObject).SetMaterial(m)
		x := NewIntersection(shape, 1)
		state := NewSurfaceInteraction(x, r)

		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, math.Pi))

		if want, have := Black, integrator.reflect(DefaultContext(), state.Interaction); !have.Eq(want) {
			t.Errorf("incorrect color for reflection. have=%s want=%s", have, want)
		}
	})

	t.Run("reflection of reflective surface", func(t *testing.T) {
		m := NewMaterialProperties()
		m.SetReflective(0.5)

		p := NewPlane()
		p.SetMaterial(m)
		p.Translate(NewTranslation(0, -1, 0))

		w := DefaultWorld()
		w.AddObject(p)

		r := NewRay(NewPoint(0, 0, -3), NewVector(0, -math.Sqrt2/2, math.Sqrt2/2))
		x := NewIntersection(p, math.Sqrt2)

		state := NewSurfaceInteraction(x, r)

		rr := NewRay(state.over, state.reflect)
		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, 0))
		col := integrator.ColorAt(DefaultContext(), rr)
		res := col.Scale(m.Reflective())

		t.Logf("point: %v", state.point)
		t.Logf("normal: %v", state.normal)
		t.Logf("eye: %v", state.eye)
		t.Logf("overpoint: %v", state.over)
		t.Logf("reflection vector: %v", state.reflect)
		t.Logf("color: %v", col)
		t.Logf("color mul: %v", res)

		if want, have := NewColor(0.190332, 0.237915, 0.142749), integrator.reflect(DefaultContext(), state.Interaction); !have.Eq(want) {
			t.Errorf("incorrect color for reflection. have=%s want=%s", have, want)
		}

		if want, have := NewColor(0.876757, 0.92434, 0.829174), integrator.shade(DefaultContext(), state); !have.Eq(want) {
			t.Errorf("incorrect color for shading. have=%s want=%s", have, want)
		}
	})

	t.Run("mutually reflective surfaces", func(t *testing.T) {
		w := NewWorld()
		w.SetLight(NewPointLight(NewPoint(0, 0, 0), White))

		m := NewMaterialProperties()
		m.SetReflective(1)

		lower := NewPlane()
		lower.SetMaterial(m)
		lower.Translate(NewTranslation(0, -1, 0))

		upper := NewPlane()
		upper.SetMaterial(m)
		upper.Translate(NewTranslation(0, 1, 0))

		w.AddObject(lower)
		w.AddObject(upper)

		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, 0))
		integrator.ColorAt(DefaultContext(), NewRay(NewPoint(0, 0, 0), NewVector(0, 1, 0)))
	})

	t.Run("refracted color of opaque surface", func(t *testing.T) {
		w := DefaultWorld()
		shape := w.Objects()[0]
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))

		xs := Intersections{
			NewIntersection(shape, 4),
			NewIntersection(shape, 6),
		}

		state := NewSurfaceInteraction(xs[0], r, xs...)
		integrator := NewWhittedIntegrator(w, NewCamera(100, 100, 0))
		have := integrator.refract(DefaultContext(), state.Interaction)

		if want := Black; !have.Eq(want) {
			t.Errorf("incorrect color for refraction. have=%s want=%s", have, want)
		}
	})

	t.Run("refracted color at maximum recursion depth", func(t *testing.T) {
		w := DefaultWorld()

		m := NewMaterialProperties()
		m.SetTransparency(1)
		m.SetRefractiveIndex(1.5)

		shape := w.Objects()[0]
		shape.(MutableObject).SetMaterial(m)

		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))
		xs := Intersections{
			NewIntersection(shape, 4),
			NewIntersection(shape, 6),
		}

		state := NewSurfaceInteraction(xs[0], r, xs...)

		ctx := DefaultContext()
		ctx.remaining = 0

		integrator := NewWhittedIntegrator(w, NewCamera(1, 1, 0))
		have := integrator.refract(ctx, state.Interaction)
		if want := Black; !have.Eq(want) {
			t.Errorf("incorrect color for refraction. have=%s want=%s", have, want)
		}
	})

	t.Run("refracted color during total internal refraction", func(t *testing.T) {
		w := DefaultWorld()

		m := NewMaterialProperties()
		m.SetTransparency(1)
		m.SetRefractiveIndex(1)

		shape := w.Objects()[0]
		shape.(MutableObject).SetMaterial(m)

		r := NewRay(NewPoint(0, 0, math.Sqrt2/2), NewVector(0, 1, 0))
		xs := Intersections{
			NewIntersection(shape, -math.Sqrt2/2),
			NewIntersection(shape, math.Sqrt2/2),
		}

		state := NewSurfaceInteraction(xs[1], r, xs...)

		integrator := NewWhittedIntegrator(w, NewCamera(1, 1, 1))
		have := integrator.refract(DefaultContext(), state.Interaction)
		if want := Black; !have.Eq(want) {
			t.Errorf("incorrect color for refraction. have=%s want=%s", have, want)
		}
	})

	t.Run("refracted color with refracted ray", func(t *testing.T) {
		w := DefaultWorld()

		m := NewDefaultMaterialProperties()
		m.SetAmbient(White)
		m.SetPattern(NewTestPattern())

		a := w.Objects()[0]
		a.(MutableObject).SetMaterial(m)

		b := w.Objects()[1]
		b.(MutableObject).SetMaterial(&Glass)

		r := NewRay(NewPoint(0, 0, 0.1), NewVector(0, 1, 0))

		xs := Intersections{
			NewIntersection(a, -0.9899),
			NewIntersection(b, -0.4899),
			NewIntersection(b, 0.4899),
			NewIntersection(a, 0.9899),
		}

		state := NewSurfaceInteraction(xs[2], r, xs...)
		integrator := NewWhittedIntegrator(w, NewCamera(1, 1, 1))

		have := integrator.refract(DefaultContext(), state.Interaction)

		if want := NewColor(0, 0.998875, 0.047219); !have.Eq(want) {
			t.Errorf("incorrect color for refraction. have=%s want=%s", have, want)
		}
	})

	t.Run("shading with a transparent material", func(t *testing.T) {
		w := DefaultWorld()

		floor := NewPlane()
		floor.Translate(NewTranslation(0, -1, 0))
		floor.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetTransparency(0.5)
			m.SetRefractiveIndex(1.5)
			return m
		}())


		ball := NewSphere()
		ball.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetColor(Red)
			m.SetAmbient(NewGray(0.5))
			return m
		}())
		ball.Translate(NewTranslation(0, -3.5, -0.5))

		w.AddObject(floor)
		w.AddObject(ball)

		r := NewRay(NewPoint(0, 0, -3), NewVector(0, -math.Sqrt2/2, math.Sqrt2/2))
		xs := Intersections{
			NewIntersection(floor, math.Sqrt2),
		}

		state := NewSurfaceInteraction(xs[0], r, xs...)
		integrator := NewWhittedIntegrator(w, NewCamera(1, 1, 1))
		have := integrator.shade(DefaultContext(), state)

		if want := NewColor(0.93642, 0.68642, 0.68642); !have.Eq(want) {
			t.Errorf("incorrect color for shading. have=%s want=%s", have, want)
		}
	})

	t.Run("shading with a transparent material", func(t *testing.T) {
		w := DefaultWorld()

		floor := NewPlane()
		floor.Translate(NewTranslation(0, -1, 0))

		floor.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetReflective(0.5)
			m.SetTransparency(0.5)
			m.SetRefractiveIndex(1.5)
			return m
		}())

		ball := NewSphere()
		ball.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetColor(Red)
			m.SetAmbient(NewGray(0.5))
			return m
		}())

		ball.Translate(NewTranslation(0, -3.5, -0.5))

		w.AddObject(floor)
		w.AddObject(ball)

		r := NewRay(NewPoint(0, 0, -3), NewVector(0, -math.Sqrt2/2, math.Sqrt2/2))
		xs := Intersections{
			NewIntersection(floor, math.Sqrt2),
		}

		state := NewSurfaceInteraction(xs[0], r, xs...)
		integrator := NewWhittedIntegrator(w, NewCamera(1, 1, 1))
		have := integrator.shade(DefaultContext(), state)
		if want := NewColor(0.93391, 0.69643, 0.69243); !have.Eq(want) {
			t.Errorf("incorrect color for shading. have=%s want=%s", have, want)
		}
	})
}
