package raytracer

import (
	"math"
	"testing"
)

func TestNormals(t *testing.T) {
	t.Run("converting point from world to object space", func(t *testing.T) {
		g1 := NewGroup()
		g2 := NewGroup()

		g1.RotateY(math.Pi / 2)
		g2.Scale(NewScale(2, 2, 2))

		g1.AddObject(g2)

		s := NewSphere()
		s.Translate(NewTranslation(5, 0, 0))

		g2.AddObject(s)

		p := WorldToObject(s, NewPoint(-2, 0, -10))

		if want := NewPoint(0, 0, -1); !p.Eq(want) {
			t.Errorf("incorrect object point for point. have=%v want=%v", p, want)
		}
	})

	t.Run("convert normal from object space to world space", func(t *testing.T) {
		g1 := NewGroup()
		g1.RotateY(math.Pi / 2)

		g2 := NewGroup()
		g2.Scale(NewScale(1, 2, 3))

		s := NewSphere()
		s.Translate(NewTranslation(5, 0, 0))

		g2.AddObject(s)
		g1.AddObject(g2)

		sq3 := Scalar(math.Sqrt(3) / 3)

		n := NormalToWorld(s, NewVector(sq3, sq3, sq3))

		if want := NewVector(0.285714, 0.428571, -0.857142); !n.Eq(want) {
			t.Errorf("incorrect vector for transforming from normal. have=%v want=%v", n, want)
		}
	})

	t.Run("finding normal of child object", func(t *testing.T) {
		g1 := NewGroup()
		g1.RotateY(math.Pi / 2)

		g2 := NewGroup()
		g2.Scale(NewScale(1, 2, 3))

		s := NewSphere()
		s.Translate(NewTranslation(5, 0, 0))

		g2.AddObject(s)
		g1.AddObject(g2)

		n := NormalAt(s, NewPoint(1.7321, 1.1547, -5.5774), Intersection{})

		if want := NewVector(0.285703, 0.428543, -0.85716); !n.Eq(want) {
			t.Errorf("incorrect normal for point on shere. have=%v want=%v", n, want)
		}
	})
}
