package raytracer

type Matrix [4]Tuple

func IdentityMatrix() Matrix {
	return Matrix{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}
}

func (m Matrix) Eq(o Matrix) bool {
	for h := range m[:] {
		for w := range m[h][:] {
			if !m[h][w].Eq(o[h][w]) {
				return false
			}
		}
	}
	return true
}

func (m Matrix) Mul(o Matrix) Matrix {
	var x Matrix
	for row := range m[:] {
		for col := range m[row][:] {
			x[row][col] = (m[row][0]*o[0][col] +
				m[row][1]*o[1][col] +
				m[row][2]*o[2][col] +
				m[row][3]*o[3][col])
		}
	}
	return x
}

func (m Matrix) Mulv(t Tuple) (x Tuple) {
	x[0] = tdot(m[0], t)
	x[1] = tdot(m[1], t)
	x[2] = tdot(m[2], t)
	x[3] = tdot(m[3], t)
	return
}


func (m Matrix) Transpose() Matrix {
	var x Matrix
	for row := range m[:] {
		for col := range m[row][:] {
			x[col][row] = m[row][col]
		}
	}
	return x
}

func (m Matrix) SubMatrix(row, column int) (x Matrix3D) {
	var (
		xr int
		xc int
	)

	for mr := range m[:] {
		if mr == row {
			continue
		}
		for mc := range m[mr][:] {
			if mc == column {
				continue
			}
			x[xr][xc] = m[mr][mc]
			xc++
		}

		xr++
		xc = 0
	}

	return
}

func (m Matrix) Minor(row, column int) Scalar {
	return m.SubMatrix(row, column).Determinant()
}

func (m Matrix) Cofactor(row, column int) Scalar {
	minor := m.Minor(row, column)
	if (row+column)%2 == 1 {
		return -minor
	}
	return minor
}

func (m Matrix) Determinant() Scalar {
	return m[0][0]*m.Cofactor(0, 0) +
		m[0][1]*m.Cofactor(0, 1) +
		m[0][2]*m.Cofactor(0, 2) +
		m[0][3]*m.Cofactor(0, 3)
}

func (m Matrix) Invert() Matrix {
	var (
		x Matrix
		d = m.Determinant()
	)

	for row := range m[:] {
		for col := range m[row][:] {
			cofactor := m.Cofactor(row, col)
			x[col][row] = cofactor / d // col, row transposition is intentional
		}
	}

	return x
}

func (m Matrix) ptr() *Matrix {
	return &m
}

type Matrix3D [3][3]Scalar

func (m Matrix3D) Eq(o Matrix3D) bool {
	for h := range m[:] {
		for w := range m[h][:] {
			if !m[h][w].Eq(o[h][w]) {
				return false
			}
		}
	}
	return true
}

func (m Matrix3D) SubMatrix(row, column int) (x Matrix2D) {
	var (
		xr int
		xc int
	)

	for mr := range m[:] {
		if mr == row {
			continue
		}
		for mc := range m[mr][:] {
			if mc == column {
				continue
			}
			x[xr][xc] = m[mr][mc]
			xc++
		}

		xr++
		xc = 0
	}

	return
}

func (m Matrix3D) Minor(row, column int) Scalar {
	return m.SubMatrix(row, column).Determinant()
}

func (m Matrix3D) Cofactor(row, column int) Scalar {
	minor := m.Minor(row, column)
	if (row+column)%2 == 1 {
		return -minor
	}
	return minor
}

func (m Matrix3D) Determinant() Scalar {
	return m[0][0]*m.Cofactor(0, 0) +
		m[0][1]*m.Cofactor(0, 1) +
		m[0][2]*m.Cofactor(0, 2)
}

type Matrix2D [2][2]Scalar

func (m Matrix2D) Eq(o Matrix2D) bool {
	for h := range m[:] {
		for w := range m[h][:] {
			if !m[h][w].Eq(o[h][w]) {
				return false
			}
		}
	}
	return true
}

func (m Matrix2D) Determinant() Scalar {
	return m[0][0]*m[1][1] - m[0][1]*m[1][0]
}
