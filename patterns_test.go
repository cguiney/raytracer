package raytracer

import "testing"

var (
	_ Pattern = &TestPattern{}
)

type TestPattern struct {
	transformation
}

func NewTestPattern() *TestPattern {
	return &TestPattern{
		transformation: newTransformation(),
	}
}

func (t *TestPattern) At(si SurfaceInteraction) Color {
	var p = si.point
	return NewColor(p.X(), p.Y(), p.Z())
}

type patternTest struct {
	at   Point
	want Color
}

func runPatternTests(t *testing.T, pattern Pattern, tests []patternTest) {
	t.Helper()
	for i, tt := range tests {
		var si = SurfaceInteraction{Interaction: Interaction{point: tt.at}}
		if have := pattern.At(si); !have.Eq(tt.want) {
			t.Errorf(
				"incorrect color at point %v for test %d. have=%s want=%s",
				tt.at, i, have, tt.want)
		}
	}
}

func TestStripe(t *testing.T) {

	t.Run("texture is constant in Y", func(t *testing.T) {
		tests := []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0, 1, 0), want: White},
			{at: NewPoint(0, 2, 0), want: White},
		}

		runPatternTests(t, NewStripe(White, Black), tests)
	})

	t.Run("texture is constant in Z", func(t *testing.T) {
		tests := []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0, 0, 1), want: White},
			{at: NewPoint(0, 0, 1), want: White},
		}

		runPatternTests(t, NewStripe(White, Black), tests)
	})

	t.Run("texture is alternates in X", func(t *testing.T) {
		tests := []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0.9, 0, 0), want: White},
			{at: NewPoint(1.0, 0, 0), want: Black},
			{at: NewPoint(-0.1, 0, 0), want: Black},
			{at: NewPoint(-1.0, 0, 0), want: Black},
			{at: NewPoint(-1.1, 0, 0), want: White},
		}

		runPatternTests(t, NewStripe(White, Black), tests)
	})

	t.Run("with object transformation", func(t *testing.T) {
		object := NewSphere()
		object.Scale(NewScale(2, 2, 2))

		pattern := NewStripe(White, Black)
		var si = SurfaceInteraction{
			Interaction: Interaction{
				point: NewPoint(1.5, 0, 0),
			},
		}
		have := PatternOnObject(pattern, object, si)

		if !have.Eq(White) {
			t.Errorf("incorrect color at point. have=%s want=%s", have, White)
		}
	})

	t.Run("with texture transformation", func(t *testing.T) {
		object := NewSphere()

		pattern := NewStripe(White, Black)
		pattern.Scale(NewScale(2, 2, 2))

		si := SurfaceInteraction{
			Interaction: Interaction{
				point: NewPoint(1.5, 0, 0),
			},
		}

		have := PatternOnObject(pattern, object, si)
		if !have.Eq(White) {
			t.Errorf("incorrect color at point. have=%s want=%s", have, White)
		}
	})

	t.Run("with texture and object transformation", func(t *testing.T) {
		object := NewSphere()
		object.Scale(NewScale(2, 2, 2))

		pattern := NewStripe(White, Black)
		pattern.Scale(NewScale(0.5, 0, 0))

		si := SurfaceInteraction{
			Interaction: Interaction{
				point: NewPoint(2.5, 0, 0),
			},
		}

		have := PatternOnObject(pattern, object, si)
		if !have.Eq(White) {
			t.Errorf("incorrect color at point. have=%s want=%s", have, White)
		}
	})
}

func TestGradient(t *testing.T) {
	t.Run("gradient linearly interpolates between colors", func(t *testing.T) {
		g := NewGradient(White, Black)

		tests := []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0.25, 0, 0), want: NewColor(0.75, 0.75, 0.75)},
			{at: NewPoint(0.50, 0, 0), want: NewColor(0.50, 0.50, 0.50)},
			{at: NewPoint(0.75, 0, 0), want: NewColor(0.25, 0.25, 0.25)},
		}

		runPatternTests(t, g, tests)
	})
}

func TestRing(t *testing.T) {
	t.Run("extends in both X and Z", func(t *testing.T) {
		r := NewRing(White, Black)

		tests := []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(1, 0, 0), want: Black},
			{at: NewPoint(0, 0, 1), want: Black},
			// 0.708 is slightly more than sqrt(2)/2
			{at: NewPoint(0.708, 0, 0.708), want: Black},
		}

		runPatternTests(t, r, tests)
	})
}

func TestCheckers(t *testing.T) {
	t.Run("texture should repeat in X", func(t *testing.T) {
		c := NewCheckers(White, Black)
		runPatternTests(t, c, []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0.99, 0, 0), want: White},
			{at: NewPoint(1.01, 0, 0), want: Black},
		})
	})

	t.Run("texture should repeat in Y", func(t *testing.T) {
		c := NewCheckers(White, Black)
		runPatternTests(t, c, []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0, 0.99, 0), want: White},
			{at: NewPoint(0, 1.01, 0), want: Black},
		})
	})

	t.Run("texture should repeat in Z", func(t *testing.T) {
		c := NewCheckers(White, Black)
		runPatternTests(t, c, []patternTest{
			{at: NewPoint(0, 0, 0), want: White},
			{at: NewPoint(0, 0, 0.99), want: White},
			{at: NewPoint(0, 0, 1.01), want: Black},
		})
	})
}
