package raytracer

import (
	"fmt"
)

type Tuple [4]Scalar

func (t Tuple) Eq(o Tuple) bool { return teq(t, o) }

func teq(a, b Tuple) bool {
	return eq(a[0], b[0]) && eq(a[1], b[1]) && eq(a[2], b[2]) && eq(a[3], b[3])
}

func tadd(a, b Tuple) Tuple {
	return Tuple{a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3]}
}

func tsub(a, b Tuple) Tuple {
	return Tuple{a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3]}
}

func tmul(a Tuple, s Scalar) Tuple {
	return Tuple{a[0] * s, a[1] * s, a[2] * s, a[3] * s}
}

func tdiv(a Tuple, s Scalar) Tuple {
	return Tuple{a[0] / s, a[1] / s, a[2] / s, a[3] / s}
}

func tdot(a, b Tuple) Scalar {
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3]
}

type Point struct{ tuple Tuple }

func NewPoint(x, y, z Scalar) Point {
	return Point{Tuple{x, y, z, 1}}
}

func (p Point) X() Scalar { return p.tuple[0] }
func (p Point) Y() Scalar { return p.tuple[1] }
func (p Point) Z() Scalar { return p.tuple[2] }
func (p Point) w() Scalar { return p.tuple[3] }

func (p Point) Eq(o Point) bool {
	return teq(p.tuple, o.tuple)
}

func (p Point) TravelTo(v Vector) Point {
	return Point{tadd(p.tuple, v.tuple)}
}

func (p Point) TravelAway(v Vector) Point {
	return Point{tsub(p.tuple, v.tuple)}
}

func (p Point) PathFrom(o Point) Vector {
	return Vector{tsub(p.tuple, o.tuple)}
}

func (p Point) Translate(t Translation) Point {
	return Point{t.matrix.Mulv(p.tuple)}
}

func (p Point) Scale(s Scale) Point {
	return Point{s.matrix.Mulv(p.tuple)}
}

func (p Point) Rotate(r Rotation) Point {
	return Point{r.matrix.Mulv(p.tuple)}
}

func (p Point) Shear(s Shear) Point {
	return Point{s.matrix.Mulv(p.tuple)}
}

func (p Point) Mul(s Scalar) Point {
	return Point{tmul(p.tuple, s)}
}

func (p Point) Add(o Point) Point {
	return Point{tadd(p.tuple, o.tuple)}
}

func (p Point) transform(t Transformation) Point {
	return Point{t.m.Mulv(p.tuple)}
}

func (p Point) String() string {
	return fmt.Sprintf("[%s %s %s]", p.tuple[0], p.tuple[1], p.tuple[2])
}

func (p Point) Dimension(a Axis) Scalar {
	switch a {
	case X:
		return p.X()
	case Y:
		return p.Y()
	case Z:
		return p.Z()
	default:
		panic("unknown axis")
	}
}

type Vector struct{ tuple Tuple }

func NewVector(x, y, z Scalar) Vector {
	return Vector{Tuple{x, y, z, 0}}
}

func (v Vector) X() Scalar { return v.tuple[0] }
func (v Vector) Y() Scalar { return v.tuple[1] }
func (v Vector) Z() Scalar { return v.tuple[2] }
func (v Vector) w() Scalar { return v.tuple[3] }

func (v Vector) Eq(o Vector) bool {
	return teq(v.tuple, o.tuple)
}

func (v Vector) Add(o Vector) Vector {
	return Vector{tadd(v.tuple, o.tuple)}
}

func (v Vector) Sub(o Vector) Vector {
	return Vector{tsub(v.tuple, o.tuple)}
}

func (v Vector) Mul(s Scalar) Vector {
	return Vector{tmul(v.tuple, s)}
}

func (v Vector) Negate() (n Vector) {
	return n.Sub(v)
}

func (v Vector) Length() Scalar {
	return v.X().Pow2() +
		v.Y().Pow2() +
		v.Z().Pow2() +
		v.w().Pow2()
}

func (v Vector) Magnitude() Scalar {
	return v.Length().Sqrt()
}

func (v Vector) Normalize() Vector {
	return Vector{tdiv(v.tuple, v.Magnitude())}
}

func (v Vector) Dot(o Vector) Scalar {
	return tdot(v.tuple, o.tuple)
}

func (v Vector) Cross(o Vector) (c Vector) {
	c.tuple[0] = v.tuple[1]*o.tuple[2] - v.tuple[2]*o.tuple[1]
	c.tuple[1] = v.tuple[2]*o.tuple[0] - v.tuple[0]*o.tuple[2]
	c.tuple[2] = v.tuple[0]*o.tuple[1] - v.tuple[1]*o.tuple[0]
	return
}

func (v Vector) Rotate(r Rotation) Vector {
	return Vector{r.matrix.Mulv(v.tuple)}
}

func (v Vector) Scale(s Scale) Vector {
	return Vector{s.matrix.Mulv(v.tuple)}
}

func (v Vector) transform(t Transformation) Vector {
	x := Vector{
		t.m.Mulv(v.tuple),
	}
	x.tuple[3] = 0
	return x
}

func (v Vector) Reflect(norm Vector) Vector {
	return v.Sub(norm.Mul(2 * v.Dot(norm)))
}

func (v Vector) Forward(normal Vector) Vector {
	if normal.Dot(v) < 0 {
		return v.Negate()
	}
	return v
}

func (v Vector) String() string {
	return fmt.Sprintf("[%s %s %s]", v.tuple[0], v.tuple[1], v.tuple[2])
}

func (v Vector) MaxExtent() Axis {
	if v.X() > v.Y() && v.X() > v.Z() {
		return X
	}
	if v.Y() > v.Z() {
		return Y
	}
	return Z
}

func (v Vector) Dimension(a Axis) Scalar {
	switch a {
	case X:
		return v.X()
	case Y:
		return v.Y()
	case Z:
		return v.Z()
	default:
		panic("unknown axis")
	}
}
