package raytracer

import (
	"fmt"
	"math"
	"testing"
)

var _ Object = &TestShape{}

type TestShape struct {
	ray *Ray
	shape
}

func NewTestShape() *TestShape {
	return &TestShape{shape: newShape()}
}

func (t *TestShape) Bounds() (Point, Point) {
	return NewPoint(-1, -1, -1), NewPoint(1, 1, 1)
}

func (t *TestShape) NormalAt(Point, Intersection) Vector {
	return NewVector(1, 1, 1)
}

func (t *TestShape) Intersect(r Ray, intersections *Intersections) {
	t.ray = &r
	return
}

func (t TestShape) SurfaceAt(Interaction) Surface {
	return Surface{}
}

func TestSphere(t *testing.T) {
	t.Run("glassy sphere construction", func(t *testing.T) {

		s := NewGlassSphere()
		m := s.Material().(*MaterialProperties)

		if want, have := Scalar(1.0), m.Transparency(); !have.Eq(want) {
			t.Errorf("incorrect transparency. want=%f have=%f", want, have)
		}
		if want, have := Scalar(1.5), m.RefractiveIndex(); !have.Eq(want) {
			t.Errorf("incorrect transparency. want=%f have=%f", want, have)
		}
	})

	t.Run("intersection", func(t *testing.T) {
		type intersectionTest struct {
			name       string
			ray        Ray
			intersects []Scalar
		}

		tests := []intersectionTest{
			{
				name:       "intersection at midpoint",
				ray:        NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1)),
				intersects: []Scalar{4.0, 6.0},
			},
			{
				name:       "ray intersects at tangeint",
				ray:        NewRay(NewPoint(0, 1, -5), NewVector(0, 0, 1)),
				intersects: []Scalar{5.0, 5.0},
			},
			{
				name: "ray is not intersecting sphere",
				ray:  NewRay(NewPoint(0, 2, -5), NewVector(0, 0, 1)),
			},
			{
				name:       "ray originates inside sphere",
				ray:        NewRay(NewPoint(0, 0, 0), NewVector(0, 0, 1)),
				intersects: []Scalar{-1.0, 1.0},
			},
			{
				name:       "sphere is behind ray",
				ray:        NewRay(NewPoint(0, 0, 5), NewVector(0, 0, 1)),
				intersects: []Scalar{-6.0, -4.0},
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				s := NewSphere()

				xs := Intersections{}

				Intersect(tt.ray, s, &xs)

				if len(xs) != len(tt.intersects) {
					t.Errorf("incorrect number of intersections. have=%d want=%d",
						len(xs), len(tt.intersects))
					t.FailNow()
				}

				for i, x := range xs {
					if x.Object() != s {
						t.Errorf("object incorrectly set on intersection. have=%+v want=%+v",
							x.Object(), s)
					}
					if !x.Time().Eq(tt.intersects[i]) {
						t.Errorf("incorrect intersection at position %d. have=%+v want=%+v",
							i, x, tt.intersects[i])
					}
				}
			})
		}
	})

	t.Run("transformation intersections", func(t *testing.T) {
		type transformTest struct {
			name       string
			r          Ray
			trans      func(*Sphere)
			intersects Intersections
		}

		tests := []transformTest{
			{
				name:  "intersecting scaled sphere with ray",
				r:     NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1)),
				trans: func(s *Sphere) { s.Scale(NewScale(2, 2, 2)) },
				intersects: Intersections{
					NewIntersection(nil, 3),
					NewIntersection(nil, 7),
				},
			},
			{
				name:       "intersecting translated sphere with ray",
				r:          NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1)),
				trans:      func(s *Sphere) { s.Translate(NewTranslation(5, 0, 0)) },
				intersects: Intersections{},
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				s := NewSphere()
				tt.trans(s)

				have := Intersections{}
				Intersect(tt.r, s, &have)

				if len(have) != len(tt.intersects) {
					t.Errorf("incorrect number of intersections for transformation. have=%d want=%d",
						len(have), len(tt.intersects))
					t.FailNow()
				}

				for i, x := range have {
					if want, have := tt.intersects[i].Time(), x.Time(); !have.Eq(want) {
						t.Errorf("incorrect time for intersection %d. have=%f want=%f", i, have, want)
					}
				}
			})
		}
	})

	t.Run("normal", func(t *testing.T) {
		type normalTest struct {
			name string
			s    *Sphere
			at   Point
			want Vector
		}

		var (
			sq3 = Scalar(math.Sqrt(3) / 3)
			sq2 = Scalar(math.Sqrt2 / 2)
		)

		tests := []normalTest{
			{
				name: "point on the x axis",
				s:    NewSphere(),
				at:   NewPoint(1, 0, 0),
				want: NewVector(1, 0, 0),
			},
			{
				name: "point on the y axis",
				s:    NewSphere(),
				at:   NewPoint(0, 1, 0),
				want: NewVector(0, 1, 0),
			},
			{
				name: "point on the z axis",
				s:    NewSphere(),
				at:   NewPoint(0, 0, 1),
				want: NewVector(0, 0, 1),
			},
			{
				name: "nonaxial point",
				s:    NewSphere(),
				at:   NewPoint(sq3, sq3, sq3),
				want: NewVector(sq3, sq3, sq3),
			},
			{
				name: "translated sphere",
				s: func() *Sphere {
					s := NewSphere()
					s.Translate(NewTranslation(0, 1, 0))
					return s
				}(),
				at:   NewPoint(0, 1.70711, -0.70711),
				want: NewVector(0, 0.70711, -0.70711),
			},
			{
				name: "transformed sphere",
				s: func() *Sphere {
					s := NewSphere()
					s.Scale(NewScale(1, 0.5, 1))
					s.Rotate(NewRotationZ(math.Pi / 5))
					return s
				}(),
				at:   NewPoint(0, sq2, -sq2),
				want: NewVector(0, 0.97014, -0.24254),
			},
		}

		for _, tt := range tests {
			tt := tt

			t.Run(tt.name, func(t *testing.T) {
				n := NormalAt(tt.s, tt.at, Intersection{})

				if !n.Eq(tt.want) {
					t.Errorf("incorrect normal vector for point. have=%v want=%v", n, tt.want)
				}

				if normalized := n.Normalize(); !n.Eq(normalized) {
					t.Errorf("normal vector is not normalized")
				}
			})
		}
	})

	t.Run("has default material", func(t *testing.T) {
		s := NewSphere()
		m := s.Material().(*MaterialProperties)
		if *m != *NewMaterialProperties() {
			t.Errorf("incorrect default material.")
		}
	})

	t.Run("can be assigned material", func(t *testing.T) {
		m := NewMaterialProperties()
		m.SetAmbient(NewGray(1))

		s := NewSphere()
		s.SetMaterial(m)

		if s.Material() != m {
			t.Errorf("material improperly assigned")
		}
	})
}

func TestPlane(t *testing.T) {
	t.Run("normal of plane is constant", func(t *testing.T) {
		p := NewPlane()

		points := []Point{
			NewPoint(0, 0, 0),
			NewPoint(10, 0, -10),
			NewPoint(-5, 0, 150),
		}

		for _, pt := range points {
			if want, have := NewVector(0, 1, 0), p.NormalAt(pt, Intersection{}); !have.Eq(want) {
				t.Errorf("incorrect normal vector for point. have=%v want=%v", have, want)
			}
		}
	})

	t.Run("intersection", func(t *testing.T) {
		type planeIntersectTest struct {
			name       string
			plane      *Plane
			ray        Ray
			intersects []Scalar
		}

		tests := []planeIntersectTest{
			{
				name:  "intersect with ray parallel to plane",
				plane: NewPlane(),
				ray:   NewRay(NewPoint(0, 10, 0), NewVector(0, 0, 1)),
			},
			{
				name:  "intersect with ray coplanar to plane",
				plane: NewPlane(),
				ray:   NewRay(NewPoint(0, 0, 0), NewVector(0, 0, 1)),
			},
			{
				name:       "intersect with plane from above",
				plane:      NewPlane(),
				ray:        NewRay(NewPoint(0, 1, 0), NewVector(0, -1, 0)),
				intersects: []Scalar{1},
			},
			{
				name:       "intersect with plane from below",
				plane:      NewPlane(),
				ray:        NewRay(NewPoint(0, -1, 0), NewVector(0, 1, 0)),
				intersects: []Scalar{1},
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				have := Intersections{}
				Intersect(tt.ray, tt.plane, &have)

				if len(have) != len(tt.intersects) {
					t.Errorf("incorrect number of intersections. have=%d want=%d", len(have), len(tt.intersects))
				}

				for i, x := range have {
					if want, have := tt.intersects[i], x.Time(); !have.Eq(want) {
						t.Errorf("incorrect intersection time for intersection %d. have=%f want=%f", i, have, want)
					}
					if x.Object() != tt.plane {
						t.Errorf("object incorrectly set on intersection %d. have=%+v want=%+v", i, x.Object(), tt.plane)
					}
				}
			})
		}
	})
}

func TestCube(t *testing.T) {
	t.Run("ray intersecting cube", func(t *testing.T) {
		type cubeIntersectTest struct {
			name   string
			origin Point
			dir    Vector
			t1     Scalar
			t2     Scalar
		}

		tests := []cubeIntersectTest{
			{
				name:   "+x",
				origin: NewPoint(5, 0.5, 0),
				dir:    NewVector(-1, 0, 0),
				t1:     4,
				t2:     6,
			},

			{
				name:   "-x",
				origin: NewPoint(-5, 0.5, 0),
				dir:    NewVector(1, 0, 0),
				t1:     4,
				t2:     6,
			},
			{
				name:   "+y",
				origin: NewPoint(0.5, 5, 0),
				dir:    NewVector(0, -1, 0),
				t1:     4,
				t2:     6,
			},
			{
				name:   "-y",
				origin: NewPoint(0.5, -5, 0),
				dir:    NewVector(0, 1, 0),
				t1:     4,
				t2:     6,
			},
			{
				name:   "+z",
				origin: NewPoint(0.5, 0, 5),
				dir:    NewVector(0, 0, -1),
				t1:     4,
				t2:     6,
			},
			{
				name:   "+z",
				origin: NewPoint(0.5, 0, -5),
				dir:    NewVector(0, 0, 1),
				t1:     4,
				t2:     6,
			},
			{
				name:   "inside",
				origin: NewPoint(0, 0.5, 0),
				dir:    NewVector(0, 0, 1),
				t1:     -1,
				t2:     1,
			},
		}

		for _, tt := range tests {
			c := NewCube()
			r := NewRay(tt.origin, tt.dir)

			xs := Intersections{}
			Intersect(r, c, &xs)

			if len(xs) != 2 {
				t.Fatalf("expected 2 intersections. got=%d", len(xs))
			}

			if have := xs[0].Time(); !have.Eq(tt.t1) {
				t.Errorf("incorrect t1. have=%f want=%f", have, tt.t1)
			}
			if have := xs[1].Time(); !have.Eq(tt.t2) {
				t.Errorf("incorrect t2. have=%f want=%f", have, tt.t2)
			}
		}
	})

	t.Run("ray misses cube", func(t *testing.T) {
		rays := []Ray{
			NewRay(NewPoint(-2, 0, 0), NewVector(0.2673, 0.5345, 0.8018)),
			NewRay(NewPoint(0, -2, 0), NewVector(0.8018, 0.2673, 0.5345)),
			NewRay(NewPoint(0, 0, -2), NewVector(0.5345, 0.8018, 0.2673)),
			NewRay(NewPoint(2, 0, 2), NewVector(0, 0, -1)),
			NewRay(NewPoint(0, 2, 2), NewVector(0, -1, 0)),
			NewRay(NewPoint(2, 2, 0), NewVector(-1, 0, 0)),
		}

		c := NewCube()
		for _, r := range rays {
			xs := Intersections{}
			Intersect(r, c, &xs)
			if len(xs) != 0 {
				t.Errorf("did not expect ray %v to intersect cube", r)
			}
		}
	})

	t.Run("cube normal", func(t *testing.T) {
		type normalTest struct {
			p    Point
			want Vector
		}

		tests := []normalTest{
			{p: NewPoint(1, 0.5, -0.8), want: NewVector(1, 0, 0)},
			{p: NewPoint(-1, -0.2, 0.9), want: NewVector(-1, 0, 0)},
			{p: NewPoint(-0.4, 1, -0.1), want: NewVector(0, 1, 0)},
			{p: NewPoint(-0.6, 0.3, 1), want: NewVector(0, 0, 1)},
			{p: NewPoint(0.4, 0.4, -1), want: NewVector(0, 0, -1)},
			{p: NewPoint(1, 1, 1), want: NewVector(1, 0, 0)},
			{p: NewPoint(-1, -1, -1), want: NewVector(-1, 0, 0)},
		}

		c := NewCube()
		for _, tt := range tests {
			if have := NormalAt(c, tt.p, Intersection{}); !have.Eq(tt.want) {
				t.Errorf("incorrect vector for normal at point %v. have=%v want=%v", tt.p, have, tt.want)
			}
		}
	})
}

func TestCylinder(t *testing.T) {
	t.Run("rays miss cylinder", func(t *testing.T) {
		rays := []Ray{
			NewRay(NewPoint(1, 0, 0), NewVector(0, 1, 0)),
			NewRay(NewPoint(0, 0, 0), NewVector(0, 1, 0)),
			NewRay(NewPoint(0, 0, -5), NewVector(1, 1, 1)),
		}

		c := NewCylinder()
		for _, r := range rays {
			xs := Intersections{}
			Intersect(r, c, &xs)
			if len(xs) != 0 {
				t.Errorf("did not expect ray %v to intersect cube", r)
			}
		}
	})

	t.Run("rays hit cylinder", func(t *testing.T) {
		type cylinderTest struct {
			origin Point
			dir    Vector
			t0     Scalar
			t1     Scalar
		}

		tests := []cylinderTest{
			{
				origin: NewPoint(1, 0, -5),
				dir:    NewVector(0, 0, 1),
				t0:     5,
				t1:     5,
			},
			{
				origin: NewPoint(0, 0, -5),
				dir:    NewVector(0, 0, 1),
				t0:     4,
				t1:     6,
			},
			{
				origin: NewPoint(0.5, 0, -5),
				dir:    NewVector(0.1, 1, 1),
				t0:     6.80798,
				t1:     7.08872,
			},
		}

		c := NewCylinder()
		for i, tt := range tests {
			tt := tt

			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				r := NewRay(tt.origin, tt.dir.Normalize())

				xs := Intersections{}
				Intersect(r, c, &xs)

				if len(xs) != 2 {
					t.Fatalf("expected 2 intersections. got=%d", len(xs))
				}

				if have := xs[0].Time(); !have.Eq(tt.t0) {
					t.Errorf("incorrect time for first intersection. have=%f want=%f",
						have, tt.t0)
				}
				if have := xs[1].Time(); !have.Eq(tt.t1) {
					t.Errorf("incorrect time for second intersection. have=%f want=%f",
						have, tt.t1)
				}
			})

		}
	})

	t.Run("cylinder normal", func(t *testing.T) {
		type normalTest struct {
			p    Point
			want Vector
		}

		tests := []normalTest{
			{p: NewPoint(1, 0, 0), want: NewVector(1, 0, 0)},
			{p: NewPoint(0, 5, -1), want: NewVector(0, 0, -1)},
			{p: NewPoint(0, -2, 1), want: NewVector(0, 0, 1)},
			{p: NewPoint(-1, 1, 0), want: NewVector(-1, 0, 0)},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCylinder()
				n := c.NormalAt(tt.p, Intersection{})

				if !n.Eq(tt.want) {
					t.Errorf("incorrect normal for point %v. have=%v want=%v",
						tt.p, n, tt.want)
				}
			})
		}
	})

	t.Run("cylinder truncation defaults to infinity", func(t *testing.T) {
		c := NewCylinder()
		if !math.IsInf(float64(c.Minimum()), -1) {
			t.Errorf("expected cylinder minimum to be -inf. got=%f", c.Minimum())
		}
		if !math.IsInf(float64(c.Maximum()), 1) {
			t.Errorf("expected cylinder maximum to be -inf. got=%f", c.Maximum())
		}
	})

	t.Run("intersecting constrained cylinder", func(t *testing.T) {
		type cylinderTest struct {
			p     Point
			v     Vector
			count int
		}

		tests := []cylinderTest{
			{p: NewPoint(0, 1.5, 0), v: NewVector(0.1, 1, 0), count: 0},
			{p: NewPoint(0, 3, -5), v: NewVector(0, 0, 1), count: 0},
			{p: NewPoint(0, 0, -5), v: NewVector(0, 0, 1), count: 0},
			{p: NewPoint(0, 2, -5), v: NewVector(0, 0, 1), count: 0},
			{p: NewPoint(0, 1, -5), v: NewVector(0, 0, 1), count: 0},
			{p: NewPoint(0, 1.5, -2), v: NewVector(0, 0, 1), count: 2},
		}

		for i, tt := range tests {
			tt := tt

			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCylinder()
				c.Truncate(1, 2)

				xs := Intersections{}
				c.Intersect(NewRay(tt.p, tt.v.Normalize()), &xs)

				if len(xs) != tt.count {
					t.Errorf("incorrect count of intersections. have=%d want=%d", len(xs), tt.count)
				}
			})
		}

	})

	t.Run("cylinders can be closed", func(t *testing.T) {
		c := NewCylinder()

		if c.Closed() {
			t.Error("expected cylinder to default to open")
		}

		c.SetClosed(true)

		if !c.Closed() {
			t.Error("expected cylinder to be closed after calling SetClosed(true)")
		}
	})

	t.Run("intersecting caps of closed cylinder", func(t *testing.T) {
		type cylinderTest struct {
			p     Point
			v     Vector
			count int
		}

		tests := []cylinderTest{
			{p: NewPoint(0, 3, 0), v: NewVector(0, -1, 0), count: 2},
			{p: NewPoint(0, 3, -2), v: NewVector(0, -1, 2), count: 2},
			{p: NewPoint(0, 4, -2), v: NewVector(0, -1, 1), count: 2},
			{p: NewPoint(0, 0, -2), v: NewVector(0, 1, 2), count: 2},
			{p: NewPoint(0, -1, -2), v: NewVector(0, 1, 1), count: 2},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCylinder()
				c.Truncate(1, 2)
				c.SetClosed(true)

				r := NewRay(tt.p, tt.v.Normalize())

				xs := Intersections{}
				c.Intersect(r, &xs)

				if len(xs) != tt.count {
					t.Errorf("incorrect number of intersections. have=%d want=%d", len(xs), tt.count)
				}
			})
		}
	})

	t.Run("normal of capped end of capped cylinder", func(t *testing.T) {
		type normalTest struct {
			p    Point
			want Vector
		}

		tests := []normalTest{
			{p: NewPoint(0, 1, 0), want: NewVector(0, -1, 0)},
			{p: NewPoint(0.5, 1, 0), want: NewVector(0, -1, 0)},
			{p: NewPoint(0, 2, 0), want: NewVector(0, 1, 0)},
			{p: NewPoint(0.5, 2, 0), want: NewVector(0, 1, 0)},
			{p: NewPoint(0, 2, 0.5), want: NewVector(0, 1, 0)},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCylinder()
				c.Truncate(1, 2)
				c.SetClosed(true)

				n := c.NormalAt(tt.p, Intersection{})

				if !n.Eq(tt.want) {
					t.Errorf("incorrect normal for point %v. have=%v want=%v",
						tt.p, n, tt.want)
				}
			})
		}
	})
}

func TestCone(t *testing.T) {
	t.Run("intersecting cone with ray", func(t *testing.T) {
		type intersectTest struct {
			p  Point
			v  Vector
			t0 Scalar
			t1 Scalar
		}

		tests := []intersectTest{
			{p: NewPoint(0, 0, -5), v: NewVector(0, 0, 1), t0: 5, t1: 5},
			{p: NewPoint(0, 0, -5), v: NewVector(1, 1, 1), t0: 8.66025, t1: 8.66025},
			{p: NewPoint(1, 1, -5), v: NewVector(-0.5, -1, 1), t0: 4.55006, t1: 49.44994},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCone()
				r := NewRay(tt.p, tt.v.Normalize())

				xs := Intersections{}
				c.Intersect(r, &xs)
				if len(xs) != 2 {
					t.Fatalf("expected 2 intersections. got=%d", len(xs))
				}

				if have := xs[0].Time(); !have.Eq(tt.t0) {
					t.Errorf("incorrect time for first intersection. have=%f want=%f",
						have, tt.t0)
				}
				if have := xs[1].Time(); !have.Eq(tt.t1) {
					t.Errorf("incorrect time for second intersection. have=%f want=%f",
						have, tt.t1)
				}
			})

		}
	})

	t.Run("intersecting cone with ray parallel to one of its halves", func(t *testing.T) {
		c := NewCone()
		r := NewRay(NewPoint(0, 0, -1), NewVector(0, 1, 1).Normalize())

		xs := Intersections{}
		c.Intersect(r, &xs)

		if len(xs) != 1 {
			t.Fatalf("expected one intersection. got=%d", len(xs))
		}

		if want, have := Scalar(0.35355), xs[0].Time(); !have.Eq(want) {
			t.Errorf("incorrect time for intersection. have=%f want=%f", have, want)
		}
	})

	t.Run("intersecting a cone end caps", func(t *testing.T) {
		type intersectionTest struct {
			p     Point
			v     Vector
			count int
		}

		tests := []intersectionTest{
			{p: NewPoint(0, 0, -5), v: NewVector(0, 1, 0), count: 0},
			{p: NewPoint(0, 0, -0.25), v: NewVector(0, 1, 1), count: 2},
			{p: NewPoint(0, 0, -0.25), v: NewVector(0, 1, 0), count: 4},
		}

		for i, tt := range tests {
			tt := tt

			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCone()
				c.Truncate(-0.5, 0.5)
				c.SetClosed(true)
				r := NewRay(tt.p, tt.v.Normalize())

				xs := Intersections{}
				c.Intersect(r, &xs)
				if len(xs) != tt.count {
					t.Fatalf("incorrect number of intersections. have=%d want=%d", len(xs), tt.count)
				}
			})
		}
	})

	t.Run("cone normals", func(t *testing.T) {
		type normalTest struct {
			p Point
			v Vector
		}

		tests := []normalTest{
			{p: NewPoint(0, 0, 0), v: NewVector(0, 0, 0)},
			{p: NewPoint(1, 1, 1), v: NewVector(1, -math.Sqrt2, 1)},
			{p: NewPoint(-1, -1, 0), v: NewVector(-1, 1, 0)},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				c := NewCone()
				n := c.NormalAt(tt.p, Intersection{})

				if !n.Eq(tt.v) {
					t.Errorf("incorrect normal for point. have=%v want=%v", n, tt.v)
				}
			})
		}
	})
}

func TestTriangle(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		var (
			p1 = NewPoint(0, 1, 0)
			p2 = NewPoint(-1, 0, 0)
			p3 = NewPoint(1, 0, 0)
		)

		tri := NewTriangle(p1, p2, p3)

		if !tri.corners[0].Eq(p1) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.corners[0], p1)
		}
		if !tri.corners[1].Eq(p2) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.corners[1], p2)
		}
		if !tri.corners[2].Eq(p3) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.corners[2], p3)
		}

		if want := NewVector(-1, -1, 0); !tri.edges[0].Eq(want) {
			t.Errorf("incorrect edge. have=%v want=%v", tri.edges[0], want)
		}
		if want := NewVector(1, -1, 0); !tri.edges[1].Eq(want) {
			t.Errorf("incorrect edge. have=%v want=%v", tri.edges[1], want)
		}

		if want := NewVector(0, 0, -1); !tri.normals[0].Eq(want) {
			t.Errorf("incorrect normal. have=%v want=%v", tri.normals[0], want)
		}
	})

	t.Run("normal of triangle", func(t *testing.T) {
		tri := NewTriangle(
			NewPoint(0, 1, 0),
			NewPoint(-1, 0, 0),
			NewPoint(1, 0, 0))

		if have := tri.NormalAt(NewPoint(0, 0.5, 0), Intersection{}); !have.Eq(tri.normals[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.normals[0])
		}
		if have := tri.NormalAt(NewPoint(-0.5, 0.75, 0), Intersection{}); !have.Eq(tri.normals[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.normals[0])
		}
		if have := tri.NormalAt(NewPoint(0.5, 0.25, 0), Intersection{}); !have.Eq(tri.normals[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.normals[0])
		}
	})

	t.Run("intersecting a ray parallel to triangle", func(t *testing.T) {
		tri := NewTriangle(
			NewPoint(0, 1, 0),
			NewPoint(-1, 0, 0),
			NewPoint(1, 0, 0))

		r := NewRay(NewPoint(0, -1, -2), NewVector(0, 1, 0))

		have := Intersections{}
		if tri.Intersect(r, &have); len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray misses p1-p3 edge", func(t *testing.T) {
		tri := NewTriangle(
			NewPoint(0, 1, 0),
			NewPoint(-1, 0, 0),
			NewPoint(1, 0, 0))

		r := NewRay(NewPoint(1, 1, -2), NewVector(0, 0, 1))

		have := Intersections{}
		if tri.Intersect(r, &have); len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray misses p1-p2 edge", func(t *testing.T) {
		tri := NewTriangle(
			NewPoint(0, 1, 0),
			NewPoint(-1, 0, 0),
			NewPoint(1, 0, 0))

		r := NewRay(NewPoint(-1, 1, -2), NewVector(0, 0, 1))

		have := Intersections{}
		if tri.Intersect(r, &have); len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray strikes triangle", func(t *testing.T) {
		tri := NewTriangle(
			NewPoint(0, 1, 0),
			NewPoint(-1, 0, 0),
			NewPoint(1, 0, 0))

		r := NewRay(NewPoint(0, 0.5, -2), NewVector(0, 0, 1))

		xs := Intersections{}
		tri.Intersect(r, &xs)
		if len(xs) != 1 {
			t.Errorf("expected exactly one intersection. got=%d", len(xs))
		}

		if xs[0].Time() != 2 {
			t.Errorf("incorrect time for intersection. got=%f", xs[0].Time())
		}
	})
}

func TestSmoothTriangle(t *testing.T) {

	tri := NewSmoothTriange(
		NewPoint(0, 1, 0),
		NewPoint(-1, 0, 0),
		NewPoint(1, 0, 0),
		NewVector(0, 1, 0),
		NewVector(-1, 0, 0),
		NewVector(1, 0, 0),
	)
	t.Run("intersection with smooth triangle stores UV", func(t *testing.T) {
		r := NewRay(NewPoint(-0.2, 0.3, -2), NewVector(0, 0, 1))
		xs := Intersections{}

		tri.Intersect(r, &xs)

		if !xs[0].U().Eq(0.45) {
			t.Errorf("incorrect U. have=%f want=%f", xs[0].U(), 0.45)
		}

		if !xs[0].V().Eq(0.25) {
			t.Errorf("incorrect V. have=%f want=%f", xs[0].V(), 0.25)
		}

	})

	t.Run("uses UV to interpolate the normal", func(t *testing.T) {
		x := NewIntersectionWithUV(tri, 1, 0.45, 0.25)
		n := NormalAt(tri, NewPoint(0, 0, 0), x)

		want := NewVector(-0.5547, 0.83205, 0)

		if !n.Eq(want) {
			t.Errorf("incorrect normal for intersection. have=%v want=%v", n, want)
		}
	})

	t.Run("preparing normal on smooth triangle", func(t *testing.T) {
		r := NewRay(NewPoint(-0.2, 0.3, -2), NewVector(0, 0, 1))
		x := NewIntersectionWithUV(tri, 1, 0.45, 0.25)

		state := NewSurfaceInteraction(x, r, x)
		want := NewVector(-0.5547, 0.83205, 0)

		if !state.normal.Eq(want) {
			t.Errorf("incorrect normal for intersection state. have=%v want=%v", state.normal, want)
		}
	})
}

func TestConcentricSampleDisk(t *testing.T) {
	ctx := DefaultContext()
	for i := 0; i < 100; i++ {
		sample := concentricSampleDisk(ctx)
		t.Log("x, y", sample.tuple[0], sample.tuple[1])
	}
}
