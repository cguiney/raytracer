package raytracer

import "math/rand"

const perlinPermutations = 256

type Perlin struct {
	seed int64
	rng  *rand.Rand

	// double sized to avoid modulo operations
	permutations [perlinPermutations * 2]byte
}

func NewPerlin(seed int64) *Perlin {
	p := Perlin{
		seed: seed,
		rng:  rand.New(rand.NewSource(seed)),
	}

	// populate the permutations with 0..255, then shuffle
	for i := range p.permutations[:perlinPermutations] {
		p.permutations[i] = byte(i)
	}

	p.rng.Shuffle(len(p.permutations[:]), func(i, j int) {
		p.permutations[i], p.permutations[j] = p.permutations[j], p.permutations[i]
	})

	copy(p.permutations[perlinPermutations:], p.permutations[:perlinPermutations])

	return &p
}

func (p *Perlin) gradient(x, y, z int, dx, dy, dz Scalar) Scalar {
	var (
		px = int(p.permutations[x])
		py = int(p.permutations[px+y])
		pz = int(p.permutations[py+z])
		h  = p.permutations[pz]
	)

	h &= 15

	var u, v Scalar

	if h < 8 || h == 12 || h == 13 {
		u = dx
	} else {
		u = dy
	}

	if h < 4 || h == 12 || h == 13 {
		v = dy
	} else {
		v = dz
	}

	if h&1 > 0 {
		u = -u
	}

	if h&2 > 0 {
		v = -v
	}

	return u + v
}

func (p *Perlin) weight(t Scalar) Scalar {
	var (
		t3 = t * t * t
		t4 = t3 * t
	)

	return 6*t4*t - 15*t4 + 10*t3
}

func (p *Perlin) noise(u, v, w Scalar) Scalar {
	var (
		ix = u.Floor()
		iy = v.Floor()
		iz = w.Floor()

		dx = u - Scalar(ix)
		dy = v - Scalar(iy)
		dz = w - Scalar(iz)
	)

	ix &= perlinPermutations - 1
	iy &= perlinPermutations - 1
	iz &= perlinPermutations - 1

	var (
		w000 = p.gradient(ix, iy, iz, dx, dy, dz)
		w100 = p.gradient(ix+1, iy, iz, dx-1, dy, dz)
		w010 = p.gradient(ix, iy+1, iz, dx, dy-1, dz)
		w110 = p.gradient(ix+1, iy+1, iz, dx-1, dy-1, dz)
		w001 = p.gradient(ix, iy, iz+1, dx, dy, dz-1)
		w101 = p.gradient(ix+1, iy, iz+1, dx-1, dy, dz-1)
		w011 = p.gradient(ix, iy+1, iz+1, dx, dy-1, dz-1)
		w111 = p.gradient(ix+1, iy+1, iz+1, dx-1, dy-1, dz-1)

		wx = p.weight(dx)
		wy = p.weight(dy)
		wz = p.weight(dz)

		x00 = lerp(wx, w000, w100)
		x10 = lerp(wx, w010, w110)
		x01 = lerp(wx, w001, w101)
		x11 = lerp(wx, w011, w111)
		y0  = lerp(wy, x00, x10)
		y1  = lerp(wy, x01, x11)
	)

	return lerp(wz, y0, y1)
}

func (p *Perlin) ScalarAt(si SurfaceInteraction) Scalar {
	var (
		u = si.Shading.UV.U
		v = si.Shading.UV.V
	)
	return p.noise(u, v, 1-u-v)
}

func (p *Perlin) At(si SurfaceInteraction) Color {
	var (
		u = si.Shading.UV.U
		v = si.Shading.UV.V
		n = p.noise(u, v, 1-u-v)
	)

	return NewGray(n)
}

func (p *Perlin) NoiseAt(pt Point) Scalar {
	var (
		u = pt.X()
		v = pt.Y()
		w = 1 - u - v
	)
	return p.noise(u, v, w)
}

func smoothStep(min, max, value Scalar) Scalar {
	v := Scalar.Clamp((value-min)/(max-min), 0, 1)
	return v * v * ((-2 * v) + 3)
}

type FBM struct {
	perlin  Perlin
	omega   Scalar
	octives int
}

func NewFBM(seed int64, omega Scalar, octives int) *FBM {
	return &FBM{
		perlin:  *NewPerlin(seed),
		omega:   omega,
		octives: octives,
	}
}

func (f *FBM) NoiseAt(p Point) Scalar {
	var (
		len2 = Scalar(0) // TODO implement partial derivitates for ray differentials
		n    = Scalar.Clamp(-1-0.5*Scalar.Log2(len2), 0, Scalar(f.octives))
		nint = Scalar.Floor(n)

		sum       = Scalar(0)
		lambda    = Scalar(1)
		amplitude = Scalar(1)
	)

	for i := 0; i < f.octives; i++ {
		sum += amplitude * f.perlin.NoiseAt(p.Mul(lambda))
		lambda *= 1.99
		amplitude *= f.omega
	}

	npartial := n - Scalar(nint)
	sum += amplitude * smoothStep(0.3, 0.7, npartial) * f.perlin.NoiseAt(p.Mul(lambda))

	return sum
}

func (f *FBM) Noise(si SurfaceInteraction) Scalar {
	return f.NoiseAt(si.point)
}
