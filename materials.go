package raytracer

import "image"

var (
	defaultMaterial = func() MaterialProperties {
		m := NewMaterialProperties()
		m.SetColor(NewColor(0.8, 1, 0.6))
		m.SetAmbient(NewGray(0.1))
		m.SetDiffuse(NewGray(0.7))
		m.SetSpecular(NewGray(0.2))
		m.SetShininess(200)
		return *m
	}()

	Glass = func() MaterialProperties {
		m := NewMaterialProperties()
		m.SetTransparency(1)
		m.SetRefractiveIndex(1.5)
		return *m
	}()
)

type Emitter interface {
	Emission() Color
}

type PhysicalMaterial interface {
	Scattering(si SurfaceInteraction, mode TransportMode) *BSDF
}

type Material interface {
	Emitter
	PhysicalMaterial
}

type EmissiveMaterial struct {
	Emitter
	PhysicalMaterial
}

type Emission Color

func (e Emission) Emission() Color {
	return Color(e)
}

// Legacy property bag
type MaterialProperties struct {
	name            string
	color           Color
	ambient         Color
	diffuse         Color
	specular        Color
	emission        Color
	shininess       Scalar
	metalness       Scalar
	roughness       Scalar
	reflective      Scalar
	transparency    Scalar
	refractiveIndex Scalar
	pattern         Pattern

	colorMap   Texture
	diffuseMap Texture
}

func NewMaterialProperties() *MaterialProperties {
	m := &MaterialProperties{
		color:           White,
		colorMap:        White,
		ambient:         NewGray(0.1),
		diffuse:         NewGray(0.9),
		diffuseMap:      NewGray(0.9),
		specular:        NewGray(0.9),
		emission:        Black,
		shininess:       200.0,
		refractiveIndex: 1,
	}

	return m
}

func NewDefaultMaterialProperties() *MaterialProperties {
	m := NewMaterialProperties()
	m.SetColor(NewColor(0.8, 1, 0.6))
	m.SetAmbient(NewGray(0.1))
	m.SetDiffuse(NewGray(0.7))
	m.SetSpecular(NewGray(0.2))
	m.SetShininess(200)
	return m
}

func (m *MaterialProperties) Name() string {
	return m.name
}

func (m *MaterialProperties) Color() Color {
	return m.color
}

func (m *MaterialProperties) ColorMap() Texture {
	if m.colorMap == nil {
		return Black
	}
	return m.colorMap
}

func (m *MaterialProperties) Ambient() Color {
	return m.ambient
}

func (m *MaterialProperties) Diffuse() Color {
	return m.diffuse
}

func (m *MaterialProperties) DiffuseMap() Texture {
	if m.diffuseMap == nil {
		return Black
	}
	return m.diffuseMap
}

func (m *MaterialProperties) Specular() Color {
	return m.specular
}

func (m *MaterialProperties) Emission() Color {
	return m.emission
}

func (m *MaterialProperties) Shininess() Scalar {
	return m.shininess
}

func (m *MaterialProperties) Metalness() Scalar {
	return m.metalness
}

func (m *MaterialProperties) Roughness() Scalar {
	return m.roughness
}

func (m *MaterialProperties) Reflective() Scalar {
	return m.reflective
}

func (m *MaterialProperties) Transparency() Scalar {
	return m.transparency
}

func (m *MaterialProperties) RefractiveIndex() Scalar {
	return m.refractiveIndex
}

func (m *MaterialProperties) Pattern() Pattern {
	return m.pattern
}

func (m *MaterialProperties) SetName(n string) {
	m.name = n
}

func (m *MaterialProperties) SetColor(c Color) {
	m.color = c
	m.colorMap = c
}

func (m *MaterialProperties) SetColorMap(img image.Image) {
	m.colorMap = NewImage(img)
}

func (m *MaterialProperties) SetAmbient(a Color) {
	m.ambient = a
}

func (m *MaterialProperties) SetSpecular(s Color) {
	m.specular = s
}

func (m *MaterialProperties) SetEmission(e Color) {
	m.emission = e
}

func (m *MaterialProperties) SetDiffuse(d Color) {
	m.diffuse = d
	m.diffuseMap = d
}

func (m *MaterialProperties) SetDiffuseMap(kdmap image.Image) {
	m.diffuseMap = NewImage(kdmap)
}

func (m *MaterialProperties) SetShininess(s Scalar) {
	if s > 1000 {
		s = 1000
	}
	m.shininess = s
}

func (m *MaterialProperties) SetMetalness(v Scalar) {
	m.metalness = v
}

func (m *MaterialProperties) SetRoughness(r Scalar) {
	m.roughness = r
}

func (m *MaterialProperties) SetReflective(r Scalar) {
	m.reflective = r
}

func (m *MaterialProperties) SetTransparency(t Scalar) {
	m.transparency = t
}

func (m *MaterialProperties) SetRefractiveIndex(r Scalar) {
	m.refractiveIndex = r
}

func (m *MaterialProperties) SetPattern(p Pattern) {
	m.pattern = p
}

func (m *MaterialProperties) Scattering(si SurfaceInteraction, t TransportMode) *BSDF {
	bsdf := NewBSDF(si.Surface)

	if transparency := NewGray(m.Transparency()); !transparency.Eq(Black) {

		bsdf.add(NewSpecularTransmission(transparency, 1, m.RefractiveIndex(), t))

		roughness := roughnessToAlpha(m.Roughness())
		bsdf.add(MicrofacetTransmission{
			transparency: transparency,
			dist:         NewTrowbridgeReitzDist(roughness),
			fres:         NewFresnelDialectric(1, m.RefractiveIndex()),
			etaA:         1,
			etaB:         m.RefractiveIndex(),
		})
	} else {
		if pattern := m.Pattern(); pattern != nil {
			bsdf.add(NewLambertianReflection(PatternOnObject(pattern, si.object, si)))
		} else {
			if color := m.ColorMap().At(si); !color.Eq(Black) {
				bsdf.add(NewLambertianReflection(color))
			}
		}
		if diffuse := m.DiffuseMap().At(si).Add(m.Ambient()); !diffuse.Eq(Black) {
			bsdf.add(NewLambertianReflection(diffuse))
		}
	}

	if metalness := m.Metalness(); !metalness.Eq(0) {
		var (
			dist = NewTrowbridgeReitzDist(roughnessToAlpha(m.Roughness()))
			fres = FresnelSchlick{f0: m.ColorMap().At(si)}
		)
		bsdf.add(NewMicrofacetReflection(White, dist, fres))
	}

	if specular := m.Specular(); !specular.Eq(Black) {
		var (
			roughness Scalar = 1.0
			dist             = NewTrowbridgeReitzDist(roughnessToAlpha(roughness))
			// fres = NewFresnelDialectric(1, material.RefractiveIndex())
			fres = FresnelSchlick{f0: specular}
		)
		bsdf.add(NewMicrofacetReflection(specular, dist, fres))
		bsdf.add(NewSpecularReflection(specular, NewFresnelNoop(White)))
	}

	if reflectiveness := m.Reflective(); !reflectiveness.Eq(0) {
		bsdf.add(NewSpecularReflection(
			NewGray(reflectiveness),
			NewFresnelNoop(White)))
	}

	return bsdf
}

type GlassMaterial struct {
	roughness    Scalar
	ior          Scalar
	transparency Texture
	reflection   Texture
}

func NewGlassMaterial(roughness, ior Scalar, transparency, reflection Texture) GlassMaterial {
	return GlassMaterial{
		roughness:    roughness,
		ior:          ior,
		transparency: transparency,
		reflection:   reflection,
	}
}

func (g GlassMaterial) Scattering(si SurfaceInteraction, mode TransportMode) *BSDF {
	bsdf := NewBSDF(si.Surface)

	var (
		r        = g.reflection.At(si)
		t        = g.transparency.At(si)
		specular = g.roughness == 0
	)

	if specular {
		if !t.Eq(Black) {
			bsdf.add(NewSpecularTransmission(t, 1, g.ior, mode))
		}

		if !r.Eq(Black) {
			bsdf.add(NewSpecularReflection(r, NewFresnelNoop(White)))
		}
	} else {
		dist := NewTrowbridgeReitzDist(g.roughness)
		fresnel := NewFresnelDialectric(1, g.ior)

		if !t.Eq(Black) {
			bsdf.add(MicrofacetTransmission{
				transparency: t,
				dist:         dist,
				fres:         fresnel,
				etaA:         1,
				etaB:         g.ior,
			})
		}

		if !r.Eq(Black) {
			bsdf.add(MicrofacetReflection{
				r:       r,
				dist:    dist,
				fresnel: fresnel,
			})
		}
	}

	return bsdf
}

type MetalMaterial struct {
	roughness ScalarTexture
	albedo    Texture
}

func NewMetalMaterial(roughness ScalarTexture, albedo Texture) MetalMaterial {
	return MetalMaterial{
		roughness: roughness,
		albedo:    albedo,
	}
}

func (m MetalMaterial) Scattering(si SurfaceInteraction, mode TransportMode) *BSDF {
	alpha := roughnessToAlpha(m.roughness.ScalarAt(si))
	dist := NewTrowbridgeReitzDist(alpha)

	// TODO use FresnelConductor in the future
	fresnel := FresnelSchlick{f0: m.albedo.At(si)}

	bsdf := NewBSDF(si.Surface)
	bsdf.add(NewMicrofacetReflection(White, dist, fresnel))
	return bsdf
}

type MirrorMaterial struct {
	reflection Texture
}

func NewMirrorMaterial(reflection Texture) MirrorMaterial {
	return MirrorMaterial{reflection: reflection}
}

func (m MirrorMaterial) Scattering(si SurfaceInteraction, mode TransportMode) *BSDF {
	bsdf := NewBSDF(si.Surface)
	bsdf.add(NewSpecularReflection(m.reflection.At(si), FresnelNoop{White}))
	return bsdf
}
