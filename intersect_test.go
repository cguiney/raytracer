package raytracer

import (
	"fmt"
	"math"
	"testing"
)

func TestIntersection(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		s := NewSphere()

		i := NewIntersection(s, 3.5)

		if want, have := Scalar(3.5), i.Time(); !have.Eq(want) {
			t.Errorf("improperly assigned time. have=%f want=%f", have, want)
		}

		if want, have := s, i.Object(); want != have {
			t.Errorf("object is not equal to input sphere. have=%+v want=%+v", have, want)
		}
	})

	t.Run("Union", func(t *testing.T) {
		s := NewSphere()

		n, m := NewIntersection(s, 1), NewIntersection(s, 2)

		xs := Intersections{n, m}

		if len(xs) != 2 {
			t.Errorf("incorrect length for intersection. have=%d want=%d", len(xs), 2)
		}

		if want, have := Scalar(1), xs[0].Time(); !have.Eq(want) {
			t.Errorf("incorrect time for first intersection. have=%f want=%f", have, want)
		}
		if want, have := Scalar(2), xs[1].Time(); !have.Eq(want) {
			t.Errorf("incorrect time for first intersection. have=%f want=%f", have, want)
		}
	})

	t.Run("hits", func(t *testing.T) {
		type hitTest struct {
			name       string
			intersects Intersections
			hits       bool
			want       Intersection
		}

		s := NewSphere()

		tests := []hitTest{
			{
				name: "all intersections with positive t",
				intersects: Intersections{
					NewIntersection(s, 1),
					NewIntersection(s, 2),
				},
				hits: true,
				want: NewIntersection(s, 1),
			},
			{
				name: "some intersections have negative t",
				intersects: Intersections{
					NewIntersection(s, -1),
					NewIntersection(s, 1),
				},
				hits: true,
				want: NewIntersection(s, 1),
			},
			{
				name: "all intersections have negative t",
				intersects: Intersections{
					NewIntersection(s, -2),
					NewIntersection(s, -1),
				},
				hits: false,
			},
			{
				name: "hit is always lowest nonnegative intersection",
				intersects: Intersections{
					NewIntersection(s, 5),
					NewIntersection(s, 7),
					NewIntersection(s, -3),
					NewIntersection(s, 2),
				},
				hits: true,
				want: NewIntersection(s, 2),
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				have, ok := tt.intersects.Hit()
				if ok != tt.hits {
					if ok {
						t.Errorf("hit when should not have. have=%+v", have)
					} else {
						t.Errorf("did not hit when should have. want=%+v", tt.want)
					}
				}
				if have != tt.want {
					t.Errorf("incorrect hit intersection. have=%+v want=%+v", have, tt.want)
				}
			})
		}
	})

	t.Run("ray transformation", func(t *testing.T) {
		type transformTest struct {
			name  string
			r     Ray
			trans func(Ray) Ray
			want  Ray
		}

		tests := []transformTest{
			{
				name:  "translation",
				r:     NewRay(NewPoint(1, 2, 3), NewVector(0, 1, 0)),
				trans: func(r Ray) Ray { return r.Translate(NewTranslation(3, 4, 5)) },
				want:  NewRay(NewPoint(4, 6, 8), NewVector(0, 1, 0)),
			},
			{
				name:  "scaling",
				r:     NewRay(NewPoint(1, 2, 3), NewVector(0, 1, 0)),
				trans: func(r Ray) Ray { return r.Scale(NewScale(2, 3, 4)) },
				want:  NewRay(NewPoint(2, 6, 12), NewVector(0, 3, 0)),
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				if have := tt.trans(tt.r); !have.Eq(tt.want) {
					t.Errorf("incorrect result of ray transform. have=%v want=%v", have, tt.want)
				}
			})
		}
	})

	t.Run("intersection state - hit on outside", func(t *testing.T) {
		s := NewSphere()
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))

		x := NewIntersection(s, 4)

		state := NewSurfaceInteraction(x, r)

		if state.inside {
			t.Errorf("expected intersection to occur on outside of sphere")
		}
	})

	t.Run("intersection state - hit on inside", func(t *testing.T) {
		s := NewSphere()
		r := NewRay(NewPoint(0, 0, 0), NewVector(0, 0, 1))
		x := NewIntersection(s, 1)
		state := NewSurfaceInteraction(x, r)

		if want := NewPoint(0, 0, 1); !state.point.Eq(want) {
			t.Errorf("incorrect point for intersection. have=%v want=%v", state.point, want)
		}

		if want := NewVector(0, 0, -1); !state.eye.Eq(want) {
			t.Errorf("incorrect eye vector for intersection. have=%v want=%v", state.eye, want)
		}

		if want := NewVector(0, 0, -1); !state.normal.Eq(want) {
			t.Errorf("incorrect normal vector for intersection. have=%v want=%v", state.normal, want)
		}

		if !state.inside {
			t.Errorf("expected intersection to be inside sphere")
		}
	})

	t.Run("precomputing reflection vector", func(t *testing.T) {
		plane := NewPlane()
		sq2 := Scalar(math.Sqrt2 / 2)
		r := NewRay(NewPoint(0, 1, -1), NewVector(0, -sq2, sq2))

		x := NewIntersection(plane, math.Sqrt2)

		state := NewSurfaceInteraction(x, r)

		if want := NewVector(0, sq2, sq2); !state.reflect.Eq(want) {
			t.Errorf("incorrect reflection vector. have=%v want=%v", state.reflect, want)
		}
	})

	t.Run("finding exit and enter refractive indices", func(t *testing.T) {
		a := NewGlassSphere()
		a.Scale(NewScale(2, 2, 2))

		b := NewGlassSphere()
		b.Translate(NewTranslation(0, 0, -0.25))
		b.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetRefractiveIndex(2.0)
			return m
		}())

		c := NewGlassSphere()
		c.Translate(NewTranslation(0, 0, 0.25))
		c.SetMaterial(func() Material {
			m := NewMaterialProperties()
			m.SetRefractiveIndex(2.5)
			return m
		}())

		r := NewRay(NewPoint(0, 0, -4), NewVector(0, 0, 1))
		xs := Intersections{
			NewIntersection(a, 2),
			NewIntersection(b, 2.75),
			NewIntersection(c, 3.25),
			NewIntersection(b, 4.75),
			NewIntersection(c, 5.25),
			NewIntersection(a, 6),
		}

		type nTest struct {
			index int
			exit  Scalar
			enter Scalar
		}

		tests := []nTest{
			{0, 1.0, 1.5},
			{1, 1.5, 2.0},
			{2, 2.0, 2.5},
			{3, 2.5, 2.5},
			{4, 2.5, 1.5},
			{5, 1.5, 1.0},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", tt.index), func(t *testing.T) {
				state := NewSurfaceInteraction(xs[tt.index], r, xs...)
				if !state.exit.Eq(tt.exit) {
					t.Errorf("incorrect exit. have=%f want=%f", state.exit, tt.exit)
				}
				if !state.enter.Eq(tt.enter) {
					t.Errorf("incorrect enter. have=%f want=%f", state.enter, tt.enter)
				}
			})

		}
	})

	t.Run("underpoint is offset below surface", func(t *testing.T) {
		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))
		shape := NewGlassSphere()
		shape.Translate(NewTranslation(0, 0, 1))

		x := NewIntersection(shape, 5)

		state := NewSurfaceInteraction(x, r, x)

		if state.under.Z() <= epsilon/2 {
			t.Errorf("underpoint is less than expected. have=%f", state.under.Z())
		}

		if state.point.Z() >= state.under.Z() {
			t.Errorf("under point is above point. have=%f want=%f", state.under.Z(), state.point.Z())
		}
	})

	t.Run("schlick approximation under total internal reflection", func(t *testing.T) {
		shape := NewGlassSphere()
		r := NewRay(NewPoint(0, 0, math.Sqrt2/2), NewVector(0, 1, 0))
		xs := Intersections{
			NewIntersection(shape, -math.Sqrt2/2),
			NewIntersection(shape, math.Sqrt2/2),
		}

		state := NewSurfaceInteraction(xs[1], r, xs...)

		if have := state.reflectance(); !have.Eq(1) {
			t.Errorf("incorrect reflectance. have=%f want=%f", have, 1.0)
		}
	})

	t.Run("reflectance of perpendicular ray", func(t *testing.T) {
		shape := NewGlassSphere()
		r := NewRay(NewPoint(0, 0, 0), NewVector(0, 1, 0))
		xs := Intersections{
			NewIntersection(shape, -1),
			NewIntersection(shape, 1),
		}

		state := NewSurfaceInteraction(xs[1], r, xs...)

		have := state.reflectance()
		if !have.Eq(0.04) {
			t.Errorf("incorrect reflectance. have=%f want=%f", have, 0.04)
		}
	})

	t.Run("reflectance with small angle and n1 > n2", func(t *testing.T) {
		shape := NewGlassSphere()
		r := NewRay(NewPoint(0, 0.99, -2), NewVector(0, 0, 1))
		xs := Intersections{
			NewIntersection(shape, 1.8589),
		}

		state := NewSurfaceInteraction(xs[0], r, xs...)
		have := state.reflectance()
		if want := Scalar(0.48873); !have.Eq(want) {
			t.Errorf("incorrect reflectance. have=%f want=%f", have, want)
		}
	})
}

func TestStackSet(t *testing.T) {
	t.Run("insertion", func(t *testing.T) {
		var (
			s      stackSet
			object = NewSphere()
		)

		s.insert(object)
		if s.len() != 1 {
			t.Errorf("incorrect length after insertion. have=%d want=%d", s.len(), 1)
		}
	})

	t.Run("membership", func(t *testing.T) {
		var (
			s      stackSet
			object = NewSphere()
		)

		s.insert(object)
		if !s.contains(object) {
			t.Errorf("expected s to contain object.")
		}

		if s.contains(NewSphere()) {
			t.Errorf("did not expect s to contain object")
		}

		// expect  objects to be equated by address, not by value
		if s.contains(NewSphere()) {
			t.Errorf("did not expect s to contain similar sphere")
		}
	})

	t.Run("removal", func(t *testing.T) {
		var (
			s       stackSet
			objects = []Object{
				NewSphere(),
				NewSphere(),
				NewSphere(),
			}
		)

		for _, obj := range objects {
			s.insert(obj)
		}

		s.remove(objects[1])
		if s.len() != 2 {
			t.Errorf("incorrect length. have=%d want=%d", s.len(), 2)
		}

		if s.contains(objects[1]) {
			t.Errorf("expected object to be absent")
		}

		s.remove(objects[2])

		if s.peek() != objects[0] {
			t.Errorf("unexpected result of peek. have=%+v want=%v", s.peek(), objects[0])
		}

	})
}
