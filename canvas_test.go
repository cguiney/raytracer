package raytracer

import (
	"bytes"
	"testing"

	"gitlab.com/cguiney/raytracer/internal/ppm"
)

func TestColors(t *testing.T) {
	t.Run("Color constructor", func(t *testing.T) {
		c := NewColor(-0.5, 0.4, 1.7)

		if c.Red() != -0.5 {
			t.Errorf("incorrect red value. have=%f want=%f", c.Red(), -0.5)
		}
		if c.Green() != 0.4 {
			t.Errorf("incorrect green value. have=%f want=%f", c.Green(), 0.4)
		}
		if c.Blue() != 1.7 {
			t.Errorf("incorrect green value. have=%f want=%f", c.Blue(), 1.7)
		}
	})

	t.Run("Color addition", func(t *testing.T) {
		a, b := NewColor(0.9, 0.6, 0.75), NewColor(0.7, 0.1, 0.25)
		want := NewColor(1.6, 0.7, 1.0)

		if have := a.Add(b); !have.Eq(want) {
			t.Errorf("incorrect result of addition between colors %s and %s. have=%s want=%s",
				a, b, have, want)
		}
	})

	t.Run("Color scaling", func(t *testing.T) {
		c := NewColor(0.2, 0.3, 0.4)
		s := Scalar(2)
		want := NewColor(0.4, 0.6, 0.8)

		if have := c.Scale(s); !have.Eq(want) {
			t.Errorf("incorrect result of multiplication of color %s by %f. have=%s want=%s",
				c, s, have, want)
		}
	})

	t.Run("Color multiplication", func(t *testing.T) {
		a, b := NewColor(1, 0.2, 0.4), NewColor(0.9, 1, 0.1)
		want := NewColor(0.9, 0.2, 0.04)

		if have := a.Mul(b); !have.Eq(want) {
			t.Errorf("incorrect result of multiplication between colors %s and %s. have=%s want=%s",
				a, b, have, want)
		}
	})
}

func TestCanvas(t *testing.T) {
	t.Run("Canvas constructor", func(t *testing.T) {
		c := NewCanvas(10, 20)

		if c.Width() != 10 {
			t.Errorf("Incorrect canvas width have=%d want=%d", c.Width(), 10)
		}

		if c.Height() != 20 {
			t.Errorf("Incorrect canvas height have=%d want=%d", c.Height(), 20)
		}
	})

	t.Run("Write and read pixel", func(t *testing.T) {
		c := NewCanvas(10, 20)
		red := NewColor(1, 0, 0)

		c.DrawPixel(2, 3, red)

		if have := c.PixelAt(2, 3); !have.Eq(red) {
			t.Errorf("incorrect color at position (2,3). have=%s want=%s", have, red)
		}
	})

	// TODO:
	// These should really be part of the PPM package
	// but for now, I want to exercise the canvas
	t.Run("Encoding to ppm", func(t *testing.T) {
		c := NewCanvas(5, 3)

		c.DrawPixel(0, 0, NewColor(1.5, 0, 0))
		c.DrawPixel(2, 1, NewColor(0, 0.5, 0))
		c.DrawPixel(4, 2, NewColor(-0.5, 0, 1))

		buffer := bytes.Buffer{}
		encoder := ppm.NewEncoder()

		if err := encoder.Encode(&buffer, c); err != nil {
			t.Error("failed encoding canvas to ppm:", err)
		}

		const header = "P3\n5 3\n255\n"

		output := buffer.String()
		if have := output[:len(header)]; have != header {
			t.Errorf("incorrect output from encoding canvas. have=%q want=%q", have, header)
		}

		const lines = `255 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 128 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 255
`

		if have := output[len(header):]; have != lines {
			t.Errorf("incorrect output from encoding canvas. have=%q want=%q", have, lines)
		}

		if last := output[len(output)-1]; last != '\n' {
			t.Errorf("ppm output did not end in newline")
		}

	})

	t.Run("Constructing ppm pixel data", func(t *testing.T) {
		c := NewCanvas(10, 2)

		for y := 0; y < c.Height(); y++ {
			for x := 0; x < c.Width(); x++ {
				c.DrawPixel(x, y, NewColor(1, 0.8, 0.6))
			}
		}

		buffer := bytes.Buffer{}
		encoder := ppm.NewEncoder()

		if err := encoder.Encode(&buffer, c); err != nil {
			t.Error("failed encoding canvas to ppm:", err)
		}

		const header = "P3\n10 2\n255\n"

		output := buffer.String()
		if have := output[:len(header)]; have != header {
			t.Errorf("incorrect output from encoding canvas. have=%q want=%q", have, header)
		}

		const lines = `255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153
`

		if have := output[len(header):]; have != lines {
			t.Errorf("incorrect output from encoding canvas. have=%q want=%q", have, lines)
		}

		if last := output[len(output)-1]; last != '\n' {
			t.Errorf("ppm output did not end in newline")
		}
	})

}
