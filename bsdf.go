package raytracer

import "math"

type BxDF interface {
	Type() BxDFType
	Eval(wo, wi Vector) Color
	Sample(ctx RenderCtx, wo Vector) SurfaceSample
	Pdf(wo, wi Vector) Scalar
}

type BSDF struct {
	surface Surface

	s Vector // X axis
	n Vector // Y axis
	t Vector // Z axis

	// distribution functions to be sampled from
	bxdfs []BxDF
}

func (b *BSDF) add(df BxDF) {
	b.bxdfs = append(b.bxdfs, df)
}

func (b *BSDF) matches(t BxDFType) (n int) {
	for _, bxdf := range b.bxdfs {
		if bxdf.Type().Match(t) {
			n++
		}
	}
	return
}

func (b *BSDF) worldToLocal(v Vector) Vector {
	return NewVector(
		v.Dot(b.s),
		v.Dot(b.n),
		v.Dot(b.t),
	)
}

func (b *BSDF) localToWorld(v Vector) Vector {
	return NewVector(
		(b.s.X()*v.X())+(b.n.X()*v.Y())+(b.t.X()*v.Z()),
		(b.s.Y()*v.X())+(b.n.Y()*v.Y())+(b.t.Y()*v.Z()),
		(b.s.Z()*v.X())+(b.n.Z()*v.Y())+(b.t.Z()*v.Z()),
	)
}

func (b *BSDF) Eval(worldWo, worldWi Vector, flags BxDFType) (f Color, pdf Scalar) {
	var (
		wo      = b.worldToLocal(worldWo)
		wi      = b.worldToLocal(worldWi)
		normal  = b.surface.Geometry.Normal
		reflect = worldWi.Dot(normal)*worldWo.Dot(normal) > 0
	)

	if wo.Y() == 0 {
		return Black, 0
	}

	for _, bxdf := range b.bxdfs {
		var (
			isReflective   = reflect && bxdf.Type().Match(BSDF_REFLECTION)
			isTransmissive = !reflect && bxdf.Type().Match(BSDF_TRANSMISSION)
		)
		if bxdf.Type().Match(flags) && (isReflective || isTransmissive) {
			f = f.Add(bxdf.Eval(wo, wi))
		}
	}

	pdf = b.Pdf(worldWo, worldWi, flags)
	return f, pdf
}

func (b *BSDF) randomBxDF(ctx RenderCtx, t BxDFType) (bxdf BxDF, candidates int, ok bool) {
	n := b.matches(t)
	if n == 0 {
		return nil, 0, false
	}

	component := int(ctx.RandomScalar() * Scalar(n))
	if n-1 < component {
		component = n - 1
	}

	// find an actual bxdf to use
	var count = component
	for i := range b.bxdfs {
		if b.bxdfs[i].Type().Match(t) {
			bxdf = b.bxdfs[i]

			count--
			if count == 0 {
				break
			}
		}
	}

	assert("bxdf is found", func() bool {
		return bxdf != nil
	})

	return bxdf, n, true
}

func (b *BSDF) Sample(ctx RenderCtx, eye Vector, t BxDFType) (SurfaceSample, bool) {
	bxdf, candidates, ok := b.randomBxDF(ctx, t)
	if !ok {
		return SurfaceSample{}, false
	}

	// can now sample bxdf
	wo := b.worldToLocal(eye)

	// prevent any division by zero in a bxdf
	// if Y is exactly zero, just terminate the path
	if wo.Y() == 0 {
		return SurfaceSample{}, false
	}

	sample := bxdf.Sample(ctx, wo)
	if sample.PDF.Eq(0) {
		return SurfaceSample{}, false
	}

	// store the local wi so that it can be used in pdf calculation
	wi := sample.Direction

	// prep values for return
	// transform to world coordinates
	sample.Flags = bxdf.Type()
	sample.Direction = b.localToWorld(sample.Direction)

	if !bxdf.Type().Match(BSDF_SPECULAR) {
		for i := range b.bxdfs {
			// current bxdf is already included in pdf calculation
			if b.bxdfs[i] == bxdf {
				continue
			}
			pdf := b.bxdfs[i].Pdf(wo, wi)

			assert("pdf is gte zero", func() bool {
				return pdf >= 0
			})

			sample.PDF += pdf
		}

		assert("one or more candidate bxdf", func() bool {
			return candidates >= 1
		})

		sample.PDF /= Scalar(candidates)
	}

	if !bxdf.Type().Match(BSDF_SPECULAR) {
		var (
			normal  = b.surface.Geometry.Normal
			reflect = sample.Direction.Dot(normal)*eye.Dot(normal) > 0
			f       = Black
		)

		for _, bxdf := range b.bxdfs {
			var (
				isReflective   = reflect && bxdf.Type().Match(BSDF_REFLECTION)
				isTransmissive = !reflect && bxdf.Type().Match(BSDF_TRANSMISSION)
			)
			if bxdf.Type().Match(t) && (isReflective || isTransmissive) {
				f = f.Add(bxdf.Eval(wo, wi))
			}
		}
		sample.Color = f
	}

	return sample, true
}

func (b *BSDF) Pdf(wo, wi Vector, flags BxDFType) (pdf Scalar) {
	wo = b.worldToLocal(wo)
	wi = b.worldToLocal(wi)

	if wo.Y() == 0 {
		return  0
	}

	var matches int
	for _, bxdf := range b.bxdfs {
		if bxdf.Type().Match(flags) {
			matches++
			pdf += bxdf.Pdf(wo, wi)
		}
	}

	return pdf / Scalar(matches)
}

type BxDFType uint16

const (
	BSDF_REFLECTION BxDFType = 1 << iota
	BSDF_TRANSMISSION
	BSDF_DIFFUSE
	BSDF_GLOSSY
	BSDF_SPECULAR
	BSDF_ALL = BSDF_REFLECTION | BSDF_TRANSMISSION | BSDF_DIFFUSE | BSDF_GLOSSY | BSDF_SPECULAR
)

func (b BxDFType) Is(t BxDFType) bool {
	return b&t == t
}

func (b BxDFType) Match(t BxDFType) bool {
	return b&t != 0
}

type SurfaceSample struct {
	Flags     BxDFType
	Color     Color
	Direction Vector
	PDF       Scalar
}

type TransportMode uint8

const (
	Radiance TransportMode = iota
	Importance
)

type Fresnel interface {
	Eval(cosThetaI Scalar) Color
}

type FresnelDialectric struct {
	etaI, etaT Scalar
}

func NewFresnelDialectric(etaI, etaT Scalar) FresnelDialectric {
	return FresnelDialectric{
		etaI: etaI,
		etaT: etaT,
	}
}

func (f FresnelDialectric) Eval(cosThetaI Scalar) Color {
	// potentially swap indices of refraction
	cosThetaI = cosThetaI.Clamp(-1, 1)

	var etaI, etaT = f.etaI, f.etaT

	if entering := cosThetaI > 0; !entering {
		etaI, etaT = etaT, etaI
		cosThetaI = cosThetaI.Abs()
	}

	// compute cosThetaT using snell's law
	var (
		sinThetaI = max(0, 1-cosThetaI.Pow2()).Sqrt()
		sinThetaT = etaI / etaT * sinThetaI
		cosThetaT = max(0, 1-sinThetaT.Pow2()).Sqrt()
	)

	if sinThetaT >= 1 {
		return White
	}

	var (
		rparl = ((etaT * cosThetaI) - (etaI * cosThetaT)) / ((etaT * cosThetaI) + (etaI * cosThetaT))
		rperp = ((etaI * cosThetaI) - (etaT * cosThetaT)) / ((etaI * cosThetaI) + (etaT * cosThetaT))
	)

	return NewGray((rparl.Pow2() + rperp.Pow2()) / 2)
}

type FresnelConductor struct {
	etaI, etaT Scalar
	k          Scalar
}

func NewFresnelConductor(etaI, etaT, k Scalar) FresnelConductor {
	return FresnelConductor{
		etaI: etaI,
		etaT: etaT,
		k:    k,
	}
}

// http://www.pbr-book.org/3ed-2018/Reflection_Models/Specular_Reflection_and_Transmission.html#eq:frcond
func (f FresnelConductor) Eval(cosThetaI Scalar) Color {
	// potentially swap indices of refraction
	cosThetaI = cosThetaI.Abs().Clamp(-1, 1)

	var (
		eta = f.etaT / f.etaI // index of refraction
		k   = f.k / f.etaI    // light absorbtion coefficient

		cosThetaI2 = cosThetaI.Pow2()
		sinThetaI2 = 1 - cosThetaI2

		eta2 = eta.Pow2()
		k2   = k.Pow2()

		t0 = eta2 - k2 - sinThetaI2

		a2b2 = (t0.Pow2() + (4 * eta2 * k2)).Sqrt()

		t1 = a2b2 + cosThetaI2
		a  = (0.5 * (a2b2 + t0)).Sqrt()
		t2 = 2 * a * cosThetaI

		rs = (t1 - t2) / (t1 + t2)

		t3 = cosThetaI2*a2b2 + sinThetaI2.Pow2()
		t4 = t2 * sinThetaI2

		rp = rs * ((t3 - t4) / (t3 + t4))
	)

	return NewGray(0.5 * (rp + rs))
}

type FresnelNoop struct {
	Color
}

func NewFresnelNoop(c Color) FresnelNoop {
	return FresnelNoop{c}
}

func (f FresnelNoop) Eval(cosThetaI Scalar) Color {
	return f.Color
}

type SpecularTransmission struct {
	t          Color
	etaA, etaB Scalar
	fresnel    Fresnel
	mode       TransportMode
}

func NewSpecularTransmission(t Color, etaA, etaB Scalar, mode TransportMode) SpecularTransmission {
	etaA = etaA.Clamp(1, etaA)
	etaB = etaB.Clamp(1, etaB)
	return SpecularTransmission{
		t:       t,
		etaA:    etaA,
		etaB:    etaB,
		fresnel: NewFresnelDialectric(etaA, etaB),
		mode:    mode,
	}
}

func (s SpecularTransmission) Type() BxDFType {
	return BSDF_TRANSMISSION | BSDF_SPECULAR
}

func (s SpecularTransmission) Eval(wi, wo Vector) Color {
	return Black
}

func (s SpecularTransmission) Sample(ctx RenderCtx, wo Vector) SurfaceSample {
	var (
		etaI = s.etaA
		etaT = s.etaB
	)

	if entering := wo.Y() > 0; !entering {
		etaI, etaT = etaT, etaI
	}

	wi, ok := refract(wo, NewVector(0, 1, 0).Forward(wo), etaI/etaT)
	if !ok {
		return SurfaceSample{}
	}

	fres := s.fresnel.Eval(costheta(wi))
	ft := s.t.Mul(White.Sub(fres))

	// if s.mode == Radiance {
	//ft = ft.Scale(etaI.Pow2() / etaT.Pow2())
	//	}

	ft = ft.Scale(1.0 / wi.Y().Abs())

	return SurfaceSample{
		Color:     ft,
		Direction: wi,
		PDF:       1,
	}
}

func (s SpecularTransmission) Pdf(wo, wi Vector) Scalar {
	//if sameHemisphere(wo, wi) {
	//	return costheta(wi).Abs() * (1 / math.Pi)
	// }
	return 0
	// return 1
	// return pdf(wo, wi)
}

type SpecularReflection struct {
	r       Color
	fresnel Fresnel
}

func NewSpecularReflection(r Color, fresnel Fresnel) SpecularReflection {
	return SpecularReflection{
		r:       r,
		fresnel: fresnel,
	}
}

func (s SpecularReflection) Type() BxDFType {
	return BSDF_REFLECTION | BSDF_SPECULAR
}

func (s SpecularReflection) Eval(wo, wi Vector) Color {
	return Black
}

func (s SpecularReflection) Sample(ctx RenderCtx, wo Vector) SurfaceSample {
	var (
		wi       = NewVector(-wo.X(), wo.Y(), -wo.Z())
		cosTheta = costheta(wi)
		f        = s.fresnel.Eval(cosTheta)
	)
	return SurfaceSample{
		Color:     f.Mul(s.r.Scale(1 / cosTheta.Abs())),
		Direction: wi,
		PDF:       1,
	}
}

func (s SpecularReflection) Pdf(wo, wi Vector) Scalar {
	return 0
}

type LambertianReflection struct {
	r Color
}

func NewLambertianReflection(r Color) LambertianReflection {
	return LambertianReflection{r: r}
}

func (lr LambertianReflection) Type() BxDFType {
	return BSDF_REFLECTION | BSDF_DIFFUSE
}

func (lr LambertianReflection) Eval(_, _ Vector) Color {
	return lr.r.Scale(1 / math.Pi)
}

func (lr LambertianReflection) Rho() Color {
	return lr.r
}

func (lr LambertianReflection) Sample(ctx RenderCtx, wo Vector) SurfaceSample {
	wi := cosineSampleHemisphere(ctx)
	if wo.Y() < 0 {
		wi = NewVector(wi.X(), -wi.Y(), wi.Z())
	}

	return SurfaceSample{
		Color:     lr.Eval(wo, wi),
		Direction: wi,
		PDF:       lr.Pdf(wo, wi),
	}
}

func (lr LambertianReflection) Pdf(wo, wi Vector) Scalar {
	if sameHemisphere(wo, wi) {
		return costheta(wi).Abs() * (1 / math.Pi)
	}
	return 0
}

type FresnelSchlick struct {
	f0 Color // color at 0deg
}

func (f FresnelSchlick) Eval(cosThetaI Scalar) Color {
	vh := Scalar(math.Pow(float64(1-cosThetaI), 5))
	return f.f0.Add(White.Sub(f.f0).Scale(vh))
}

func NewBSDF(surface Surface) *BSDF {
	b := new(BSDF)
	b.surface = surface
	b.s = b.surface.Shading.DPDU.Normalize()
	b.n = b.surface.Shading.Normal
	b.t = b.n.Cross(b.s)
	return b
}



func refract(wi, n Vector, eta Scalar) (wt Vector, ok bool) {
	// compute cosThetaT using snells law
	var (
		cosThetaI  = n.Dot(wi)
		sin2ThetaI = max(0, 1-cosThetaI.Pow2())
		sin2ThetaT = eta.Pow2() * sin2ThetaI
		cosThetaT  = Scalar.Sqrt(1 - sin2ThetaT)
	)

	if sin2ThetaT >= 1 {
		// total internal refraction
		return
	}

	wt = wi.Negate().Mul(eta).Add(n.Mul(eta*cosThetaI - cosThetaT))
	ok = true

	return
}

func reflect(wo, n Vector) Vector {
	//return NewVector(-wo.X(), wo.Y(), -wo.Z())
	// return wo.Reflect(n)
	return wo.Negate().Add(n.Mul(2 * wo.Dot(n)))
}

func concentricSampleDisk(ctx RenderCtx) Point2D {
	var randoms = [2]Scalar{
		ctx.RandomScalar(),
		ctx.RandomScalar(),
	}

	randoms[0] = (randoms[0] * 2) - 1
	randoms[1] = (randoms[1] * 2) - 1

	if randoms[0] == 0 && randoms[1] == 0 {
		return NewPoint2D(0, 0)
	}

	var (
		x, y  = randoms[0], randoms[1]
		theta Scalar
		r     Scalar
	)

	if x.Abs() > y.Abs() {
		r = x
		theta = (math.Pi / 4) * (y / x)
	} else {
		r = y
		theta = (math.Pi / 2) - (math.Pi/4)*(x/y)
	}

	return NewPoint2D(theta.Cos(), theta.Sin()).Mul(r)
}

func cosineSampleHemisphere(ctx RenderCtx) Vector {
	var (
		d = concentricSampleDisk(ctx)
		u = d.U().Pow2()
		v = d.V().Pow2()
		z = Scalar(math.Max(0, float64(1-u-v))).Sqrt()
	)
	return NewVector(d.U(), z, d.V())
}

func cosineSampleHemispherePDF(cosTheta Scalar) Scalar {
	return cosTheta * (1 / math.Pi)
}

func sameHemisphere(w, wp Vector) bool {
	return w.Y()*wp.Y() > 0
}
