package raytracer_test

import (
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/wavefront"
	"io"
	"os"
	"testing"
)

func compare(t *testing.T, ctx raytracer.RenderCtx, c *raytracer.Camera, have, want *raytracer.World) {
	for y := 0; y < c.Height(); y++ {
		for x := 0; x < c.Width(); x++ {
			ray := c.RayForPixel(raytracer.Scalar(x), raytracer.Scalar(y))

			var (
				haveXS raytracer.Intersections
				wantXS raytracer.Intersections
			)

			have.Intersect(ray, &haveXS)
			want.Intersect(ray, &wantXS)

			compareIntersections(t, haveXS, wantXS)
		}
	}
}

func TestCompareWorlds(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	fh, err := os.Open("resources/dragon.obj")
	if err != nil {
		t.Fatal(err)
	}

	w := wavefront.New()

	if err := w.Parse(fh); err != nil {
		t.Fatal("failed parsing: ", err)
	}

	if _, err := fh.Seek(0, io.SeekStart); err != nil {
		t.Fatal(err)
	}

	p := wavefront.NewLegacy()
	if err := p.Parse(fh); err != nil {
		t.Fatal("failed parsing legacy: ", err)
	}

	have := raytracer.NewWorld()
	have.AddObject(w.Build())

	want := raytracer.NewWorld()
	want.AddObject(p.Group)

	c := raytracer.NewCamera(500, 200, 1.2)

	c.ViewTransform(raytracer.NewViewTransform(
		raytracer.NewPoint(0, 2.5, -10),
		raytracer.NewPoint(0, 1, 0),
		raytracer.NewVector(0, 1, 0),
	))

	compare(t, raytracer.DefaultContext(), c, have, want)
}

func compareVectors(t *testing.T, have []raytracer.Vector, want []raytracer.Vector) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of vectors. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Fatalf("incorrect vector %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func comparePoints(t *testing.T, have []raytracer.Point, want []raytracer.Point) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of points. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Fatalf("incorrect point %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func compareIntersections(t *testing.T, have raytracer.Intersections, want raytracer.Intersections) {
	if len(have) != len(want) {
		t.Errorf("differing number of intersections, have=%d want=%d", len(have), len(want))
	}

	for i, x := range have {
		if x.Time() != want[i].Time() {
			t.Fatalf("intersection %d occurs at different time. have=%s want=%s", i, x.Time(), want[i].Time())
		}

		if !x.U().Eq(want[i].U()) {
			t.Fatalf("intersection %d has different U values. have=%s want=%s", i, x.U(), want[i].U())
		}

		if !x.V().Eq(want[i].V()) {
			t.Fatalf("intersection %d has different V values. have=%s want=%s", i, x.V(), want[i].V())
		}

		comparePoints(t,
			x.Object().(*raytracer.MeshTriangle).Points(),
			want[i].Object().(*raytracer.Triangle).Points(),
		)

		compareVectors(t,
			x.Object().(*raytracer.MeshTriangle).Edges(),
			want[i].Object().(*raytracer.Triangle).Edges(),
		)

		compareVectors(t,
			x.Object().(*raytracer.MeshTriangle).Normals(),
			want[i].Object().(*raytracer.Triangle).Normals(),
		)

		haveN := raytracer.NormalAt(x.Object(), raytracer.NewPoint(1, 1, 1), x)
		wantN := raytracer.NormalAt(want[i].Object(), raytracer.NewPoint(1, 1, 1), want[i])

		compareVectors(t, []raytracer.Vector{haveN}, []raytracer.Vector{wantN})
	}
}
