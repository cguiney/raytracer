# RayTracer

This is an implementation of the ray tracer described in [The Ray Tracer Challenge](http://raytracerchallenge.com/) 

# Current Status
This project is still in active development.
It is currently not accepting any contributions due to the educational aspect of the project.

Everything up to _Chapter 14 - Groups_ is implemented in the `raytracer` package.

The program `rt` lags currently is lagging behind, not having all shapes and patterns available to it.

There are currently no API stability guarantees.
# Installation

```
go get gitlab.com/cguiney/raytracer

go install gitlab.com/cguiney/raytracer/cmd/rt
```

# Tests
To run tests:

```
go test ./...
```

# Examples
Example yaml files can be found in the `examples/` directory.

They can be used to render complex scenes:
```
rt -o cubes.png examples/cubes.yml
```

# Samples

Some sample images generated by the ray tracer are available in the `samples/` directory.

These images were generated at various different stages of development.


# Road Map
### Core
 - Constructive Solid Geometry
 - Texture mapping
 - Focal blur
 - Soft shadows
 - Antialiasing
 - More
 
 ### rt
 - Cones
 - Scripting
 - Multiple camera support
   
### Usability
- Support tracking progress of render
 
### Performance
- Intersection memory usage
- Bounding Volume Heirarchies
- Octrees