package raytracer

import (
	"math"
	"testing"
)

func TestTranslation(t *testing.T) {
	t.Run("multiplying by translation matrix", func(t *testing.T) {
		transform := NewTranslation(5, -3, 2)
		p := NewPoint(-3, 4, 5)

		if want, have := NewPoint(2, 1, 7), p.Translate(transform); !have.Eq(want) {
			t.Errorf("incorrect result multiplying by translation matrix. have=%v want=%v", have, want)
		}
	})

	t.Run("multiplying by inverse of a translation matrix", func(t *testing.T) {
		transform := NewTranslation(5, -3, 2)
		inv := transform.Invert()
		p := NewPoint(-3, 4, 5)

		if want, have := NewPoint(-8, 7, 3), p.Translate(inv); !have.Eq(want) {
			t.Errorf("incorrect result multiplying inverse translation. have=%v want=%v", have, want)
		}
	})
}

func TestScaling(t *testing.T) {
	t.Run("Scaling matrix applied to point", func(t *testing.T) {
		s := NewScale(2, 3, 4)
		p := NewPoint(-4, 6, 8)

		if want, have := NewPoint(-8, 18, 32), p.Scale(s); !have.Eq(want) {
			t.Errorf("incorrect result of scaling. have=%v want=%v", have, want)
		}
	})

	t.Run("Scaling matrix applied to vector", func(t *testing.T) {
		s := NewScale(2, 3, 4)
		v := NewVector(-4, 6, 8)

		if want, have := NewVector(-8, 18, 32), v.Scale(s); !have.Eq(want) {
			t.Errorf("incorrect result of scaling. have=%v want=%v", have, want)
		}
	})

	t.Run("Inverse scaling matrix applied to vector", func(t *testing.T) {
		s := NewScale(2, 3, 4)
		inv := s.Invert()
		v := NewVector(-4, 6, 8)
		if want, have := NewVector(-2, 2, 2), v.Scale(inv); !have.Eq(want) {
			t.Errorf("incorrect result of inverse scaling. have=%v want=%v", have, want)
		}
	})

	t.Run("Reflection is scaling by negative value", func(t *testing.T) {
		s := NewScale(-1, 1, 1)
		p := NewPoint(2, 3, 4)

		if want, have := NewPoint(-2, 3, 4), p.Scale(s); !have.Eq(want) {
			t.Errorf("incorrect result of reflection. have=%v want=%v", have, want)
		}
	})
}

func TestRotation(t *testing.T) {
	type rotationTest struct {
		name     string
		point    Point
		rotation Rotation
		want     Point
	}

	var sq2 = Scalar(math.Sqrt2 / 2)

	tests := []rotationTest{
		{
			name:     "Rotating point around the x axis by half quarter",
			point:    NewPoint(0, 1, 0),
			rotation: NewRotationX(math.Pi / 4),
			want:     NewPoint(0, Scalar(math.Sqrt(float64(2))/2), Scalar(math.Sqrt(float64(2))/2)),
		},
		{
			name:     "Rotating point around the x axis by full quarter",
			point:    NewPoint(0, 1, 0),
			rotation: NewRotationX(math.Pi / 2),
			want:     NewPoint(0, 0, 1),
		},
		{
			name:     "Rotating point around the x axis by half quarter inversed",
			point:    NewPoint(0, 1, 0),
			rotation: NewRotationX(math.Pi / 4).Invert(),
			want:     NewPoint(0, sq2, -sq2),
		},
		{
			name:     "Rotating point around the y axis by half quarter",
			point:    NewPoint(0, 0, 1),
			rotation: NewRotationY(math.Pi / 4),
			want:     NewPoint(sq2, 0, sq2),
		},
		{
			name:     "Rotating point around the y axis by full quarter",
			point:    NewPoint(0, 0, 1),
			rotation: NewRotationY(math.Pi / 2),
			want:     NewPoint(1, 0, 0),
		},
		{
			name:     "Rotating point around the z axis by half quarter",
			point:    NewPoint(0, 1, 0),
			rotation: NewRotationZ(math.Pi / 4),
			want:     NewPoint(-sq2, sq2, 0),
		},
		{
			name:     "Rotating point around the z axis by full quarter",
			point:    NewPoint(0, 1, 0),
			rotation: NewRotationZ(math.Pi / 2),
			want:     NewPoint(-1, 0, 0),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if have := tt.point.Rotate(tt.rotation); !have.Eq(tt.want) {
				t.Errorf("incorrect result of rotation. have=%v want=%v", have, tt.want)
			}
		})
	}
}

func TestShearing(t *testing.T) {
	type shearingTest struct {
		name  string
		point Point
		shear Shear
		want  Point
	}

	tests := []shearingTest{
		{
			name:  "moves x in proportion to y",
			point: NewPoint(2, 3, 4),
			shear: NewShear(1, 0, 0, 0, 0, 0),
			want:  NewPoint(5, 3, 4),
		},
		{
			name:  "moves x in proportion to z",
			point: NewPoint(2, 3, 4),
			shear: NewShear(0, 1, 0, 0, 0, 0),
			want:  NewPoint(6, 3, 4),
		},
		{
			name:  "moves y in proportion to x",
			point: NewPoint(2, 3, 4),
			shear: NewShear(0, 0, 1, 0, 0, 0),
			want:  NewPoint(2, 5, 4),
		},
		{
			name:  "moves y in proportion to z",
			point: NewPoint(2, 3, 4),
			shear: NewShear(0, 0, 0, 1, 0, 0),
			want:  NewPoint(2, 7, 4),
		},
		{
			name:  "moves z in proportion to x",
			point: NewPoint(2, 3, 4),
			shear: NewShear(0, 0, 0, 0, 1, 0),
			want:  NewPoint(2, 3, 6),
		},
		{
			name:  "moves z in proportion to y",
			point: NewPoint(2, 3, 4),
			shear: NewShear(0, 0, 0, 0, 0, 1),
			want:  NewPoint(2, 3, 7),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if have := tt.point.Shear(tt.shear); !have.Eq(tt.want) {
				t.Errorf("incorrect result of rotation. have=%v want=%v", have, tt.want)
			}
		})
	}
}

func TestTransformationSequencing(t *testing.T) {
	p := NewPoint(1, 0, 1)
	r := NewRotationX(math.Pi / 2)
	s := NewScale(5, 5, 5)
	tr := NewTranslation(10, 5, 7)

	p2 := p.Rotate(r)
	if want := NewPoint(1, -1, 0); !p2.Eq(want) {
		t.Errorf("incorrect result of rotation. have=%v want=%v", p2, want)
	}

	p3 := p2.Scale(s)
	if want := NewPoint(5, -5, 0); !p3.Eq(want) {
		t.Errorf("incorrect result of rotation. have=%v want=%v", p3, want)
	}

	p4 := p3.Translate(tr)
	if want := NewPoint(15, 0, 7); !p4.Eq(want) {
		t.Errorf("incorrect result of rotation. have=%v want=%v", p4, want)
	}
}

func TestFluentApiSequencing(t *testing.T) {
	p := NewPoint(1, 0, 1)
	r := NewRotationX(math.Pi / 2)
	s := NewScale(5, 5, 5)
	tr := NewTranslation(10, 5, 7)

	have := p.Rotate(r).Scale(s).Translate(tr)

	if want := NewPoint(15, 0, 7); !have.Eq(want) {
		t.Errorf("incorrect result of transformations. have=%v want=%v", have, want)
	}
}

func TestViewTransform(t *testing.T) {
	type viewTransformTest struct {
		name     string
		from, to Point
		up       Vector
		want     Matrix
	}

	tests := []viewTransformTest{
		{
			name: "default orientation is identity matrix",
			from: NewPoint(0, 0, 0),
			to:   NewPoint(0, 0, -1),
			up:   NewVector(0, 1, 0),
			want: IdentityMatrix(),
		},
		{
			name: "looking in positive z direction",
			from: NewPoint(0, 0, 0),
			to:   NewPoint(0, 0, 1),
			up:   NewVector(0, 1, 0),
			want: NewScale(-1, 1, -1).matrix,
		},
		{
			name: "moves the world",
			from: NewPoint(0, 0, 8),
			to:   NewPoint(0, 0, 0),
			up:   NewVector(0, 1, 0),
			want: NewTranslation(0, 0, -8).matrix,
		},
		{
			name: "arbitrary transform",
			from: NewPoint(1, 3, 2),
			to:   NewPoint(4, -2, 8),
			up:   NewVector(1, 1, 0),
			want: Matrix{
				{-0.50709, 0.50709, 0.67612, -2.36643},
				{0.76772, 0.60609, 0.12122, -2.82843},
				{-0.35857, 0.59761, -0.71714, 0},
				{0, 0, 0, 1},
			},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			have := NewViewTransform(tt.from, tt.to, tt.up)
			if !have.matrix.Eq(tt.want) {
				t.Errorf("incorrect view transform matrix.\n\thave=%v\n\twant=%v", have.matrix, tt.want)
			}
		})
	}
}
