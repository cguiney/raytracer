package raytracer

import (
	"fmt"
	"math"
)

const (
	epsilon = 0.00001
)

var (
	inf = Scalar(math.Inf(1))
	// negative infinity
	ninf = Scalar(math.Inf(-1))
)

func eq(a, b Scalar) bool {
	return math.Abs(float64(a)-float64(b)) < epsilon
}

func min(x, y Scalar) Scalar {
	// return Scalar(math.Min(float64(x), float64(y)))
	if x < y {
		return x
	}
	return y
}

func max(x, y Scalar) Scalar {
	// return Scalar(math.Max(float64(x), float64(y)))
	if x > y {
		return x
	}
	return y
}

func absmax(x, y Scalar) Scalar {
	return max(abs(x), abs(y))
}

// returns the min of the triplet x, y, z
func min3(x, y, z Scalar) Scalar {
	return min(x, min(y, z))
}

// returns the max of the triplet x, y, z
func max3(x, y, z Scalar) Scalar {
	return max(x, max(y, z))
}

// take the max of the absolute value of the triplet x,y,z
func absmax3(x, y, z Scalar) Scalar {
	return max3(abs(x), abs(y), abs(z))
}

func abs(x Scalar) Scalar {
	return Scalar(math.Abs(float64(x)))
}

// fix up behavior from math.Mod
// https://www.reddit.com/r/golang/comments/bnvik4/modulo_in_golang/en9otem/
func mod(x, d Scalar) Scalar {
	if d < 0 {
		d = -d
	}

	x = Scalar(math.Mod(float64(x), float64(d)))
	if x < 0 {
		return x + d
	}

	return x
}

func sin2theta(v Vector) Scalar {
	return max(1-cos2theta(v), 0)
}

func sintheta(v Vector) Scalar {
	return sin2theta(v).Sqrt()
}

func sinphi(v Vector) Scalar {
	var s = sintheta(v)
	if s.Eq(0) {
		return 0
	}

	return (v.Z() / s).Clamp(-1, 1)
}

func sin2phi(v Vector) Scalar {
	return sinphi(v).Pow2()
}

func cosphi(v Vector) Scalar {
	var s = sintheta(v)
	if s.Eq(0) {
		return 1
	}

	return (v.X() / s).Clamp(-1, 1)
}

func cos2phi(v Vector) Scalar {
	return cosphi(v).Pow2()
}

func cos2theta(v Vector) Scalar {
	return v.Y().Pow2()
}

func costheta(v Vector) Scalar {
	return v.Y()
}

func tantheta(v Vector) Scalar {
	return sintheta(v) / cos2theta(v)
}

func lerp(t, v1, v2 Scalar) Scalar {
	return ((1-t)*v1) + (t*v2)
}

type Scalar float64

func (s Scalar) String() string { return fmt.Sprintf("%.5f", s) }

func (s Scalar) Eq(o Scalar) bool {
	return eq(s, o)
}

func (s Scalar) Pow2() Scalar {
	return s * s
	//	return Scalar(math.Pow(float64(s), 2))
}

func (s Scalar) Sqrt() Scalar {
	return Scalar(math.Sqrt(float64(s)))
}

func (s Scalar) Abs() Scalar {
	return Scalar(math.Abs(float64(s)))
}

func (s Scalar) Mod(d Scalar) Scalar {
	return mod(s, d)
}

func (s Scalar) Round() Scalar {
	return Scalar(math.Round(float64(s)))
}

func (s Scalar) Sin() Scalar {
	return Scalar(math.Sin(float64(s)))
}

func (s Scalar) Cos() Scalar {
	return Scalar(math.Cos(float64(s)))
}

func (s Scalar) Clamp(low, high Scalar) Scalar {
	if s < low {
		return low
	}
	if s > high {
		return high
	}
	return s
}

func (s Scalar) IsInf() bool {
	return math.IsInf(float64(s), 1) || math.IsInf(float64(s), -1)
}

func (s Scalar) Atan() Scalar {
	return Scalar(math.Atan(float64(s)))
}

func (s Scalar) Tan() Scalar {
	return Scalar(math.Tan(float64(s)))
}

func (s Scalar) Log() Scalar {
	return Scalar(math.Log(float64(s)))
}

func (s Scalar) Log2() Scalar {
	return Scalar(math.Log2(float64(s)))
}

func (s Scalar) Floor() int {
	return int(math.Floor(float64(s)))
}

func (s Scalar) ScalarAt(si SurfaceInteraction) Scalar {
	return s
}
