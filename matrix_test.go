package raytracer

import (
	"math"
	"testing"
)

type matrixIndexTest struct {
	x, y int
	want Scalar
}

func TestMatrix(t *testing.T) {
	t.Run("Matrix construction", func(t *testing.T) {
		m := Matrix{
			{1, 2, 3, 4},
			{5.5, 6.5, 7.5, 8.5},
			{9, 10, 11, 12},
			{13.5, 14.5, 15.5, 16.5},
		}

		tests := []matrixIndexTest{
			{0, 0, 1},
			{0, 3, 4},
			{1, 0, 5.5},
			{1, 2, 7.5},
			{2, 2, 11},
			{3, 0, 13.5},
			{3, 2, 15.5},
		}

		for _, tt := range tests {
			if have := m[tt.x][tt.y]; !have.Eq(tt.want) {
				t.Errorf("incorrect value at (%d, %d) have=%f want=%f",
					tt.x, tt.y, have, tt.want)
			}
		}
	})

	t.Run("Equality", func(t *testing.T) {
		m := Matrix{
			{1, 2, 3, 4},
			{5, 6, 7, 8},
			{9, 8, 7, 6},
			{5, 4, 3, 2},
		}
		n := m

		if !m.Eq(n) {
			t.Error("expected matrices to be equal")
		}

		n = Matrix{
			{2, 3, 4, 5},
			{6, 7, 8, 9},
			{8, 7, 6, 5},
			{4, 3, 2, 1},
		}
		if m.Eq(n) {
			t.Error("expected matrices to be different")
		}
	})

	t.Run("Multiplication", func(t *testing.T) {
		a := Matrix{
			{1, 2, 3, 4},
			{5, 6, 7, 8},
			{9, 8, 7, 6},
			{5, 4, 3, 2},
		}

		b := Matrix{
			{-2, 1, 2, 3},
			{3, 2, 1, -1},
			{4, 3, 6, 5},
			{1, 2, 7, 8},
		}

		want := Matrix{
			{20, 22, 50, 48},
			{44, 54, 114, 108},
			{40, 58, 110, 102},
			{16, 26, 46, 42},
		}

		if have := a.Mul(b); !have.Eq(want) {
			t.Errorf("matrix multiplication did not yield correct results have=%v want=%v", have, want)
		}
	})

	t.Run("vector multiplication", func(t *testing.T) {
		A := Matrix{
			{1, 2, 3, 4},
			{2, 4, 4, 2},
			{8, 6, 4, 1},
			{0, 0, 0, 1},
		}

		b := Tuple{1, 2, 3, 1}
		want := Tuple{18, 24, 33, 1}

		if have := A.Mulv(b); !have.Eq(want) {
			t.Errorf("incorrect result of multiplying matrix by tuple have=%v want=%v",
				have, want)
		}
	})

	t.Run("multiplication by identity matrix", func(t *testing.T) {
		A := Matrix{
			{0, 1, 2, 4},
			{1, 2, 4, 8},
			{2, 4, 8, 16},
			{4, 8, 16, 34},
		}

		if have := A.Mul(IdentityMatrix()); !have.Eq(A) {
			t.Errorf("expected result of matrix multiplication with identity matrix to equal input matrix. have=%v want=%v", have, A)
		}
	})

	t.Run("tuple multiplication of identity matrix", func(t *testing.T) {
		a := Tuple{1, 2, 3, 4}

		if have := IdentityMatrix().Mulv(a); !have.Eq(a) {
			t.Errorf("expected result of tuple multiplication with identity matrix to equal input tuple. have=%v want=%v", have, a)
		}
	})

	t.Run("matrix transposition", func(t *testing.T) {
		A := Matrix{
			{0, 9, 3, 0},
			{9, 8, 0, 8},
			{1, 8, 5, 3},
			{0, 0, 5, 8},
		}

		want := Matrix{
			{0, 9, 1, 0},
			{9, 8, 8, 0},
			{3, 0, 5, 5},
			{0, 8, 3, 8},
		}

		if have := A.Transpose(); !have.Eq(want) {
			t.Errorf("incorrect results of transposition. have=%v want=%v", have, want)
		}
	})

	t.Run("transpose of identity matrix", func(t *testing.T) {
		if have := IdentityMatrix().Transpose(); !have.Eq(IdentityMatrix()) {
			t.Errorf("expected transposition of identity matrix to equal identity matrix. have=%v", have)
		}
	})

	t.Run("Submatrix", func(t *testing.T) {
		A := Matrix{
			{-6, 1, 1, 6},
			{-8, 5, 8, 6},
			{-1, 0, 8, 2},
			{-7, 1, -1, 1},
		}

		want := Matrix3D{
			{-6, 1, 6},
			{-8, 8, 6},
			{-7, -1, 1},
		}

		if have := A.SubMatrix(2, 1); !have.Eq(want) {
			t.Errorf("incorrect result of submatrix. have=%v want=%v", have, want)
		}
	})

	t.Run("Cofactor", func(t *testing.T) {
		A := Matrix{
			{-2, -8, 3, 5},
			{-3, 1, 7, 3},
			{1, 2, -9, 6},
			{-6, 7, 7, -9},
		}

		type cofactorTest struct {
			row, col int
			cofactor Scalar
		}

		tests := []cofactorTest{
			{0, 0, 690},
			{0, 1, 447},
			{0, 2, 210},
			{0, 3, 51},
		}

		for _, tt := range tests {
			if have := A.Cofactor(tt.row, tt.col); !have.Eq(tt.cofactor) {
				t.Errorf("incorrect cofactor have=%f want=%f", have, tt.cofactor)
			}
		}
	})

	t.Run("determinant", func(t *testing.T) {
		A := Matrix{
			{-2, -8, 3, 5},
			{-3, 1, 7, 3},
			{1, 2, -9, 6},
			{-6, 7, 7, -9},
		}

		want := Scalar(-4071)

		if have := A.Determinant(); !have.Eq(want) {
			t.Errorf("incorrect determinant. have=%f want=%f", have, want)
		}
	})

	t.Run("inversion", func(t *testing.T) {
		A := Matrix{
			{-5, 2, 6, -8},
			{1, -5, 1, 8},
			{7, 7, -6, -7},
			{1, -3, 7, 4},
		}
		B := A.Invert()

		want := Matrix{
			{0.21805, 0.45113, 0.24060, -0.04511},
			{-0.80827, -1.45677, -0.44361, 0.52068},
			{-0.07895, -0.22368, -0.05263, 0.19737},
			{-0.52256, -0.81391, -0.30075, 0.30639},
		}

		if want, have := Scalar(532), A.Determinant(); !have.Eq(want) {
			t.Errorf("incorrect determinant for matrix A. have=%f want=%f", have, want)
		}

		if want, have := Scalar(-160), A.Cofactor(2, 3); !have.Eq(want) {
			t.Errorf("incorrect cofactor for (2, 3) in matrix A. have=%f want=%f", have, want)
		}

		if want, have := Scalar(-160)/Scalar(532), B[3][2]; !have.Eq(want) {
			t.Errorf("incorrect value for (3, 2) in matrix B. have=%f want=%f", have, want)
		}

		if want, have := Scalar(105), A.Cofactor(3, 2); !have.Eq(want) {
			t.Errorf("incorrect cofactor for (3, 2) in matrix A. have=%f want=%f", have, want)
		}

		if want, have := Scalar(105)/Scalar(532), B[2][3]; !have.Eq(want) {
			t.Errorf("incorrect value for (2, 3) in matrix B. have=%f want=%f", have, want)
		}

		if !B.Eq(want) {
			t.Errorf("incorrect inverted matrix. have=%v want=%v", B, want)
		}
	})

	t.Run("inversion", func(t *testing.T) {
		type inversionTest struct {
			input Matrix
			want  Matrix
		}

		tests := []inversionTest{
			{
				input: Matrix{
					{8, -5, 9, 2},
					{7, 5, 6, 1},
					{-6, 0, 9, 6},
					{-3, 0, -9, -4},
				},
				want: Matrix{
					{-0.15385, -0.15385, -0.28205, -0.53846},
					{-0.07692, 0.12308, 0.02564, 0.03077},
					{0.35897, 0.35897, 0.43590, 0.92308},
					{-0.69231, -0.69231, -0.76923, -1.92308},
				},
			},
			{
				input: Matrix{
					{9, 3, 0, 9},
					{-5, -2, -6, -3},
					{-4, 9, 6, 4},
					{-7, 6, 6, 2},
				},
				want: Matrix{
					{-0.04074, -0.07778, 0.14444, -0.22222},
					{-0.07778, 0.03333, 0.36667, -0.33333},
					{-0.02901, -0.14630, -0.10926, 0.12963},
					{0.17778, 0.06667, -0.26667, 0.33333},
				},
			},
		}

		for _, tt := range tests {
			if have := tt.input.Invert(); !have.Eq(tt.want) {
				t.Errorf("incorrect inverted matrix.\nhave=%v\nwant=%v", have, tt.want)
			}
		}
	})

	t.Run("multiplying product by inverse", func(t *testing.T) {
		A := Matrix{
			{3, -9, 7, 3},
			{3, -8, 2, -9},
			{-4, 4, 4, 1},
			{-6, 5, -1, 1},
		}

		B := Matrix{
			{8, 2, 2, 2},
			{3, -1, 7, 0},
			{7, 0, 5, 4},
			{6, -2, 0, 5},
		}

		C := A.Mul(B)

		if have := C.Mul(B.Invert()); !have.Eq(A) {
			t.Errorf("multiplying by inverse did not produce original matrix.\nhave=%v\nwant=%v",
				have, A)
		}
	})
}

func TestMatrix2D(t *testing.T) {
	t.Run("Matrix Construction", func(t *testing.T) {
		m := Matrix2D{
			{-3, 5},
			{1, -2},
		}

		tests := []matrixIndexTest{
			{0, 0, -3},
			{0, 1, 5},
			{1, 0, 1},
			{1, 1, -2},
		}

		for _, tt := range tests {
			if have := m[tt.x][tt.y]; !have.Eq(tt.want) {
				t.Errorf("incorrect value at (%d, %d) have=%f want=%f",
					tt.x, tt.y, have, tt.want)
			}
		}
	})

	t.Run("Equality", func(t *testing.T) {
		m := Matrix2D{
			{1, 2},
			{5, 6},
		}
		n := m

		if !m.Eq(n) {
			t.Error("expected matrices to be equal")
		}

		n = Matrix2D{
			{2, 3},
			{6, 7},
		}
		if m.Eq(n) {
			t.Error("expected matrices to be different")
		}
	})

	t.Run("Determinant", func(t *testing.T) {
		A := Matrix2D{
			{1, 5},
			{-3, 2},
		}
		want := Scalar(17)

		if have := A.Determinant(); !have.Eq(want) {
			t.Errorf("incorrect result of determinant have=%f want=%f", have, want)
		}

	})
}

func TestMatrix3D(t *testing.T) {
	t.Run("Matrix Construction", func(t *testing.T) {
		m := Matrix3D{
			{-3, 5, 0},
			{1, -2, -7},
			{0, 1, 1},
		}

		tests := []matrixIndexTest{
			{0, 0, -3},
			{1, 1, -2},
			{2, 2, 1},
		}

		for _, tt := range tests {
			if have := m[tt.x][tt.y]; !have.Eq(tt.want) {
				t.Errorf("incorrect value at (%d, %d) have=%f want=%f",
					tt.x, tt.y, have, tt.want)
			}
		}
	})

	t.Run("Equality", func(t *testing.T) {
		m := Matrix3D{
			{1, 2, 3},
			{5, 6, 7},
			{9, 8, 7},
		}
		n := m

		if !m.Eq(n) {
			t.Error("expected matrices to be equal")
		}

		n = Matrix3D{
			{2, 3, 4},
			{6, 7, 8},
			{8, 7, 6},
		}
		if m.Eq(n) {
			t.Error("expected matrices to be different")
		}
	})

	t.Run("SubMatrix", func(t *testing.T) {
		a := Matrix3D{
			{1, 5, 0},
			{-3, 2, 7},
			{0, 6, -3},
		}
		want := Matrix2D{
			{-3, 2},
			{0, 6},
		}

		if have := a.SubMatrix(0, 2); !have.Eq(want) {
			t.Errorf("incorrect result of submatrix  have=%v want=%v", have, want)
		}
	})

	t.Run("Minor", func(t *testing.T) {
		A := Matrix3D{
			{3, 5, 0},
			{2, -1, -7},
			{6, -1, 5},
		}

		B := A.SubMatrix(1, 0)
		want := Scalar(25)

		if have := B.Determinant(); !have.Eq(want) {
			t.Errorf("incorrect determinant of submatrix %v. have=%v want=%v", B, have, want)
		}

		if have := A.Minor(1, 0); !have.Eq(want) {
			t.Errorf("incorrect minor of matrix %v. have=%v want=%v", A, have, want)
		}
	})

	t.Run("cofactor", func(t *testing.T) {
		A := Matrix3D{
			{3, 5, 0},
			{2, -1, -7},
			{6, -1, 5},
		}

		type cofactorTest struct {
			row, col        int
			minor, cofactor Scalar
		}

		tests := []cofactorTest{
			{0, 0, -12, -12},
			{1, 0, 25, -25},
		}

		for _, tt := range tests {
			minor := A.Minor(tt.row, tt.col)
			cofactor := A.Cofactor(tt.row, tt.col)

			if !tt.minor.Eq(minor) {
				t.Errorf("incorrect minor for (%d, %d) have=%f want=%f", tt.row, tt.col, minor, tt.minor)
			}

			if !tt.cofactor.Eq(cofactor) {
				t.Errorf("incorrect cofactor for (%d, %d) have=%f want=%f", tt.row, tt.col, cofactor, tt.cofactor)
			}
		}
	})

	t.Run("Determinant", func(t *testing.T) {
		A := Matrix3D{
			{1, 2, 6},
			{-5, 8, -4},
			{2, 6, 4},
		}

		if want, have := Scalar(56), A.Cofactor(0, 0); !have.Eq(want) {
			t.Errorf("incorrect cofactor for (0, 0). have=%f want=%f", have, want)
		}

		if want, have := Scalar(12), A.Cofactor(0, 1); !have.Eq(want) {
			t.Errorf("incorrect cofactor for (0, 1). have=%f want=%f", have, want)
		}

		if want, have := Scalar(-46), A.Cofactor(0, 2); !have.Eq(want) {
			t.Errorf("incorrect cofactor for (0, 2). have=%f want=%f", have, want)
		}

		if want, have := Scalar(-196), A.Determinant(); !have.Eq(want) {
			t.Errorf("incorrect determinant for matrix. have=%f want=%f", have, want)
		}
	})
}

var t Tuple

func BenchmarkMatrixMulv(b *testing.B) {
	m := Matrix{
		{math.Pi, math.E, math.Sqrt2, math.Phi},
		{math.Pi, math.E, math.Sqrt2, math.Phi},
		{math.Pi, math.E, math.Sqrt2, math.Phi},
		{math.Pi, math.E, math.Sqrt2, math.Phi},
	}
	t := Tuple{math.Pi, math.E, math.Sqrt2, math.Phi}

	for n := 0; n < b.N; n++ {
		t = m.Mulv(t)
	}
}