package raytracer

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"strings"
	"sync"
	"time"
)

type WhittedIntegrator struct {
	world    *World
	camera   *Camera
	previews chan chan Canvas
}

func NewWhittedIntegrator(world *World, camera *Camera) *WhittedIntegrator {
	return &WhittedIntegrator{
		world:    world,
		camera:   camera,
		previews: make(chan chan Canvas, 1),
	}
}

func (w *WhittedIntegrator) reflect(ctx RenderCtx, hit Interaction) Color {
	if hit.object.Material().(*MaterialProperties).Reflective() == 0 {
		return Black
	}

	r := NewRay(hit.over, hit.reflect)
	col := w.ColorAt(ctx, r)
	res := col.Scale(hit.object.Material().(*MaterialProperties).Reflective())
	return res
}

func (w *WhittedIntegrator) refract(ctx RenderCtx, hit Interaction) Color {
	// shouldn't be needed due to check in ColorAt()
	// but the book had a test for it being called by refract()
	if ctx.remaining < 0 {
		return Black
	}

	if hit.object.Material().(*MaterialProperties).Transparency() == 0 {
		return Black
	}

	var (
		// Inverse of Snells law - n1 / n2
		ratio = float64(hit.exit / hit.enter)
		// cos(theta_i) is the same as the dot product of the eye and normal vectors
		cosi = float64(hit.eye.Dot(hit.normal))

		// using trigonomic identity to determine sin(theta_t)^2
		sin = math.Pow(ratio, 2) * (1 - math.Pow(cosi, 2))

		cost = math.Sqrt(1 - sin)

		isTotalInternalRefraction = sin > 1
	)

	if isTotalInternalRefraction {
		return Black
	}

	var (
		// value to scale normal by
		ns = Scalar(ratio*cosi - cost)
		// value to scale eye by
		es = Scalar(ratio)

		// direction of new refracted ray
		direction = hit.normal.Mul(ns).Sub(hit.eye.Mul(es))

		// new ray being refracted
		refraction = NewRay(hit.under, direction)
	)

	color := w.ColorAt(ctx, refraction)
	trans := color.Scale(hit.object.Material().(*MaterialProperties).Transparency())
	return trans
}

func (w *WhittedIntegrator) shade(ctx RenderCtx, hit SurfaceInteraction) (color Color) {
	m := hit.object.Material().(*MaterialProperties)
	for _, light := range w.world.Lights() {
		if _, ok := light.(PointLight); !ok {
			continue
		}

		shadowed := w.isShadowed(light.(PointLight), hit.over)
		surface := Lighting(
			hit.object.Material().(*MaterialProperties),
			hit.object,
			light.(PointLight),
			hit.over,
			hit.eye,
			hit.normal,
			shadowed,
			hit.Surface.Shading.UV,
		)

		reflection := w.reflect(ctx, hit.Interaction)
		refraction := w.refract(ctx, hit.Interaction)

		if m.Transparency() > 0 && m.Reflective() > 0 {
			reflectance := hit.reflectance()
			reflection = reflection.Scale(reflectance)
			refraction = refraction.Scale(1 - reflectance)
		}
		color = color.Add(surface.Add(reflection).Add(refraction))
	}

	color = color.Add(m.Emission())

	return
}

func (w *WhittedIntegrator) isShadowed(light PointLight, p Point) bool {
	var (
		path      = light.Position().PathFrom(p)
		distance  = path.Magnitude()
		direction = path.Normalize()
		ray       = NewRay(p, direction)

		intersections Intersections
	)

	w.world.Intersect(ray, &intersections)

	hit, ok := intersections.Hit()
	if !ok {
		return false
	}

	if !hit.Object().Shadows() {
		return false
	}

	return hit.Time() < distance
}

func (w *WhittedIntegrator) ColorAt(ctx RenderCtx, r Ray) Color {
	ctx.remaining--

	// base case, there's been some sort of reflection loop
	// that needs to be terminated
	if ctx.remaining < 0 {
		return NewColor(0, 0, 0)
	}

	var intersections Intersections
	w.world.Intersect(r, &intersections)

	hit, ok := intersections.Hit()
	if !ok {
		return NewColor(0, 0, 0)
	}

	return w.shade(ctx, NewSurfaceInteraction(hit, r, intersections...))
}

func (w *WhittedIntegrator) Preview() Canvas {
	preview := make(chan Canvas, 1)
	w.previews <- preview
	return <-preview
}

func (w *WhittedIntegrator) Render(ctx RenderCtx) Canvas {
	image := NewCanvas(w.camera.width, w.camera.height)

	var (
		width, height = w.camera.Width(), w.camera.Height()

		wg     sync.WaitGroup
		lines  = make(chan int, ctx.concurrency+1)
		pixels = make(chan pixel, height)
	)

	// feed the workers
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(lines)
		for y := 0; y < height; y++ {
			lines <- y
		}
	}()

	// work work work
	for i := 0; i < ctx.concurrency; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for y := range lines {
				for x := 0; x < width; x++ {
					ray := w.camera.RayForPixel(Scalar(x), Scalar(y))
					color := w.ColorAt(ctx, ray)
					pixels <- pixel{x, y, color}
				}
			}
		}()
	}

	// wait for all workers
	go func() {
		wg.Wait()
		close(pixels)
	}()

	var i = 0

	for {
		select {
		case pixel, ok := <-pixels:
			if !ok {
				return image
			}
			i++
			if i%1000 == 0 {
				log.Printf("%d/%d (%.5f%%) rendered \n",
					i, width*height, (float64(i)/float64(width*height))*100)
			}
			image.DrawPixel(pixel.x, pixel.y, pixel.color)
		case preview := <-w.previews:
			preview <- image.Clone()
		}
	}
}

type sample struct {
	Canvas
	time.Time
	time.Duration
}

type PathIntegrator struct {
	samples int // samples per pixel

	previews chan chan Canvas
}

func NewPathIntegrator(samples int) *PathIntegrator {
	return &PathIntegrator{
		samples:  samples,
		previews: make(chan chan Canvas, 1),
	}
}

func (p *PathIntegrator) Preview() Canvas {
	preview := make(chan Canvas, 1)
	p.previews <- preview
	return <-preview
}

func (p *PathIntegrator) sample(ctx RenderCtx, world *World, camera *Camera) sample {
	var (
		width, height = camera.Width(), camera.Height()
		sample        = sample{
			Canvas: NewCanvas(width, height),
			Time:   time.Now(),
		}

		t = tracer{
			camera: camera,
			world:  world,
		}
	)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			color := t.tracePixel(ctx, Scalar(x), Scalar(y))
			assert("color is valid", color.Valid)

			sample.Canvas.DrawPixel(x, y, color)
		}
		//log.Printf("[sample %d] rendered %d/%d rows (%.5f%%)",
		//	sample, y, height, float64(y)/float64(height))
	}
	sample.Duration = time.Since(sample.Time)

	return sample
}

func (p *PathIntegrator) Sample(ctx RenderCtx, world *World, camera *Camera) Canvas {
	s := p.sample(ctx, world, camera)
	return s.Canvas
}

func (p *PathIntegrator) Render(ctx RenderCtx, world *World, camera *Camera) Canvas {
	image := NewCanvas(camera.width, camera.height)

	var (
		width, height = camera.Width(), camera.Height()

		wg      sync.WaitGroup
		samples = make(chan int, ctx.concurrency+1)
		results = make(chan sample, ctx.concurrency*2)
	)

	// feed the workers

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(samples)
		for i := 0; i < p.samples; i++ {
			samples <- i
		}
	}()

	// work work work
	for i := 0; i < ctx.concurrency; i++ {
		wg.Add(1)
		go func(ctx RenderCtx, id int64) {
			defer wg.Done()
			// TODO seed better
			ctx.rng = rand.New(rand.NewSource(time.Now().Unix() + id))
			//ctx = WithRNG(ctx, rand.New(rand.NewSource(id)))
			for range samples {
				results <- p.sample(ctx, world, camera)
			}
		}(ctx, int64(i))
	}

	// wait for all workers
	go func() {
		wg.Wait()
		close(results)
	}()

	average := func(n int) Canvas {
		averaged := NewCanvas(width, height)
		for y := 0; y < height; y++ {
			for x := 0; x < width; x++ {
				sum := image.PixelAt(x, y)
				averaged.DrawPixel(x, y, sum.Scale(1/Scalar(n)))
			}
		}
		return averaged
	}

	var i = 0
	for {
		select {
		case result, ok := <-results:
			if !ok {
				return average(i)
			}
			i++

			for y := 0; y < height; y++ {
				for x := 0; x < width; x++ {

					sum := image.PixelAt(x, y)
					sample := result.PixelAt(x, y)
					total := sum.Add(sample)

					if total.tuple[0] < 0 || total.tuple[1] < 0 || total.tuple[2] < 0 {
						panic("color summation is less than zero")
					}

					image.DrawPixel(x, y, total)
				}
			}

			log.Printf("sample completed in %s. %d/%d (%.5f%%) samples rendered\n",
				result.Duration, i, p.samples, (float64(i)/float64(p.samples))*100)
		case preview := <-p.previews:
			preview <- average(i)
		}
	}
}

type tracer struct {
	camera *Camera // camera state
	world  *World  // world state

	sum Color // summation of contributions

	intersections Intersections // intersection buffer
	paths         []path        // path buffer
}

func (t *tracer) randomLight(ctx RenderCtx) Light {
	return t.world.lights[ctx.rng.Intn(len(t.world.lights))]
}

func (t *tracer) bounce(ctx RenderCtx, path path) (path, bool) {
	path.ray = path.next      // path continues from the previous path
	path.depth++              // increment depth
	path.contribution = Black // reset contribution
	path.emission = Black

	if path.depth >= ctx.remaining {
		return path, false
	}

	t.world.Intersect(path.ray, &t.intersections)

	// check for intersection
	hit, ok := t.intersections.Hit()
	if path.depth == 1 || path.specular {
		if ok {
			path.emission = hit.object.Material().Emission().Mul(path.weight).Clamp(0, 1)
			// if light, ok := hit.Object().(LightObject); ok {
			// path.emission = light.
			// }

			// path.emission = hit.Object().Light().Emission().Clamp(0, 1)
		} else {
			//for _, light := range p.world.lights {
			//	path.emission = path.emission.Add(path.weight.Mul(light.Emission(r)))
			//}
		}
	}

	// didn't hit anything
	if !ok {
		return path, false
	}

	// get contribution for this path
	// compile bsdf
	// select light to sample
	// estimate direct contribution
	si := NewSurfaceInteraction(hit, path.ray, t.intersections...)
	path.interaction = si.Interaction
	path.surface = si.Surface

	path.bsdf = path.interaction.object.Material().Scattering(si, Importance)
	path.light = t.randomLight(ctx)

	direct := t.shade(ctx, path)
	assert("contribution is valid", direct.Valid)

	nlights := Scalar(len(t.world.lights))
	contrib := direct.Scale(nlights)
	contrib = path.weight.Mul(contrib)
	path.contribution = contrib

	assert("contribution is valid", path.contribution.Valid)

	// sample bsdf to get new path
	sample, ok := path.bsdf.Sample(ctx, path.interaction.eye, BSDF_ALL)
	if !ok {
		return path, false
	}

	// terminate the path if the sample returned black
	// it's possible for the sample to return black with a non-zero pdf
	// in this case, no further contributions will be made, and continuing
	// to try to collect samples could produce a division by zero from
	if sample.Color.Eq(Black) {
		return path, false
	}

	// determine if bounce is due to a specular reflection
	path.specular = sample.Flags.Match(BSDF_SPECULAR)

	// determine next path to take
	if sample.Flags.Match(BSDF_TRANSMISSION) {
		path.next = NewRay(path.interaction.under, sample.Direction)
	} else {
		path.next = NewRay(path.interaction.over, sample.Direction)
	}

	var (
		normal    = path.surface.Shading.Normal
		color     = sample.Color
		direction = sample.Direction
	)

	color = color.Scale(direction.Dot(normal).Abs())
	path.sample = sample
	path.weight = path.weight.Mul(color).Scale(1 / sample.PDF)

	//russian roulette
	if path.depth > 3 {
		q := max(0.05, 1-path.weight.tuple[1])
		//q := max(0.5, 1-path.weight.tuple[1])
		if s := ctx.rng.Float64(); Scalar(s) < q {
			//println("terminating from roulette")
			return path, false
		}

		path.weight = path.weight.Scale(1 / (1 - q))
		//path.weight = path.weight.Scale(1/1 - q)
	}

	return path, true
}

func (t *tracer) trace(ctx RenderCtx, ray Ray) Color {
	init := path{
		next:   ray,
		weight: White,
	}

	t.sum = Black
	t.paths = t.paths[:0]
	t.paths = append(t.paths, init)

	for {
		// cast the ray into the world
		path, ok := t.bounce(ctx, t.paths[len(t.paths)-1])

		// include even if this is path terminates
		// if no contribution, then it will simply add zero
		t.sum = t.sum.Add(path.contribution)
		t.sum = t.sum.Add(path.emission)
		t.paths = append(t.paths, path)

		// check if terminal path
		// if so...terminate
		if !ok {
			break
		}

		assert("color is valid", path.contribution.Valid)
		assert("weight is valid", path.weight.Valid)
	}

	return t.sum
}

var btMu sync.Mutex

func (t *tracer) backtrace() {
	btMu.Lock()
	defer btMu.Unlock()

	for i, path := range t.paths {

		indent := strings.Repeat("\t", i)

		fmt.Printf("%sray: %v\n", indent, path.ray)
		if path.interaction.object != nil {
			fmt.Printf("%sobject: %T\n", indent, path.interaction.object)
			fmt.Printf("%smaterial: %+v\n", indent, path.interaction.object.Material())
		}

		fmt.Printf("%scontribution: %v\n", indent, path.contribution)
		fmt.Printf("%semission: %v\n", indent, path.emission)
		fmt.Printf("%sbounced to %v\n", indent, path.next)
		fmt.Printf("%seye: %v\n", indent, path.interaction.eye)
		fmt.Printf("%sshading normal: %v\n", indent, path.surface.Shading.Normal)
		fmt.Printf("%ssample direction: %v\n", indent, path.sample.Direction)
		fmt.Printf("%ssample pdf: %v\n", indent, path.sample.PDF)
		fmt.Printf("%ssample color: %v\n", indent, path.sample.Color)
		fmt.Printf("%sweight:%v\n", indent, path.weight)
		fmt.Printf("---\n")
	}
}

func (t *tracer) tracePixel(ctx RenderCtx, x, y Scalar) Color {
	defer func() {
		if r := recover(); r != nil {
			t.backtrace()
			panic(r)
		}
	}()
	xx := x + ctx.RandomScalar()
	yy := y + ctx.RandomScalar()

	ray := t.camera.RayForPixel(xx, yy)

	contribution := t.trace(ctx, ray)

	return contribution
}

func (t *tracer) lighting(ctx RenderCtx, path path, flags BxDFType) Color {
	// Compute the lighting contribution
	// Sample the light
	// This gets two main things
	// 1. A point on the light's surface.
	//    If it's a point light, then it is the position of the singularity
	//    If it's an area light, then it's some sampled position on the surface of the object
	//    This point is used for determining if the surface interaction is occluded
	// 2. A direction for scaling the surface color based on the angle between the surface and the light
	li, ok := path.light.SampleAt(ctx, path.interaction)
	if !ok {
		return Black
	}

	// 2. Evaluate the bsdf at the chosen sample point on the light
	surface, sPDF := path.bsdf.Eval(li.Direction, path.interaction.eye, flags)
	surface = surface.Scale(li.Direction.Dot(path.surface.Shading.Normal).Abs())

	// Skip additional computations if the surface doesn't contribute anything
	if surface.Eq(Black) {
		return Black
	}

	// Do an occlusion test.
	// If the surface is not occluded by the light, accumulate the light's contribution
	if t.shadowed(li.Point, path.interaction.over) {
		return Black
	}

	// Return lighting contribution
	if path.light.IsDeltaDistribution() {
		return surface.Mul(li.Color.Scale(1 / li.PDF))
	}

	weight := powerHeuristic(1, li.PDF, 1, sPDF)
	return surface.Mul(li.Color).Scale(weight / li.PDF)
}

func (t *tracer) shade(ctx RenderCtx, path path) Color {
	flags := BSDF_ALL
	if path.specular {
		flags ^= BSDF_SPECULAR
	}

	// get the direct lighting contribution
	direct := t.lighting(ctx, path, flags)
	assert("direct lighting contribution is valid", direct.Valid)
	if path.light.IsDeltaDistribution() {
		// multiple importance sampling is not applicable to delta distribution lights
		// because there's no chance that the BSDF can sample in the direction of the light
		return direct
	}

	// multiple importance sampling

	// sample the BSDF to get a reflection/refraction direction
	sample, ok := path.bsdf.Sample(ctx, path.interaction.eye, flags)
	if !ok {
		return direct
	}

	sample.Color = sample.Color.Scale(sample.Direction.Dot(path.surface.Shading.Normal).Abs())
	if sample.Color.Eq(Black) {
		return direct
	}

	var (
		weight   Scalar = 1
		lighting Color
	)

	// no weighting needs to be computed if the surface returned a specular sample
	// in that case, the contribution should remain 100% towards the surface
	if !sample.Flags.Match(BSDF_SPECULAR) {
		// get the pdf of the light having any radiance towards the sample direction
		lightPDF := path.light.PdfAt(ctx, path.interaction, sample.Direction)
		if lightPDF == 0 {
			// if there's no chance of the light sending any radiance towards the sampled direction
			// just return the direct lighting
			return direct
		}

		// balance the surface sample with the light pdf
		weight = powerHeuristic(1, sample.PDF, 1, lightPDF)
	}

	// determine if the sampled direction from the surface intersects
	// with the path's chosen light source
	ray := NewRay(path.interaction.over, sample.Direction)
	t.world.Intersect(ray, &t.intersections)

	hit, ok := t.intersections.Hit()
	if !ok {
		// TODO: account for infinite area lights
		return direct
	}

	// check to see if the path's light source is an area light
	// if it isn't, there isn't anything to contribute
	lo, ok := path.light.(LightObject)
	if !ok {
		return direct
	}

	// check if the path light is the object that was intersected
	// if it wasn't, there is no contribution
	if hit.Object() != lo.Object {
		return direct
	}

	// get the normal to determine which hemisphere the intersection occurred in
	normal := NormalAt(hit.Object(), ray.Position(hit.Time()), hit)
	lighting = lo.radiance(normal, sample.Direction)

	if !lighting.Eq(Black) {
		direct = direct.Add(sample.Color.Mul(lighting).Scale(weight / sample.PDF))
	}

	return direct
}

func (t *tracer) shadowed(light, surface Point) bool {
	var (
		path      = light.PathFrom(surface)
		distance  = path.Magnitude()
		direction = path.Normalize()
		ray       = NewRay(surface, direction)
	)

	t.world.Intersect(ray, &t.intersections)
	hit, ok := t.intersections.Hit()
	if !ok {
		return false
	}

	if !hit.Object().Shadows() {
		return false
	}

	if !hit.Object().Material().Emission().Eq(Black) {
		return false
	}

	return hit.Time() < distance
}

type path struct {
	ray         Ray   // direction of path
	next        Ray   // direction of next ray
	light       Light // light used to sample
	surface     Surface
	interaction Interaction
	depth       int // depth of path

	bsdf   *BSDF         // bsdf computed by path
	sample SurfaceSample // sample used to compute direction of next path

	emission     Color // contribution due to object emitting light
	contribution Color // direct contribution
	specular     bool  // if the bounce is caused by a specular reflection
	weight       Color
}
