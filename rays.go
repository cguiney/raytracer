package raytracer

import "fmt"

type Ray struct {
	origin    Point
	direction Vector
}

func NewRay(o Point, d Vector) Ray {
	return Ray{o, d}
}

func (r Ray) Origin() Point {
	return r.origin
}

func (r Ray) Direction() Vector {
	return r.direction
}

func (r Ray) Position(t Scalar) Point {
	return r.origin.TravelTo(r.direction.Mul(t))
}

func (r Ray) Eq(o Ray) bool {
	return r.Origin().Eq(o.Origin()) && r.Direction().Eq(o.Direction())
}

func (r Ray) Translate(t Translation) Ray {
	return NewRay(
		r.Origin().Translate(t),
		r.Direction(),
	)
}

func (r Ray) Scale(s Scale) Ray {
	return NewRay(
		r.Origin().Scale(s),
		r.Direction().Scale(s),
	)
}

func (r Ray) Shear(s Shear) Ray {
	return NewRay(
		r.Origin().Shear(s),
		r.Direction(),
	)
}

// Intersect transforms the ray into the object's space
// and computes all intersections with that object
//func (r Ray) Intersect(obj Object) Intersections {
//	// r = r.transform(obj.Transformation().Invert())
//	//return obj.Intersect(r)
//	return Intersect(r, obj)
//}

func (r Ray) transform(t Transformation) (x Ray) {
	x.origin = Point{t.m.Mulv(r.origin.tuple)}
	x.direction = Vector{t.m.Mulv(r.direction.tuple)}
	return
}

func (r Ray) String() string {
	return fmt.Sprintf("[%s -> %s]", r.origin, r.direction)
}