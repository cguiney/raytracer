package main

import (
	"log"
	"net/http"
	_ "net/http/pprof"
)

func httpServer() {
	app, admin := http.NewServeMux(), http.DefaultServeMux
	app.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, *out)
	})

	admin.HandleFunc("/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	admin.HandleFunc("/alive", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	go func() {
		if err := http.ListenAndServe(":8080", app); err != nil && err != http.ErrServerClosed {
			log.Fatal("http server failed to serve", err)
		}
	}()

	go func() {
		if err := http.ListenAndServe(":8081", admin); err != nil && err != http.ErrServerClosed {
			log.Fatal("http server failed to serve", err)
		}
	}()
}
