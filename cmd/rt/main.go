package main

import (
	"flag"
	"gitlab.com/cguiney/raytracer"
	scene "gitlab.com/cguiney/raytracer/internal/scene"
	"image/png"
	"io"
	"log"
	_ "net/http/pprof"
	"os"
	"path/filepath"
	"time"
)

var (
	out          = flag.String("o", "render.png", "output file")
	resourceroot = flag.String("r", "./resources", "resource root directory")
	samples      = flag.Int("s", 16384, "number of samples to collect")
	integrator   = flag.String("i", "mc", "Integrator to use.  Options are 'mc' for Monte Carlo Integrator or 'w' for Whitted")
)

func main() {
	flag.Parse()

	if len(flag.Args()) == 0 {
		log.Fatal("input yaml file not provided")
	}

	input := flag.Arg(0)

	var (
		env = scene.NewEnvironmentInDir(*resourceroot)
		s   *scene.Scene
		err error
	)

	scene.LoadPresets(env)

	switch ext := filepath.Ext(input); ext {
	case ".zip":
		s, err = scene.LoadBundle(env, input)
	default:
		s, err = scene.LoadFile(env, input)
	}

	if err != nil {
		log.Fatal("could not build scene:", err)
	}

	fh, err := os.Create(*out)
	if err != nil {
		log.Fatal("could not create output file:", err)
	}

	go httpServer()

	start := time.Now()

	var (
		render func()
		preview func() raytracer.Canvas
		final = make(chan raytracer.Canvas, 1)
	)
	switch *integrator {
	case "w":
		s.Optimize()
		t := raytracer.NewWhittedIntegrator(s.World(), s.Camera())
		render = func() {
			final <- t.Render(raytracer.DefaultContext())
		}

	case "mc":
		t := raytracer.NewPathIntegrator(*samples)
		render = func() {
			final <- s.Render(t)
		}
		preview = t.Preview

	default:
		log.Fatal("invalid integrator")
	}

	go render()

	func() {
		for {

			select {
			case render := <-final:
				if err := fh.Truncate(0); err != nil {
					log.Fatal("failed truncating file", err)
				}
				if _, err := fh.Seek(0, io.SeekStart); err != nil {
					log.Fatal("failed seeking to beginning of output", err)
				}

				if err := png.Encode(fh, render); err != nil {
					log.Fatal("failed rendering scene:", err)
				}
				return
			case <-time.After(10 * time.Second):
				p := preview()

				if err := fh.Truncate(0); err != nil {
					log.Fatal("failed truncating file", err)
				}
				if _, err := fh.Seek(0, io.SeekStart); err != nil {
					log.Fatal("failed seeking to beginning of output", err)
				}

				if err := png.Encode(fh, p); err != nil {
					log.Fatal("failed rendering scene:", err)
				}

				if err := fh.Sync(); err != nil {
					log.Fatal("failed syncing file:", err)
				}

				log.Println("wrote preview to", fh.Name())
				log.Println("intersections: ", raytracer.GetMeshTriangleIntersectTestCount())
			}
		}
	}()

	log.Println("completed rendering in ", time.Since(start))
	log.Println("triangle intersection tests: ", raytracer.GetTriangleIntersectTestCount())
	log.Println("mesh triangle intersection tests: ", raytracer.GetMeshTriangleIntersectTestCount())
}
