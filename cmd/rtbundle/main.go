package main

import (
	"flag"
	"gitlab.com/cguiney/raytracer/internal/scene"
	"log"
	"os"
)

var (
	resources = flag.String("r", "./resources", "resource root directory")
	out       = flag.String("o", "bundle.zip", "output bundle file")
)

func main() {
	flag.Parse()

	fh, err := os.Create(*out)
	if err != nil {
		log.Fatal("failed creating output file", err)
	}

	env := scene.NewEnvironmentInDir(*resources)
	if err := scene.Bundle(env, flag.Arg(0), fh); err != nil {
		log.Fatal("failed writing bundle:", err)
	}
	if err := fh.Close(); err != nil {
		log.Fatal("failed closing bundle:", err)
	}
}
