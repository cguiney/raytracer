package main

import (
	"log"
	"os"
)

func main() {
	switch action := os.Args[1]; action {
	case "accumulator":
		runAccumulator()
	case "tracer":
		runTracer()
	default:
		log.Fatal("unknown action ", action)
	}
}
