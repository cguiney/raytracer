package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/scene"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"
)

type Sampler interface {
	Sample(ctx raytracer.RenderCtx, world *raytracer.World, camera *raytracer.Camera) raytracer.Canvas
}

func trace(sampler Sampler, scene *scene.Scene, concurrency int, accumulator string) {
	var (
		wg  sync.WaitGroup
		ctx = raytracer.DefaultContext()
	)

	// be sure to optimize the scene
	scene.Optimize()

	// work work work
	for i := 0; i < concurrency; i++ {
		wg.Add(1)
		go func(ctx raytracer.RenderCtx, id int64) {
			defer wg.Done()
			ctx = raytracer.WithRNG(ctx, rand.New(rand.NewSource(time.Now().Unix()+id)))

			// sample until the accumulator says to stop
			for {
				s := sample{
					count:  1,
					canvas: sampler.Sample(ctx, scene.World(), scene.Camera()),
				}

				b, err := json.Marshal(&s)
				if err != nil {
					log.Fatal(err)
				}

				resp, err := http.Post("http://"+accumulator+"/", "application/json", bytes.NewReader(b))
				if err != nil {
					log.Fatal(err)
				}

				switch resp.StatusCode {
				case http.StatusBadRequest:
					log.Printf("sent invalid request. sample:\n%s", string(b))
				case http.StatusUnprocessableEntity:
					log.Println("accumulator indicated enough samples")
					return
				default:
					log.Println("sent sample to accumulator")
				}
			}
		}(ctx, int64(i))
	}
	wg.Wait()
}

func runTracer() {
	var (
		flags        = flag.NewFlagSet("rtworker tracer", flag.ExitOnError)
		accumulator  = flags.String("a", "localhost:8080", "http address of accumulator")
		resourceroot = flags.String("r", "./resources", "resource root directory")
	)

	flags.Parse(os.Args[2:])

	input := flags.Arg(0)
	var (
		env = scene.NewEnvironmentInDir(*resourceroot)
		s   *scene.Scene
		err error
	)

	scene.LoadPresets(env)

	switch ext := filepath.Ext(input); ext {
	case ".zip":
		s, err = scene.LoadBundle(env, input)
	default:
		s, err = scene.LoadFile(env, input)
	}

	if err != nil {
		log.Fatal("could not build scene:", err)
	}

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	log.Println("waiting for accumulator to report ready...")
	for {
		resp, err := client.Get("http://" + *accumulator + "/ready")
		if err != nil {
			log.Println("error ready checking accumulator:", err)
			time.Sleep(time.Second)
			continue
		}

		if resp.StatusCode != http.StatusOK {
			log.Println("accumulator ready check returned status:", resp.StatusCode)
			time.Sleep(time.Second)
			continue
		}

		break
	}

	log.Println("accumulator ready, starting trace")
	defer log.Println("tracing completed")
	trace(raytracer.NewPathIntegrator(0), s, runtime.NumCPU(), *accumulator)
}
