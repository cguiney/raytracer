package main

import (
	"encoding/json"
	"gitlab.com/cguiney/raytracer"
	"strings"
	"testing"
)

func TestMarshalJSON(t *testing.T) {
	x := sample{
		count:  123,
		canvas: raytracer.Canvas{},
	}

	b, err := json.Marshal(&x)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(b), `"count":123`) {
		t.Error("json did not contain expected content. got=", string(b))
	}
}
