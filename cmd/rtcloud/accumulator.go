package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"gitlab.com/cguiney/raytracer"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
)

type atomicpng struct {
	mu  sync.Mutex //protects png
	png *bytes.Buffer
}

func newAtomicPNG() *atomicpng {
	a := &atomicpng{}

	c := raytracer.NewCanvas(1, 1)
	c.DrawPixel(0, 0, raytracer.Black)
	a.Encode(c)

	return a
}

func (a *atomicpng) Encode(img image.Image) error {
	b := bytes.NewBuffer(nil)
	if err := png.Encode(b, img); err != nil {
		return err
	}

	a.mu.Lock()
	defer a.mu.Unlock()

	a.png = b
	return nil
}

func (a *atomicpng) Reader() io.Reader {
	a.mu.Lock()
	defer a.mu.Unlock()

	return bytes.NewReader(a.png.Bytes())
}

func httpServer(samples chan sample, p *atomicpng, n int) {
	app, admin := http.NewServeMux(), http.DefaultServeMux

	var (
		mu       sync.Mutex // protects received, samples
		received int        // count of received samples
	)

	app.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			b, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Println("error reading sample:", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			s := sample{}
			if err := json.Unmarshal(b, &s); err != nil {
				log.Println("error unmarshalling sample:", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			if s.count == 0 {
				log.Printf("invalid sample: sample count is zero.  input:\n%s\n", string(b))
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			mu.Lock()
			defer mu.Unlock()
			received += s.count

			log.Printf("received %d samples; progress %d/%d; %.5f%% complete", s.count,
				received, n, float64(received)/float64(n))

			if received >= n {
				w.WriteHeader(http.StatusUnprocessableEntity)
				if samples != nil {
					close(samples)
				}
				samples = nil
				return
			}

			samples <- s
		default:
			w.WriteHeader(http.StatusOK)
			w.Header().Set("Content-Type", "image/png")

			if _, err := io.Copy(w, p.Reader()); err != nil {
				log.Println("failed writing image to http response:", err)
			}
		}
	})

	admin.HandleFunc("/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	admin.HandleFunc("/alive", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	go func() {
		if err := http.ListenAndServe(":8080", app); err != nil && err != http.ErrServerClosed {
			log.Fatal("http server failed to serve", err)
		}
	}()

	go func() {
		if err := http.ListenAndServe(":8081", admin); err != nil && err != http.ErrServerClosed {
			log.Fatal("http server failed to serve", err)
		}
	}()
}

func accumulate(samples <-chan sample, renders chan<- sample) {
	var (
		totals sample
	)

	for s := range samples {
		// on the first s, just take it as the initial value
		if totals.count == 0 {
			totals = s
			continue
		}

		for y := 0; y < totals.canvas.Height(); y++ {
			for x := 0; x < totals.canvas.Width(); x++ {
				sum := totals.canvas.PixelAt(x, y).Add(s.canvas.PixelAt(x, y))
				totals.canvas.DrawPixel(x, y, sum)
			}
		}
		totals.count += s.count

		current := totals.canvas.Clone()
		for y := 0; y < totals.canvas.Height(); y++ {
			for x := 0; x < totals.canvas.Width(); x++ {
				avg := totals.canvas.PixelAt(x, y).Scale(1 / raytracer.Scalar(totals.count))
				current.DrawPixel(x, y, avg)
			}
		}

		renders <- sample{totals.count, current}
	}
}

func write(renders <-chan sample, p *atomicpng) {
	for render := range renders {
		if err := p.Encode(render.canvas); err != nil {
			log.Println("failed encoding png:", err)
		}
	}
}

func runAccumulator() {
	var (
		flags    = flag.NewFlagSet("rtworker accumulator", flag.ExitOnError)
		workers  = flags.Int("workers", 8, "estimated number of workers. used for setting buffer values")
		nsamples = flags.Int("samples", 1024, "number of samples to accumulate")
	)

	flags.Parse(os.Args[2:])

	var (
		samples = make(chan sample, 2**workers)
		renders = make(chan sample, 1)
		p       = newAtomicPNG()
		wg      sync.WaitGroup
	)

	log.Println("collecting", *nsamples, "samples")

	wg.Add(1)
	go func() {
		defer wg.Done()
		accumulate(samples, renders)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		write(renders, p)
	}()

	httpServer(samples, p, *nsamples)

	wg.Wait()
}
