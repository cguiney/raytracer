package main

import (
	"encoding/json"
	"errors"
	"gitlab.com/cguiney/raytracer"
)

type sample struct {
	count  int
	canvas raytracer.Canvas
}

func (s *sample) MarshalJSON() ([]byte, error) {
	type sampleJSON struct {
		Count  int                     `json:"count"`
		Width  int                     `json:"width"`
		Height int                     `json:"height"`
		Pixels [][][3]raytracer.Scalar `json:"pixels"`
	}

	var msg sampleJSON
	msg.Count = s.count
	msg.Width = s.canvas.Width()
	msg.Height = s.canvas.Height()

	msg.Pixels = make([][][3]raytracer.Scalar, s.canvas.Height())
	for y := 0; y < s.canvas.Height(); y++ {
		msg.Pixels[y] = make([][3]raytracer.Scalar, s.canvas.Width())
		for x := 0; x < s.canvas.Width(); x++ {
			c := s.canvas.PixelAt(x, y)
			msg.Pixels[y][x] = [3]raytracer.Scalar{c.Red(), c.Green(), c.Blue()}
		}
	}

	return json.Marshal(msg)
}

func (s *sample) UnmarshalJSON(b []byte) error {
	type sampleJSON struct {
		Count  int                     `json:"count"`
		Width  int                     `json:"width"`
		Height int                     `json:"height"`
		Pixels [][][3]raytracer.Scalar `json:"pixels"`
	}

	var msg sampleJSON
	if err := json.Unmarshal(b, &msg); err != nil {
		return err
	}

	s.count = msg.Count
	s.canvas = raytracer.NewCanvas(msg.Width, msg.Height)

	// sanity check
	if len(msg.Pixels) != msg.Height {
		return errors.New("sample height does not match pixel rows")
	}

	for y := 0; y < s.canvas.Height(); y++ {
		if len(msg.Pixels[y]) != msg.Width {
			return errors.New("sample width does not match pixel columns")
		}

		for x := 0; x < s.canvas.Width(); x++ {
			c := raytracer.NewColor(
				msg.Pixels[y][x][0],
				msg.Pixels[y][x][1],
				msg.Pixels[y][x][2],
			)
			s.canvas.DrawPixel(x, y, c)
		}
	}

	return nil
}
