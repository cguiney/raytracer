package main

import (
	"flag"
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"image"
	"image/png"
	"os"
	"time"
)

func main() {
	flag.Parse()

	start := time.Now()

	//noise := raytracer.NewFBM(1141, 0.5, 8)
	noise := raytracer.NewPerlin(1141)

	img := image.NewGray(image.Rect(0, 0, 800, 600))

	const scale = 5
	for y := 0; y < img.Bounds().Max.Y; y++ {
		for x := 0; x < img.Bounds().Max.X; x++ {

			var (
				s = raytracer.Scalar(x) / raytracer.Scalar(img.Bounds().Max.X) * scale
				t = raytracer.Scalar(y) / raytracer.Scalar(img.Bounds().Max.Y) * scale
			)

			p := raytracer.NewPoint(s, t, 0)
			//p = p.Mul(3)
			noise := noise.NoiseAt(p)
			c := raytracer.NewGray(noise)
			img.Set(x, y, c.RGBA())
		}
	}

	fh, err := os.Create(flag.Arg(0))
	if err != nil {
		panic(err)
	}

	if err := png.Encode(fh, img); err != nil {
		panic(err)
	}

	fmt.Println("finished in", time.Since(start))
}
