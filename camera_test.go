package raytracer

import (
	"math"
	"testing"
)

func TestCamera(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		c := NewCamera(160, 120, Scalar(math.Pi/2))

		if want, have := 160, c.Width(); have != want {
			t.Errorf("incorrect width for camera. have=%d want=%d", want, have)
		}
		if want, have := 120, c.Height(); have != want {
			t.Errorf("incorrect height for camera. have=%d want=%d", want, have)
		}
	})

	t.Run("pixel size", func(t *testing.T) {
		type pixelSizeTest struct {
			name string
			w, h int
			want Scalar
		}

		tests := []pixelSizeTest{
			{
				name: "horizontal canvas",
				w:    200,
				h:    125,
				want: 0.01,
			},
			{
				name: "vertical canvas",
				w:    125,
				h:    200,
				want: 0.01,
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				c := NewCamera(tt.w, tt.h, math.Pi/2)
				if have := c.PixelSize(); !have.Eq(tt.want) {
					t.Errorf("incorrect pixel size. have=%f want=%f", have, tt.want)
				}
			})
		}
	})

	t.Run("ray generation", func(t *testing.T) {
		type rayGenerationTest struct {
			name          string
			c             *Camera
			x, y          int
			wantOrigin    Point
			wantDirection Vector
		}

		var sq2 = Scalar(math.Sqrt2 / 2)
		tests := []rayGenerationTest{
			{
				name:          "center of canvas",
				c:             NewCamera(201, 101, math.Pi/2),
				x:             100,
				y:             50,
				wantOrigin:    NewPoint(0, 0, 0),
				wantDirection: NewVector(0, 0, -1),
			},
			{
				name:          "corner of canvas",
				c:             NewCamera(201, 101, math.Pi/2),
				x:             0,
				y:             0,
				wantOrigin:    NewPoint(0, 0, 0),
				wantDirection: NewVector(0.66519, 0.33259, -0.66851),
			},
			{
				name: "camera is transformed",
				c: func() *Camera {
					c := NewCamera(201, 101, math.Pi/2)
					c.Rotate(NewRotationY(math.Pi / 4))
					c.Translate(NewTranslation(0, -2, 5))
					return c
				}(),
				x:             100,
				y:             50,
				wantOrigin:    NewPoint(0, 2, -5),
				wantDirection: NewVector(sq2, 0, -sq2),
			},
		}

		for _, tt := range tests {
			tt := tt
			t.Run(tt.name, func(t *testing.T) {
				r := tt.c.RayForPixel(Scalar(tt.x), Scalar(tt.y))
				if have := r.Origin(); !have.Eq(tt.wantOrigin) {
					t.Errorf("incorrect origin. have=%v want=%v", have, tt.wantOrigin)
				}
				if have := r.Direction(); !have.Eq(tt.wantDirection) {
					t.Errorf("incorrect direction. have=%v want=%v", have, tt.wantDirection)
				}
			})
		}
	})

	t.Run("rendering world", func(t *testing.T) {
		var (
			from = NewPoint(0, 0, -5)
			to   = NewPoint(0, 0, 0)
			up   = NewVector(0, 1, 0)

			c = func() *Camera {
				c := NewCamera(11, 11, math.Pi/2)
				c.ViewTransform(NewViewTransform(from, to, up))
				return c
			}()

			w          = DefaultWorld()
			integrator = NewWhittedIntegrator(w, c)
			image      = integrator.Render(DefaultContext())
			pixel      = image.PixelAt(5, 5)
		)

		if want := NewColor(0.38066, 0.47583, 0.2855); !pixel.Eq(want) {
			t.Errorf("incorrect color for pixel. have=%v want=%v", pixel, want)
		}
	})
}

var c *Camera

func BenchmarkRender(b *testing.B) {
	world, camera := NewSampleScene()
	integrator := NewWhittedIntegrator(world, camera)
	for n := 0; n < b.N; n++ {
		integrator.Render(DefaultContext())
		c = camera
	}
}
