package raytracer

import (
	"fmt"
	"math"
	"sort"
	"testing"
)

func TestGroups(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		g := NewGroup()

		if *g.Transformation().m != IdentityMatrix() {
			t.Errorf("expected default transform to be identity matrix")
		}

		if len(g.Objects()) != 0 {
			t.Errorf("expected object set to start empty")
		}
	})

	t.Run("adding child to group", func(t *testing.T) {
		g := NewGroup()
		s := NewSphere()

		g.AddObject(s)

		if s.Parent() != g {
			t.Errorf("expected AddObject to set parent on child. want=%+v have=%+v", g, s.Parent())
		}
	})

	t.Run("intersecting ray with empty group", func(t *testing.T) {
		g := NewGroup()

		r := NewRay(
			NewPoint(0, 0, 0),
			NewVector(0, 0, 1),
		)

		xs := Intersections{}
		g.Intersect(r, &xs)

		if have := len(xs); have != 0 {
			t.Errorf("expected no intersections from empty group. got=%d", have)
		}
	})

	t.Run("intersecting ray with nonempty group", func(t *testing.T) {
		g := NewGroup()

		s1 := NewSphere()
		s2 := NewSphere()
		s3 := NewSphere()

		s2.Translate(NewTranslation(0, 0, -3))
		s3.Translate(NewTranslation(5, 0, 0))

		g.AddObject(s1)
		g.AddObject(s2)
		g.AddObject(s3)

		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))

		xs := Intersections{}

		g.Intersect(r, &xs)

		sort.Sort(&xs)

		if len(xs) != 4 {
			t.Fatalf("expected 4 intersections. got=%d", len(xs))
		}

		want := []Object{s2, s2, s1, s1}

		for i, x := range xs {
			if x.Object() != want[i] {
				t.Errorf("incorrect object for intersection %d. have=%+v want=%+v", i, x.Object(), want[i])
			}
		}
	})

	t.Run("intersecting transformed group", func(t *testing.T) {
		g := NewGroup()
		g.Scale(NewScale(2, 2, 2))

		s := NewSphere()
		s.Translate(NewTranslation(5, 0, 0))

		g.AddObject(s)

		r := NewRay(NewPoint(10, 0, -10), NewVector(0, 0, 1))

		xs := Intersections{}
		Intersect(r, g, &xs)

		if len(xs) != 2 {
			t.Errorf("expected 4 intersections. got=%d", len(xs))
		}
	})

	t.Run("group has bounding box that contains children", func(t *testing.T) {
		s := NewSphere()
		s.Translate(NewTranslation(2, 5, -3))
		s.Scale(NewScale(2, 2, 2))

		c := NewCylinder()
		c.Truncate(-2, 2)
		c.Translate(NewTranslation(-4, -1, 4))
		c.Scale(NewScale(0.5, 1, 0.5))

		g := NewGroup()
		g.AddObject(s)
		g.AddObject(c)

		min, max := g.Bounds()
		if want := NewPoint(-4.5, -3, -5); !min.Eq(want) {
			t.Errorf("incorrect lower bound. have=%v want=%v", min, want)
		}
		if want := NewPoint(4, 7, 4.5); !max.Eq(want) {
			t.Errorf("incorrect upper bound. have=%v want=%v", max, want)
		}
	})

	t.Run("intersecting ray+group doesnt test children if box is missed", func(t *testing.T) {
		g := NewGroup()
		s := NewTestShape()
		g.AddObject(s)

		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 1, 0))

		g.Intersect(r, nil)

		if s.ray != nil {
			t.Errorf("expected test shape not to have intersection tested")
		}
	})

	t.Run("intersecting ray+group doesnt test children if box is missed", func(t *testing.T) {
		g := NewGroup()
		s := NewTestShape()
		g.AddObject(s)

		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 1, 0))

		g.Intersect(r, nil)

		if s.ray != nil {
			t.Errorf("expected test shape not to have intersection tested")
		}
	})

	t.Run("intersecting ray+group tests children if box is hit", func(t *testing.T) {
		g := NewGroup()
		s := NewTestShape()
		g.AddObject(s)

		r := NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1))

		xs := Intersections{}
		g.Intersect(r, &xs)

		if s.ray == nil {
			t.Errorf("expected test shape to have intersection tested")
		}
	})
}

func TestBoundingBox(t *testing.T) {
	t.Run("includes point", func(t *testing.T) {

		b := NewBoundingBox()
		b.Include(NewPoint(-5, 2, 0))
		b.Include(NewPoint(7, 0, -3))

		if have, want := b.Lower(), NewPoint(-5, 0, -3); !have.Eq(want) {
			t.Errorf("incorrect lower bound. have=%v want=%v", have, want)
		}

		if have, want := b.Upper(), NewPoint(7, 2, 0); !have.Eq(want) {
			t.Errorf("incorrect lower bound. have=%v want=%v", have, want)
		}
	})

	t.Run("adding one box to another", func(t *testing.T) {
		b1 := NewBoundingBox()
		b1.Include(NewPoint(-5, -2, 0))
		b1.Include(NewPoint(7, 4, 4))

		b2 := NewBoundingBox()
		b2.Include(NewPoint(8, -7, -2))
		b2.Include(NewPoint(14, 2, 8))

		b1.Add(&b2)

		if want := NewPoint(-5, -7, -2); !b1.Lower().Eq(want) {
			t.Errorf("incorrect lower bound. have=%v want=%v", b1.Lower(), want)
		}

		if want := NewPoint(14, 4, 8); !b1.Upper().Eq(want) {
			t.Errorf("incorrect upper bound. have=%v want=%v", b1.Upper(), want)
		}
	})

	t.Run("check to see if box contains point", func(t *testing.T) {
		b := NewBoundingBox()
		b.Include(NewPoint(5, -2, 0))
		b.Include(NewPoint(11, 4, 7))

		type includesTest struct {
			p         Point
			contained bool
		}

		tests := []includesTest{
			{NewPoint(5, -2, 0), true},
			{NewPoint(11, 4, 7), true},
			{NewPoint(8, 1, 3), true},
			{NewPoint(3, 0, 3), false},
			{NewPoint(8, -4, 3), false},
			{NewPoint(8, 1, -1), false},
			{NewPoint(13, 1, 3), false},
			{NewPoint(8, 5, 3), false},
			{NewPoint(8, 1, 8), false},
		}

		for _, tt := range tests {
			if have := b.Includes(tt.p); have != tt.contained {
				t.Errorf("incorrect result for includes for point %v. have=%t want=%t", tt.p, have, tt.contained)
			}
		}
	})

	t.Run("check to see if box contains box", func(t *testing.T) {

		type containsTest struct {
			min, max  Point
			contianed bool
		}

		tests := []containsTest{
			{NewPoint(5, -2, 0), NewPoint(11, 4, 7), true},
			{NewPoint(6, -1, 1), NewPoint(10, 3, 6), true},
			{NewPoint(4, -3, -1), NewPoint(10, 3, 6), false},
			{NewPoint(6, -1, 1), NewPoint(12, 5, 8), false},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				b := NewBoundingBox()
				b.Update(NewPoint(5, -2, 0), NewPoint(11, 4, 7))

				b2 := NewBoundingBox()
				b2.Update(tt.min, tt.max)

				if have := b.Contains(&b2); have != tt.contianed {
					t.Errorf("incorrect result of contains. have=%t want=%t", have, tt.contianed)
				}
			})
		}
	})

	t.Run("transforming a bounding box", func(t *testing.T) {
		b := NewBoundedBox(NewPoint(-1, - 1, -1), NewPoint(1, 1, 1))
		xform := NewTransformation()
		xform.RotateX(math.Pi / 4)
		xform.RotateY(math.Pi / 4)
		b.Transform(xform)

		if want := NewPoint(-1.414213, -1.707106, -1.707106); !b.Lower().Eq(want) {
			t.Errorf("incorrect lower bound. have=%s want=%s", b.Lower(), want)
		}
		if want := NewPoint(1.414213, 1.707106, 1.707106); !b.Upper().Eq(want) {
			t.Errorf("incorrect upper bound. have=%s want=%s", b.Upper(), want)
		}
	})

	t.Run("querying bounding box in parent space", func(t *testing.T) {
		s := NewSphere()
		s.Translate(NewTranslation(1, -3, 5))
		s.Scale(NewScale(0.5, 2, 4))

		b := NewParentSpaceBoundingBox(s)
		if want := NewPoint(0.5, -5, 1); !b.Lower().Eq(want) {
			t.Errorf("incorrect lower bound. have=%v want=%v", b.Lower(), want)
		}

		if want := NewPoint(1.5, -1, 9); !b.Upper().Eq(want) {
			t.Errorf("incorrect upper bound. have=%v want=%v", b.Upper(), want)
		}
	})

	t.Run("intersecting ray", func(t *testing.T) {
		type intersectTest struct {
			p    Point
			v    Vector
			want bool
		}

		tests := []intersectTest{
			{NewPoint(5, 0.5, 0), NewVector(-1, 0, 0), true},
			{NewPoint(-5, 0.5, 0), NewVector(1, 0, 0), true},
			{NewPoint(0.5, 5, 0), NewVector(0, -1, 0), true},
			{NewPoint(0.5, -5, 0), NewVector(0, 1, 0), true},
			{NewPoint(0.5, 0, 5), NewVector(0, 0, -1), true},
			{NewPoint(0.5, 0, -5), NewVector(0, 0, 1), true},
			{NewPoint(0, 0.5, 0), NewVector(0, 0, 1), true},
			{NewPoint(-2, 0, 0), NewVector(2, 4, 6), false},
			{NewPoint(0, -2, 0), NewVector(6, 2, 4), false},
			{NewPoint(0, 0, -2), NewVector(4, 6, 2), false},
			{NewPoint(2, 0, 2), NewVector(0, 0, -1), false},
			{NewPoint(0, 2, 2), NewVector(0, -1, 0), false},
			{NewPoint(2, 2, 0), NewVector(-1, 0, 0), false},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				b := NewBoundedBox(NewPoint(-1, -1, -1), NewPoint(1, 1, 1))
				r := NewRay(tt.p, tt.v.Normalize())

				if have := b.Intersects(r); have != tt.want {
					t.Errorf("incorrect result of intersect. have=%t want=%t. p=%v v=%v", have, tt.want, tt.p, tt.v)
				}
			})
		}
	})

	t.Run("intersecting ray with non-cubic bounding box", func(t *testing.T) {
		type intersectTest struct {
			p    Point
			v    Vector
			want bool
		}

		tests := []intersectTest{
			{NewPoint(15, 1, 2), NewVector(-1, 0, 0), true},
			{NewPoint(-5, -1, 4), NewVector(1, 0, 0), true},
			{NewPoint(7, 6, 5), NewVector(0, -1, 0), true},
			{NewPoint(9, -5, 6), NewVector(0, 1, 0), true},
			{NewPoint(8, 2, 12), NewVector(0, 0, -1), true},
			{NewPoint(6, 0, -5), NewVector(0, 0, 1), true},
			{NewPoint(8, 1, 3.5), NewVector(0, 0, 1), true},
			{NewPoint(9, -1, -8), NewVector(2, 4, 6), false},
			{NewPoint(8, 3, -4), NewVector(6, 2, 4), false},
			{NewPoint(9, -1, -2), NewVector(4, 6, 2), false},
			{NewPoint(4, 0, 9), NewVector(0, 0, -1), false},
			{NewPoint(8, 6, -1), NewVector(0, -1, 0), false},
			{NewPoint(12, 5, 4), NewVector(-1, 0, 0), false},
		}

		for i, tt := range tests {
			tt := tt
			t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
				b := NewBoundedBox(NewPoint(5, -2, 0), NewPoint(11, 4, 7))
				r := NewRay(tt.p, tt.v.Normalize())

				if have := b.Intersects(r); have != tt.want {
					t.Errorf("incorrect result of intersect. have=%t want=%t. p=%v v=%v", have, tt.want, tt.p, tt.v)
				}
			})
		}
	})

	t.Run("splitting a bounding box", func(t *testing.T) {
		type splitTest struct {
			name  string
			in    BoundingBox
			left  BoundingBox
			right BoundingBox
		}

		tests := []splitTest{
			{
				name:  "splitting perfect cube",
				in:    NewBoundedBox(NewPoint(-1, -4, -5), NewPoint(9, 6, 5)),
				left:  NewBoundedBox(NewPoint(-1, -4, -5), NewPoint(4, 6, 5)),
				right: NewBoundedBox(NewPoint(4, -4, -5), NewPoint(9, 6, 5)),
			},
			{
				name:  "splitting x-wide box",
				in:    NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(9, 5.5, 3)),
				left:  NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(4, 5.5, 3)),
				right: NewBoundedBox(NewPoint(4, -2, -3), NewPoint(9, 5.5, 3)),
			},
			{
				name:  "splitting y-wide box",
				in:    NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(5, 8, 3)),
				left:  NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(5, 3, 3)),
				right: NewBoundedBox(NewPoint(-1, 3, -3), NewPoint(5, 8, 3)),
			},

			{
				name:  "splitting z-wide box",
				in:    NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(5, 3, 7)),
				left:  NewBoundedBox(NewPoint(-1, -2, -3), NewPoint(5, 3, 2)),
				right: NewBoundedBox(NewPoint(-1, -2, 2), NewPoint(5, 3, 7)),
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				left, right, _ := tt.in.Split()
				if want := tt.left.Lower(); !left.Lower().Eq(want) {
					t.Errorf("incorrect lower bound for left box. have=%v want=%v", left.Lower(), want)
				}

				if want := tt.left.Upper(); !left.Upper().Eq(want) {
					t.Errorf("incorrect upper bound for left box. have=%v want=%v", left.Upper(), want)
				}

				if want := tt.right.Lower(); !right.Lower().Eq(want) {
					t.Errorf("incorrect lower bound for right box. have=%v want=%v", right.Lower(), want)
				}

				if want := tt.right.Upper(); !right.Upper().Eq(want) {
					t.Errorf("incorrect upper bound for right box. have=%v want=%v", right.Upper(), want)
				}
			})
		}
	})

	t.Run("bounding box membership", func(t *testing.T) {
		t.SkipNow()
		box := NewBoundedBox(
			NewPoint(-0.0004, 0.6679, -0.854),
			NewPoint(0.6489, 1.0754, 0.7756),
		)

		tri := NewBoundedBox(
			NewPoint(0.4122, 0.9809, -0.0685),
			NewPoint(0.4283, 0.9936, 0.0032))

		if !box.Contains(&tri) {
			t.Fatal("expected box to contain tri")
		}

		left, right, _ := box.Split()

		if !left.Contains(&tri) {
			t.Logf("left :\n\t%v -> %v\n", left.min, left.max)
			t.Logf("right:\n\t%v -> %v\n", right.min, right.max)
			t.Fatal("expected either left or right to contain tri")
		}

	})
}
