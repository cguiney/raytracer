package raytracer

import (
	"fmt"
	"math/rand"
	"sort"
	"sync"
	"sync/atomic"
)

var (
	// compile time assertions
	_ Object      = new(Mesh)
	_ Object      = new(MeshGroup)
	_ Object      = new(MeshTriangle)
	_ ObjectGraph = new(Mesh)
	_ ObjectGraph = new(MeshGroup)
)

type UV struct {
	U, V Scalar
}

func (uv UV) Add(o UV) UV {
	return UV{uv.U + o.U, uv.V + o.V}
}

func (uv UV) Sub(o UV) UV {
	return UV{uv.U - o.U, uv.V - o.V}
}

func (uv UV) Scale(s Scalar) UV {
	return UV{uv.U * s, uv.V * s}
}

type Mesh struct {
	normals  []Vector
	vertices []Point
	textures []UV

	edges  [][2]Vector
	bounds []BoundingBox

	// triangle number -> 3 vertex indices
	vertexIndices  [][3]int
	normalIndices  [][3]int
	textureIndices [][3]int

	smoothing []bool

	groups []MeshGroup

	index meshNode

	parent Object
	box    BoundingBox

	shape
}

func (m *Mesh) Vertex(n int) Point {
	return m.vertices[n]
}

func (m *Mesh) Normal(n int) Vector {
	return m.normals[n]
}

func (m *Mesh) UV(n int) UV {
	return m.textures[n]
}

func (m *Mesh) Groups() []MeshGroup {
	return m.groups
}

func (m *Mesh) Children() (objects []Object) {
	objects = make([]Object, 0, len(m.groups))

	for i := range m.groups {
		objects = append(objects, &m.groups[i])
	}

	return
}

func (m *Mesh) Bounds() (Point, Point) {
	return m.index.bounds.Bounds()
}

func (m *Mesh) NormalAt(Point, Intersection) Vector {
	panic("NormalAt() called on mesh")
}

func (m *Mesh) SurfaceAt(Interaction) Surface {
	panic("SurfaceAt() called on mesh")
}

func (m *Mesh) Intersect(r Ray, intersections *Intersections) {
	//if !material.box.Intersects(r) {
	//	return
	//}

	m.index.Intersect(r, intersections)

	//for _, g := range material.groups {
	//	// groups don't allow for individual transformations
	//	// so in theory, can do g.Intersect(r) directly
	//	g.Intersect(r, intersections)
	//}
}

func (m *Mesh) Triangles() int {
	return len(m.vertexIndices)
}

type meshNode struct {
	bounds    BoundingBox
	nodes     []meshNode
	triangles []MeshTriangle
}

const indexThreshold = 4

func indexSpaceSplit(m *meshNode, depth int) {
	if len(m.triangles) <= indexThreshold {
		return
	}

	var left, center, right meshNode

	leftb, rightb, _ := m.bounds.Split()
	left.bounds = NewBoundingBox()
	right.bounds = NewBoundingBox()
	center.bounds = NewBoundingBox()

	for _, t := range m.triangles {
		var (
			// box     = NewParentSpaceBoundingBox(&t)
			inLeft  = leftb.Contains(&t)
			inRight = rightb.Contains(&t)
		)

		if inLeft {
			left.bounds.Add(&t)
			left.triangles = append(left.triangles, t)
		} else if inRight {
			right.bounds.Add(&t)
			right.triangles = append(right.triangles, t)
		} else {
			center.bounds.Add(&t)
			center.triangles = append(center.triangles, t)
		}
	}

	// if there wasn't any distribution farther down the tree
	if len(left.triangles) == len(m.triangles) ||
		len(right.triangles) == len(m.triangles) ||
		len(center.triangles) == len(m.triangles) {

		indexSortSplit(&left, depth+1)
		indexSortSplit(&right, depth+1)
		indexSortSplit(&center, depth+1)
	}

	indexSpaceSplit(&left, depth+1)
	indexSpaceSplit(&right, depth+1)
	indexSpaceSplit(&center, depth+1)

	m.triangles = nil
	m.nodes = append(m.nodes, left, right, center)
}

func indexSortSplit(m *meshNode, depth int) {
	if len(m.triangles) <= indexThreshold {
		return
	}

	var left, right meshNode
	axis := Axis(rand.Intn(3))
	sort.Slice(m.triangles, func(i, j int) bool {
		ibox := NewBoundedBox(m.triangles[i].Bounds())
		jbox := NewBoundedBox(m.triangles[j].Bounds())

		switch axis {
		case X:
			return ibox.min.X()-jbox.min.X() < 0
		case Y:
			return ibox.min.Y()-jbox.min.Z() < 0
		case Z:
			return ibox.min.Z()-jbox.min.Z() < 0
		default:
			panic("unknown axis")
		}
	})

	for _, t := range m.triangles[:len(m.triangles)/2] {
		left.bounds.Add(&t)
		left.triangles = append(left.triangles, t)
	}

	for _, t := range m.triangles[len(m.triangles)/2:] {
		right.bounds.Add(&t)
		right.triangles = append(right.triangles, t)
	}

	indexSortSplit(&left, depth+1)
	indexSortSplit(&right, depth+1)

	m.triangles = nil
	m.nodes = append(m.nodes, left, right)
}

func indexSplitEqual(m *meshNode, depth int) {
	if len(m.triangles) <= 2 {
		return
	}

	dim := m.bounds.MaxExtent()
	sort.Slice(m.triangles, func(i, j int) bool {
		icentroid := Centroid(m.triangles[i].Bounds()).Dimension(dim)
		jcentroid := Centroid(m.triangles[j].Bounds()).Dimension(dim)
		return icentroid < jcentroid
	})

	mid := len(m.triangles) / 2

	var left, right meshNode

	left.triangles = m.triangles[:mid]
	for _, t := range m.triangles[:mid] {
		left.bounds.Update(t.Bounds())
	}

	right.triangles = m.triangles[mid:]
	for _, t := range m.triangles[mid:] {
		right.bounds.Update(t.Bounds())
	}

	indexSplitEqual(&left, depth+1)
	indexSplitEqual(&right, depth+1)

	m.triangles = nil
	m.nodes = append(m.nodes, left, right)
}

const maxObjectsPerNode = 2

func indexSAH(m *meshNode, depth int) {
	type bucket struct {
		box   BoundingBox
		count int
	}

	if len(m.triangles) <= indexThreshold {
		indexSplitEqual(m, depth)
		return
	}

	const nBuckets = 12
	buckets := [nBuckets]bucket{}

	bounds := NewBoundingBox()
	for _, t := range m.triangles {
		bounds.Include(Centroid(t.Bounds()))
	}

	dim := bounds.MaxExtent()

	if bounds.Lower().Dimension(dim) == bounds.Upper().Dimension(dim) {
		return
	}

	for _, t := range m.triangles {
		centroid := Centroid(t.Bounds())
		offset := bounds.Offset(centroid)
		dim := offset.Dimension(dim)
		bucket := int(nBuckets * dim)
		if bucket == nBuckets {
			bucket = nBuckets - 1
		}

		buckets[bucket].box.Update(t.Bounds())
		buckets[bucket].count++
	}

	costs := [nBuckets - 1]Scalar{}
	for i := range costs {
		var lower, upper bucket

		for j := 0; j <= i; j++ {
			lower.box.Update(buckets[j].box.Bounds())
			lower.count += buckets[j].count
		}

		for j := i + 1; j < nBuckets; j++ {
			upper.box.Update(buckets[j].box.Bounds())
			upper.count += buckets[j].count
		}

		costs[i] = 0.125 +
			(Scalar(lower.count)*lower.box.Area()+
				(Scalar(upper.count)*upper.box.Area()))/bounds.Area()
	}

	var (
		minCost       = costs[0]
		minCostBucket = 0
	)
	for i := 1; i < nBuckets-1; i++ {
		if costs[i] < minCost {
			minCost, minCostBucket = costs[i], i
		}
	}

	leafCost := Scalar(len(m.triangles))
	if !(len(m.triangles) > maxObjectsPerNode || minCost < leafCost) {
		return
	}

	// it's worth it to make a leaf node
	// sort by bucket
	// find by midpoint
	// split
	sort.Slice(m.triangles, func(i, j int) bool {
		ibucket := int(nBuckets * bounds.Offset(Centroid(m.triangles[i].Bounds())).Dimension(dim))
		if ibucket == nBuckets {
			ibucket = nBuckets - 1
		}

		jbucket := int(nBuckets * bounds.Offset(Centroid(m.triangles[j].Bounds())).Dimension(dim))
		if jbucket == nBuckets {
			jbucket = nBuckets - 1
		}

		return ibucket < jbucket
	})

	mid := sort.Search(len(m.triangles), func(i int) bool {
		bucket := int(nBuckets * bounds.Offset(Centroid(m.triangles[i].Bounds())).Dimension(dim))
		if bucket == nBuckets {
			bucket = nBuckets - 1
		}

		return bucket > minCostBucket
	})

	if mid >= len(m.triangles) {
		panic("midpoint not found")
	}

	var left, right meshNode

	left.triangles = m.triangles[:mid]
	for _, t := range left.triangles {
		left.bounds.Update(t.Bounds())
	}

	right.triangles = m.triangles[mid:]
	for _, t := range right.triangles {
		right.bounds.Update(t.Bounds())
	}

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		indexSAH(&left, depth+1)
	}()

	go func() {
		defer wg.Done()
		indexSAH(&right, depth+1)
	}()

	wg.Wait()

	m.triangles = nil
	m.nodes = append(m.nodes, left, right)
}

func (m *meshNode) index() {
	index := indexSAH
	//index := indexSpaceSplit
	//index := indexSortSplit
	index(m, 0)
}

func (m *meshNode) Intersect(r Ray, intersections *Intersections) {
	if !m.bounds.Intersects(r) {
		return
	}

	for _, node := range m.nodes {
		node.Intersect(r, intersections)
	}

	for i := range m.triangles {
		m.triangles[i].Intersect(r, intersections)
	}
}

type MeshGroup struct {
	name     string
	mesh     *Mesh
	material Material

	meshNode
}

func (g *MeshGroup) Name() string {
	return g.name
}

func (g *MeshGroup) NormalAt(Point, Intersection) Vector {
	panic("logic error: NormalAt called on group")
}

func (g *MeshGroup) Transformation() Transformation {
	return NewTransformation()
}

func (g *MeshGroup) Bounds() (Point, Point) {
	return g.bounds.Bounds()
}

func (g *MeshGroup) SetParent(Object) {
	panic("logic error: SetParent called on MeshGroup")
}

func (g *MeshGroup) Parent() Object {
	return g.mesh
}

func (g *MeshGroup) SurfaceAt(Interaction) Surface {
	panic("SurfaceAt() called on MeshGroup")
}

func (g *MeshGroup) Material() Material {
	if g.material == nil {
		return g.mesh.Material()
	}
	return g.material
}

func (g *MeshGroup) Shadows() bool {
	return g.mesh.Shadows()
}

func (g *MeshGroup) Children() (objects []Object) {
	var walk func(node meshNode)
	walk = func(node meshNode) {
		for _, node := range node.nodes {
			walk(node)
		}
		for i := range node.triangles {
			objects = append(objects, &node.triangles[i])
		}
	}
	walk(g.meshNode)

	return
}

func (g *MeshGroup) Area() Scalar {
	panic("Area() called on MeshGroup")
}

func (g *MeshGroup) Sample(RenderCtx) ObjectSample {
	panic("implement me")
}

func (g *MeshGroup) PdfAt(RenderCtx, Interaction, Vector) Scalar {
	panic("implement me")
}

func (g *MeshGroup) Triangles() []MeshTriangle {
	return g.triangles
}

type MeshTriangle struct {
	g *MeshGroup
	n int   // triangle number in the mesh

	// cached vertexIndices - cheaper to pay for space here than
	// endure lookup time
	v [3]int
}

func (t MeshTriangle) normalAt(hit Intersection) Vector {
	var normals = t.normals()
	if t.smooth() {
		return normals[1].Mul(hit.uv.U).Add(
			normals[2].Mul(hit.uv.V),
		).Add(
			normals[0].Mul(1 - hit.uv.U - hit.uv.V),
		)
		// return interpolateNormal(normals, hit.uv.U, hit.uv.V, 1-hit.uv.U-hit.uv.V)
	}

	return *normals[0]
}

func (t *MeshTriangle) NormalAt(p Point, hit Intersection) Vector {
	return t.normalAt(hit)
}

// Given a normalized vector, returns two vectors that are orthoganal to it,
// forming a 3d coordinate system
func coordinateSystem(n Vector) (x, y Vector) {
	if n.X().Abs() > n.Y().Abs() {
		var f = (n.X().Pow2() + n.Z().Pow2()).Sqrt()
		x = NewVector(-n.Z(), 0, n.X()).Mul(1 / f)
	} else {
		var f = (n.Y().Pow2() + n.Z().Pow2()).Sqrt()
		x = NewVector(0, n.Z(), -n.Y()).Mul(1 / f)
	}

	if have := n.Dot(x); !have.Eq(0) {
		panic(fmt.Sprintf("n dot x should be zero. have=%f\n\tn: %v\n\tx: %v", have, n, x))
	}

	y = n.Cross(x)
	return
}

func (t *MeshTriangle) SurfaceAt(interaction Interaction) Surface {
	var (
		u = interaction.uv.U
		v = interaction.uv.V
		w = 1 - u - v
		n = interpolateNormal(t.normals(), u, v, w)

		// interpolate texturing UV coordinates
		uv = t.uv()

		uvTexture = (uv[1].Scale(u)).Add(uv[2].Scale(v)).Add(uv[0].Scale(w))
	)

	// Compute dpdu, dpdv
	var (
		p           = t.vertices()
		duv02       = uv[1].Sub(uv[0])
		duv12       = uv[2].Sub(uv[0])
		dp02        = p[1].PathFrom(*p[0])
		dp12        = p[2].PathFrom(*p[0])
		determinant = (duv02.U * duv12.V) - (duv02.V * duv12.U)
		dpdu        Vector
		dpdv        Vector
	)

	if determinant == 0 {
		n := p[0].PathFrom(*p[1]).Cross(p[2].PathFrom(*p[1])).Normalize()
		dpdu, dpdv = coordinateSystem(n)
	} else {
		var invdet = 1 / determinant
		dpdu = dp02.Mul(duv12.V)
		dpdu = dpdu.Sub(dp12.Mul(duv02.V))
		dpdu = dpdu.Mul(invdet)

		dpdv = dp02.Negate()
		dpdv = dpdv.Mul(-duv12.U)
		dpdv = dpdv.Add(dp12.Mul(duv02.U))
		dpdv = dpdv.Mul(invdet)
	}

	// Compute dndu, dndv, ss, ts
	var (
		dndu Vector
		dndv Vector
	)
	if t.smooth() {
		var (
			normals = t.normals()
			dn1     = normals[1].Sub(*normals[0])
			dn2     = normals[2].Sub(*normals[0])
		)

		if determinant != 0 {
			var invdet = 1 / determinant
			dndu = dn1.Mul(duv12.V).Sub(dn2.Mul(duv02.V)).Mul(invdet)
			dndv = dn1.Mul(-duv12.U).Add(dn2.Mul(duv02.U)).Mul(invdet)
		}
	}

	return Surface{
		Geometry: SurfaceGeometry{
			Normal: dpdu.Cross(dpdv).Normalize().Forward(n),
			// Normal: n,
			UV: interaction.uv,
		},
		Shading: SurfaceShading{
			Normal: n,
			UV:     uvTexture,
			DPDU:   dpdu,
			DPDV:   dpdv,
			DNDU:   dndu,
			DNDV:   dndv,
		},
	}
}

var meshTriangleIntersectTests uint64

func GetMeshTriangleIntersectTestCount() uint64 {
	return atomic.LoadUint64(&meshTriangleIntersectTests)
}

func (t *MeshTriangle) Intersect(r Ray, intersections *Intersections) {
	atomic.AddUint64(&meshTriangleIntersectTests, 1)
	// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
	var (
		vertices   = t.vertices()
		edges      = t.edges()
		dirCrossE2 = r.direction.Cross(edges[1])
		det        = edges[0].Dot(dirCrossE2)
	)

	if det.Abs() < epsilon {
		return
	}

	var (
		f          = 1.0 / det
		p1ToOrigin = r.origin.PathFrom(*vertices[0])
		u          = f * dirCrossE2.Dot(p1ToOrigin)
	)

	if u < 0 || u > 1 {
		return
	}

	var (
		originCrossE1 = p1ToOrigin.Cross(edges[0])
		v             = f * r.direction.Dot(originCrossE1)
	)

	if v < 0 || (u+v) > 1 {
		return
	}

	// have an intersection
	var time = f * edges[1].Dot(originCrossE1)

	intersections.Append(NewIntersectionWithUV(t, time, u, v))
}

func (t *MeshTriangle) Bounds() (min, max Point) {
	return t.mesh().bounds[t.n].Bounds()
}

func (t *MeshTriangle) Parent() Object {
	return t.g
}

func (t *MeshTriangle) SetParent(Object) {
	panic("SetParent() called on MeshTriangle")
}

func (t *MeshTriangle) Material() Material {
	return t.g.Material()
}

func (t *MeshTriangle) Transformation() Transformation {
	return NewTransformation()
}

func (t *MeshTriangle) Shadows() bool {
	return t.mesh().Shadows()
}

func (t *MeshTriangle) Area() Scalar {
	var e = t.edges()
	return 0.5 * e[0].Cross(e[1]).Magnitude()
}

func uniformSampleTriangle(ctx RenderCtx) Tuple2D {
	var (
		r = ctx.RandomScalar().Sqrt()
		u = 1 - r
		v = ctx.RandomScalar() * r
	)
	return Tuple2D{u, v}
}

func interpolatePoints(vertices [3]*Point, u, v, w Scalar) (p Point) {
	p = vertices[1].Mul(u)
	p = p.Add(vertices[2].Mul(v))
	p = p.Add(vertices[0].Mul(w))
	return
}

func interpolateNormal(normals [3]*Vector, u, v, w Scalar) (n Vector) {
	n = normals[1].Mul(u)
	n = n.Add(normals[2].Mul(v))
	n = n.Add(normals[0].Mul(w))
	return
}

func (t *MeshTriangle) Sample(ctx RenderCtx) ObjectSample {
	var (
		// get random barycentric coords
		b = uniformSampleTriangle(ctx)
		u = b[0]
		v = b[1]
		w = 1 - b[0] - b[1]

		// interpolate to get point
		vertices = t.vertices()
		edges    = t.edges()
		p        = interpolatePoints(vertices, u, v, w)
		n        = edges[0].Cross(edges[1]).Normalize()
	)

	n = n.Forward(interpolateNormal(t.normals(), u, v, w))
	area := t.Area()
	return ObjectSample{
		Point:  p,
		Normal: n,
		PDF:    1 / area,
	}
}

func (t *MeshTriangle) PdfAt(ctx RenderCtx, interaction Interaction, out Vector) Scalar {
	return objectPDF(t, interaction, out)
}

func (t *MeshTriangle) Points() (p []Point) {
	var v = t.vertices()
	for _, vert := range v[:] {
		p = append(p, *vert)
	}
	return
}

func (t *MeshTriangle) Edges() []Vector {
	var e = t.edges()
	return e[:]
}

func (t *MeshTriangle) Normals() (n []Vector) {
	var norms = t.normals()
	for _, norm := range norms {
		n = append(n, *norm)
	}
	return
}

func (t *MeshTriangle) UVs() []UV {
	var uv = t.uv()
	return uv[:]
}

func (t *MeshTriangle) normals() [3]*Vector {
	var n = t.g.mesh.normalIndices[t.n]
	if t.smooth() {
		return [3]*Vector{
			&t.g.mesh.normals[n[0]],
			&t.g.mesh.normals[n[1]],
			&t.g.mesh.normals[n[2]],
		}
	}

	return [3]*Vector{
		&t.g.mesh.normals[n[0]],
		&t.g.mesh.normals[n[0]],
		&t.g.mesh.normals[n[0]],
	}
}

func (t *MeshTriangle) mesh() *Mesh {
	return t.g.mesh
}

func (t *MeshTriangle) smooth() bool {
	return t.g.mesh.smoothing[t.n]
}

func (t *MeshTriangle) vertices() (v [3]*Point) {
	// var i = t.g.mesh.vertexIndices[t.n]
	var i = t.v
	v[0] = &t.g.mesh.vertices[i[0]]
	v[1] = &t.g.mesh.vertices[i[1]]
	v[2] = &t.g.mesh.vertices[i[2]]
	return
}

func (t *MeshTriangle) edges() (e [2]Vector) {
	e = t.g.mesh.edges[t.n]
	return
}

func (t *MeshTriangle) uv() (uv [3]UV) {
	var i = t.g.mesh.textureIndices[t.n]
	uv[0] = t.g.mesh.textures[i[0]]
	uv[1] = t.g.mesh.textures[i[1]]
	uv[2] = t.g.mesh.textures[i[2]]
	return
}

type MeshBuilder struct {
	mesh Mesh
}

func (b *MeshBuilder) DefineVertex(p Point) int {
	b.mesh.vertices = append(b.mesh.vertices, p)
	return len(b.mesh.vertices) - 1
}

func (b *MeshBuilder) DefineNormal(n Vector) int {
	b.mesh.normals = append(b.mesh.normals, n)
	return len(b.mesh.normals) - 1
}

func (b *MeshBuilder) DefineUV(uv UV) int {
	b.mesh.textures = append(b.mesh.textures, uv)
	return len(b.mesh.textures) - 1
}

func (b *MeshBuilder) DefinePolygon(faces []Face) {
	// resolve any relative vertex values
	for i := range faces {
		if relative := faces[i].Vertex; relative < 0 {
			faces[i].Vertex = len(b.mesh.vertices) + relative + 1
		}
		if relative := faces[i].Normal; relative != nil && *relative < 0 {
			absolute := len(b.mesh.normals) + *relative + 1
			faces[i].Normal = &absolute
		}
		if relative := faces[i].UV; relative != nil && *relative < 0 {
			absolute := len(b.mesh.textures) + *relative + 1
			faces[i].UV = &absolute
		}
	}
	if len(faces) < 3 {
		panic("insufficient vertices defined")
	} else if len(faces) > 3 {
		b.fanTriangulate(faces)
	} else {
		b.pushTriangle([...]Face{faces[0], faces[1], faces[2]})
	}
}

func (b *MeshBuilder) DefineGroup(name string) {
	b.mesh.groups = append(b.mesh.groups, MeshGroup{
		name: name,
		mesh: &b.mesh,
		meshNode: meshNode{
			bounds: NewBoundingBox(),
		},
	})
}

func (b *MeshBuilder) DefineMaterial(m MaterialProperties) {
	b.group().material = &m
}

func (b *MeshBuilder) pushUV(faces [3]Face) {
	var texturing = faces[0].UV != nil && faces[1].UV != nil && faces[2].UV != nil

	for _, face := range faces {
		if !texturing && face.UV != nil {
			panic("either all or no faces may have UV set")
		}
	}

	if !texturing {
		b.mesh.textureIndices = append(b.mesh.textureIndices, [...]int{
			b.DefineUV(UV{1, 0}),
			b.DefineUV(UV{0, 1}),
			b.DefineUV(UV{1, 1}),
		})
		return
	}

	b.mesh.textureIndices = append(b.mesh.textureIndices, [...]int{
		*faces[0].UV,
		*faces[1].UV,
		*faces[2].UV,
	})
}

func (b *MeshBuilder) pushNormals(faces [3]Face, edges [2]Vector) {
	var smooth = faces[0].Normal != nil && faces[1].Normal != nil && faces[2].Normal != nil

	for _, face := range faces {
		if !smooth && face.Normal != nil {
			panic("either all or no faces may have normals set")
		}
	}

	b.mesh.smoothing = append(b.mesh.smoothing, smooth)

	if smooth {
		b.mesh.normalIndices = append(b.mesh.normalIndices, [...]int{
			*faces[0].Normal,
			*faces[1].Normal,
			*faces[2].Normal,
		})
		return
	}

	var normal = edges[1].Cross(edges[0]).Normalize()
	// if there aren't any normals provided, pre-compute the normal
	b.mesh.normalIndices = append(b.mesh.normalIndices, [3]int{
		b.DefineNormal(normal),
	})
}

func (b *MeshBuilder) pushTriangle(faces [3]Face) {
	// index the vertices
	b.mesh.vertexIndices = append(b.mesh.vertexIndices, [...]int{
		faces[0].Vertex,
		faces[1].Vertex,
		faces[2].Vertex,
	})

	// gather up the actual points for pre-computation
	var p = [...]Point{
		b.mesh.vertices[faces[0].Vertex],
		b.mesh.vertices[faces[1].Vertex],
		b.mesh.vertices[faces[2].Vertex],
	}

	// pre-compute edges, used for triangle intersection, and possibly normals
	var edges = [...]Vector{
		p[1].PathFrom(p[0]),
		p[2].PathFrom(p[0]),
	}

	// store edges
	b.mesh.edges = append(b.mesh.edges, edges)

	// add normals & uv
	b.pushNormals(faces, edges)
	b.pushUV(faces)

	// make sure the bounds of the triangle are kept
	b.mesh.bounds = append(b.mesh.bounds, func() BoundingBox {
		b := NewBoundingBox()
		for _, pt := range p {
			b.Include(pt)
		}
		return b
	}())

	// bind to a group
	var (
		group = b.group()
		t     = MeshTriangle{
			g: group,
			n: b.mesh.Triangles() - 1,
			v: b.mesh.vertexIndices[b.mesh.Triangles()-1],
		}
	)

	// update the group bounds for optimization
	group.bounds.Update(t.Bounds())

	// formally add to the group
	group.triangles = append(group.triangles, t)

	// update mesh bounding box
	b.mesh.index.bounds.Update(t.Bounds())
}

func (b *MeshBuilder) group() *MeshGroup {
	if len(b.mesh.groups) == 0 {
		b.DefineGroup("")
	}

	return &b.mesh.groups[len(b.mesh.groups)-1]
}

func (b *MeshBuilder) fanTriangulate(faces []Face) {
	if len(faces) < 3 {
		panic("calling fanTriangulate() on less than 3 vertices")
	}

	for i := 1; i < len(faces)-1; i++ {
		b.pushTriangle([...]Face{
			faces[0],
			faces[i],
			faces[i+1],
		})
	}
}

func (b *MeshBuilder) Build() *Mesh {
	m := b.mesh
	m.shape = newShape()

	// shallow copy groups otherwise meshes will share the same underlying array
	// the groups will share the same underlying arrays to triangle indices
	// but having different physical groups allows for different materials
	// for different calls to build()
	//
	// without the copy, each subsequent Build() would update *all* versions of the mesh
	groups := make([]MeshGroup, len(m.groups))
	copy(groups, m.groups)

	m.groups = groups

	for i := range m.groups {
		// update the copied group pointers to point to the new mesh
		m.groups[i].mesh = &m

		triangles := make([]MeshTriangle, len(m.groups[i].triangles))
		copy(triangles, m.groups[i].triangles)

		// same deal with the MeshTriangle instances, they need to be rewritten to
		// point to the new groups...that point to the new mesh
		m.groups[i].triangles = triangles
		for j := range m.groups[i].triangles {
			m.groups[i].triangles[j].g = &m.groups[i]
		}

		m.index.triangles = append(m.index.triangles, triangles...)
		// material.groups[i].index()
	}

	m.index.index()

	return &m
}

type Face struct {
	Vertex int

	// optional indices
	Normal *int
	UV     *int
}
