package raytracer

import "math"

type Transformation struct {
	transformation
}

func NewTransformation() Transformation {
	return Transformation{newTransformation()}
}

func (t Transformation) Invert() (tr Transformation) {
	tr = Transformation{
		transformation{
			m:           t.inverted,
			inverted:    t.m,
			transposed:  t.itransposed,
			itransposed: t.transposed,
		},
	}
	return
}

func (t Transformation) Transpose() Transformation {
	return Transformation{transformation{
		m:           t.transposed,
		inverted:    t.itransposed,
		transposed:  t.m,
		itransposed: t.inverted,
	}}
}

type transformation struct {
	m           *Matrix
	inverted    *Matrix
	transposed  *Matrix
	itransposed *Matrix // inverted transposed
}

var identityTransformation = transformation{
	m:           IdentityMatrix().ptr(),
	inverted:    IdentityMatrix().Invert().ptr(),
	transposed:  IdentityMatrix().Transpose().ptr(),
	itransposed: IdentityMatrix().Invert().Transpose().ptr(),
}

func newTransformation() transformation {
	return identityTransformation
}

func newTransformationFromMatrix(m Matrix) transformation {
	inv := m.Invert()
	return transformation{
		m:           m.ptr(),
		inverted:    inv.ptr(),
		transposed:  m.Transpose().ptr(),
		itransposed: inv.Transpose().ptr(),
	}
}

func (t *transformation) set(m Matrix) {
	*t = newTransformationFromMatrix(m)
}

func (t *transformation) Translate(trans Translation) {
	t.set(t.m.Mul(trans.matrix))
}

func (t *transformation) Scale(scale Scale) {
	t.set(t.m.Mul(scale.matrix))
}

func (t *transformation) Shear(shear Shear) {
	t.set(t.m.Mul(shear.matrix))
}

func (t *transformation) Rotate(r Rotation) {
	t.set(t.m.Mul(r.matrix))
}

func (t *transformation) RotateX(x Scalar) {
	t.Rotate(NewRotationX(x))
}

func (t *transformation) RotateY(y Scalar) {
	t.Rotate(NewRotationY(y))
}

func (t *transformation) RotateZ(z Scalar) {
	t.Rotate(NewRotationZ(z))
}

func (t *transformation) Transformation() Transformation {
	return Transformation{*t}
}

type Translation struct {
	matrix Matrix
}

func NewTranslation(x, y, z Scalar) Translation {
	t := Translation{IdentityMatrix()}
	t.matrix[0][3] = x
	t.matrix[1][3] = y
	t.matrix[2][3] = z
	return t
}

func (t Translation) Invert() Translation {
	return Translation{t.matrix.Invert()}
}

type Scale struct {
	matrix Matrix
}

func NewScale(x, y, z Scalar) Scale {
	s := Scale{IdentityMatrix()}
	s.matrix[0][0] = x
	s.matrix[1][1] = y
	s.matrix[2][2] = z
	return s
}

func (s Scale) Invert() Scale {
	return Scale{s.matrix.Invert()}
}

type Rotation struct {
	matrix Matrix
}

func NewRotationX(r Scalar) Rotation {
	rm := Rotation{
		Matrix{
			{1, 0, 0, 0},
			{0, Scalar(math.Cos(float64(r))), Scalar(-math.Sin(float64(r))), 0},
			{0, Scalar(math.Sin(float64(r))), Scalar(math.Cos(float64(r))), 0},
			{0, 0, 0, 1},
		},
	}
	return rm
}

func NewRotationY(r Scalar) Rotation {
	return Rotation{
		Matrix{
			{Scalar(math.Cos(float64(r))), 0, Scalar(math.Sin(float64(r))), 0},
			{0, 1, 0, 0},
			{Scalar(-math.Sin(float64(r))), 0, Scalar(math.Cos(float64(r))), 0},
			{0, 0, 0, 1},
		},
	}
}

func NewRotationZ(r Scalar) Rotation {
	return Rotation{
		Matrix{
			{Scalar(math.Cos(float64(r))), Scalar(-math.Sin(float64(r))), 0, 0},
			{Scalar(math.Sin(float64(r))), Scalar(math.Cos(float64(r))), 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1},
		},
	}
}

func (r Rotation) Invert() Rotation {
	return Rotation{r.matrix.Invert()}
}

type Shear struct {
	matrix Matrix
}

func NewShear(xy, xz, yx, yz, zx, zy Scalar) Shear {
	return Shear{
		Matrix{
			{1, xy, xz, 0},
			{yx, 1, yz, 0},
			{zx, zy, 1, 0},
			{0, 0, 0, 1},
		},
	}
}

type ViewTransform struct {
	matrix Matrix
}

func NewViewTransform(from, to Point, up Vector) ViewTransform {
	var (
		forward = to.PathFrom(from).Normalize()
		left    = forward.Cross(up.Normalize())
		trueUp  = left.Cross(forward)

		orientation = Matrix{
			{left.X(), left.Y(), left.Z(), 0},
			{trueUp.X(), trueUp.Y(), trueUp.Z(), 0},
			{-forward.X(), -forward.Y(), -forward.Z(), 0},
			{0, 0, 0, 1},
		}

		translation = NewTranslation(-from.X(), -from.Y(), -from.Z())
	)

	return ViewTransform{orientation.Mul(translation.matrix)}
}

func (v ViewTransform) Eq(o ViewTransform) bool {
	return v.matrix.Eq(o.matrix)
}
