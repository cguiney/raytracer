package raytracer

type Transformer interface {
	// Transformation returns the matrix used to transform the object
	Transformation() Transformation
}

type ObjectSample struct {
	Point
	Normal    Vector
	Direction Vector
	PDF       Scalar
}

type Object interface {
	Transformer

	Bounded

	SetParent(Object)

	Parent() Object

	// Material returns the material of the object
	Material() Material

	// NormalAt returns the Normal for the given Point in object space
	NormalAt(Point, Intersection) Vector

	SurfaceAt(Interaction) Surface

	// Intersect returns a list of intersections for the Ray in object space
	Intersect(Ray, *Intersections)

	Shadows() bool

	Sample(RenderCtx) ObjectSample

	PdfAt(RenderCtx, Interaction, Vector) Scalar

	Area() Scalar
}

type MutableObject interface {
	Object
	SetMaterial(Material)
}

type ObjectGraph interface {
	Object

	Children() []Object
}

func WalkGraph(o Object, fn func(Object)) {
	fn(o)

	graph, ok := o.(ObjectGraph)
	if !ok {
		return
	}

	for _, obj := range graph.Children() {
		WalkGraph(obj, fn)
	}
}

func hitObject(obj Object, r Ray) (Intersection, bool) {
	var xs Intersections
	Intersect(r, obj, &xs)
	return xs.Hit()
}

func objectPDF(obj Object, interaction Interaction, out Vector) Scalar {
	ray := NewRay(interaction.over, out)

	hit, ok := hitObject(obj, ray)
	if !ok {
		return 0
	}

	lightX := NewInteraction(hit, ray)

	ndotout := lightX.normal.Dot(out.Negate()).Abs()
	// ndotout := lightX.normal.Dot(out).Abs()
	// ndotdir := normal.Dot(direction.Negate()).Abs()

	distance := interaction.point.PathFrom(lightX.point).Length()
	area := obj.Area()
	pdf := distance / (ndotout * area)
	// pdf := distance / (ndotdir * area)
	return pdf
}

func PointToWorld(object Object, p Point) Point {
	p = p.transform(object.Transformation().Invert().Transpose())

	if object.Parent() != nil {
		p = PointToWorld(object.Parent(), p)
	}

	return p
}

func VectorToWorld(object Object, v Vector) Vector {
	v = v.transform(object.Transformation().Invert().Transpose())

	if parent := object.Parent(); parent != nil {
		v = VectorToWorld(parent, v)
	}

	return v
}

// sampleAt will sample a point on an object randomly and
func SampleAt(ctx RenderCtx, obj Object, interaction Interaction) ObjectSample {
	s := obj.Sample(ctx)
	s.Point = PointToWorld(obj, s.Point)
	s.Normal = NormalToWorld(obj, s.Normal)

	p := interaction.point

	direction := s.Point.PathFrom(p)
	distance := direction.Length()
	if distance == 0 {
		s.PDF = 0
		return s
	}

	direction = direction.Normalize()

	ndotdir := s.Normal.Dot(direction.Negate()).Abs()
	pdf := s.PDF * (distance / ndotdir)
	if pdf == inf {
		s.PDF = 0
	}

	s.PDF = pdf
	s.Direction = direction
	return s
}

func SurfaceAt(obj Object, interaction Interaction) Surface {
	interaction.point = WorldToObject(interaction.object, interaction.point)
	s := obj.SurfaceAt(interaction)

	s.Geometry.Normal = NormalToWorld(obj, s.Geometry.Normal)
	s.Shading.Normal = NormalToWorld(obj, s.Shading.Normal)
	s.Shading.DPDU = VectorToWorld(obj, s.Shading.DPDU)
	s.Shading.DPDV = VectorToWorld(obj, s.Shading.DPDV)
	s.Shading.DNDU = NormalToWorld(obj, s.Shading.DNDU)
	s.Shading.DNDU = NormalToWorld(obj, s.Shading.DNDU)

	return s
}

func Intersect(r Ray, obj Object, xs *Intersections) {
	r = rayToObject(obj, r)
	obj.Intersect(r, xs)
}

func Intersects(r Ray, obj Object) bool {
	xs := make(Intersections, 0, 2)
	r = rayToObject(obj, r)
	obj.Intersect(r, &xs)
	return len(xs) > 0
}

func rayToObject(object Object, r Ray) Ray {
	return r.transform(object.Transformation().Invert())
}
