package raytracer

func assert(msg string, f func() bool) {
	if !f() {
		panic("assertion failed: " + msg)
	}
}
