package raytracer

var _ Object = &Group{}

type Group struct {
	shape
	bounds   BoundingBox
	children []Object
}

func NewGroup() *Group {
	return &Group{
		shape:  newShape(),
		bounds: NewBoundingBox(),
	}
}

func (g *Group) Objects() []Object {
	return g.children
}

func (g *Group) SurfaceAt(Interaction) Surface {
	panic("SurfaceAt called on group")
}

func (g *Group) AddObject(obj Object) {
	g.children = append(g.children, obj)
	obj.SetParent(g)

	box := NewParentSpaceBoundingBox(obj)
	g.bounds.Update(box.Bounds())
}

func (g *Group) NormalAt(Point, Intersection) Vector {
	panic("logic error: NormalAt called on group")
}

func (g *Group) Intersect(r Ray, intersections *Intersections) {
	if !g.bounds.Intersects(r) {
		return
	}

	for _, obj := range g.children {
		Intersect(r, obj, intersections)
	}
}

func (g *Group) Bounds() (min, max Point) {
	return g.bounds.Bounds()
	/*b := NewBoundingBox()

	for _, child := range g.children {
		cbox := NewParentSpaceBoundingBox(child)
		b.Add(cbox)
	}

	return b.Bounds()*/

}

type BoundingBox struct {
	min, max Point
	points   int
}

type Bounded interface {
	Bounds() (Point, Point)
}

func NewParentSpaceBoundingBox(obj Object) BoundingBox {
	b := NewBoundedBox(obj.Bounds())
	b.Transform(obj.Transformation())
	return b
}

func NewBoundingBox() BoundingBox {
	return BoundingBox{
		min: NewPoint(inf, inf, inf),
		max: NewPoint(ninf, ninf, ninf),
	}
}

func NewBoundedBox(min, max Point) BoundingBox {
	b := NewBoundingBox()
	b.Update(min, max)
	return b
}

type Axis int

const (
	X Axis = iota
	Y
	Z
)

func (b *BoundingBox) Split() (left, right BoundingBox, axis Axis) {
	var (
		dx = b.max.X() - b.min.X()
		dy = b.max.Y() - b.min.Y()
		dz = b.max.Z() - b.min.Z()

		x0, y0, z0 = b.min.X(), b.min.Y(), b.min.Z()
		x1, y1, z1 = b.max.X(), b.max.Y(), b.max.Z()

		greatest = absmax3(dx, dy, dz)
	)

	switch greatest {
	case dx:
		x0 = x0 + dx/2
		x1 = x0
		axis = X
	case dy:
		y0 = y0 + dy/2
		y1 = y0
		axis = Y
	case dz:
		z0 = z0 + dz/2
		z1 = z0
		axis = Z
	default:
		panic("floating point comparison failure")
	}

	var (
		min = NewPoint(x0, y0, z0)
		max = NewPoint(x1, y1, z1)
	)

	left = NewBoundedBox(b.min, max)
	right = NewBoundedBox(min, b.max)

	return
}

func (b *BoundingBox) Include(p Point) {
	if b.points == 0 {
		*b = NewBoundingBox()
	}

	b.points++
	b.min = NewPoint(
		min(b.min.X(), p.X()),
		min(b.min.Y(), p.Y()),
		min(b.min.Z(), p.Z()))
	b.max = NewPoint(
		max(b.max.X(), p.X()),
		max(b.max.Y(), p.Y()),
		max(b.max.Z(), p.Z()))

}

func (b *BoundingBox) Includes(p Point) bool {
	var (
		x = p.X() >= b.min.X() && p.X() <= b.max.X()
		y = p.Y() >= b.min.Y() && p.Y() <= b.max.Y()
		z = p.Z() >= b.min.Z() && p.Z() <= b.max.Z()
	)

	return x && y && z

	/*
		return (p.X() >= b.min.X() && p.X() <= b.max.X()) &&
			(p.Y() >= b.min.Y() && p.Y() <= b.max.Y()) &&
			(p.Z() >= b.min.Z() && p.Z() <= b.max.Z())
	*/
}

func (b *BoundingBox) Update(lower, upper Point) {
	b.Include(lower)
	b.Include(upper)
}

func (b *BoundingBox) Add(bo Bounded) {
	b.Update(bo.Bounds())
}

func (b *BoundingBox) Contains(bo Bounded) bool {
	var (
		min, max = bo.Bounds()
		lower    = b.Includes(min)
		upper    = b.Includes(max)
	)
	return lower && upper
}

func (b *BoundingBox) Bounds() (Point, Point) {
	return b.Lower(), b.Upper()
}

func (b *BoundingBox) Lower() Point {
	return b.min
}

func (b *BoundingBox) Upper() Point {
	return b.max
}

func (b *BoundingBox) Transform(t Transformation) {
	var points = [...]Point{
		b.min,
		NewPoint(b.min.X(), b.min.Y(), b.max.Z()),
		NewPoint(b.min.X(), b.max.Y(), b.min.Z()),
		NewPoint(b.min.X(), b.max.Y(), b.max.Z()),

		NewPoint(b.max.X(), b.min.Y(), b.min.Z()),
		NewPoint(b.max.X(), b.min.Y(), b.max.Z()),
		NewPoint(b.max.X(), b.max.Y(), b.min.Z()),
		b.max,
	}

	*b = NewBoundingBox()

	for _, p := range points[:] {
		b.Include(p.transform(t))
	}
}

func (b *BoundingBox) Intersects(r Ray) bool {
	xtmin, xtmax := checkAxis(r.origin.tuple[0], r.direction.tuple[0], b.min.tuple[0], b.max.tuple[0])
	ytmin, ytmax := checkAxis(r.origin.tuple[1], r.direction.tuple[1], b.min.tuple[1], b.max.tuple[1])
	ztmin, ztmax := checkAxis(r.origin.tuple[2], r.direction.tuple[2], b.min.tuple[2], b.max.tuple[2])

	tmin := max3(xtmin, ytmin, ztmin)
	tmax := min3(xtmax, ytmax, ztmax)

	if tmin > tmax {
		return false
	}
	return true
}

func (b *BoundingBox) Centroid() Point {
	return Centroid(b.min, b.max)
}

func (b *BoundingBox) MaxExtent() Axis {
	return b.max.PathFrom(b.min).MaxExtent()
}

func (b *BoundingBox) Diagonal() Vector {
	return b.max.PathFrom(b.min)
}

func (b *BoundingBox) Offset(p Point) Vector {
	o := p.PathFrom(b.min)

	var (
		x = o.X()
		y = o.Y()
		z = o.Z()
	)

	if b.max.X() > b.min.X() {
		x /= b.max.X() - b.min.X()
	}
	if b.max.Y() > b.min.Y() {
		y /= b.max.Y() - b.min.Y()
	}
	if b.max.Z() > b.min.Z() {
		z /= b.max.Z() - b.min.Z()
	}

	return NewVector(x, y, z)
}

func (b *BoundingBox) Area() Scalar {
	d := b.Diagonal()

	return 2 * (d.X()*d.Y() + d.X()*d.Z() + d.Y()*d.Z())
}

func (b *BoundingBox) Sphere() BoundingSphere {
	return NewBoundedSphere(b.min, b.max)
}

func Centroid(min, max Point) Point {
	return min.Mul(0.5).Add(max.Mul(0.5))
}

type BoundingSphere struct {
	min, max Point
	center   Point
	radius   Scalar
}

func NewBoundedSphere(min, max Point) BoundingSphere {
	center := min.Add(max).Mul(0.5)
	return BoundingSphere{
		min:    min,
		max:    max,
		center: center,
		radius: center.PathFrom(max).Length(),
	}
}

func (b BoundingSphere) Raidus() Scalar {
	return b.radius
}

func (b BoundingSphere) Center() Point {
	return b.center
}

func (b BoundingSphere) Bounds() (min, max Point) {
	return b.min, b.max
}
