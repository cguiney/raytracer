package raytracer

import "testing"

func TestMaterial(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		m := NewMaterialProperties()

		if want, have := NewColor(1, 1, 1), m.Color(); !have.Eq(want) {
			t.Errorf("unexpected default color. have=%v want=%v", have, want)
		}
		if want, have := NewGray(0.1), m.Ambient(); !have.Eq(want) {
			t.Errorf("unexpected default ambient. have=%v want=%v", have, want)
		}
		if want, have := NewGray(0.9), m.Diffuse(); !have.Eq(want) {
			t.Errorf("unexpected default diffuse. have=%v want=%v", have, want)
		}
		if want, have := NewGray(0.9), m.Specular(); !have.Eq(want) {
			t.Errorf("unexpected default specular. have=%v want=%v", have, want)
		}
		if want, have := Scalar(200.0), m.Shininess(); !have.Eq(want) {
			t.Errorf("unexpected default shininess. have=%v want=%v", have, want)
		}
		if want, have := Scalar(0), m.Reflective(); !have.Eq(want) {
			t.Errorf("unexpected default reflectiveness. have=%f want=%f", have, want)
		}
	})

	t.Run("Lighting with texture applied", func(t *testing.T) {
		m := NewMaterialProperties()
		m.SetAmbient(NewGray(1))
		m.SetDiffuse(NewGray(0))
		m.SetSpecular(NewGray(0))
		m.SetPattern(NewStripe(White, Black))

		eyev := NewVector(0, 0, -1)
		normalv := NewVector(0, 0, -1)
		light := NewPointLight(NewPoint(0, 0, -10), White)

		type lightingTest struct {
			at   Point
			want Color
		}

		tests := []lightingTest{
			{at: NewPoint(0.9, 0, 0), want: White},
			{at: NewPoint(1.1, 0, 0), want: Black},
		}

		for _, tt := range tests {
			have := Lighting(m, NewSphere(), light, tt.at, eyev, normalv, false, UV{})
			if !have.Eq(tt.want) {
				t.Errorf("incorrect color at point %v. have=%s want=%s",
					tt.at, have, tt.want)
			}
		}
	})
}
