package raytracer

import (
	"math"
	"testing"
)

func TestRays(t *testing.T) {
	t.Run("ray construction", func(t *testing.T) {
		origin := NewPoint(1, 2, 3)
		direction := NewVector(4, 5, 6)

		r := NewRay(origin, direction)

		if !r.Origin().Eq(origin) {
			t.Errorf("ray origin improperly set.  have=%v want=%v", r.Origin(), origin)
		}

		if !r.Direction().Eq(direction) {
			t.Errorf("ray direction improperly set. have=%v want=%v", r.Direction(), direction)
		}
	})

	t.Run("ray position", func(t *testing.T) {
		type positionTest struct {
			t    Scalar
			want Point
		}

		tests := []positionTest{
			{0, NewPoint(2, 3, 4)},
			{1, NewPoint(3, 3, 4)},
			{-1, NewPoint(1, 3, 4)},
			{2.5, NewPoint(4.5, 3, 4)},
		}

		r := NewRay(NewPoint(2, 3, 4), NewVector(1, 0, 0))

		for _, tt := range tests {
			if have := r.Position(tt.t); !have.Eq(tt.want) {
				t.Errorf("incorrect result of position at time %f. have=%v want=%v",
					tt.t, have, tt.want)
			}
		}
	})
}

var x Ray // it's an x-ray!

func BenchmarkRayTransform(b *testing.B) {
	r := NewRay(NewPoint(0, 1, 0), NewVector(1, math.Pi, math.Pi/2))
	t := Transformation{newTransformationFromMatrix(NewTranslation(math.Pi, math.Sqrt2, math.E).matrix)}

	for n := 0; n < b.N; n++ {
		x = r.transform(t)
	}
}