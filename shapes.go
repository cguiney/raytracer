package raytracer

import (
	"math"
	"sync/atomic"
)

type shape struct {
	transformation
	material Material
	shadows  bool

	parent Object
}

func newShape() shape {
	return shape{
		transformation: newTransformation(),
		material:       NewMaterialProperties(),
		shadows:        true,
	}
}

func (s *shape) Material() Material {
	return s.material
}

func (s *shape) SetMaterial(m Material) {
	s.material = m
}

func (s *shape) Transformation() Transformation {
	return Transformation{s.transformation}
}

func (s *shape) SetParent(obj Object) {
	s.parent = obj
}

func (s *shape) Parent() Object {
	return s.parent
}

func (s *shape) Shadows() bool {
	return s.shadows
}

func (s *shape) SetShadows(b bool) {
	s.shadows = b
}

func (s *shape) Area() Scalar {
	panic("implement me")
}

func (s *shape) Sample(ctx RenderCtx) ObjectSample {
	panic("implement me")
}

func (s *shape) PdfAt(ctx RenderCtx, interaction Interaction, out Vector) Scalar {
	panic("implement me")
}

type bound struct {
	min, max Scalar
	closed   bool
}

func newBound() bound {
	return bound{
		min: Scalar(math.Inf(-1)),
		max: Scalar(math.Inf(1)),
	}
}

func (b *bound) Minimum() Scalar {
	return b.min
}

func (b *bound) Maximum() Scalar {
	return b.max
}

func (b *bound) TruncateLower(min Scalar) {
	b.min = min
}

func (b *bound) TruncateUpper(max Scalar) {
	b.max = max
}

func (b *bound) Truncate(min, max Scalar) {
	b.TruncateLower(min)
	b.TruncateUpper(max)
}

func (b *bound) Closed() bool {
	return b.closed
}

func (b *bound) Opened() bool {
	return !b.Closed()
}

func (b *bound) SetClosed(t bool) {
	b.closed = t
}

type Sphere struct {
	shape
}

func NewSphere() *Sphere {
	return &Sphere{
		shape: newShape(),
	}
}

func NewGlassSphere() *Sphere {
	s := NewSphere()
	g := Glass
	s.SetMaterial(&g)
	return s
}

func (s *Sphere) Intersect(r Ray, intersections *Intersections) {
	path := r.Origin().PathFrom(NewPoint(0, 0, 0))
	a := float64(r.Direction().Dot(r.Direction()))
	b := float64(2 * r.Direction().Dot(path))
	c := float64(path.Dot(path) - 1)

	d := (b * b) - (4 * a * c)

	if d < 0 {
		return
	}

	intersections.Append(
		NewIntersection(s, Scalar((-b-math.Sqrt(d))/(2*a))),
		NewIntersection(s, Scalar((-b+math.Sqrt(d))/(2*a))),
	)
}

func (s *Sphere) Bounds() (Point, Point) {
	return NewPoint(-1, -1, -1), NewPoint(1, 1, 1)
}

func (s *Sphere) NormalAt(op Point, intersection Intersection) Vector {
	return op.PathFrom(NewPoint(0, 0, 0))
}

func radians(d Scalar) Scalar {
	return (math.Pi / 180) * d.Clamp(0, 360)
}

func (s *Sphere) SurfaceAt(interaction Interaction) (surface Surface) {
	var (
		p      = interaction.point
		normal = p.PathFrom(NewPoint(0, 0, 0))

		phiMax    = radians(360)
		thetaMin  = -1.0
		thetaMax  = 1.0
		thetaDiff = Scalar(thetaMax - thetaMin)

		// azimuth angle
		// -pi < theta <= pi
		// angle increases clockwise as viewed from above
		theta = math.Atan2(float64(p.X()), float64(p.Z()))

		// vector pointing from sphere's origin to p
		// happens to be equal to sphere's radius
		vec    = NewVector(p.X(), p.Y(), p.Z())
		radius = vec.Magnitude()

		// polar angle
		// 0 <= phi <= pi
		phi = math.Acos(float64(p.Y() / radius))

		// - 0.5 < rawU <= 0.5
		rawU = theta / (2 * math.Pi)

		// 0 <= u < 1
		// fixes u so that it increases counter-clockwise as viewed from above
		u = Scalar(1 - (rawU + 0.5))

		// v should be 0 at south pole of sphere
		// 1 at north pole
		// filpped over by subtracting by 1
		v = Scalar(1 - (phi / math.Pi))

		// Compute partial derivatives dpdu, dpdv
		zRadius    = (p.X().Pow2() + p.Y().Pow2()).Sqrt()
		invZRadius = 1 / zRadius
		cosPhi     = p.X() * invZRadius
		sinPhi     = p.Y() * invZRadius
		sinTheta   = Scalar(math.Sin(theta))
		dpdu       = NewVector(-phiMax*p.Y(), phiMax*p.X(), 0)
		dpdv       = NewVector(p.Z()*cosPhi, p.Z()*sinPhi, -radius*sinTheta).Mul(thetaDiff)

		// Compute partial derivatives for normals
		d2pduu = NewVector(p.X(), p.Y(), 0).Mul(-phiMax * phiMax)
		d2pduv = NewVector(-sinPhi, cosPhi, 0).Mul(thetaDiff * p.X() * phiMax)
		d2pdvv = NewVector(p.X(), p.Y(), p.Z()).Mul((-thetaDiff) * thetaDiff)

		E = dpdu.Dot(dpdu)
		F = dpdu.Dot(dpdv)
		G = dpdv.Dot(dpdv)
		N = dpdu.Cross(dpdv).Normalize()

		e = N.Dot(d2pduu)
		f = N.Dot(d2pduv)
		g = N.Dot(d2pdvv)

		invEGF2 = 1 / (E*G - F.Pow2())

		dndu = dpdu.Mul((f*F - e*G) * invEGF2).Add(dpdv.Mul((e*F - f*E) * invEGF2))
		dndv = dpdu.Mul((g*F - f*G) * invEGF2).Add(dpdv.Mul((f*F - g*E) * invEGF2))
	)

	return Surface{
		Geometry: SurfaceGeometry{
			UV:     UV{u, v},
			Normal: normal,
		},
		Shading: SurfaceShading{
			UV:     UV{u, v},
			Normal: normal,
			DPDU:   dpdu,
			DPDV:   dpdu,
			DNDU:   dndu,
			DNDV:   dndv,
		},
	}
}

var _ Object = &Plane{}

type Plane struct {
	shape
}

func NewPlane() *Plane {
	return &Plane{
		shape: newShape(),
	}
}

func (p *Plane) NormalAt(pt Point, intersection Intersection) Vector {
	return NewVector(0, 1, 0)
}

func (p *Plane) Intersect(r Ray, intersections *Intersections) {
	if r.direction.Y().Abs() < epsilon {
		return
	}

	intersections.Append(
		NewIntersection(p, -r.Origin().Y()/r.direction.Y()),
	)
}

func (p *Plane) SurfaceAt(interaction Interaction) Surface {
	var pt = interaction.point
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: NewVector(0, 1, 0),
			UV:     UV{pt.X().Mod(1), pt.Z().Mod(1)},
		},
		Shading: SurfaceShading{
			UV:     UV{pt.X().Mod(1), pt.Z().Mod(1)},
			Normal: NewVector(0, 1, 0),
			DPDU:   NewVector(-pt.Z(), 0, pt.X()),
			DPDV:   NewVector(pt.X(), 0, pt.Z()),
		},
	}
}

func (p *Plane) Bounds() (Point, Point) {
	return NewPoint(ninf, 0, ninf), NewPoint(inf, 0, inf)
}

func (p *Plane) Lights() []Light {
	return nil
}

func checkAxis(origin, direction, lower, upper Scalar) (min Scalar, max Scalar) {
	var (
		tminNumerator = lower - origin
		tmaxNumerator = upper - origin
	)

	if math.Abs(float64(direction)) >= epsilon {
		var denom = 1 / direction
		min = tminNumerator * denom
		max = tmaxNumerator * denom
	} else {
		min = tminNumerator * inf
		max = tmaxNumerator * inf
	}

	if min > max {
		min, max = max, min
	}

	return
}

type Cube struct {
	shape
}

func NewCube() *Cube {
	return &Cube{shape: newShape()}
}

func (c *Cube) Intersect(r Ray, intersections *Intersections) {
	xtmin, xtmax := c.checkAxis(r.Origin().X(), r.Direction().X())
	ytmin, ytmax := c.checkAxis(r.Origin().Y(), r.Direction().Y())
	ztmin, ztmax := c.checkAxis(r.Origin().Z(), r.Direction().Z())

	tmin := max3(xtmin, ytmin, ztmin)
	tmax := min3(xtmax, ytmax, ztmax)

	if tmin > tmax {
		return
	}

	intersections.Append(
		NewIntersection(c, tmin),
		NewIntersection(c, tmax),
	)
}

func (c *Cube) NormalAt(pt Point, intersection Intersection) Vector {
	return c.normalAt(pt)
}

func (c *Cube) normalAt(pt Point) Vector {
	var max = absmax3(pt.X(), pt.Y(), pt.Z())
	switch {
	case max == abs(pt.X()):
		return NewVector(pt.X(), 0, 0)
	case max == abs(pt.Y()):
		return NewVector(0, pt.Y(), 0)
	default:
		return NewVector(0, 0, pt.Z())
	}
}

func (c *Cube) SurfaceAt(interaction Interaction) Surface {
	var (
		pt   = interaction.point
		side = SideFromPoint(pt)
		u, v = cubeMappers[side](pt)

		normal Vector
		dpdu   Vector
		dpdv   Vector
	)

	var max = absmax3(pt.X(), pt.Y(), pt.Z())
	switch {
	case max == abs(pt.X()):
		normal = NewVector(pt.X(), 0, 0)
		dpdu = NewVector(0, pt.Z(), pt.Y())
		dpdv = NewVector(0, pt.Y(), pt.Z())
	case max == abs(pt.Y()):
		normal = NewVector(0, pt.Y(), 0)
		dpdu = NewVector(-pt.Z(), 0, pt.X())
		dpdv = NewVector(pt.X(), 0, pt.Z())
	default:
		normal = NewVector(0, 0, pt.Z())
		dpdu = NewVector(-pt.Y(), pt.X(), 0)
		dpdv = NewVector(pt.X(), pt.Y(), 0)
	}

	switch side {
	case Up:
		//normal = NewVector(0, 1, 0)
		dpdu = NewVector(0, 0, 1)
		dpdv = NewVector(1, 0, 0)
	case Down:
		//normal = NewVector(0, -1, 0)
		dpdu = NewVector(-pt.Z(), 0, -pt.X())
		dpdv = NewVector(pt.X(), 0, pt.Z())
	case Left:
		// normal = NewVector(-1, 0, 0)
		dpdu = NewVector(0, -pt.Z(), pt.Y())
		dpdv = NewVector(0, pt.Y(), pt.Z())
	case Right:
		// normal = NewVector(1, 0, 0)
		dpdu = NewVector(0, 0, -1)
		dpdv = NewVector(0, -1, 0)
	case Front:
		// normal = NewVector(0, 0, 1)
		dpdu = NewVector(pt.Y(), pt.X(), 0)
		dpdv = NewVector(pt.X(), pt.Y(), 0)
	case Back:
		// normal = NewVector(0, 0, 1)
		dpdu = NewVector(pt.Y(), pt.X(), 0)
		dpdv = NewVector(pt.X(), pt.Y(), 0)
	}

	// dpdu = dpdv.Cross(normal).Forward(normal)
	// DPDU:   NewVector(-pt.Z(), 0, pt.X()),
	// DPDV:   NewVector(pt.X(), 0, pt.Z()),

	dpdu, dpdv = coordinateSystem(normal)

	return Surface{
		Geometry: SurfaceGeometry{
			Normal: normal,
			UV:     UV{u, v},
		},
		Shading: SurfaceShading{
			Normal: normal,
			UV:     UV{u, v},
			DPDU:   dpdu,
			DPDV:   dpdv,
		},
	}
}

func (c *Cube) Bounds() (Point, Point) {
	return NewPoint(-1, -1, -1), NewPoint(1, 1, 1)
}

func (c *Cube) Lights() []Light {
	return nil
}

func (c *Cube) checkAxis(origin, direction Scalar) (min Scalar, max Scalar) {
	return checkAxis(origin, direction, -1, 1)
}

type Cylinder struct {
	shape
	bound
}

func NewCylinder() *Cylinder {
	return &Cylinder{
		shape: newShape(),
		bound: newBound(),
	}
}

func (cyl *Cylinder) normalAt(p Point) Vector {
	var (
		dist = p.X().Pow2() + p.Z().Pow2()
		max  = cyl.max - epsilon
		min  = cyl.min + epsilon
	)

	if dist < 1 && p.Y() > max {
		return NewVector(0, 1, 0)
	}

	if dist < 1 && p.Y() <= min {
		return NewVector(0, -1, 0)
	}

	return NewVector(p.X(), 0, p.Z())
}

func (cyl *Cylinder) NormalAt(p Point, intersection Intersection) Vector {
	return cyl.normalAt(p)
}

func (cyl *Cylinder) SurfaceAt(interaction Interaction) Surface {
	var n = cyl.normalAt(interaction.point)
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: n,
		},
		Shading: SurfaceShading{
			Normal: n,
		},
	}
}

func (cyl *Cylinder) Intersect(r Ray, intersections *Intersections) {
	var a = r.Direction().X().Pow2() + r.Direction().Z().Pow2()
	if a.Eq(0) {
		cyl.intersectCaps(r, intersections)
		return
	}

	var (
		b = (2 * r.Origin().X() * r.Direction().X()) +
			(2 * r.Origin().Z() * r.Direction().Z())

		c = r.Origin().X().Pow2() + r.Origin().Z().Pow2() - 1

		disc = b.Pow2() - (4 * a * c)
	)

	if disc < 0 {
		return
	}

	var (
		t0 = (-b - disc.Sqrt()) / (2 * a)
		t1 = (-b + disc.Sqrt()) / (2 * a)
	)

	if t0 > t1 {
		t0, t1 = t1, t0
	}

	var (
		y0 = r.Origin().Y() + r.Direction().Y()*t0
		y1 = r.Origin().Y() + r.Direction().Y()*t1
	)

	if cyl.min < y0 && y0 < cyl.max {
		intersections.Append(NewIntersection(cyl, t0))
	}

	if cyl.min < y1 && y1 < cyl.max {
		intersections.Append(NewIntersection(cyl, t1))
	}

	cyl.intersectCaps(r, intersections)

	return
}

func (cyl *Cylinder) Bounds() (Point, Point) {
	return NewPoint(-1, cyl.min, -1), NewPoint(1, cyl.max, 1)
}

func (cyl *Cylinder) Lights() []Light {
	return cyl.Lights()
}

// checks to see if intersection at t is within a radius of 1
// from the y axis
func (cyl *Cylinder) hitsCap(r Ray, t Scalar) bool {
	var (
		x = r.Origin().X() + r.Direction().X()*t
		z = r.Origin().Z() + r.Direction().Z()*t
	)

	return x.Pow2()+z.Pow2() <= 1
}

func (cyl *Cylinder) intersectCaps(r Ray, intersections *Intersections) {
	// caps only matter if cylinder is closed, and might possibly be
	// intersected by the ray
	if cyl.Opened() {
		return
	}

	if r.Direction().Y().Eq(0) {
		return
	}

	var (
		// just some shorthands to keep equations short
		oy = r.Origin().Y()    // Origin Y
		dy = r.Direction().Y() // Direction Y
	)

	// check for intersection with lower cap by intersecting
	// the ray with the plane at y = cyl.min
	if t := (cyl.min - oy) / dy; cyl.hitsCap(r, t) {
		intersections.Append(NewIntersection(cyl, t))
	}

	// check for intersection with lower cap by intersecting
	// the ray with the plane at y = cyl.min
	if t := (cyl.max - oy) / dy; cyl.hitsCap(r, t) {
		intersections.Append(NewIntersection(cyl, t))
	}
}

type Cone struct {
	shape
	bound
}

func NewCone() *Cone {
	return &Cone{
		shape: newShape(),
		bound: newBound(),
	}
}

func (co *Cone) Intersect(r Ray, intersections *Intersections) {
	var (
		dir = r.Direction()
		ori = r.Origin()
		a   = dir.X().Pow2() - dir.Y().Pow2() + dir.Z().Pow2()
		b   = (2 * ori.X() * dir.X()) -
			(2 * ori.Y() * dir.Y()) +
			(2 * ori.Z() * dir.Z())
		c    = ori.X().Pow2() - ori.Y().Pow2() + ori.Z().Pow2()
		disc = b.Pow2() - (4 * a * c)
	)

	if a.Eq(0) {
		if b.Eq(0) {
			return
		}
		intersections.Append(NewIntersection(co, -c/(2*b)))
	}

	if disc < 0 {
		return
	}

	var (
		t0 = (-b - disc.Sqrt()) / (2 * a)
		t1 = (-b + disc.Sqrt()) / (2 * a)
	)

	if t0 > t1 {
		t0, t1 = t1, t0
	}

	var (
		y0 = r.Origin().Y() + r.Direction().Y()*t0
		y1 = r.Origin().Y() + r.Direction().Y()*t1
	)

	if co.min < y0 && y0 < co.max {
		intersections.Append(NewIntersection(co, t0))
	}

	if co.min < y1 && y1 < co.max {
		intersections.Append(NewIntersection(co, t1))
	}

	co.intersectCaps(r, intersections)
}

func (co *Cone) normalAt(p Point) Vector {
	y := (p.X().Pow2() + p.Z().Pow2()).Sqrt()

	if p.Y() > 0 {
		y = -y
	}

	return NewVector(p.X(), y, p.Z())
}

func (co *Cone) NormalAt(p Point, intersection Intersection) Vector {
	return co.normalAt(p)
}

func (co *Cone) SurfaceAt(interaction Interaction) Surface {
	var n = co.normalAt(interaction.point)
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: n,
		},
		Shading: SurfaceShading{
			Normal: n,
		},
	}
}

func (co *Cone) Bounds() (min, max Point) {
	var limit = absmax(co.min, co.max)
	min = NewPoint(-limit, co.min, -limit)
	max = NewPoint(limit, co.max, limit)
	return
}

func (co *Cone) Lights() []Light {
	return nil
}

func (co *Cone) intersectCaps(r Ray, intersections *Intersections) {
	// caps only matter if cylinder is closed, and might possibly be
	// intersected by the ray
	if co.Opened() {
		return
	}

	if r.Direction().Y().Eq(0) {
		return
	}

	var (
		// just some shorthands to keep equations short
		oy = r.Origin().Y()    // Origin Y
		dy = r.Direction().Y() // Direction Y
	)

	// check for intersection with lower cap by intersecting
	// the ray with the plane at y = cyl.min
	if t := (co.min - oy) / dy; co.hitsCap(r, t, co.min) {
		intersections.Append(NewIntersection(co, t))
	}

	// check for intersection with lower cap by intersecting
	// the ray with the plane at y = cyl.min
	if t := (co.max - oy) / dy; co.hitsCap(r, t, co.max) {
		intersections.Append(NewIntersection(co, t))
	}
}

func (co *Cone) hitsCap(r Ray, t Scalar, y Scalar) bool {
	var (
		x = r.Origin().X() + r.Direction().X()*t
		z = r.Origin().Z() + r.Direction().Z()*t
	)

	return x.Pow2()+z.Pow2() <= y.Abs()
}

type Triangle struct {
	parent  Object
	corners [3]Point
	edges   [2]Vector
	normals [3]Vector

	smooth bool
}

func NewTriangle(p1, p2, p3 Point) *Triangle {
	var edges = [2]Vector{
		p2.PathFrom(p1),
		p3.PathFrom(p1),
	}

	return &Triangle{
		corners: [3]Point{p1, p2, p3},
		edges:   edges,
		normals: [3]Vector{edges[1].Cross(edges[0]).Normalize()},
	}
}

func NewSmoothTriange(p1, p2, p3 Point, n1, n2, n3 Vector) *Triangle {
	t := NewTriangle(p1, p2, p3)
	t.normals = [3]Vector{n1, n2, n3}
	t.smooth = true
	return t
}

func (t *Triangle) Transformation() Transformation {
	return Transformation{newTransformation()}
}

func (t *Triangle) SetParent(obj Object) {
	t.parent = obj
}

func (t *Triangle) Parent() Object {
	return t.parent
}

func (t *Triangle) Material() Material {
	if t.parent != nil {
		return t.Parent().Material()
	}
	return NewMaterialProperties()
}

func (t *Triangle) NormalAt(p Point, hit Intersection) Vector {
	return t.normalAt(hit)
}

var triangleIntersectTests uint64

func GetTriangleIntersectTestCount() uint64 {
	return atomic.LoadUint64(&triangleIntersectTests)
}

func (t *Triangle) normalAt(hit Intersection) Vector {
	if t.smooth {
		return t.normals[1].Mul(hit.uv.U).Add(
			t.normals[2].Mul(hit.uv.V),
		).Add(
			t.normals[0].Mul(1 - hit.uv.U - hit.uv.V),
		)
	}
	return t.normals[0]
}

func (t *Triangle) SurfaceAt(interaction Interaction) Surface {
	var n = t.normalAt(interaction.hit)
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: n,
		},
		Shading: SurfaceShading{
			Normal: n,
		},
	}
}

func (t *Triangle) Intersect(r Ray, intersections *Intersections) {
	// atomic.AddUint64(&triangleIntersectTests, 1)

	// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
	var (
		dirCrossE2 = r.Direction().Cross(t.edges[1])
		det        = t.edges[0].Dot(dirCrossE2)
	)

	if det.Abs() < epsilon {
		return
	}

	var (
		f          = 1.0 / det
		p1ToOrigin = r.Origin().PathFrom(t.corners[0])
		u          = f * dirCrossE2.Dot(p1ToOrigin)
	)

	if u < 0 || u > 1 {
		return
	}

	var (
		originCrossE1 = p1ToOrigin.Cross(t.edges[0])
		v             = f * r.Direction().Dot(originCrossE1)
	)

	if v < 0 || (u+v) > 1 {
		return
	}

	time := f * t.edges[1].Dot(originCrossE1)

	intersections.Append(NewIntersectionWithUV(t, time, u, v))
}

func (t *Triangle) Points() []Point {
	return t.corners[:]
}

func (t *Triangle) Normals() []Vector {
	if t.smooth {
		return t.normals[:]
	}

	return t.normals[:1]
}

func (t *Triangle) Bounds() (min, max Point) {
	b := NewBoundingBox()

	for _, p := range t.Points() {
		b.Include(p)
	}

	return b.Bounds()
}

func (t *Triangle) Shadows() bool {
	if t.Parent() == nil {
		return true
	}
	return t.Parent().Shadows()
}

func (t *Triangle) Sample(RenderCtx) ObjectSample {
	panic("implement me")
}

func (t *Triangle) PdfAt(RenderCtx, Interaction, Vector) Scalar {
	panic("implement me")
}

func (t *Triangle) Area() Scalar {
	panic("implement me")
}

func (t *Triangle) Edges() []Vector {
	return t.edges[:]
}

type Disk struct {
	shape

	radius      Scalar
	innerRaidus Scalar
	height      Scalar
	phiMax      Scalar
}

func NewDisk(height, radius, innerRadius, phiMax Scalar) *Disk {
	return &Disk{
		shape: newShape(),

		radius:      radius,
		innerRaidus: innerRadius,
		height:      height,
		phiMax:      radians(phiMax.Clamp(0, 360)),
	}
}

func (d *Disk) Bounds() (Point, Point) {
	b := NewBoundedBox(
		NewPoint(-d.radius, -d.radius, d.height),
		NewPoint(d.radius, d.radius, d.height),
	)

	return b.Bounds()
}

func (d *Disk) NormalAt(Point, Intersection) Vector {
	return NewVector(0, 1, 0)
}

func (d *Disk) SurfaceAt(interaction Interaction) Surface {
	var (
		p      = interaction.point
		rhit   = Scalar.Sqrt(p.X().Pow2() + p.Z().Pow2())
		radius = d.innerRaidus - d.radius

		dpdu   = NewVector(-d.phiMax*p.Z(), 0, d.phiMax*p.X())
		dpdv   = NewVector(p.X(), 0, p.Z()).Mul(radius / rhit)
		normal = dpdu.Cross(dpdv).Normalize().Forward(NewVector(0, 1, 0))
		//normal = NewVector(0, 1, 0)
	)
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: normal,
			UV:     interaction.uv,
		},
		Shading: SurfaceShading{
			Normal: normal,
			UV:     interaction.uv,
			DPDU:   dpdu,
			DPDV:   dpdv,
		},
	}
}

func (d *Disk) Intersect(r Ray, intersections *Intersections) {
	if r.Direction().Y().Eq(0) {
		return
	}

	hit := (d.height - r.Origin().Y()) / r.Direction().Y()

	p := r.Position(hit)

	dist2 := p.X().Pow2() + p.Z().Pow2()

	if dist2 > d.radius.Pow2() || dist2 < d.innerRaidus.Pow2() {
		return
	}

	phi := Scalar(math.Atan2(float64(p.Z()), float64(p.X())))
	if phi < 0 {
		phi += 2 * math.Pi
	}

	if phi > d.phiMax {
		return
	}

	var (
		u    = phi / d.phiMax
		rhit = dist2.Sqrt()
		v    = (d.radius - rhit) / (d.radius - d.innerRaidus) // 1 - ((rhit - d.innerRaidus) / (d.radius - d.innerRaidus))
	)

	x := NewIntersectionWithUV(d, hit, u, v)
	// todo: figure out how to implement this hack without being so hacky
	// x.p = NewPoint(x.p.X(), d.height, x.p.Z())

	intersections.Append(x)
}

// TODO doesn't handle when innerRadius > 0
// http://www.pbr-book.org/3ed-2018/Light_Transport_I_Surface_Reflection/Sampling_Light_Sources.html#SamplingShapes
func (d *Disk) Sample(ctx RenderCtx) ObjectSample {
	var (
		pd   = concentricSampleDisk(ctx)
		pobj = NewPoint(pd.U()*d.radius, d.height, pd.V()*d.radius)
	)

	return ObjectSample{
		Point:  pobj,
		Normal: NewVector(0, 1, 0),
		PDF:    1 / d.Area(),
	}
}

func (d *Disk) PdfAt(ctx RenderCtx, interaction Interaction, out Vector) Scalar {
	return objectPDF(d, interaction, out)
}

func (d *Disk) Area() Scalar {
	return d.phiMax * 0.5 * (d.radius.Pow2() - d.innerRaidus.Pow2())
}

func (d *Disk) Pdf() Scalar {
	return 1 / d.Area()
}

type Rectangle struct {
	shape
}

func NewRectangle() *Rectangle {
	return &Rectangle{
		shape: newShape(),
	}
}

func (r *Rectangle) Bounds() (Point, Point) {
	return NewPoint(-1, 0, -1), NewPoint(1, 0, 1)
}

func (r *Rectangle) NormalAt(Point, Intersection) Vector {
	return NewVector(0, 1, 0)
}

func (r *Rectangle) SurfaceAt(interaction Interaction) Surface {
	var pt = interaction.point
	return Surface{
		Geometry: SurfaceGeometry{
			Normal: NewVector(0, 1, 0),
			UV:     UV{pt.X().Mod(1), pt.Z().Mod(1)},
		},
		Shading: SurfaceShading{
			Normal: NewVector(0, 1, 0),
			DPDU:   NewVector(-pt.Z(), 0, pt.X()),
			DPDV:   NewVector(pt.X(), 0, pt.Z()),
		},
	}
}

func (r *Rectangle) Intersect(ray Ray, intersections *Intersections) {
	if ray.direction.Y().Abs() < epsilon {
		return
	}

	var (
		t = -ray.Origin().Y() / ray.direction.Y()
		x = ray.origin.X() + t*ray.direction.X()
		z = ray.origin.Z() + t*ray.direction.Z()
	)

	if x.Abs() > 1 {
		return
	}
	if z.Abs() > 1 {
		return
	}

	intersections.Append(
		NewIntersection(r, t),
	)
}
