package raytracer

import (
	"math"
	"testing"
)

func TestLights(t *testing.T) {
	t.Run("construction", func(t *testing.T) {
		pos := NewPoint(0, 0, 0)
		intensity := NewColor(1, 1, 1)

		light := NewPointLight(pos, intensity)

		if have := light.Position(); !have.Eq(pos) {
			t.Errorf("incorrect position for light. have=%v want=%v", have, pos)
		}

		if have := light.Intensity(); !have.Eq(intensity) {
			t.Errorf("incorrect intensity for light. have=%v want=%v", have, intensity)
		}
	})
}

func TestLighting(t *testing.T) {
	m := NewMaterialProperties()
	pos := NewPoint(0, 0, 0)
	sq2 := Scalar(math.Sqrt2 / 2)
	type lightingTest struct {
		name string

		eye    Vector
		normal Vector
		light  PointLight
		want   Color
	}

	tests := []lightingTest{
		{
			name:   "lighting with eye between the light and the surface",
			eye:    NewVector(0, 0, -1),
			normal: NewVector(0, 0, -1),
			light:  NewPointLight(NewPoint(0, 0, -10), NewColor(1, 1, 1)),
			want:   NewColor(1.9, 1.9, 1.9),
		},
		{
			name:   "lighting with eye between light and surface - eye offset 45 degrees",
			eye:    NewVector(0, sq2, -sq2),
			normal: NewVector(0, 0, -1),
			light:  NewPointLight(NewPoint(0, 0, -10), NewColor(1, 1, 1)),
			want:   NewColor(1, 1, 1),
		},
		{
			name:   "lighting with eye opposite surface - light offset 45 degrees",
			eye:    NewVector(0, 0, -1),
			normal: NewVector(0, 0, -1),
			light:  NewPointLight(NewPoint(0, 10, -10), NewColor(1, 1, 1)),
			want:   NewColor(0.7364, 0.7364, 0.7364),
		},
		{
			name:   "lighting with eye in path of reflection vector",
			eye:    NewVector(0, -sq2, -sq2),
			normal: NewVector(0, 0, -1),
			light:  NewPointLight(NewPoint(0, 10, -10), NewColor(1, 1, 1)),
			want:   NewColor(1.6364, 1.6364, 1.6364),
		},
		{
			name:   "lighting with light behind surface",
			eye:    NewVector(0, 0, -1),
			normal: NewVector(0, 0, -1),
			light:  NewPointLight(NewPoint(0, 0, 10), NewColor(1, 1, 1)),
			want:   NewColor(0.1, 0.1, 0.1),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			have := Lighting(m, NewSphere(), tt.light, pos, tt.eye, tt.normal, false, UV{})
			if !have.Eq(tt.want) {
				t.Errorf("incorrect color for lighting. have=%v want=%v", have, tt.want)
			}
		})
	}
}
