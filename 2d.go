package raytracer

type Tuple2D [2]Scalar

func (t Tuple2D) Eq(o Tuple2D) bool { return teq2D(t, o) }

func teq2D(a, b Tuple2D) bool {
	return eq(a[0], b[0]) && eq(a[1], b[1])
}

func tadd2D(a, b Tuple2D) Tuple2D {
	return Tuple2D{a[0] + b[0], a[1] + b[1]}
}

func tsub2D(a, b Tuple2D) Tuple2D {
	return Tuple2D{a[0] - b[0], a[1] - b[1]}
}

func tmul2D(a Tuple2D, s Scalar) Tuple2D {
	return Tuple2D{a[0] * s, a[1] * s}
}

func tdiv2D(a Tuple2D, s Scalar) Tuple2D {
	return Tuple2D{a[0] / s, a[1] / s}
}

func tdot2D(a, b Tuple2D) Scalar {
	return a[0]*b[0] + a[1]*b[1]
}

type Point2D struct {
	tuple Tuple2D
}

func NewPoint2D(u, v Scalar) Point2D {
	return Point2D{Tuple2D{u, v}}
}

func (p Point2D) Eq(o Point2D) bool {
	return teq2D(p.tuple, o.tuple)
}

func (p Point2D) TravelTo(v Vector2D) Point2D {
	return Point2D{tadd2D(p.tuple, v.tuple)}
}

func (p Point2D) TravelAway(v Vector2D) Point2D {
	return Point2D{tsub2D(p.tuple, v.tuple)}
}

func (p Point2D) PathFrom(o Point2D) Vector2D {
	return Vector2D{tsub2D(p.tuple, o.tuple)}
}

func (p Point2D) Mul(s Scalar) Point2D {
	return Point2D{tuple: tmul2D(p.tuple, s)}
}

func (p Point2D) U() Scalar {
	return p.tuple[0]
}

func (p Point2D) V() Scalar {
	return p.tuple[1]
}

type Vector2D struct {
	tuple Tuple2D
}

func NewVector2D(x, y Scalar) Vector2D {
	return Vector2D{tuple: Tuple2D{x, y}}
}

func (v Vector2D) Eq(o Vector2D) bool {
	return teq2D(v.tuple, o.tuple)
}

func (v Vector2D) Add(o Vector2D) Vector2D {
	return Vector2D{tadd2D(v.tuple, o.tuple)}
}

func (v Vector2D) Sub(o Vector2D) Vector2D {
	return Vector2D{tsub2D(v.tuple, o.tuple)}
}

func (v Vector2D) Mul(s Scalar) Vector2D {
	return Vector2D{tmul2D(v.tuple, s)}
}

func (v Vector2D) Negate() Vector2D {
	return Vector2D{}.Sub(v)
}

func (v Vector2D) X() Scalar {
	return v.tuple[0]
}

func (v Vector2D) Y() Scalar {
	return v.tuple[1]
}

func (v Vector2D) Magnitude() Scalar {
	return (v.X().Pow2() + v.Y().Pow2()).Sqrt()
}

func (v Vector2D) Normalize() Vector2D {
	return Vector2D{tdiv2D(v.tuple, v.Magnitude())}
}

func (v Vector2D) Dot(o Vector2D) Scalar {
	return tdot2D(v.tuple, o.tuple)
}
