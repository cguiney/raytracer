package raytracer

import "testing"

func TestMeshTriangle(t *testing.T) {
	var (
		b  MeshBuilder
		p1 = NewPoint(0, 1, 0)
		p2 = NewPoint(-1, 0, 0)
		p3 = NewPoint(1, 0, 0)
	)

	b.DefinePolygon([]Face{
		{Vertex: b.DefineVertex(p1)},
		{Vertex: b.DefineVertex(p2)},
		{Vertex: b.DefineVertex(p3)},
	})

	m := b.Build()
	tri := m.Groups()[0].Triangles()[0]

	t.Run("construction", func(t *testing.T) {
		if !tri.Points()[0].Eq(p1) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.Points()[0], p1)
		}
		if !tri.Points()[1].Eq(p2) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.Points()[1], p2)
		}
		if !tri.Points()[2].Eq(p3) {
			t.Errorf("incorrect corner. have=%v want=%v", tri.Points()[2], p3)
		}

		if want := NewVector(-1, -1, 0); !tri.Edges()[0].Eq(want) {
			t.Errorf("incorrect edge. have=%v want=%v", tri.Edges()[0], want)
		}
		if want := NewVector(1, -1, 0); !tri.Edges()[1].Eq(want) {
			t.Errorf("incorrect edge. have=%v want=%v", tri.Normals()[1], want)
		}

		if want := NewVector(0, 0, -1); !tri.Normals()[0].Eq(want) {
			t.Errorf("incorrect normal. have=%v want=%v", tri.Normals()[0], want)
		}
	})

	t.Run("normal of triangle", func(t *testing.T) {

		if have := tri.NormalAt(NewPoint(0, 0.5, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
		if have := tri.NormalAt(NewPoint(-0.5, 0.75, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
		if have := tri.NormalAt(NewPoint(0.5, 0.25, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
	})

	t.Run("uv of triangle", func(t *testing.T) {

		if have := tri.NormalAt(NewPoint(0, 0.5, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
		if have := tri.NormalAt(NewPoint(-0.5, 0.75, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
		if have := tri.NormalAt(NewPoint(0.5, 0.25, 0), Intersection{}); !have.Eq(tri.Normals()[0]) {
			t.Errorf("incorrect normal. have=%v want=%v", have, tri.Normals()[0])
		}
	})

	t.Run("intersecting a ray parallel to triangle", func(t *testing.T) {
		r := NewRay(NewPoint(0, -1, -2), NewVector(0, 1, 0))

		have := Intersections{}
		tri.Intersect(r, &have)

		if len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray misses p1-p3 edge", func(t *testing.T) {
		r := NewRay(NewPoint(1, 1, -2), NewVector(0, 0, 1))
		have := Intersections{}
		tri.Intersect(r, &have)

		if len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray misses p1-p2 edge", func(t *testing.T) {
		r := NewRay(NewPoint(-1, 1, -2), NewVector(0, 0, 1))

		have := Intersections{}
		tri.Intersect(r, &have)

		if len(have) != 0 {
			t.Errorf("expected no intersections. got=%d", len(have))
		}
	})

	t.Run("ray strikes triangle", func(t *testing.T) {
		r := NewRay(NewPoint(0, 0.5, -2), NewVector(0, 0, 1))
		xs := Intersections{}

		tri.Intersect(r, &xs)
		if len(xs) != 1 {
			t.Errorf("expected exactly one intersection. got=%d", len(xs))
		}

		if xs[0].Time() != 2 {
			t.Errorf("incorrect time for intersection. got=%f", xs[0].Time())
		}
	})
}

func TestSmoothMeshTriangle(t *testing.T) {
	var (
		b MeshBuilder

		n1 = b.DefineNormal(NewVector(0, 1, 0))
		n2 = b.DefineNormal(NewVector(-1, 0, 0))
		n3 = b.DefineNormal(NewVector(1, 0, 0))
	)

	b.DefinePolygon([]Face{
		{Vertex: b.DefineVertex(NewPoint(0, 1, 0)), Normal: &n1},
		{Vertex: b.DefineVertex(NewPoint(-1, 0, 0)), Normal: &n2},
		{Vertex: b.DefineVertex(NewPoint(1, 0, 0)), Normal: &n3},
	})

	m := b.Build()
	tri := &m.Groups()[0].Triangles()[0]

	t.Run("intersection with smooth triangle stores UV", func(t *testing.T) {
		r := NewRay(NewPoint(-0.2, 0.3, -2), NewVector(0, 0, 1))

		xs := Intersections{}
		tri.Intersect(r, &xs)

		if !xs[0].U().Eq(0.45) {
			t.Errorf("incorrect U. have=%f want=%f", xs[0].U(), 0.45)
		}

		if !xs[0].V().Eq(0.25) {
			t.Errorf("incorrect V. have=%f want=%f", xs[0].V(), 0.25)
		}

	})

	t.Run("uses UV to interpolate the normal", func(t *testing.T) {
		x := NewIntersectionWithUV(tri, 1, 0.45, 0.25)
		n := NormalAt(tri, NewPoint(0, 0, 0), x)

		want := NewVector(-0.5547, 0.83205, 0)

		if !n.Eq(want) {
			t.Errorf("incorrect normal for intersection. have=%v want=%v", n, want)
		}
	})

	t.Run("preparing normal on smooth triangle", func(t *testing.T) {
		r := NewRay(NewPoint(-0.2, 0.3, -2), NewVector(0, 0, 1))
		x := NewIntersectionWithUV(tri, 1, 0.45, 0.25)

		state := NewSurfaceInteraction(x, r, x)
		want := NewVector(-0.5547, 0.83205, 0)

		if !state.normal.Eq(want) {
			t.Errorf("incorrect normal for intersection state. have=%v want=%v", state.normal, want)
		}
	})
}

func TestMeshGroup(t *testing.T) {
	var (
		b  MeshBuilder
		p1 = NewPoint(0, 1, 0)
		p2 = NewPoint(-1, 0, 0)
		p3 = NewPoint(1, 0, 0)
	)

	b.DefinePolygon([]Face{
		{Vertex: b.DefineVertex(p1)},
		{Vertex: b.DefineVertex(p2)},
		{Vertex: b.DefineVertex(p3)},
	})

	m := b.Build()

	mg := m.Groups()[0]

	g := NewGroup()
	g.AddObject(NewTriangle(p1, p2, p3))

	rays := []Ray{
		NewRay(NewPoint(0, 0, -5), NewVector(0, 0, 1)),
		NewRay(NewPoint(0, 0, -1), NewVector(0, 0, 1)),
		NewRay(NewPoint(0, 0, -1), NewVector(0, 0, 2)),
		NewRay(NewPoint(0.5, 0.5, 0.5), NewVector(0.5, 0.5, 0.5)),
		NewRay(NewPoint(0.1, 0.1, 0.1), NewVector(0.5, 0.5, 0.5)),
		NewRay(NewPoint(0, 0.5, -2), NewVector(0, 0, 1)),
	}

	for _, r := range rays {
		have := Intersections{}
		mg.Intersect(r, &have)

		want := Intersections{}
		g.Intersect(r, &want)

		if len(have) != len(want) {
			t.Errorf("differing number of intersections, have=%d want=%d", len(have), len(want))
		}

		for i, x := range have {
			if x.Time() != want[i].Time() {
				t.Errorf("intersection %d occurs at different time. have=%s want=%s", i, x.Time(), want[i].Time())
			}

			if !x.U().Eq(want[i].U()) {
				t.Errorf("intersection %d has different U values. have=%s want=%s", i, x.U(), want[i].U())
			}

			if !x.V().Eq(want[i].V()) {
				t.Errorf("intersection %d has different V values. have=%s want=%s", i, x.V(), want[i].V())
			}
		}
	}

}

func TestCoordinateSystem(t *testing.T) {
	x := NewVector(1, 0, 0)

	y, z := coordinateSystem(x)

	t.Logf("y: %v", y)
	t.Logf("z: %v", z)
}
