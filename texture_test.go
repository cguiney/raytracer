package raytracer

import (
	"fmt"
	"image"
	"math"
	"strings"
	"testing"

	// make sure this is imported for the init() side effect
	_ "gitlab.com/cguiney/raytracer/internal/ppm"
)

type uvPatternTest struct {
	u, v Scalar
	want Color
}

func runUVTests(t *testing.T, pattern Texture, tests []uvPatternTest) {
	for i, tt := range tests {
		tt := tt
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			si := SurfaceInteraction{
				Surface: Surface{
					Shading: SurfaceShading{
						UV: UV{tt.u, tt.v},
					},
				},
			}
			if have := pattern.At(si); !have.Eq(tt.want) {
				t.Errorf("incorrect color. have=%v want=%v", have, tt.want)
			}
		})
	}
}

func TestUVCheckers(t *testing.T) {
	checkers := CheckersTexture(2, 2, Black, White)

	runUVTests(t, checkers, []uvPatternTest{
		{0.0, 0.0, Black},
		{0.5, 0.0, White},
		{0.0, 0.5, White},
		{0.5, 0.5, Black},
		{1.0, 1.0, Black},
	})
}

type uvMapTest struct {
	given Point
	wantU Scalar
	wantV Scalar
}

func runUVMapTest(t *testing.T, m MapperFunc, tests []uvMapTest) {
	t.Helper()
	for _, tt := range tests {
		tt := tt

		haveU, haveV := m.Map(tt.given)
		if !haveU.Eq(tt.wantU) {
			t.Errorf("incorrect U. have=%s want=%s", haveU, tt.wantU)
		}

		if !haveV.Eq(tt.wantV) {
			t.Errorf("incorrect V. have=%s want=%s", haveV, tt.wantV)
		}
	}
}

func TestSphericalMap(t *testing.T) {
	runUVMapTest(t, SphericalMap, []uvMapTest{
		{NewPoint(0, 0, -1), 0.0, 0.5},
		{NewPoint(1, 0, 0), 0.25, 0.5},
		{NewPoint(0, 0, 1), 0.5, 0.5},
		{NewPoint(-1, 0, 0), 0.75, 0.5},
		{NewPoint(0, 1, 0), 0.5, 1.0},
		{NewPoint(0, -1, 0), 0.5, 0},
		{NewPoint(math.Sqrt2/2, math.Sqrt2/2, 0), 0.25, 0.75},
	})
}

func TestTextureMap(t *testing.T) {
	t.Run("spherical mapping", func(t *testing.T) {
		tests := []patternTest{
			{NewPoint(0.4315, 0.4670, 0.7719), White},
			{NewPoint(-0.9654, 0.2552, -0.0534), Black},
			{NewPoint(0.1039, 0.7090, 0.6975), White},
			{NewPoint(-0.4986, -0.7856, -0.3663), Black},
			{NewPoint(-0.0317, -0.9395, 0.3411), Black},
			{NewPoint(0.4809, -0.7721, 0.4154), Black},
			{NewPoint(0.0285, -0.9612, -0.2745), Black},
			{NewPoint(-0.5734, -0.2162, -0.7903), White},
			{NewPoint(0.7688, -0.1470, 0.6223), Black},
			{NewPoint(-0.7652, 0.2175, 0.6060), Black},
		}

		pattern := NewTextureMap(CheckersTexture(16, 8, Black, White), MapperFunc(SphericalMap))

		runPatternTests(t, pattern, tests)
	})

	t.Run("planar mapping", func(t *testing.T) {
		tests := []uvMapTest{
			{NewPoint(0.25, 0, 0.5), 0.25, 0.5},
			{NewPoint(0.25, 0, -0.25), 0.25, 0.75},
			{NewPoint(0.25, 0.5, -0.25), 0.25, 0.75},
			{NewPoint(1.25, 0, 0.5), 0.25, 0.5},
			{NewPoint(0.25, 0, -1.75), 0.25, 0.25},
			{NewPoint(1, 0, -1), 0.0, 0.0},
			{NewPoint(0, 0, 0), 0.0, 0.0},
		}

		runUVMapTest(t, PlanarMap, tests)
	})

	t.Run("cylindrical mapping", func(t *testing.T) {
		tests := []uvMapTest{
			{NewPoint(0, 0, -1), 0.0, 0.0},
			{NewPoint(0, 0.5, -1), 0.0, 0.5},
			{NewPoint(0, 1, -1), 0.0, 0.0},
			{NewPoint(0.70711, 0.5, -0.70711), 0.125, 0.5},
			{NewPoint(1, 0.5, 0), 0.25, 0.5},
			{NewPoint(0.70711, 0.5, 0.70711), 0.375, 0.5},
			{NewPoint(0, -0.25, 1), 0.5, 0.75},
			{NewPoint(-0.70711, 0.5, 0.70711), 0.625, 0.5},
			{NewPoint(-1, 1.25, 0), 0.75, 0.25},
			{NewPoint(-0.70711, 0.5, -0.70711), 0.875, 0.5},
		}

		runUVMapTest(t, CylinderMap, tests)
	})

	t.Run("uv align check texture", func(t *testing.T) {
		var (
			main = White
			ul   = Red
			ur   = Red
			bl   = Green
			br   = Green

			pattern = AlignCheckTexture(main, ul, ur, bl, br)
		)
		tests := []uvPatternTest{
			{0.5, 0.5, main},
			{0.1, 0.9, ul},
			{0.9, 0.9, ur},
			{0.1, 0.1, bl},
			{0.9, 0.1, br},
		}

		runUVTests(t, pattern, tests)
	})

	t.Run("cubic mapping", func(t *testing.T) {
		type cubicPatternTest struct {
			fn    TextureFunc
			given Point
			want  Color
		}

		var (
			L = AlignCheckTexture(Yellow, Cyan, Red, Blue, Brown)
			F = AlignCheckTexture(Cyan, Red, Yellow, Brown, Green)
			R = AlignCheckTexture(Red, Yellow, Purple, Green, White)
			B = AlignCheckTexture(Green, Purple, Cyan, White, Blue)
			U = AlignCheckTexture(Brown, Cyan, Purple, Red, Yellow)
			D = AlignCheckTexture(Purple, Brown, Green, Blue, White)
		)
		tests := []cubicPatternTest{
			{L, NewPoint(-1, 0, 0), Yellow},
			{L, NewPoint(-1, 0.9, -0.9), Cyan},
			{L, NewPoint(-1, 0.9, 0.9), Red},
			{L, NewPoint(-1, -0.9, -0.9), Blue},
			{L, NewPoint(-1, -0.9, 0.9), Brown},
			{F, NewPoint(0, 0, 1), Cyan},
			{F, NewPoint(-0.9, 0.9, 1), Red},
			{F, NewPoint(0.9, 0.9, 1), Yellow},
			{F, NewPoint(-0.9, -0.9, 1), Brown},
			{F, NewPoint(0.9, -0.9, 1), Green},
			{R, NewPoint(1, 0, 0), Red},
			{R, NewPoint(1, 0.9, 0.9), Yellow},
			{R, NewPoint(1, 0.9, -0.9), Purple},
			{R, NewPoint(1, -0.9, 0.9), Green},
			{R, NewPoint(1, -0.9, -0.9), White},
			{B, NewPoint(0, 0, -1), Green},
			{B, NewPoint(0.9, 0.9, -1), Purple},
			{B, NewPoint(-0.9, 0.9, -1), Cyan},
			{B, NewPoint(0.9, -0.9, -1), White},
			{B, NewPoint(-0.9, -0.9, -1), Blue},
			{U, NewPoint(0, 1, 0), Brown},
			{U, NewPoint(-0.9, 1, -0.9), Cyan},
			{U, NewPoint(0.9, 1, -0.9), Purple},
			{U, NewPoint(-0.9, 1, 0.9), Red},
			{U, NewPoint(0.9, 1, 0.9), Yellow},
			{D, NewPoint(0, -1, 0), Purple},
			{D, NewPoint(-0.9, -1, 0.9), Brown},
			{D, NewPoint(0.9, -1, 0.9), Green},
			{D, NewPoint(-0.9, -1, -0.9), Blue},
			{D, NewPoint(0.9, -1, -0.9), White},
		}

		for i, tt := range tests {
			pattern := NewTextureMap(tt.fn, MapperFunc(CubeMap))

			si := SurfaceInteraction{
				Surface:     Surface{},
				Interaction: Interaction{
					point: tt.given,
				},
			}

			if have := pattern.At(si); !have.Eq(tt.want) {
				t.Errorf(
					"incorrect color at point %v for test %d. have=%s want=%s",
					tt.given, i, have, tt.want)
			}
		}
	})
}

func TestFaceFromPoint(t *testing.T) {
	type faceTest struct {
		given Point
		want  Side
	}

	tests := []faceTest{
		{NewPoint(-1, 0.5, -0.25), Left},
		{NewPoint(1.1, -0.75, 0.8), Right},
		{NewPoint(0.1, 0.6, 0.9), Front},
		{NewPoint(-0.7, 0, -2), Back},
		{NewPoint(0.5, 1, 0.9), Up},
		{NewPoint(-0.2, -1.3, 1.1), Down},
	}

	for _, tt := range tests {
		if have := SideFromPoint(tt.given); have != tt.want {
			t.Errorf("incorrect face for point %v. have=%v want=%v", tt.given, have, tt.want)
		}
	}
}

func TestCubeMappers(t *testing.T) {

	runUVMapTest(t, cubeMappers[Front], []uvMapTest{
		{NewPoint(-0.5, 0.5, 1), 0.25, 0.75},
		{NewPoint(0.5, -0.5, 1), 0.75, 0.25},
	})

	runUVMapTest(t, cubeMappers[Back], []uvMapTest{
		{NewPoint(0.5, 0.5, -1), 0.25, 0.75},
		{NewPoint(-0.5, -0.5, -1), 0.75, 0.25},
	})

	runUVMapTest(t, cubeMappers[Left], []uvMapTest{
		{NewPoint(-1, 0.5, -0.5), 0.25, 0.75},
		{NewPoint(-1, -0.5, 0.5), 0.75, 0.25},
	})

	runUVMapTest(t, cubeMappers[Right], []uvMapTest{
		{NewPoint(1, 0.5, 0.5), 0.25, 0.75},
		{NewPoint(1, -0.5, -0.5), 0.75, 0.25},
	})

	runUVMapTest(t, cubeMappers[Up], []uvMapTest{
		{NewPoint(-0.5, 1, -0.5), 0.25, 0.75},
		{NewPoint(0.5, 1, 0.5), 0.75, 0.25},
	})

	runUVMapTest(t, cubeMappers[Down], []uvMapTest{
		{NewPoint(-0.5, -1, 0.5), 0.25, 0.75},
		{NewPoint(0.5, -1, -0.5), 0.75, 0.25},
	})
}

func TestUVImage(t *testing.T) {
	t.Run("image based uv texture", func(t *testing.T) {
		const input = `P3
    10 10
    10
    0 0 0  1 1 1  2 2 2  3 3 3  4 4 4  5 5 5  6 6 6  7 7 7  8 8 8  9 9 9
    1 1 1  2 2 2  3 3 3  4 4 4  5 5 5  6 6 6  7 7 7  8 8 8  9 9 9  0 0 0
    2 2 2  3 3 3  4 4 4  5 5 5  6 6 6  7 7 7  8 8 8  9 9 9  0 0 0  1 1 1
    3 3 3  4 4 4  5 5 5  6 6 6  7 7 7  8 8 8  9 9 9  0 0 0  1 1 1  2 2 2
    4 4 4  5 5 5  6 6 6  7 7 7  8 8 8  9 9 9  0 0 0  1 1 1  2 2 2  3 3 3
    5 5 5  6 6 6  7 7 7  8 8 8  9 9 9  0 0 0  1 1 1  2 2 2  3 3 3  4 4 4
    6 6 6  7 7 7  8 8 8  9 9 9  0 0 0  1 1 1  2 2 2  3 3 3  4 4 4  5 5 5
    7 7 7  8 8 8  9 9 9  0 0 0  1 1 1  2 2 2  3 3 3  4 4 4  5 5 5  6 6 6
    8 8 8  9 9 9  0 0 0  1 1 1  2 2 2  3 3 3  4 4 4  5 5 5  6 6 6  7 7 7
    9 9 9  0 0 0  1 1 1  2 2 2  3 3 3  4 4 4  5 5 5  6 6 6  7 7 7  8 8 8
`

		img, _, err := image.Decode(strings.NewReader(input))
		if err != nil {
			t.Fatal("unexpected error decoding image: ", err)
		}

		pattern := NewImage(img)

		tests := []uvPatternTest{
			{0, 0, NewColor(0.901961, 0.901961, 0.901961)},
			{0.3, 0, NewColor(0.2, 0.2, 0.2)},
			{0.6, 0.3, NewColor(0.101961, 0.101961, 0.101961)},
			{1, 1, NewColor(0.901961, 0.901961, 0.901961)},
		}

		runUVTests(t, pattern, tests)
	})
}
