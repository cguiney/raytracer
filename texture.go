package raytracer

import (
	"image"
	"math"
)

type Texture interface {
	At(SurfaceInteraction) Color
}

type ScalarTexture interface {
	ScalarAt(SurfaceInteraction) Scalar
}

type TextureFunc func(SurfaceInteraction) Color

func (f TextureFunc) At(si SurfaceInteraction) Color {
	return f(si)
}

type Mapper interface {
	Map(p Point) (u Scalar, v Scalar)
}

type MapperFunc func(p Point) (Scalar, Scalar)

func (f MapperFunc) Map(p Point) (Scalar, Scalar) {
	return f(p)
}

type MapTexture struct {
	texture Texture
	mapper  Mapper
	transformation
}

func NewTextureMap(pattern Texture, mapper Mapper) *MapTexture {
	return &MapTexture{
		texture:        pattern,
		mapper:         mapper,
		transformation: newTransformation(),
	}
}

func (t *MapTexture) At(si SurfaceInteraction) Color {
	si.point = si.point.transform(t.Transformation())
	si.Shading.UV.U, si.Shading.UV.V = t.mapper.Map(si.point)
	return t.texture.At(si)
}

type TextureTransform struct {
	texture Texture
	transformation
}

func NewTextureTransform(t Texture) *TextureTransform {
	return &TextureTransform{
		texture:        t,
		transformation: newTransformation(),
	}
}

func (t *TextureTransform) At(si SurfaceInteraction) Color {
	si.point = si.point.transform(t.Transformation())
	return t.texture.At(si)
}

type ScalarTextureTransform struct {
	texture ScalarTexture
	transformation
}

func NewScalarTextureTransform(t ScalarTexture) *ScalarTextureTransform {
	return &ScalarTextureTransform{
		texture:        t,
		transformation: newTransformation(),
	}
}

func (t *ScalarTextureTransform) ScalarAt(si SurfaceInteraction) Scalar {
	si.point = si.point.transform(t.Transformation())
	return t.texture.ScalarAt(si)
}

func SphericalMap(p Point) (u Scalar, v Scalar) {
	var (
		// azimuth angle
		// -pi < theta <= pi
		// angle increases clockwise as viewed from above
		theta = math.Atan2(float64(p.X()), float64(p.Z()))

		// vector pointing from sphere's origin to p
		// happens to be equal to sphere's radius
		vec    = NewVector(p.X(), p.Y(), p.Z())
		radius = vec.Magnitude()

		// polar angle
		// 0 <= phi <= pi
		phi = math.Acos(float64(p.Y() / radius))

		// - 0.5 < rawU <= 0.5
		rawU = theta / (2 * math.Pi)
	)

	// 0 <= u < 1
	// fixes u so that it increases counter-clockwise as viewed from above
	u = Scalar(1 - (rawU + 0.5))

	// v should be 0 at south pole of sphere
	// 1 at north pole
	// filpped over by subtracting by 1
	v = Scalar(1 - (phi / math.Pi))

	return u, v
}

func PlanarMap(p Point) (u, v Scalar) {
	return p.X().Mod(1), p.Z().Mod(1)
}

func CylinderMap(p Point) (u, v Scalar) {
	var (
		// azimuth angle
		// -pi < theta <= pi
		// angle increases clockwise as viewed from above
		theta = math.Atan2(float64(p.X()), float64(p.Z()))

		// - 0.5 < rawU <= 0.5
		rawU = theta / (2 * math.Pi)
	)

	// 0 <= u < 1
	// fixes u so that it increases counter-clockwise as viewed from above
	u = Scalar(1 - (rawU + 0.5))

	// v goes from 0 to 1 in whole units of Y
	v = p.Y().Mod(1)

	return
}

func CubeMap(p Point) (u, v Scalar) {
	return cubeMappers[SideFromPoint(p)].Map(p)
}

func CheckersTexture(w, h Scalar, a, b Texture) TextureFunc {
	return func(si SurfaceInteraction) Color {
		var (
			u = si.Shading.UV.U
			v = si.Shading.UV.V
			x = int(math.Floor(float64(u * w)))
			y = int(math.Floor(float64(v * h)))
		)

		if (x+y)%2 == 0 {
			return a.At(si)
		}

		return b.At(si)
	}
}

func StripesTexture(a, b Texture) TextureFunc {
	return func(si SurfaceInteraction) Color {
		var (
			u = si.Shading.UV.U
			x = int(math.Floor(float64(u)))
		)

		if (x)%2 == 0 {
			return a.At(si)
		}

		return b.At(si)
	}
}

func AlignCheckTexture(main, ul, ur, bl, br Texture) TextureFunc {
	return func(si SurfaceInteraction) Color {
		var (
			u = si.Shading.UV.U
			v = si.Shading.UV.V
		)
		if v > 0.8 {
			if u < 0.2 {
				return ul.At(si)
			} else if u > 0.8 {
				return ur.At(si)
			}
		} else if v < 0.2 {
			if u < 0.2 {
				return bl.At(si)
			} else if u > 0.8 {
				return br.At(si)
			}
		}
		return main.At(si)
	}
}

type Side int

const (
	Left Side = iota
	Right
	Front
	Back
	Up
	Down
)

var cubeMappers = [...]MapperFunc{
	Left: MapperFunc(func(p Point) (u, v Scalar) {
		u = (p.Z() + 1).Mod(2.0) / 2
		v = (p.Y() + 1).Mod(2.0) / 2
		return
	}),
	Right: MapperFunc(func(p Point) (u, v Scalar) {
		u = (1 - p.Z()).Mod(2.0) / 2
		v = (p.Y() + 1).Mod(2.0) / 2
		return
	}),
	Up: MapperFunc(func(p Point) (u, v Scalar) {
		u = (p.X() + 1).Mod(2.0) / 2
		v = (1 - p.Z()).Mod(2.0) / 2
		return
	}),
	Down: MapperFunc(func(p Point) (u, v Scalar) {
		u = (p.X() + 1).Mod(2.0) / 2
		v = (p.Z() + 1).Mod(2.0) / 2
		return
	}),
	Front: MapperFunc(func(p Point) (u, v Scalar) {
		u = (p.X() + 1).Mod(2.0) / 2
		v = (p.Y() + 1).Mod(2.0) / 2
		return
	}),
	Back: MapperFunc(func(p Point) (u, v Scalar) {
		u = (1 - p.X()).Mod(2.0) / 2
		v = (p.Y() + 1).Mod(2.0) / 2
		return
	}),
}

func SideFromPoint(p Point) Side {
	var coord = absmax3(p.X(), p.Y(), p.Z())

	switch coord {
	case p.X():
		return Right
	case -p.X():
		return Left
	case p.Y():
		return Up
	case -p.Y():
		return Down
	case p.Z():
		return Front
	case -p.Z():
		return Back
	default:
		panic("failed determining face from point. floating point comparison error.")
	}
}

type CubicTexture struct {
	faces [6]Texture
	transformation
}

func NewCubicTexture(front, back, left, right, up, down Texture) *CubicTexture {
	return &CubicTexture{
		faces: [...]Texture{
			Front: front,
			Back:  back,
			Left:  left,
			Right: right,
			Up:    up,
			Down:  down,
		},
		transformation: newTransformation(),
	}
}

func (c *CubicTexture) At(si SurfaceInteraction) Color {
	face := SideFromPoint(si.point)
	si.Shading.UV.U, si.Shading.UV.V = cubeMappers[face].Map(si.point)
	return c.faces[face].At(si)
}

type ImageTexture struct {
	img image.Image
	transformation
}

func NewImage(img image.Image) *ImageTexture {
	return &ImageTexture{
		img:            img,
		transformation: newTransformation(),
	}
}

func (i *ImageTexture) At(si SurfaceInteraction) Color {
	// flip v because y = 0 on image, but
	// y = 0 is the bottom of an object
	var (
		u      = si.Shading.UV.U
		v      = si.Shading.UV.V
		bounds = i.img.Bounds()
		x      = u * Scalar(bounds.Max.X-1)
		y      = (1 - v) * Scalar(bounds.Max.Y-1)
		c      = i.img.At(int(x.Round()), int(y.Round()))
	)

	return NewColorFromRBGBA(c.RGBA())
}

type FbmTexture struct {
	fbm *FBM
}

func NewFbmTexture(seed int64, omega Scalar, octives int) FbmTexture {
	return FbmTexture{
		fbm: NewFBM(seed, omega, octives),
	}
}

func (f FbmTexture) ScalarAt(si SurfaceInteraction) Scalar {
	return f.fbm.Noise(si)
}

func (f FbmTexture) At(si SurfaceInteraction) Color {
	return NewGray(f.fbm.Noise(si))
}
