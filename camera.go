package raytracer

import (
	"math"
	"math/rand"
	"runtime"
	"sync"
)

type Camera struct {
	width, height int    // width, height in pixels
	fov           Scalar // field of view

	aspect                Scalar
	pixelSize             Scalar
	halfWidth, halfHeight Scalar

	transformation

	mu       sync.Mutex // protects preview
	previews chan chan Canvas
}

func NewCamera(w, h int, fov Scalar) *Camera {
	var (
		hv = Scalar(math.Tan(float64(fov / 2)))
		c  = Camera{
			width:          w,
			height:         h,
			fov:            fov,
			aspect:         Scalar(float64(w) / float64(h)),
			transformation: newTransformation(),
			previews:       make(chan chan Canvas, 1),
		}
	)

	if c.aspect >= 1 {
		c.halfWidth = hv
		c.halfHeight = hv / c.aspect
	} else {
		c.halfWidth = hv * c.aspect
		c.halfHeight = hv
	}

	c.pixelSize = (c.halfWidth * 2) / Scalar(c.width)

	return &c
}

func (c *Camera) Width() int {
	return c.width
}

func (c *Camera) Height() int {
	return c.height
}

func (c *Camera) PixelSize() Scalar {
	return c.pixelSize
}

func (c *Camera) ViewTransform(v ViewTransform) {
	c.transformation = newTransformationFromMatrix(c.m.Mul(v.matrix))
}

func (c *Camera) RayForPixel(px, py Scalar) Ray {
	var (
		// offset from edge of canvas to pixel's center
		xoffset = (px + 0.5) * c.pixelSize
		yoffset = (py + 0.5) * c.pixelSize

		// untransformed coordinates of pixel in world space
		// camera looks toward -z, os +x is to the left
		worldX = c.halfWidth - xoffset
		worldY = c.halfHeight - yoffset

		transform = c.Transformation().Invert()
		pixel     = NewPoint(worldX, worldY, -1).transform(transform)
		origin    = NewPoint(0, 0, 0).transform(transform)
		direction = pixel.PathFrom(origin).Normalize()
	)

	return NewRay(origin, direction)
}

type pixel struct {
	x, y  int
	color Color
}

type RenderCtx struct {
	concurrency int
	remaining   int
	rng         *rand.Rand
	samples     *int
}

func (r RenderCtx) RandomScalar() Scalar {
	*r.samples++
	return Scalar(r.rng.Float64())
}

func DefaultContext() RenderCtx {
	return RenderCtx{
		concurrency: runtime.NumCPU(),
		remaining:   5,
		rng:         rand.New(rand.NewSource(0)),
		samples:     new(int),
	}
}

func WithRNG(ctx RenderCtx, rng *rand.Rand) RenderCtx {
	ctx.rng = rng
	ctx.samples = new(int)
	return ctx
}
