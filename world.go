package raytracer

import (
	"sort"
)

type World struct {
	box     BoundingBox
	lights  []Light
	objects []Object
}

func NewWorld() *World {
	return &World{}
}

func DefaultWorld() *World {
	return &World{
		lights: []Light{NewPointLight(NewPoint(-10, 10, -10), NewColor(1, 1, 1))},
		objects: []Object{
			func() *Sphere {
				s := NewSphere()
				m := defaultMaterial
				s.SetMaterial(&m)
				return s
			}(),
			func() *Sphere {
				s := NewSphere()
				s.Scale(NewScale(0.5, 0.5, 0.5))
				return s
			}(),
		},
	}
}

func (w *World) Light() PointLight {
	return PointLight{}
}

func (w *World) SetLight(li PointLight) {
	if len(w.lights) != 0 {
		w.lights[0] = li
	} else {
		w.AddLight(li)
	}
}

func (w *World) AddLight(li Light) {
	w.lights = append(w.lights, li)
}

func (w *World) Lights() []Light {
	return w.lights
}

func (w *World) Objects() []Object {
	return w.objects
}

func (w *World) AddObject(o Object) {
	// add any emissive objects in the given object graph
	// to the list of lights in the scene so that they can contribute to
	// global illumination
	WalkGraph(o, func(object Object) {
		// skip over aggregates for now
		if _, ok := object.(ObjectGraph); ok {
			return
		}

		// skip over non-top-level objects for now
		// if object.Parent() != nil {
		// 	return
		// }

		if !object.Material().Emission().Eq(Black) {
			w.AddLight(LightObject{Object: object})
		}
	})

	w.objects = append(w.objects, o)
}

func (w *World) Intersect(r Ray, intersections *Intersections) {
	intersections.Reset()
	for _, obj := range w.objects {
		Intersect(r, obj, intersections)
	}
	sort.Sort(intersections)
}

type Optimizer interface {
	Optimize(*World)
}

func (w *World) Optimize() {
	for _, o := range w.objects {
		if _, ok := o.(*Plane); ok {
			continue
		}
		w.box.Add(o)
	}
	for _, o := range w.objects {
		WalkGraph(o, func(object Object) {
			if optimizer, ok := object.(Optimizer); ok {
				optimizer.Optimize(w)
			}
		})
	}

	for _, light := range w.lights {
		if optimizer, ok := light.(Optimizer); ok {
			optimizer.Optimize(w)
		}
	}
}
