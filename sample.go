package raytracer


import "math"


func NewSampleScene() (*World, *Camera) {
	var (
		material = func() *MaterialProperties {
			stripe := NewStripe(White, Black)
			stripe.Scale(NewScale(0.25, 1, 1))
			stripe.RotateX(45)
			stripe.RotateY(45)
			stripe.RotateZ(45)
			m := NewMaterialProperties()
			m.SetPattern(stripe)
			m.SetSpecular(NewGray(0))
			return m
		}()

		leftWall = func() *Sphere {
			m := NewMaterialProperties()
			m.SetPattern(NewGradient(Black, White))
			m.SetReflective(0.7)

			s := NewSphere()
			s.Translate(NewTranslation(0, 0, 5))
			s.Rotate(NewRotationY(Scalar(-math.Pi / 4)))
			s.Rotate(NewRotationX(Scalar(math.Pi / 2)))
			s.Scale(NewScale(10, 0.01, 10))
			s.SetMaterial(m)
			return s
		}()

		rightWall = func() *Plane {
			pattern := NewCheckers(White, Black)
			pattern.Scale(NewScale(0.15, 0.15, 0.15))
			m := NewMaterialProperties()
			m.SetPattern(pattern)

			s := NewPlane()
			s.Translate(NewTranslation(0, 0, 5))
			s.Rotate(NewRotationY(Scalar(math.Pi / 4)))
			s.Rotate(NewRotationX(Scalar(math.Pi / 2)))
			s.Scale(NewScale(10, 0.01, 10))
			s.SetMaterial(m)
			return s
		}()

		floor = func() *Plane {
			p := NewPlane()
			p.SetMaterial(material)
			return p
		}()

		middle = func() *Sphere {
			s := NewSphere()
			s.Translate(NewTranslation(-0.5, 1, 0.5))
			s.RotateY(180)
			g := Glass
			g.SetColor(Black)
			g.SetColor(Black)
			g.SetAmbient(NewGray(0.1))
			g.SetDiffuse(NewGray(0.1))
			g.SetSpecular(NewGray(1))
			g.SetShininess(300)
			g.SetTransparency(1)
			g.SetReflective(1)

			s.SetMaterial(&g)

			return s
		}()

		right = func() *Cube {
			pattern := NewGradient(
				NewColor(1, 0, 0),
				NewColor(0, 0, 1),
			)

			m := NewMaterialProperties()
			m.SetPattern(pattern)
			m.SetDiffuse(NewGray(0.7))
			m.SetSpecular(NewGray(0.3))
			m.SetTransparency(0.5)

			s := NewCube()
			s.SetMaterial(m)
			s.Translate(NewTranslation(1.5, 0.5, -0.5))
			s.Scale(NewScale(0.5, 0.5, 0.5))
			s.RotateY(45)

			return s
		}()

		left = func() *Cylinder {
			ring := NewRing(Red, Green)
			ring.Scale(NewScale(0.5, 0.25, 0.5))
			ring.RotateZ(45)

			m := NewMaterialProperties()
			m.SetPattern(ring)

			c := NewCylinder()
			c.SetClosed(true)
			c.Truncate(0, 5)
			c.Translate(NewTranslation(-1.5, 0.33, -0.75))
			c.RotateZ(45)
			c.RotateX(45)
			c.Scale(NewScale(0.33, 0.33, 0.33))
			c.SetMaterial(m)
			return c
		}()

		cone = func() *Cone {
			m := NewMaterialProperties()
			m.SetColor(Black)
			m.SetTransparency(0.3)
			m.SetReflective(0.7)
			m.SetRefractiveIndex(2)
			m.SetShininess(1)

			c := NewCone()
			c.Truncate(-2, 2)
			c.Translate(NewTranslation(2, 2, 2))
			c.SetMaterial(m)

			return c
		}()
	)

	world := NewWorld()
	world.SetLight(NewPointLight(
		NewPoint(-10, 10, -10),
		NewColor(1, 1, 1),
	))

	world.AddObject(floor)
	world.AddObject(leftWall)
	world.AddObject(rightWall)
	world.AddObject(middle)
	world.AddObject(right)
	world.AddObject(left)
	world.AddObject(cone)

	camera := NewCamera(1000, 500, math.Pi/3)
	camera.ViewTransform(NewViewTransform(
		NewPoint(1, 2.5, -10),
		NewPoint(0, 1, 0),
		NewVector(0, 1, 0),
	))

	return world, camera
}