package raytracer

import (
	"fmt"
	"image"
	"image/color"
	"math"
)

var (
	White  = NewColor(1, 1, 1)
	Black  = NewColor(0, 0, 0)
	Red    = NewColor(1, 0, 0)
	Green  = NewColor(0, 1, 0)
	Blue   = NewColor(0, 0, 1)
	Yellow = NewColor(1, 1, 0)
	Cyan   = NewColor(0, 1, 1)
	Brown  = NewColor(1, 0.5, 0)
	Purple = NewColor(1, 0, 1)
)

type Color struct {
	tuple Tuple
}

func NewColor(r, g, b Scalar) Color {
	return Color{Tuple{r, g, b}}
}

func NewGray(s Scalar) Color {
	return NewColor(s, s, s)
}

func NewColorFromRBGBA(r, g, b, a uint32) Color {
	return NewColor(
		Scalar(r)/(Scalar(math.MaxUint16)),
		Scalar(g)/(Scalar(math.MaxUint16)),
		Scalar(b)/(Scalar(math.MaxUint16)),
	)
}

func (c Color) Red() Scalar                   { return c.tuple[0] }
func (c Color) Green() Scalar                 { return c.tuple[1] }
func (c Color) Blue() Scalar                  { return c.tuple[2] }
func (c Color) RGB() (Scalar, Scalar, Scalar) { return c.Red(), c.Green(), c.Blue() }

func (c Color) Clamp(min, max Scalar) Color {
	return NewColor(
		c.Red().Clamp(min, max),
		c.Green().Clamp(min, max),
		c.Blue().Clamp(min, max),
	)
}

const maxReasonableColor = 20
func (c Color) Valid() bool {

	for _, c := range c.tuple {
		if math.IsNaN(float64(c)) {
			return false
		}
		if math.IsInf(float64(c), 1) || math.IsInf(float64(c), -1) {
			return false
		}
		//if c >= maxReasonableColor {
		//	return false
		//}
	}

	return c.Red() >= 0 && c.Green() >= 0 && c.Blue() >= 0
}

func (c Color) String() string {
	return fmt.Sprintf(
		"Color{R: %f, G: %f, B: %f}",
		c.Red(),
		c.Green(),
		c.Blue(),
	)
}

func (c Color) Add(o Color) Color {
	return Color{tadd(c.tuple, o.tuple)}
}

func (c Color) Sub(o Color) Color {
	return Color{tsub(c.tuple, o.tuple)}
}

// Mul implements the Hadamard product for multiplying two colors
func (c Color) Mul(o Color) Color {
	return NewColor(
		c.Red()*o.Red(),
		c.Green()*o.Green(),
		c.Blue()*o.Blue(),
	)
}

func (c Color) Scale(s Scalar) Color {
	return Color{tmul(c.tuple, s)}
}

func (c Color) Eq(o Color) bool {
	return teq(c.tuple, o.tuple)
}

// At() implements Texture
func (c Color) At(SurfaceInteraction) Color {
	return c
}

// clamp a color to a uint8
// values > 255 are held at 255
// values < 0 are held at 0
func clamp(s Scalar) uint8 {
	if s < 0 {
		return 0
	}
	if s > 1 {
		return 255
	}
	return uint8(math.Ceil(float64(s * 255)))
}

func (c Color) RGBA() color.RGBA {
	return color.RGBA{
		R: clamp(c.Red()),
		G: clamp(c.Green()),
		B: clamp(c.Blue()),
		A: 255,
	}
}

type Canvas struct {
	w, h   int
	pixels [][]Color
}

func NewCanvas(w, h int) Canvas {
	c := Canvas{
		w:      w,
		h:      h,
		pixels: make([][]Color, h),
	}

	for i := range c.pixels {
		c.pixels[i] = make([]Color, w)
	}

	return c
}

func (c Canvas) Clone() Canvas {
	clone := NewCanvas(c.w, c.h)
	for y := 0; y < c.Height(); y++ {
		for x := 0; x < c.Width(); x++ {
			clone.DrawPixel(x, y, c.PixelAt(x, y))
		}
	}
	return clone
}

func (c Canvas) Width() int  { return c.w }
func (c Canvas) Height() int { return c.h }

func (c Canvas) DrawPixel(x, y int, color Color) {
	/*
		index := int64(c.h)*int64(y) + int64(x)
		println("writing at", x, y, index)
		c.pixels[index] = color
	*/

	c.pixels[int64(y)][int64(x)] = color
}

func (c Canvas) PixelAt(x, y int) Color {
	/*
		index := int64(c.h)*int64(y) + int64(x)
		return c.pixels[index]
	*/

	return c.pixels[int64(y)][int64(x)]
}

// implementation for image.Image
func (c Canvas) ColorModel() color.Model {
	return color.RGBAModel
}

func (c Canvas) Bounds() image.Rectangle {
	return image.Rect(0, 0, c.w, c.h)
}

func (c Canvas) At(x, y int) color.Color {
	return c.PixelAt(x, y).RGBA()
}
