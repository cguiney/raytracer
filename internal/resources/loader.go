package resources

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Loader interface {
	// Gets full contents of resource
	ReadAll(path string) ([]byte, error)

	// Gets an io.Reader for the specified path
	// It is the callers responsibility to close the file
	Open(path string) (io.ReadCloser, error)

	// Gets a ReaderAt for the specified path
	// along with the total size of the resource
	OpenSection(path string) (Section, error)
}

type Section interface {
	io.ReaderAt
	io.Closer
	Size() int64
}

type Filesystem struct {
	Root string
}

func (f Filesystem) ReadAll(path string) ([]byte, error) {
	fh, err := f.Open(path)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(fh)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (f Filesystem) Open(path string) (io.ReadCloser, error) {
	return os.Open(filepath.Join(f.Root, path))
}

func (f Filesystem) OpenSection(path string) (Section, error) {
	fh, err := os.Open(filepath.Join(f.Root, path))
	if err != nil {
		return nil, err
	}

	stat, err := fh.Stat()
	if err != nil {
		return nil, err
	}

	sect := struct {
		*io.SectionReader
		io.Closer
	}{
		io.NewSectionReader(fh, 0, stat.Size()),
		fh,
	}

	return sect, nil
}
