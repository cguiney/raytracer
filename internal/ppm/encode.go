package ppm

import (
	"fmt"
	"image"
	"image/color"
	"io"
)

type Encoder struct {
	written int
	err     error
}

func NewEncoder() *Encoder {
	return &Encoder{}
}

func (e *Encoder) Encode(w io.Writer, m image.Image) error {
	if err := e.writeHeader(w, m); err != nil {
		return fmt.Errorf("failed encoding ppm header: %s", err)
	}

	width, height := m.Bounds().Dx(), m.Bounds().Dy()

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			e.writePixel(w, m.At(x, y))
			if x < width-1 {
				e.writeSpace(w)
			}
		}
		e.writeNewline(w)
	}

	return e.err
}

func (e *Encoder) writeNewline(w io.Writer) {
	// already in an error state, don't keep writing
	if e.err != nil {
		return
	}
	if _, err := fmt.Fprintf(w, "\n"); err != nil {
		e.err = fmt.Errorf("failed encoding ppm pixel data: %s", err)
	}
	e.written = 0
}

func (e *Encoder) writeSpace(w io.Writer) {
	// if the next channel overflow, just write the newline instead of the space
	// followed by a new line
	if e.written+4 >= 70 {
		e.writeNewline(w)
	} else {
		e.write(w, " ")
	}
}

func (e *Encoder) write(w io.Writer, out string) {
	// already in an error state, don't keep writing
	if e.err != nil {
		return
	}

	if e.written+len(out) >= 70 {
		e.writeNewline(w)
	}

	n, err := fmt.Fprintf(w, out)
	if err != nil {
		e.err = fmt.Errorf("failed encoding ppm pixel data: %s", err)
	}
	e.written += n
}

func (e *Encoder) writeChannel(w io.Writer, channel uint32) {
	e.write(w, fmt.Sprintf("%d", channel>>8))
}

func (e *Encoder) writePixel(w io.Writer, c color.Color) {
	r, g, b, _ := c.RGBA()
	e.writeChannel(w, r)
	e.writeSpace(w)
	e.writeChannel(w, g)
	e.writeSpace(w)
	e.writeChannel(w, b)
}

func (e *Encoder) writeHeader(w io.Writer, m image.Image) error {
	bounds := m.Bounds()
	if _, err := fmt.Fprintf(w, "P3\n%d %d\n%d\n", bounds.Max.X, bounds.Max.Y, 255); err != nil {
		return err
	}
	return nil
}
