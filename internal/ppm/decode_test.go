package ppm_test

import (
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/ppm"
	"image"
	"math"
	"strings"
	"testing"
)

type colorTest struct {
	x, y int
	c    raytracer.Color
}

func runColorTests(t *testing.T, img image.Image, tests []colorTest) {
	t.Helper()
	for _, tt := range tests {
		r, g, b, _ := img.At(tt.x, tt.y).RGBA()

		have := raytracer.NewColor(
			raytracer.Scalar(r)/math.MaxUint16,
			raytracer.Scalar(g)/math.MaxUint16,
			raytracer.Scalar(b)/math.MaxUint16,
		)

		if !have.Eq(tt.c) {
			t.Errorf("incorrect color for pixel (%d, %d). have=%s want=%s", tt.x, tt.y, have, tt.c)
		}
	}
}

func TestDecoding(t *testing.T) {
	t.Run("invalid header", func(t *testing.T) {
		const input = `P32
    1 1
    255
    0 0 0
`

		{
			cfg, format, err := image.DecodeConfig(strings.NewReader(input))
			t.Logf("decode result: format %q; dimensions: (%d, %d); err: %s", format, cfg.Width, cfg.Height, err)
			if err == nil {
				t.Errorf("expected error parsing, got none")
			}
		}

		{
			cfg, err := ppm.DecodeConfig(strings.NewReader(input))
			t.Logf("decode result: dimensions: (%d, %d); err: %s", cfg.Width, cfg.Height, err)
			if err == nil {
				t.Errorf("expected error parsing, got none")
			}
		}
	})

	t.Run("create config of correct size", func(t *testing.T) {
		const input = `P3
    10 2
    255
    0 0 0  0 0 0  0 0 0  0 0 0  0 0 0
    0 0 0  0 0 0  0 0 0  0 0 0  0 0 0
    0 0 0  0 0 0  0 0 0  0 0 0  0 0 0
    0 0 0  0 0 0  0 0 0  0 0 0  0 0 0`

		{

			cfg, format, err := image.DecodeConfig(strings.NewReader(input))
			if err != nil {
				t.Errorf("unexpected error parsing: %s", err)
			}
			if format != "ppm" {
				t.Errorf("incorrect format. have=%s want=%s", format, "ppm")
			}
			if cfg.Width != 10 {
				t.Errorf("incorrect width. have=%d want=%d", cfg.Width, 10)
			}
			if cfg.Height != 2 {
				t.Errorf("incorrect width. have=%d want=%d", cfg.Width, 2)
			}
		}

		{
			cfg, err := ppm.DecodeConfig(strings.NewReader(input))
			if err != nil {
				t.Errorf("unexpected error parsing: %s", err)
			}
			if cfg.Width != 10 {
				t.Errorf("incorrect width. have=%d want=%d", cfg.Width, 10)
			}
			if cfg.Height != 2 {
				t.Errorf("incorrect width. have=%d want=%d", cfg.Width, 2)
			}
		}
	})

	t.Run("reading pixel data from ppm file", func(t *testing.T) {
		const input = `P3
    4 3
    255
    255 127 0  0 127 255  127 255 0  255 255 255
    0 0 0  255 0 0  0 255 0  0 0 255
    255 255 0  0 255 255  255 0 255  127 127 127`

		img, _, err := image.Decode(strings.NewReader(input))
		if err != nil {
			t.Fatal("unexpected error:", err)
		}

		tests := []colorTest{
			{0, 0, raytracer.NewColor(1, 0.498039, 0)},
			{1, 0, raytracer.NewColor(0, 0.498039, 1)},
			{2, 0, raytracer.NewColor(0.498039, 1, 0)},
			{3, 0, raytracer.NewColor(1, 1, 1)},
			{0, 1, raytracer.NewColor(0, 0, 0)},
			{1, 1, raytracer.NewColor(1, 0, 0)},
			{2, 1, raytracer.NewColor(0, 1, 0)},
			{3, 1, raytracer.NewColor(0, 0, 1)},
			{0, 2, raytracer.NewColor(1, 1, 0)},
			{1, 2, raytracer.NewColor(0, 1, 1)},
			{2, 2, raytracer.NewColor(1, 0, 1)},
			{3, 2, raytracer.NewColor(0.498039, 0.498039, 0.498039)},
		}

		runColorTests(t, img, tests)
	})

	t.Run("parsing ignored comment lines", func(t *testing.T) {
		const input = `P3
    # this is a comment
    2 1
    # this, too
    255
    # another comment
    255 255 255
    # oh, no, comments in the pixel data!
    255 0 255`

		img, _, err := image.Decode(strings.NewReader(input))
		if err != nil {
			t.Fatal("unexpected error:", err)
		}

		runColorTests(t, img, []colorTest{
			{0, 0, raytracer.NewColor(1, 1, 1)},
			{1, 0, raytracer.NewColor(1, 0, 1)},
		})
	})

	t.Run("parsing allows triples to span lines", func(t *testing.T) {
		const input = `P3
    1 1
    255
    51
    153

    204
`

		img, _, err := image.Decode(strings.NewReader(input))
		if err != nil {
			t.Fatal("unexpected error:", err)
		}
		runColorTests(t, img, []colorTest{
			{0, 0, raytracer.NewColor(0.2, 0.6, 0.8)},
		})
	})

	t.Run("respects scale setting", func(t *testing.T) {
		const input = `P3
    2 2
    100
    100 100 100  50 50 50
    75 50 25  0 0 0`

		img, _, err := image.Decode(strings.NewReader(input))
		if err != nil {
			t.Fatal("unexpected error:", err)
		}
		runColorTests(t, img, []colorTest{
			{0, 1, raytracer.NewColor(0.752941, 0.501961, 0.250980)},
		})
	})
}
