package ppm

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"io"
	"math"
	"unicode"
)

func init() {
	image.RegisterFormat("ppm", "P3\n", Decode, DecodeConfig)
}

type header struct {
	magic         string
	width, height int
	scale         int64
}

type decoder struct {
	reader *bufio.Reader

	header
	image *image.RGBA
}

func newDecoder(r io.Reader) *decoder {
	d := &decoder{
		reader: bufio.NewReader(r),
	}
	return d
}

func (d *decoder) decodeMagic() error {
	_, err := fmt.Fscanln(d.reader, &d.magic)
	if err != nil {
		return fmt.Errorf("failed reading header: %s", err)
	}
	if d.magic != `P3` {
		return fmt.Errorf("incorrect header value. want P3 followed by newline. have=%q", d.magic)
	}

	return nil
}

func (d *decoder) decodeDimensions() error {
	n, err := fmt.Fscanln(d.reader, &d.width, &d.height)
	if err != nil {
		return fmt.Errorf("failed parsing dimension line: %s", err)
	}
	if n != 2 {
		return fmt.Errorf("failed parsing dimension line: not enough arguments")
	}

	return nil
}

func (d *decoder) decodeScale() error {
	n, err := fmt.Fscanln(d.reader, &d.scale)
	if err != nil {
		return fmt.Errorf("failed parsing scale line: %s", err)
	}
	if n != 1 {
		return fmt.Errorf("failed parsing scale line: incorrect number of arguments")
	}

	return nil
}

func (d *decoder) decodeHeader() error {
	if err := d.skipComments(); err != nil {
		return err
	}

	if err := d.decodeMagic(); err != nil {
		return err
	}

	if err := d.skipComments(); err != nil {
		return err
	}

	if err := d.decodeDimensions(); err != nil {
		return err
	}

	if err := d.skipComments(); err != nil {
		return err
	}

	if err := d.decodeScale(); err != nil {
		return err
	}

	return nil
}

func (d *decoder) skipComments() error {
	for {
		// check if input begins with a #
		b, err := d.reader.Peek(1)
		if err != nil {
			return err
		}
		if unicode.IsSpace(rune(b[0])) {
			if _, err := d.reader.Discard(1); err != nil {
				return err
			}
			continue
		}
		if b[0] == '#' {
			_, err := d.reader.ReadString('\n')
			if err != nil {
				return err
			}
		}
		return nil
	}
}

func (d *decoder) decodePixel(x, y int) error {
	if err := d.skipComments(); err != nil {
		return err
	}

	var r, g, b int64
	n, err := fmt.Fscan(d.reader, &r, &g, &b)
	if err != nil {
		return fmt.Errorf("failed reading pixel: %s", err)
	}
	if n != 3 {
		return fmt.Errorf("failed reading pixel: only %d values read", n)
	}

	var (
		fr = math.Ceil((float64(r) / float64(d.scale)) * 255)
		fg = math.Ceil((float64(g) / float64(d.scale)) * 255)
		fb = math.Ceil((float64(b) / float64(d.scale)) * 255)
	)

	d.image.Set(x, y, color.RGBA{
		R: uint8(fr),
		G: uint8(fg),
		B: uint8(fb),
	})
	return nil
}

func (d *decoder) decodeImage() error {
	d.image = image.NewRGBA(image.Rect(0, 0, d.width, d.height))
	for y := 0; y < d.height; y++ {
		for x := 0; x < d.width; x++ {
			if err := d.decodePixel(x, y); err != nil {
				return fmt.Errorf("failed decoding pixel (%d, %d): %s", x, y, err)
			}
		}
	}
	return nil
}

func DecodeConfig(r io.Reader) (image.Config, error) {
	d := newDecoder(r)
	if err := d.decodeHeader(); err != nil {
		return image.Config{}, err
	}

	return image.Config{
		Width:      d.header.width,
		Height:     d.header.height,
		ColorModel: color.RGBAModel,
	}, nil
}

func Decode(r io.Reader) (image.Image, error) {
	d := newDecoder(r)
	if err := d.decodeHeader(); err != nil {
		return nil, err
	}
	if err := d.decodeImage(); err != nil {
		return nil, err
	}

	return d.image, nil
}
