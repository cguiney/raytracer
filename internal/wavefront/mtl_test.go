package wavefront

import (
	"gitlab.com/cguiney/raytracer/internal/resources"
	"testing"
)

func TestMaterialParsing(t *testing.T) {
	const input = `mtllib sportsCar.mtl`

	p := New()
	p.Loader = resources.Filesystem{Root: "../../resources/"}

	parse(t, p, input)

	materials := []string{
		"SmallHeadLights",
		"RearLights",
		"Interior_White",
		"EngineSilver2",
	}

	for _, want := range materials {
		if _, ok := p.Materials[want]; !ok {
			t.Errorf("expected material %s to exist", want)
		}
	}
}
