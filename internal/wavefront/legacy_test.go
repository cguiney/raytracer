package wavefront_test

import (
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/wavefront"
	"io"
	"os"
	"testing"
)

func TestMeshVsSparse(t *testing.T) {
	t.Skipf("Skipping until groups implement a BVH")
	t.SkipNow()

	fh, err := os.Open("../../resources/sportsCar.obj")
	if err != nil {
		t.Fatal(err)
	}

	w := wavefront.New()

	if err := w.Parse(fh); err != nil {
		t.Fatal("failed parsing: ", err)
	}

	if _, err := fh.Seek(0, io.SeekStart); err != nil {
		t.Fatal(err)
	}

	p := wavefront.NewLegacy()
	if err := p.Parse(fh); err != nil {
		t.Fatal("failed parsing legacy: ", err)
	}

	m := w.Build()
	for i, mgroup := range m.Groups() {
		pgroup, ok := p.Groups[mgroup.Name()]
		if !ok {
			t.Fatal("could not find group in legacy impl: ", mgroup.Name())
		}

		if len(mgroup.Triangles()) != len(pgroup.Objects()) {
			t.Fatalf("differing number of triangles.  have=%d want=%d",
				len(mgroup.Triangles()), len(pgroup.Objects()))
		}

		wantMin, wantMax := pgroup.Bounds()
		haveMin, haveMax := mgroup.Bounds()

		if !wantMin.Eq(haveMin) {
			t.Errorf("differing bounding mins. group=(%d, %s) have=%f want=%f",
				i, mgroup.Name(), haveMin, wantMin)
		}
		if !wantMax.Eq(haveMax) {
			t.Errorf("differing bounding maxes. group=(%d, %s) have=%f want=%f",
				i, mgroup.Name(), haveMax, wantMax)
		}

		for i, obj := range pgroup.Objects() {
			tri := obj.(*raytracer.Triangle)
			mt := mgroup.Triangles()[i]

			comparePoints(t, tri.Points(), mt.Points())
			compareVectors(t, tri.Normals(), mt.Normals())
			compareVectors(t, tri.Edges(), mt.Edges())
			wantMin, wantMax := tri.Bounds()
			haveMin, haveMax := mt.Bounds()

			if !wantMin.Eq(haveMin) {
				t.Errorf("differing bounding mins. have=%f want=%f", haveMin, wantMin)
			}
			if !wantMax.Eq(haveMax) {
				t.Errorf("differing bounding maxes. have=%f want=%f", haveMax, wantMax)
			}

			rays := []raytracer.Ray{
				raytracer.NewRay(raytracer.NewPoint(0, 0, -5), raytracer.NewVector(0, 0, 1)),
				raytracer.NewRay(raytracer.NewPoint(0, 0, -1), raytracer.NewVector(0, 0, 1)),
				raytracer.NewRay(raytracer.NewPoint(0, 0, -1), raytracer.NewVector(0, 0, 2)),
				raytracer.NewRay(raytracer.NewPoint(0.5, 0.5, 0.5), raytracer.NewVector(0.5, 0.5, 0.5)),
				raytracer.NewRay(raytracer.NewPoint(0.1, 0.1, 0.1), raytracer.NewVector(0.5, 0.5, 0.5)),
				raytracer.NewRay(raytracer.NewPoint(0, 0.5, -2), raytracer.NewVector(0, 0, 1)),
			}

			for _, r := range rays {
				var (
					mtXS raytracer.Intersections
					tXS  raytracer.Intersections
				)

				raytracer.Intersect(r, &mt, &mtXS)
				raytracer.Intersect(r, tri, &tXS)
				
				compareIntersections(t, mtXS, tXS)
			}
		}
	}
}

func comparePoints(t *testing.T, have []raytracer.Point, want []raytracer.Point) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of points. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Errorf("incorrect point %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func compareVectors(t *testing.T, have []raytracer.Vector, want []raytracer.Vector) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of vectors. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Errorf("incorrect vector %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func compareIntersections(t *testing.T, have raytracer.Intersections, want raytracer.Intersections) {
	if len(have) != len(want) {
		t.Errorf("differing number of intersections, have=%d want=%d", len(have), len(want))
	}

	for i, x := range have {
		if x.Time() != want[i].Time() {
			t.Errorf("intersection %d occurs at different time. have=%s want=%s", i, x.Time(), want[i].Time())
		}

		if !x.U().Eq(want[i].U()) {
			t.Errorf("intersection %d has different U values. have=%s want=%s", i, x.U(), want[i].U())
		}

		if !x.V().Eq(want[i].V()) {
			t.Errorf("intersection %d has different V values. have=%s want=%s", i, x.V(), want[i].V())
		}

		haveN := raytracer.NormalAt(x.Object(), raytracer.NewPoint(1, 1, 1), x)
		wantN := raytracer.NormalAt(want[i].Object(), raytracer.NewPoint(1, 1, 1), want[i])

		compareVectors(t, []raytracer.Vector{haveN}, []raytracer.Vector{wantN})
	}
}
