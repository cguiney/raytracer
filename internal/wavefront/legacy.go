package wavefront

import (
	"bufio"
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type LegacyParser struct {
	Normals  []raytracer.Vector
	Vertices []raytracer.Point
	Textures []UV

	// Root group, contains all subgroups
	Root *raytracer.Group

	// all groups
	Groups map[string]*raytracer.Group

	// currently active group
	Group *raytracer.Group

	// collection of materials
	Materials map[string]*raytracer.MaterialProperties

	// currently active material
	Material *raytracer.MaterialProperties

	// directory to find any resources in
	// .mtl files, for example
	ResourceRoot string

	warnings []error
	line     int

	faces   []face             // face buffer
	ints    []int              // int buffer
	scalars []raytracer.Scalar // scalar buffer
}

func NewLegacy() *LegacyParser {
	g := raytracer.NewGroup()
	return &LegacyParser{
		Root:  raytracer.NewGroup(),
		Group: g,
		Groups: map[string]*raytracer.Group{
			"": g,
		},

		Materials: map[string]*raytracer.MaterialProperties{},
	}
}

func (p *LegacyParser) warn(msg string, args ...interface{}) {
	p.warnings = append(p.warnings, fmt.Errorf("line %d: %s", p.line, fmt.Sprintf(msg, args...)))
}

// parse string arguments into scalar buffer
// overwrites whatever is in scalar buffer
// returns error if any input fails to parse
func (p *LegacyParser) parseScalars(args []string) error {
	p.scalars = p.scalars[:0]
	for _, input := range args {
		f, err := strconv.ParseFloat(input, 64)
		if err != nil {
			return fmt.Errorf("failed parsing floating point: %q: %s", input, err)
		}
		p.scalars = append(p.scalars, raytracer.Scalar(f))
	}
	return nil
}

func (p *LegacyParser) parseInts(args []string) error {
	p.ints = p.ints[:0]
	for _, input := range args {
		n, err := strconv.ParseInt(input, 10, 64)
		if err != nil {
			return fmt.Errorf("failed parsing integer: %q: %s", input, err)
		}
		p.ints = append(p.ints, int(n))
	}
	return nil
}

func (p *LegacyParser) parseFaceArgs(args []string) error {
	p.faces = p.faces[:0]

	for i, input := range args {

		parts := strings.Split(input, "/")

		var f face

		if len(parts) >= 1 {
			v, err := strconv.ParseInt(parts[0], 10, 64)
			if err != nil {
				return fmt.Errorf("failed parsing vertex index: %q: %s", input, err)
			}
			f.vertIdx = int(v)
		}

		if len(parts) >= 2 {
			// second part is optional
			if parts[1] != "" {
				n, err := strconv.ParseInt(parts[1], 10, 64)
				if err != nil {
					return fmt.Errorf("failed parsing texture index: %q: %s", input, err)
				}

				f.texIdx = int(n)
			}
		}

		if len(parts) >= 3 {
			n, err := strconv.ParseInt(parts[2], 10, 64)
			if err != nil {
				return fmt.Errorf("failed parsing normal index: %q: %s", input, err)
			}
			f.normIdx = int(n)
		}

		if len(parts) == 0 {
			return fmt.Errorf("malformed face argument %d: %q", i, input)
		}

		p.faces = append(p.faces, f)
	}

	return nil
}

func (p *LegacyParser) fanTriangulate(faces []face) {
	if len(faces) < 3 {
		panic("calling fanTriangulate() on less than 3 vertices")
	}

	var smooth = p.useSmoothTriangles()

	for i := 1; i < len(faces)-1; i++ {
		if smooth {
			p.Group.AddObject(raytracer.NewSmoothTriange(
				p.Vertices[faces[0].vertIdx-1],
				p.Vertices[faces[i].vertIdx-1],
				p.Vertices[faces[i+1].vertIdx-1],
				p.Normals[faces[0].normIdx-1],
				p.Normals[faces[i].normIdx-1],
				p.Normals[faces[i+1].normIdx-1],
			))
		} else {
			p.Group.AddObject(raytracer.NewTriangle(
				p.Vertices[faces[0].vertIdx-1],
				p.Vertices[faces[i].vertIdx-1],
				p.Vertices[faces[i+1].vertIdx-1]))
		}
	}
}

func (p *LegacyParser) useSmoothTriangles() bool {
	for _, f := range p.faces {
		if f.normIdx != 0 {
			return true
		}
	}
	return false
}

func (p *LegacyParser) parseFace(args []string) {
	if len(args) < 3 {
		p.warn("expected at least three vertices. have %d", len(args))
		return
	}

	if err := p.parseFaceArgs(args); err != nil {
		p.warn("failed parsing face arguments: %s", err)
		return
	}

	// bound check
	for _, f := range p.faces {
		if len(p.Vertices) < f.vertIdx {
			p.warn("unrecognized vertex: %d", f.vertIdx)
			return
		}
	}

	// have exactly 3 vertices, add triangle directly
	if len(args) == 3 {
		if p.useSmoothTriangles() {
			p.Group.AddObject(raytracer.NewSmoothTriange(
				p.Vertices[p.faces[0].vertIdx-1],
				p.Vertices[p.faces[1].vertIdx-1],
				p.Vertices[p.faces[2].vertIdx-1],
				p.Normals[p.faces[0].normIdx-1],
				p.Normals[p.faces[1].normIdx-1],
				p.Normals[p.faces[2].normIdx-1],
			))
		} else {
			p.Group.AddObject(raytracer.NewTriangle(
				p.Vertices[p.faces[0].vertIdx-1],
				p.Vertices[p.faces[1].vertIdx-1],
				p.Vertices[p.faces[2].vertIdx-1],
			))
		}
		return
	}

	p.fanTriangulate(p.faces)
}

func (p *LegacyParser) parseVertex(args []string) {
	if len(args) != 3 {
		p.warn("expected three points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.Vertices = append(p.Vertices, raytracer.NewPoint(p.scalars[0], p.scalars[1], p.scalars[2]))
}

func (p *LegacyParser) parseNormals(args []string) {
	if len(args) != 3 {
		p.warn("expected three points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.Normals = append(p.Normals, raytracer.NewVector(p.scalars[0], p.scalars[1], p.scalars[2]))
}

func (p *LegacyParser) parseTextures(args []string) {
	if len(args) < 2 {
		p.warn("expected two points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.Textures = append(p.Textures, UV{p.scalars[0], p.scalars[1]})
}

func (p *LegacyParser) parseGroup(args []string) {
	if len(args) != 1 {
		p.warn("expected group to have exactly one argument. have %d", len(args))
		return
	}

	g := raytracer.NewGroup()
	p.Group, p.Groups[args[0]] = g, g
}

func (p *LegacyParser) parseLine(line string) {
	if line == "" { // allow blank lines
		return
	}

	if strings.HasPrefix(line, "#") {
		return
	}

	parts := strings.Fields(line)
	if len(parts) <= 1 {
		p.warn("invalid format: %q", line)
	}

	command, args := parts[0], parts[1:]

	switch command {
	case "v":
		p.parseVertex(args)
	case "f":
		p.parseFace(args)
	case "g":
		p.parseGroup(args)
	case "vn":
		p.parseNormals(args)
	case "vt":
		p.parseTextures(args)
	case "mtllib":
		p.parseMTL(args)
	case "usemtl":
		p.parseUseMTL(args)
	case "s":
		// skip smooth shading options
		return
	default:
		p.warn("unrecognized command: %s", parts[0])
	}
}

func (p *LegacyParser) Parse(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		p.line++
		p.parseLine(strings.TrimSpace(scanner.Text()))
	}

	// add all groups to root
	for _, group := range p.Groups {
		// dont bother adding empty groups
		if len(group.Objects()) == 0 {
			continue
		}
		p.Root.AddObject(group)
	}

	return scanner.Err()
}

func (p *LegacyParser) Warnings() []error {
	return p.warnings
}

func (p *LegacyParser) parseMTL(args []string) {
	if len(args) != 1 {
		p.warn("expected exactly one argument to mtllib, got=%d", len(args))
		return
	}

	fh, err := os.Open(filepath.Join(p.ResourceRoot, args[0]))
	if err != nil {
		p.warn("failed opening mtl resource %q: %s", args[0], err)
		return
	}

	mp := &legacyMtlParser{p: p}
	if err := mp.Parse(fh); err != nil {
		p.warn("failed parsing material library %q: %s", args[0], err)
		return
	}
}

func (p *LegacyParser) parseUseMTL(args []string) {
	if len(args) != 1 {
		p.warn("expected exactly one argument to usemtl, got=%d", len(args))
		return
	}

	m, ok := p.Materials[args[0]]
	if !ok {
		p.warn("undefined material %q. switching to default material", args[0])
		p.Material = raytracer.NewMaterialProperties()
		return
	}

	p.Material = m
	p.Group.SetMaterial(m)
}

type legacyMtlParser struct {
	p        *LegacyParser
	line     int
	material *raytracer.MaterialProperties

	scalars []raytracer.Scalar // scalar buffer
}

func (m *legacyMtlParser) warn(msg string, args ...interface{}) {
	m.p.warn("mtl line %d: %s", m.line, fmt.Sprintf(msg, args...))
}

func (m *legacyMtlParser) parseScalars(args []string) error {
	m.scalars = m.scalars[:0]
	for _, input := range args {
		f, err := strconv.ParseFloat(input, 64)
		if err != nil {
			return fmt.Errorf("failed parsing floating point: %q: %s", input, err)
		}
		m.scalars = append(m.scalars, raytracer.Scalar(f))
	}
	return nil
}

func (m *legacyMtlParser) parseNewMaterial(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to newmtl. got=%d", len(args))
		return
	}

	m.material = raytracer.NewMaterialProperties()
	m.p.Materials[args[0]] = m.material
}

func (m *legacyMtlParser) parseShininess(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Ns. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetShininess(m.scalars[0])
}

func (m *legacyMtlParser) parseDissolve(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to d. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetTransparency(1 - m.scalars[0])
}

func (m *legacyMtlParser) parseTransparency(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Tf. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetTransparency(m.scalars[0])
}

func (m *legacyMtlParser) parseRefractiveIndex(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Ns. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetRefractiveIndex(m.scalars[0])
}

func (m *legacyMtlParser) parseAmbient(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Ka, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetAmbient(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *legacyMtlParser) parseSpecular(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Ks, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetSpecular(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *legacyMtlParser) parseDiffuse(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Kd, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetDiffuse(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *legacyMtlParser) parseEmissive(args []string) {
	if len(args) != 3 {
		m.warn("expected exactly three arguments for Ke, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetColor(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *legacyMtlParser) parseLine(line string) {
	if line == "" { // allow blank lines
		return
	}

	if strings.HasPrefix(line, "#") {
		return
	}

	parts := strings.Fields(line)
	if len(parts) <= 1 {
		m.warn("invalid format: %q", line)
	}

	command, args := parts[0], parts[1:]

	switch command {
	case "newmtl":
		m.parseNewMaterial(args)
	case "Ns":
		m.parseShininess(args)
	case "Ka":
		m.parseAmbient(args)
	case "Ks":
		m.parseSpecular(args)
	case "Kd":
		m.parseDiffuse(args)
	case "d":
		m.parseDissolve(args)
	case "Tr":
		m.parseTransparency(args)
	case "Ni":
		m.parseRefractiveIndex(args)
	case "Ke":
		return
		// m.parseEmissive(args)
	case "illum":
		return
	default:
		m.warn("unknown command: %s", command)
	}
}

func (m *legacyMtlParser) Parse(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		m.line++
		m.parseLine(strings.TrimSpace(scanner.Text()))
	}

	return scanner.Err()
}
