package wavefront

import (
	"bufio"
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"image"
	"io"
	"path/filepath"
	"strconv"
	"strings"
)

func (p *Reader) parseMTL(args []string) {
	if len(args) != 1 {
		p.warn("expected exactly one argument to mtllib, got=%d", len(args))
		return
	}

	fh, err := p.Loader.Open(args[0])
	if err != nil {
		p.warn("failed opening mtl resource %q: %s", args[0], err)
		return
	}
	p.resources = append(p.resources, args[0])

	mp := &mtlParser{p: p}
	if err := mp.Parse(fh); err != nil {
		p.warn("failed parsing material library %q: %s", args[0], err)
		return
	}
}

func (p *Reader) parseUseMTL(args []string) {
	if len(args) != 1 {
		p.warn("expected exactly one argument to usemtl, got=%d", len(args))
		return
	}

	m, ok := p.Materials[args[0]]
	if !ok {
		p.warn("undefined material %q. switching to default material", args[0])
		p.Material = raytracer.NewMaterialProperties()
		return
	}

	p.Material = m
	p.MeshBuilder.DefineMaterial(*m)
}

type mtlParser struct {
	p        *Reader
	line     int
	material *raytracer.MaterialProperties

	scalars []raytracer.Scalar // scalar buffer
}

func (m *mtlParser) warn(msg string, args ...interface{}) {
	m.p.warn("mtl line %d: %s", m.line, fmt.Sprintf(msg, args...))
}

func (m *mtlParser) parseScalars(args []string) error {
	m.scalars = m.scalars[:0]
	for _, input := range args {
		f, err := strconv.ParseFloat(input, 64)
		if err != nil {
			return fmt.Errorf("failed parsing floating point: %q: %s", input, err)
		}
		m.scalars = append(m.scalars, raytracer.Scalar(f))
	}
	return nil
}

func (m *mtlParser) parseNewMaterial(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to newmtl. got=%d", len(args))
		return
	}

	m.material = raytracer.NewMaterialProperties()
	m.material.SetName(args[0])
	m.p.Materials[args[0]] = m.material
}

func (m *mtlParser) parseShininess(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Ns. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetShininess(m.scalars[0])
}

func (m *mtlParser) parseDissolve(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to d. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetTransparency(1 - m.scalars[0])
}

func (m *mtlParser) parseTransparency(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Tf. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetTransparency(m.scalars[0])
}

func (m *mtlParser) parseRefractiveIndex(args []string) {
	if len(args) != 1 {
		m.warn("expected one argument to Ns. got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material not created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetRefractiveIndex(m.scalars[0])
}

func (m *mtlParser) parseAmbient(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Ka, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetAmbient(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *mtlParser) parseSpecular(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Ks, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetSpecular(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *mtlParser) parseDiffuse(args []string) {
	if len(args) != 1 && len(args) != 3 {
		m.warn("expected either one or three arguments for Kd, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetDiffuse(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *mtlParser) parseDiffuseMap(args []string) {
	if len(args) != 1 && len(args) != 1 {
		m.warn("expected one argument for Kd_map, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	path := filepath.Clean(strings.ReplaceAll(args[0], "\\", string(filepath.Separator)))
	fh, err := m.p.Loader.Open(path)
	if err != nil {
		m.warn("failed opening diffuse map %q: %s", path, err)
		return
	}
	m.p.resources = append(m.p.resources, path)

	img, _, err := image.Decode(fh)
	if err != nil {
		m.warn("failed decoding diffuse map %q: %s", path, err)
	}

	m.material.SetColorMap(img)
	m.material.SetDiffuseMap(img)
}

func (m *mtlParser) parseEmission(args []string) {
	if len(args) != 3 {
		m.warn("expected exactly three arguments for Ke, got=%d", len(args))
		return
	}

	if m.material == nil {
		m.warn("no material created")
		return
	}

	if err := m.parseScalars(args); err != nil {
		m.warn("unable to parse argument: %s", err)
		return
	}

	m.material.SetEmission(raytracer.NewColor(m.scalars[0], m.scalars[1], m.scalars[2]))
}

func (m *mtlParser) parseLine(line string) {
	if line == "" { // allow blank lines
		return
	}

	if strings.HasPrefix(line, "#") {
		return
	}

	line = strings.Split(line, "#")[0]

	parts := strings.Fields(line)
	if len(parts) <= 1 {
		m.warn("invalid format: %q", line)
	}

	command, args := parts[0], parts[1:]

	switch command {
	case "newmtl":
		m.parseNewMaterial(args)
	case "Ns":
		m.parseShininess(args)
	case "Ka":
		m.parseAmbient(args)
	case "Ks":
		m.parseSpecular(args)
	case "Kd":
		m.parseDiffuse(args)
	case "map_Kd":
		m.parseDiffuseMap(args)
	case "d":
		m.parseDissolve(args)
	case "Tr":
		m.parseTransparency(args)
	case "Ni":
		m.parseRefractiveIndex(args)
	case "Ke":
		m.parseEmission(args)
	case "Tf":
		return // TODO parse transmission filtering
	case "illum":
		return
	default:
		m.warn("unknown command: %s", command)
	}
}

func (m *mtlParser) Parse(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		m.line++
		m.parseLine(strings.TrimSpace(scanner.Text()))
	}

	return scanner.Err()
}
