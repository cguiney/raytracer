package wavefront

import (
	"gitlab.com/cguiney/raytracer"
	"strings"
	"testing"
)

func parse(t *testing.T, p *Reader, input string) *raytracer.Mesh {
	t.Helper()
	if err := p.Parse(strings.NewReader(input)); err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if len(p.warnings) > 0 {
		t.Fatalf("unexpected warnings: %v", p.warnings)
	}

	return p.Build()
}

func comparePoints(t *testing.T, have []raytracer.Point, want []raytracer.Point) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of points. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Errorf("incorrect point %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func compareVectors(t *testing.T, have []raytracer.Vector, want []raytracer.Vector) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of vectors. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].Eq(v) {
			t.Errorf("incorrect vector %d. have=%v want=%v", i, have[i], v)
		}
	}
}

func compareUVs(t *testing.T, have []raytracer.UV, want []raytracer.UV) {
	t.Helper()
	if len(have) != len(want) {
		t.Fatalf("differing number of vectors. have=%d want=%d", len(have), len(want))
	}
	for i, v := range want {
		if !have[i].U.Eq(v.U) {
			t.Errorf("incorrect U %d. have=%v want=%v", i, have[i].U, v.U)
		}
		if !have[i].V.Eq(v.V) {
			t.Errorf("incorrect vector %d. have=%v want=%v", i, have[i].V, v.V)
		}
	}
}

func TestParser(t *testing.T) {
	t.Run("ignoring unrecognized lines", func(t *testing.T) {
		const input = `There was a young lady named Bright
who traveled much faster than light.
She set out one day
in a relative way,
and came back the previous night.`

		p := New()
		if err := p.Parse(strings.NewReader(input)); err != nil {
			t.Errorf("unexpected error: %s", err)
		}

		if len(p.warnings) != 5 {
			t.Errorf("expected 5 warnings")
		}
	})

	t.Run("parsing vertices", func(t *testing.T) {
		const input = `v -1 1 0
v -1.0000 0.5000 0.0000
v 1 0 0
v 1 1 0`

		p := New()
		m := parse(t, p, input)

		comparePoints(t, []raytracer.Point{
			m.Vertex(0),
			m.Vertex(1),
			m.Vertex(2),
			m.Vertex(3),
		}, []raytracer.Point{
			raytracer.NewPoint(-1, 1, 0),
			raytracer.NewPoint(-1, 0.5, 0),
			raytracer.NewPoint(1, 0, 0),
			raytracer.NewPoint(1, 1, 0),
		})
	})

	t.Run("parsing faces", func(t *testing.T) {
		const input = `v -1 1 0
v -1 0 0
v 1 0 0
v 1 1 0

f 1 2 3
f 1 3 4`

		p := New()
		m := parse(t, p, input)

		t1 := m.Groups()[0].Triangles()[0]
		t2 := m.Groups()[0].Triangles()[1]

		comparePoints(t, t1.Points(), []raytracer.Point{
			raytracer.NewPoint(-1, 1, 0),
			raytracer.NewPoint(-1, 0, 0),
			raytracer.NewPoint(1, 0, 0),
		})

		comparePoints(t, t2.Points(), []raytracer.Point{
			raytracer.NewPoint(-1, 1, 0),
			raytracer.NewPoint(1, 0, 0),
			raytracer.NewPoint(1, 1, 0),
		})
	})

	t.Run("parsing polygon", func(t *testing.T) {
		const input = `v 5 5 5
v -1 1 0
v -1 0 0
v 1 0 0
v 1 1 0
v 0 2 0

f 2 3 4 5 6`

		p := New()
		m := parse(t, p, input)

		t1 := m.Groups()[0].Triangles()[0]
		t2 := m.Groups()[0].Triangles()[1]
		t3 := m.Groups()[0].Triangles()[2]

		comparePoints(t, t1.Points(), []raytracer.Point{
			m.Vertex(1),
			m.Vertex(2),
			m.Vertex(3),
		})

		comparePoints(t, t2.Points(), []raytracer.Point{
			m.Vertex(1),
			m.Vertex(3),
			m.Vertex(4),
		})

		comparePoints(t, t3.Points(), []raytracer.Point{
			m.Vertex(1),
			m.Vertex(4),
			m.Vertex(5),
		})

	})

	t.Run("parsing groups", func(t *testing.T) {
		const input = `v -1 1 0
v -1 0 0
v 1 0 0
v 1 1 0

g FirstGroup
f 1 2 3
g SecondGroup
f 1 3 4
`

		p := New()
		m := parse(t, p, input)

		var (
			g1 = m.Groups()[0]
			g2 = m.Groups()[1]
		)

		if g1.Name() != "FirstGroup" {
			t.Fatalf("expected group 'FirstGroup' not present")
		}

		if g2.Name() != "SecondGroup" {
			t.Fatalf("expecged group 'SecondGroup' not present")
		}

		t1 := g1.Triangles()[0]
		t2 := g2.Triangles()[0]

		comparePoints(t, t1.Points(), []raytracer.Point{
			m.Vertex(0),
			m.Vertex(1),
			m.Vertex(2),
		})

		comparePoints(t, t2.Points(), []raytracer.Point{
			m.Vertex(0),
			m.Vertex(2),
			m.Vertex(3),
		})
	})

	t.Run("parsing normals", func(t *testing.T) {
		const input = `vn 0 0 1
vn 0.707 0 -0.707
vn 1 2 3`

		p := New()
		m := parse(t, p, input)

		compareVectors(t, []raytracer.Vector{
			m.Normal(0),
			m.Normal(1),
			m.Normal(2),
		}, []raytracer.Vector{
			raytracer.NewVector(0, 0, 1),
			raytracer.NewVector(0.707, 0, -0.707),
			raytracer.NewVector(1, 2, 3),
		})
	})

	t.Run("faces with normals", func(t *testing.T) {
		const input = `v 0 1 0
v -1 0 0
v 1 0 0

vn -1 0 0
vn 1 0 0
vn 0 1 0

f 1//3 2//1 3//2
f 1/0/3 2/102/1 3/14/2`

		p := New()
		m := parse(t, p, input)

		t1 := m.Groups()[0].Triangles()[0]
		t2 := m.Groups()[0].Triangles()[1]

		comparePoints(t, t1.Points(), []raytracer.Point{
			m.Vertex(0),
			m.Vertex(1),
			m.Vertex(2),
		})

		comparePoints(t, t2.Points(), []raytracer.Point{
			m.Vertex(0),
			m.Vertex(1),
			m.Vertex(2),
		})

		compareVectors(t, t1.Normals(), []raytracer.Vector{
			m.Normal(2),
			m.Normal(0),
			m.Normal(1),
		})

		compareVectors(t, t2.Normals(), []raytracer.Vector{
			m.Normal(2),
			m.Normal(0),
			m.Normal(1),
		})

	})

	t.Run("parsing textures", func(t *testing.T) {
		const input = `vt 0.382191 0.156309
	vt 0.457844 0.120638
	vt 0.483262 0.105808`

		p := New()
		m := parse(t, p, input)

		compareUVs(t, []raytracer.UV{
			m.UV(0),
			m.UV(1),
			m.UV(2),
		}, []raytracer.UV{
			{0.382191, 0.156309},
			{0.457844, 0.120638},
			{0.483262, 0.105808},
		})
	})

}
