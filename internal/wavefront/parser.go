package wavefront

import (
	"bufio"
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/resources"
	"io"
	"strconv"
	"strings"
)

type face struct {
	vertIdx, texIdx, normIdx int
}

type UV struct {
	U raytracer.Scalar
	V raytracer.Scalar
}

type Reader struct {
	raytracer.MeshBuilder

	// collection of materials
	Materials map[string]*raytracer.MaterialProperties

	// currently active material
	Material *raytracer.MaterialProperties

	// directory to find any resources in
	// .mtl files, for example
	resources.Loader

	resources []string

	warnings []error
	line     int

	faces   []raytracer.Face   // face buffer
	ints    []int              // int buffer
	scalars []raytracer.Scalar // scalar buffer
}

func New() *Reader {
	return &Reader{
		Materials: map[string]*raytracer.MaterialProperties{},
	}
}

func (p *Reader) warn(msg string, args ...interface{}) {
	p.warnings = append(p.warnings, fmt.Errorf("line %d: %s", p.line, fmt.Sprintf(msg, args...)))
}

// parse string arguments into scalar buffer
// overwrites whatever is in scalar buffer
// returns error if any input fails to parse
func (p *Reader) parseScalars(args []string) error {
	p.scalars = p.scalars[:0]
	for _, input := range args {
		f, err := strconv.ParseFloat(input, 64)
		if err != nil {
			return fmt.Errorf("failed parsing floating point: %q: %s", input, err)
		}
		p.scalars = append(p.scalars, raytracer.Scalar(f))
	}
	return nil
}

func (p *Reader) parseInts(args []string) error {
	p.ints = p.ints[:0]
	for _, input := range args {
		n, err := strconv.ParseInt(input, 10, 64)
		if err != nil {
			return fmt.Errorf("failed parsing integer: %q: %s", input, err)
		}
		p.ints = append(p.ints, int(n))
	}
	return nil
}

func (p *Reader) parseFaceArgs(args []string) error {
	p.faces = p.faces[:0]

	for i, input := range args {

		parts := strings.Split(input, "/")

		var f raytracer.Face

		if len(parts) >= 1 {
			v, err := strconv.ParseInt(parts[0], 10, 64)
			if err != nil {
				return fmt.Errorf("failed parsing vertex index: %q: %s", input, err)
			}
			f.Vertex = int(v) - 1
		}

		if len(parts) >= 2 {
			// second part is optional
			if parts[1] != "" {
				n, err := strconv.ParseInt(parts[1], 10, 64)
				if err != nil {
					return fmt.Errorf("failed parsing texture index: %q: %s", input, err)
				}

				x := int(n) - 1
				f.UV = &x
			}
		}

		if len(parts) >= 3 {
			n, err := strconv.ParseInt(parts[2], 10, 64)
			if err != nil {
				return fmt.Errorf("failed parsing normal index: %q: %s", input, err)
			}
			x := int(n) - 1
			f.Normal = &x
		}

		if len(parts) == 0 {
			return fmt.Errorf("malformed face argument %d: %q", i, input)
		}

		p.faces = append(p.faces, f)
	}

	return nil
}

func (p *Reader) parseFace(args []string) {
	if len(args) < 3 {
		p.warn("expected at least three vertices. have %d", len(args))
		return
	}

	if err := p.parseFaceArgs(args); err != nil {
		p.warn("failed parsing face arguments: %s", err)
		return
	}

	p.MeshBuilder.DefinePolygon(p.faces)
}

func (p *Reader) parseVertex(args []string) {
	if len(args) != 3 {
		p.warn("expected three points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.MeshBuilder.DefineVertex(raytracer.NewPoint(p.scalars[0], p.scalars[1], p.scalars[2]))
}

func (p *Reader) parseNormals(args []string) {
	if len(args) != 3 {
		p.warn("expected three points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.MeshBuilder.DefineNormal(raytracer.NewVector(p.scalars[0], p.scalars[1], p.scalars[2]))
}

func (p *Reader) parseTextures(args []string) {
	if len(args) < 2 {
		p.warn("expected two points. have %d", len(args))
		return
	}

	if err := p.parseScalars(args); err != nil {
		p.warn("failed parsing arguments: %s", err)
		return
	}

	p.MeshBuilder.DefineUV(raytracer.UV{U: p.scalars[0], V: p.scalars[1]})
}

func (p *Reader) parseGroup(args []string) {
	if len(args) != 1 {
		p.warn("expected group to have exactly one argument. have %d", len(args))
		return
	}

	p.MeshBuilder.DefineGroup(args[0])
}

func (p *Reader) parseLine(line string) {
	if line == "" { // allow blank lines
		return
	}

	if strings.HasPrefix(line, "#") {
		return
	}

	parts := strings.Fields(line)
	if len(parts) <= 1 {
		p.warn("invalid format: %q", line)
	}

	command, args := parts[0], parts[1:]

	switch command {
	case "v":
		p.parseVertex(args)
	case "f":
		p.parseFace(args)
	case "g":
		p.parseGroup(args)
	case "vn":
		p.parseNormals(args)
	case "vt":
		p.parseTextures(args)
	case "mtllib":
		p.parseMTL(args)
	case "usemtl":
		p.parseUseMTL(args)
	case "s":
		// skip smooth shading options
		return
	default:
		p.warn("unrecognized command: %s", parts[0])
	}
}

func (p *Reader) Parse(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		p.line++
		p.parseLine(strings.TrimSpace(scanner.Text()))
	}

	return scanner.Err()
}

func (p *Reader) Builder() raytracer.MeshBuilder {
	return p.MeshBuilder
}

func (p *Reader) Build() *raytracer.Mesh {
	return p.MeshBuilder.Build()
}

func (p *Reader) Warnings() []error {
	return p.warnings
}

func (p *Reader) Resources() []string {
	return p.resources
}
