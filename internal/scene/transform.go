package scene

import (
	"errors"
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"strings"
)

type Axis int

const (
	X Axis = iota
	Y
	Z
)

type Transformer interface {
	Scale(raytracer.Scale)
	RotateX(raytracer.Scalar)
	RotateY(raytracer.Scalar)
	RotateZ(raytracer.Scalar)
	Shear(raytracer.Shear)
	Translate(raytracer.Translation)
}

func ApplyTransforms(t Transformer, transforms []TransformDefinition) {
	for i := len(transforms) - 1; i >= 0; i-- {
		transform := transforms[i]
		transform.Apply(t)
	}
}

type TransformDefinition struct {
	Op         string
	Definition interface {
		Apply(Transformer)
	}
}

func (d *TransformDefinition) Apply(t Transformer) {
	d.Definition.Apply(t)
}

func (d *TransformDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition []interface{}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshaling transform definition: %s", err)
	}

	if len(def) == 0 {
		return errors.New("expected at least one directive in transform expression")
	}

	op, ok := def[0].(string)
	if !ok {
		return fmt.Errorf("invalid value for transform expression: %q", def[0])
	}
	d.Op = op

	args, err := toScalars(def[1:])
	if err != nil {
		return fmt.Errorf("failed parsing transform operands: %s", err)
	}

	switch strings.TrimSpace(op) {
	case "scale":
		if len(args) != 3 {
			return errors.New("expected exactly 3 arguments to scale transformation")
		}
		d.Definition = &ScaleDefinition{
			X: args[0],
			Y: args[1],
			Z: args[2],
		}

	case "rotate-x":
		if len(args) != 1 {
			return errors.New("expected exactly 1 arguments to rotation transformation")
		}
		d.Definition = &RotationDefinition{
			Axis:    X,
			Degrees: args[0],
		}
	case "rotate-y":
		if len(args) != 1 {
			return errors.New("expected exactly 1 arguments to rotation transformation")
		}
		d.Definition = &RotationDefinition{
			Axis:    Y,
			Degrees: args[0],
		}
	case "rotate-z":
		if len(args) != 1 {
			return errors.New("expected exactly 1 arguments to rotation transformation")
		}
		d.Definition = &RotationDefinition{
			Axis:    Z,
			Degrees: args[0],
		}
	case "translate":
		if len(args) != 3 {
			return fmt.Errorf("expected exactly 3 arguments to translate transformation")
		}

		d.Definition = &TranslationDefinition{
			X: args[0],
			Y: args[1],
			Z: args[2],
		}
	case "shear":
		if len(args) != 6 {
			return fmt.Errorf("expected exactly 6 arguments to shear transformation")
		}

		d.Definition = &ShearDefinition{
			XY: args[0],
			XZ: args[1],
			YX: args[2],
			YZ: args[3],
			ZX: args[4],
			ZY: args[5],
		}
	default:
		return fmt.Errorf("unknown transformation operation: %s", op)
	}
	return nil
}

type RotationDefinition struct {
	Axis
	Degrees raytracer.Scalar
}

func (r *RotationDefinition) Apply(t Transformer) {
	switch r.Axis {
	case X:
		t.RotateX(r.Degrees)
	case Y:
		t.RotateY(r.Degrees)
	case Z:
		t.RotateZ(r.Degrees)
	default:
		panic(fmt.Sprintf("unrecognized axis: %+v", r.Axis))
	}
}

type ScaleDefinition struct {
	X, Y, Z raytracer.Scalar
}

func (s *ScaleDefinition) Apply(t Transformer) {
	t.Scale(raytracer.NewScale(s.X, s.Y, s.Z))
}

type TranslationDefinition struct {
	X, Y, Z raytracer.Scalar
}

func (d *TranslationDefinition) Apply(t Transformer) {
	t.Translate(raytracer.NewTranslation(d.X, d.Y, d.Z))
}

type ShearDefinition struct {
	XY, XZ, YX, YZ, ZX, ZY raytracer.Scalar
}

func (d *ShearDefinition) Apply(t Transformer) {
	t.Shear(raytracer.NewShear(d.XY, d.XZ, d.YX, d.YZ, d.ZX, d.ZY))
}

func toScalars(in []interface{}) ([]raytracer.Scalar, error) {
	var out = make([]raytracer.Scalar, 0, len(in))

	for _, v := range in {
		switch x := v.(type) {
		case float64:
			out = append(out, raytracer.Scalar(x))
		case int:
			out = append(out, raytracer.Scalar(x))
		default:
			return nil, fmt.Errorf("expected floating point. got %t", v)
		}
	}
	return out, nil
}
