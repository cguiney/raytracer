package scene

import (
	"gitlab.com/cguiney/raytracer"
	"math"
)

type PointDefinition [3]raytracer.Scalar

func (p *PointDefinition) Build() raytracer.Point {
	return raytracer.NewPoint(p[0], p[1], p[2])
}

type VectorDefinition [3]raytracer.Scalar

func (p *VectorDefinition) Build() raytracer.Vector {
	return raytracer.NewVector(p[0], p[1], p[2])
}

type Integrator interface {
	Render(ctx raytracer.RenderCtx, world *raytracer.World, camera *raytracer.Camera) raytracer.Canvas
	Preview() raytracer.Canvas
}


type Scene struct {
	env *Environment

	world  *raytracer.World
	camera *raytracer.Camera
}

func NewScene() *Scene {
	w := raytracer.NewWorld()

	env := NewEnvironment()

	return &Scene{
		env:    env,
		world:  w,
		camera: raytracer.NewCamera(1000, 500, math.Pi/3),
	}
}

func NewSceneWithEnv(env *Environment) *Scene {
	s := NewScene()
	s.env = env
	return s
}

func (s *Scene) AddLight(light raytracer.Light) {
	s.world.AddLight(light)
}

func (s *Scene) SetCamera(c *raytracer.Camera) {
	s.camera = c
}

func (s *Scene) AddObject(object raytracer.Object) {
	s.world.AddObject(object)
}

func (s *Scene) Optimize() {
	s.world.Optimize()
}

func (s *Scene) Render(integrator Integrator) raytracer.Canvas {
	s.world.Optimize()
	return integrator.Render(raytracer.DefaultContext(), s.world, s.camera)
}

func (s *Scene) World() *raytracer.World {
	return s.world
}

func (s *Scene) Camera() *raytracer.Camera {
	return s.camera
}
