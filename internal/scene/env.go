package scene

import (
	"gitlab.com/cguiney/raytracer"
	"gitlab.com/cguiney/raytracer/internal/resources"
)

type Environment struct {
	TmpDir       string
	ResourceRoot string

	resources.Loader

	ObjectBuilders map[string]ObjectBuilder
	Materials      map[string]MaterialBuilder
	Meshes         map[string]raytracer.MeshBuilder
}

func NewEnvironment() *Environment {
	return &Environment{
		ResourceRoot:   "", // default resource root to cwd
		ObjectBuilders: make(map[string]ObjectBuilder),
		Materials:      make(map[string]MaterialBuilder),
		Meshes:         make(map[string]raytracer.MeshBuilder),
	}
}

func NewEnvironmentInDir(resourceRoot string) *Environment {
	env := NewEnvironment()
	env.Loader = resources.Filesystem{Root: resourceRoot}
	env.ResourceRoot = resourceRoot
	return env
}

func LoadPresets(env *Environment) {
	env.Materials["gold"] = func() MaterialBuilder {
		return PresetMaterial{func() raytracer.Material {
			/*
				m := raytracer.MaterialProperties{}
				m.SetColor(raytracer.NewColor(1, 0.86, 0.57))
				m.SetMetalness(0.8)
				m.SetRoughness(0.08)
				return &m
			*/

			albedo := raytracer.NewColor(1, 0.86, 0.57)
			roughness := raytracer.Scalar(0.01)

			return raytracer.EmissiveMaterial{
				Emitter:          raytracer.Emission(raytracer.Black),
				PhysicalMaterial: raytracer.NewMetalMaterial(roughness, albedo),
			}
		}()}
	}()

	env.Materials["mirror"] = func() MaterialBuilder {
		return PresetMaterial{func() raytracer.Material {
			/*
				m := raytracer.MaterialProperties{}
				m.SetColor(raytracer.NewGray(0.8))
				m.SetMetalness(1)
				m.SetRoughness(0.15)
				m.SetReflective(1)
				return &m
			*/

			return raytracer.EmissiveMaterial{
				Emitter:          raytracer.Emission(raytracer.Black),
				PhysicalMaterial: raytracer.NewMirrorMaterial(raytracer.NewGray(0.8)),
			}
		}()}
	}()

	env.Materials["glass"] = func() MaterialBuilder {
		return PresetMaterial{func() raytracer.Material {
			// m := raytracer.Glass

			m := raytracer.NewMaterialProperties()
			m.SetTransparency(1)
			m.SetRefractiveIndex(1.5)

			m.SetColor(raytracer.NewGray(1))
			m.SetAmbient(raytracer.Black)
			m.SetDiffuse(raytracer.Black)

			m.SetSpecular(raytracer.NewGray(0.6))
			m.SetRoughness(0.0001)

			// m.SetMetalness(0.2)
			// m.SetReflective(0.5)
			return m
		}()}
	}()
}
