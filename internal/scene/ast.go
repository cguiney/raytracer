package scene

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io"
	"os"
)

type Bundler interface {
	Bundle(*Environment, *Writer) error
}

type Applicator interface {
	Apply(*Scene) error
}

type DirectiveDefinition struct {
	Definition interface {
		Bundler
		Applicator
	}
}

func (d *DirectiveDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Add    *string `yaml:"add"`
		Define *string `yaml:"define"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed parsing root directive: %s", err)
	}

	if def.Add != nil && def.Define != nil {
		return fmt.Errorf("add and define may not both be declared on the same directive")
	}

	if def.Add == nil && def.Define == nil {
		return fmt.Errorf("add or define must be declared on directive")
	}

	if def.Add != nil {
		add := &AddDefinition{}
		if err := unmarshal(add); err != nil {
			return fmt.Errorf("failed directive: %s", err)
		}

		d.Definition = add
	}

	if def.Define != nil {
		define := &DefineDefinition{}
		if err := unmarshal(define); err != nil {
			return fmt.Errorf("failed directive: %s", err)
		}
		d.Definition = define
	}

	return nil
}

func (d *DirectiveDefinition) Apply(s *Scene) error {
	return d.Definition.Apply(s)
}

func (d *DirectiveDefinition) Bundle(environment *Environment, w *Writer) error {
	d.Definition.Bundle(environment, w)
	return nil
}

type AddDefinition struct {
	Definition interface {
		Applicator
		Bundler
	}
}

func (a *AddDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Type string `yaml:"add"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding add definition: %s", err)
	}

	switch def.Type {
	case "light":
		light := &LightDefinition{}
		if err := unmarshal(light); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		a.Definition = light
	case "camera":
		camera := &CameraDefinition{}
		if err := unmarshal(camera); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		a.Definition = camera
	case "sphere", "cube", "plane", "cylinder", "disk", "rectangle":
		shape := &ShapeDefinition{}
		if err := unmarshal(shape); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		a.Definition = shape
	case "group":
		group := &GroupDefinition{}
		if err := unmarshal(group); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		a.Definition = group
	default:
		ref := &ReferenceDefinition{}
		if err := unmarshal(ref); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		a.Definition = ref
	}

	return nil
}

func (a *AddDefinition) Apply(s *Scene) error {
	return a.Definition.Apply(s)
}

func (a *AddDefinition) Bundle(environment *Environment, w *Writer) error {
	a.Definition.Bundle(environment, w)
	return nil
}

type DefineType int

const (
	DefineObject DefineType = iota
	DefineShape
	DefineMaterial
	DefineReference
)

type DefineDefinition struct {
	Key  string
	Type DefineType

	Object    *FileDefinition
	Shape     *ShapeDefinition
	Material  *MaterialDefinition
	Reference *ReferenceDefinition
}

func (d *DefineDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Key   string `yaml:"define"`
		Value struct {
			Type string `yaml:"add"`
		} `yaml:"value"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed parsing define node: %s", err)
	}

	d.Key = def.Key

	switch def.Value.Type {
	case "obj":
		return d.asObject(unmarshal)
	case "sphere", "cube", "plane", "cylinder", "disk", "rectangle":
		return d.asShape(unmarshal)
	default:
		return d.resolveDefinitionType(unmarshal)
	}
}

func (d *DefineDefinition) Apply(s *Scene) error {
	switch d.Type {
	case DefineObject:
		s.env.ObjectBuilders[d.Key] = d.Object
	case DefineShape:
		s.env.ObjectBuilders[d.Key] = d.Shape
	case DefineReference:
		s.env.ObjectBuilders[d.Key] = d.Reference
	case DefineMaterial:
		s.env.Materials[d.Key] = d.Material
	default:
		return fmt.Errorf("unknown define directive")
	}

	return nil
}

func (d *DefineDefinition) Bundle(env *Environment, w *Writer) error {
	switch d.Type {
	case DefineObject:
		return d.Object.Bundle(env, w)
	case DefineShape:
		return d.Shape.Bundle(env, w)
	case DefineReference:
		return d.Reference.Bundle(env, w)
	case DefineMaterial:
		return d.Material.Bundle(env, w)
	}

	return nil
}

func (d *DefineDefinition) resolveDefinitionType(unmarshal func(interface{}) error) error {
	type set struct {
		Keys map[string]interface{} `yaml:"value"`
	}

	var k = set{}
	if err := unmarshal(&k); err != nil {
		return fmt.Errorf("failed resolving ambiguous definition: %s", err)
	}

	// materials must have either a color or a pattern
	// if it doesn't have either of these, then the directive is defining a shape
	_, hasColor := k.Keys["color"]
	_, hasPattern := k.Keys["pattern"]
	if hasColor || hasPattern {
		return d.asMaterial(unmarshal)
	}

	return d.asReference(unmarshal)
}

func (d *DefineDefinition) asMaterial(unmarshal func(interface{}) error) error {
	type definition struct {
		Value *MaterialDefinition `yaml:"value"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding material definition: %s", err)
	}

	d.Type = DefineMaterial
	d.Material = def.Value

	return nil
}

func (d *DefineDefinition) asReference(unmarshal func(interface{}) error) error {
	type definition struct {
		Value *ReferenceDefinition `yaml:"value"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding shape definition: %s", err)
	}

	d.Type = DefineReference
	d.Reference = def.Value

	return nil
}

func (d *DefineDefinition) asShape(unmarshal func(interface{}) error) error {
	type definition struct {
		Value *ShapeDefinition `yaml:"value"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding shape definition: %s", err)
	}

	d.Type = DefineShape
	d.Shape = def.Value

	return nil
}

func (d *DefineDefinition) asObject(unmarshal func(interface{}) error) error {
	type definition struct {
		Value *FileDefinition `yaml:"value"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding object definition: %s", err)
	}

	d.Type = DefineObject
	d.Object = def.Value

	return nil
}

type Description []DirectiveDefinition

func (d Description) Apply(s *Scene) error {
	for i, node := range d {
		if err := node.Apply(s); err != nil {
			return fmt.Errorf("failed applying directive #%d: %s", i, err)
		}
	}
	return nil
}

func (d Description) Bundle(env *Environment, w *Writer) error {
	for _, node := range d {
		if err := node.Bundle(env, w); err != nil {
			return err
		}
	}
	return nil
}

func Parse(r io.Reader) (Description, error) {
	var ast Description

	decoder := yaml.NewDecoder(r)
	if err := decoder.Decode(&ast); err != nil {
		return nil, fmt.Errorf("failed decoding description: %s", err)
	}

	return ast, nil
}

func ParseFile(fn string) (Description, error) {
	fh, err := os.Open(fn)
	if err != nil {
		return nil, fmt.Errorf("failed opening file: %s", err)
	}
	defer fh.Close()

	return Parse(fh)
}

func LoadFile(env *Environment, fn string) (*Scene, error) {
	ast, err := ParseFile(fn)
	if err != nil {
		return nil, fmt.Errorf("failed parsing yaml: %w", err)
	}

	s := NewSceneWithEnv(env)
	if err := ast.Apply(s); err != nil {
		return nil, fmt.Errorf("failed building scene: %w", err)
	}

	return s, nil
}
