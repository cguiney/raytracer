package scene

import (
	"fmt"
	"gitlab.com/cguiney/raytracer"
)

type LightDefinition struct {
	Type string `yaml:"type"`

	Definition interface {
		Build(*Environment) (raytracer.Light, error)
	}
}

func (d *LightDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Type string `yaml:"type"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshalling light definition: %s", err)
	}

	d.Type = def.Type

	switch def.Type {
	case "distant":
		light := &DistantLightDefinition{}
		if err := unmarshal(&light); err != nil {
			return fmt.Errorf("failed unmarshalling distant light definition: %s", err)
		}
		d.Definition = light
	case "area":
		return fmt.Errorf("area lights should be specified by adding an object with an emissive material")
	default:
		d.Type = "point"
		light := &PointLightDefinition{}
		if err := unmarshal(&light); err != nil {
			return fmt.Errorf("failed unmarshalling point light definition: %s", err)
		}
		d.Definition = light
	}
	return nil
}

func (d *LightDefinition) Apply(scene *Scene) error {
	light, err := d.Definition.Build(scene.env)
	if err != nil {
		return err
	}

	scene.AddLight(light)
	return nil
}

func (d *LightDefinition) Bundle(environment *Environment, w *Writer) error {

	return nil
}

func (d *LightDefinition) Build(environment *Environment) (raytracer.Light, error) {
	return d.Definition.Build(environment)
}

type PointLightDefinition struct {
	At        PointDefinition `yaml:"at"`
	Intensity ColorDefinition `yaml:"intensity"`
}

func (li *PointLightDefinition) Build(*Environment) (raytracer.Light, error) {
	return raytracer.NewPointLight(li.At.Build(), li.Intensity.Build()), nil
}

type DistantLightDefinition struct {
	Direction       VectorDefinition      `yaml:"direction"`
	Radiance        ColorDefinition       `yaml:"radiance"`
	Transformations []TransformDefinition `yaml:"transform"`
}

func (d *DistantLightDefinition) Build(environment *Environment) (raytracer.Light, error) {
	light := raytracer.NewDistantLight(d.Direction.Build(), d.Radiance.Build())
	ApplyTransforms(light, d.Transformations)
	return light, nil
}
