package scene

import (
	"fmt"
	"gitlab.com/cguiney/raytracer"
	"image"
	"os"
	"path/filepath"

	_ "gitlab.com/cguiney/raytracer/internal/ppm"
	_ "image/gif"
	// side effects for image.decode
	_ "image/jpeg"
	_ "image/png"
)

type PatternDefinition struct {
	Type    string            `yaml:"type"`

	Definition interface{
		Build(env *Environment) (raytracer.Pattern, error)
	}
}

func (d *PatternDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Type string `yaml:"type"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshalling pattern definition: %s", err)
	}

	d.Type = def.Type

	switch def.Type {
	case "cube":
		cube := &CubeTextureDefinition{}
		if err := unmarshal(&cube); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = cube
	case "map":
		mt := &MapTextureDefinition{}
		if err := unmarshal(&mt); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = mt
	default:
		return fmt.Errorf("failed decoding pattern definition: unknown pattern type")
	}

	return nil
}

func (d *PatternDefinition) Build(env *Environment) (raytracer.Pattern, error) {
	return d.Definition.Build(env)
}

type MapTextureDefinition struct {
	Type    string            `yaml:"type"`

	Mapping MappingDefinition `yaml:"mapping"`
	Texture TextureDefinition `yaml:"texture"`
}

func (t *MapTextureDefinition) Build(env *Environment) (raytracer.Pattern, error) {
	texture, err := t.Texture.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building texture for map: %s", err)
	}

	mapping := t.Mapping.Build()

	tm := raytracer.NewTextureMap(texture, mapping)

	return tm, nil
}

type TransformTextureDefinition struct {
	Texture         TextureDefinition     `yaml:"texture"`
	Transformations []TransformDefinition `yaml:"transform"`
}

func (d *TransformTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	texture, err := d.Texture.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building texture for transform: %s", err)
	}

	tt := raytracer.NewTextureTransform(texture)

	ApplyTransforms(tt, d.Transformations)

	return tt, nil
}

type TransformScalarTextureDefinition struct {
	Texture         ScalarTextureDefinition `yaml:"texture"`
	Transformations []TransformDefinition   `yaml:"transform"`
}

func (d TransformScalarTextureDefinition) Build(env *Environment) (raytracer.ScalarTexture, error) {
	texture, err := d.Texture.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building texture for transform: %s", err)
	}

	tt := raytracer.NewScalarTextureTransform(texture)

	ApplyTransforms(tt, d.Transformations)

	return tt, nil
}

type AlignCheckTextureDefinition struct {
	Colors struct {
		Main        ColorDefinition `yaml:"main"`
		UpperLeft   ColorDefinition `yaml:"ul"`
		UpperRight  ColorDefinition `yaml:"ur"`
		BottomLeft  ColorDefinition `yaml:"bl"`
		BottomRight ColorDefinition `yaml:"br"`
	} `yaml:"colors"`
}

func (a *AlignCheckTextureDefinition) Build(*Environment) (raytracer.Texture, error) {
	return raytracer.AlignCheckTexture(
		a.Colors.Main.Build(),
		a.Colors.UpperLeft.Build(),
		a.Colors.UpperRight.Build(),
		a.Colors.BottomLeft.Build(),
		a.Colors.BottomRight.Build()), nil
}

type CheckersTextureDefinition struct {
	Width    raytracer.Scalar     `yaml:"width"`
	Height   raytracer.Scalar     `yaml:"height"`
	Textures [2]TextureDefinition `yaml:"textures"`
}

func (c *CheckersTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	a, err := c.Textures[0].Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building checker texture: %w", err)
	}

	b, err := c.Textures[1].Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building checker texture: %w", err)

	}

	return raytracer.CheckersTexture(c.Width, c.Height, a, b), nil
}

type StripesTextureDefinition struct {
	Textures [2]TextureDefinition `yaml:"textures"`
}

func (d *StripesTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	a, err := d.Textures[0].Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building checker texture: %w", err)
	}

	b, err := d.Textures[1].Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building checker texture: %w", err)

	}

	return raytracer.StripesTexture(a, b), nil
}

type ImageTextureDefinition struct {
	File string `yaml:"file"`
}

func (i *ImageTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	fh, err := os.Open(filepath.Join(env.ResourceRoot, i.File))
	if err != nil {
		return nil, fmt.Errorf("failed opening image: %s", err)
	}
	defer fh.Close()

	img, _, err := image.Decode(fh)
	if err != nil {
		return nil, fmt.Errorf("failed decoding image: %s", err)
	}

	return raytracer.NewImage(img), nil
}

type ColorTextureDefinition struct {
	Color ColorDefinition `yaml:"color"`
}

func (c *ColorTextureDefinition) Build(*Environment) (raytracer.Texture, error) {
	return c.Color.Build(), nil
}

type TextureDefinition struct {
	Type string `yaml:"type"`

	Definition interface {
		Build(*Environment) (raytracer.Texture, error)
	}
}

func (d *TextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	if d.Definition == nil {
		return nil, fmt.Errorf("invalid texture definition")
	}
	return d.Definition.Build(env)
}

func (d *TextureDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Type string `yaml:"type"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshalling texture definition: %s", err)
	}

	d.Type = def.Type

	switch def.Type {
	case "align_check":
		align := &AlignCheckTextureDefinition{}
		if err := unmarshal(&align); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}

		d.Definition = align

	case "checkers":
		checkers := &CheckersTextureDefinition{}
		if err := unmarshal(&checkers); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = checkers
	case "stripes":
		stripes := &StripesTextureDefinition{}
		if err := unmarshal(&stripes); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = stripes
	case "image":
		img := &ImageTextureDefinition{}
		if err := unmarshal(&img); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = img

	case "color":
		color := &ColorTextureDefinition{}
		if err := unmarshal(&color); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = color
	case "perlin":
		perlin := &PerlinNoiseTextureDefinition{}
		if err := unmarshal(&perlin); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = perlin
	case "fbm":
		fbm := &FbmTextureDefinition{}
		if err := unmarshal(&fbm); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = fbm
	case "transform":
		transform := &TransformTextureDefinition{}
		if err := unmarshal(&transform); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = transform
	default:
		return fmt.Errorf("unrecognized texture type: %q", d.Type)
	}

	return nil
}

type ScalarTextureDefinition struct {
	Type string `yaml:"type"`

	Definition interface {
		Build(*Environment) (raytracer.ScalarTexture, error)
	}
}

func (d *ScalarTextureDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Type string `yaml:"type"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshalling scalar texture definition: %s", err)
	}

	d.Type = def.Type

	switch def.Type {
	case "constant":
		constant := &ConstantScalarTextureDefinition{}
		if err := unmarshal(&constant); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = constant
	case "perlin":
		perlin := &PerlinNoiseScalarTextureDefinition{}
		if err := unmarshal(&perlin); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = perlin
	case "fbm":
		fbm := &FbmScalarTextureDefinition{}
		if err := unmarshal(&fbm); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = fbm
	case "transform":
		transform := &TransformScalarTextureDefinition{}
		if err := unmarshal(&transform); err != nil {
			return fmt.Errorf("failed decoding %s definition: %s", def.Type, err)
		}
		d.Definition = transform
	default:
		return fmt.Errorf("unrecognized texture type: %q", d.Type)
	}

	return nil
}

func (d *ScalarTextureDefinition) Build(env *Environment) (raytracer.ScalarTexture, error) {
	return d.Definition.Build(env)
}

type PlanarMappingDefinition struct{}

func (PlanarMappingDefinition) Build() raytracer.Mapper {
	return raytracer.MapperFunc(raytracer.PlanarMap)
}

type SphericalMappingDefinition struct{}

func (SphericalMappingDefinition) Build() raytracer.Mapper {
	return raytracer.MapperFunc(raytracer.SphericalMap)
}

type CylindricalMappingDefinition struct{}

func (CylindricalMappingDefinition) Build() raytracer.Mapper {
	return raytracer.MapperFunc(raytracer.CylinderMap)
}

type MappingDefinition struct {
	Mapping string

	Definition interface {
		Build() raytracer.Mapper
	}
}

func (u *MappingDefinition) Build() raytracer.Mapper {
	return u.Definition.Build()
}

func (u *MappingDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var def string
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed unmarshaling uv mapping definition: %s", err)
	}

	switch def {
	case "planar":
		u.Definition = &PlanarMappingDefinition{}
	case "spherical":
		u.Definition = &SphericalMappingDefinition{}
	case "cylindrical":
		u.Definition = &CylindricalMappingDefinition{}
	default:
		return fmt.Errorf("unknown uv mapping type: %s", def)
	}

	return nil
}

type CubeTextureDefinition struct {
	Left  TextureDefinition `yaml:"left"`
	Right TextureDefinition `yaml:"right"`
	Front TextureDefinition `yaml:"front"`
	Back  TextureDefinition `yaml:"back"`
	Up    TextureDefinition `yaml:"up"`
	Down  TextureDefinition `yaml:"down"`
}

func (c *CubeTextureDefinition) Build(env *Environment) (raytracer.Pattern, error) {
	front, err := c.Front.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed buidling front texture: %s", err)
	}

	back, err := c.Back.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building back texture: %s", err)
	}

	left, err := c.Left.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building left texture: %s", err)
	}

	right, err := c.Right.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building right texture: %s", err)
	}

	up, err := c.Up.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building up texture: %s", err)
	}

	down, err := c.Down.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building down texture: %s", err)
	}

	return raytracer.NewCubicTexture(
		front,
		back,
		left,
		right,
		up,
		down,
	), nil
}

type PerlinNoiseTextureDefinition struct {
	Seed int64 `yaml:"seed"`
}

func (d *PerlinNoiseTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	return raytracer.NewPerlin(d.Seed), nil
}

type PerlinNoiseScalarTextureDefinition struct {
	Seed int64 `yaml:"seed"`
}

func (d *PerlinNoiseScalarTextureDefinition) Build(env *Environment) (raytracer.ScalarTexture, error) {
	return raytracer.NewPerlin(d.Seed), nil
}

type FbmTextureDefinition struct {
	Seed    int64            `yaml:"seed"`
	Omega   raytracer.Scalar `yaml:"omega"`
	Octaves int              `yaml:"octaves"`
}

func (d *FbmTextureDefinition) Build(env *Environment) (raytracer.Texture, error) {
	return raytracer.NewFbmTexture(d.Seed, d.Omega, d.Octaves), nil
}

type FbmScalarTextureDefinition struct {
	Seed    int64            `yaml:"seed"`
	Omega   raytracer.Scalar `yaml:"omega"`
	Octaves int              `yaml:"octaves"`
}

func (d *FbmScalarTextureDefinition) Build(env *Environment) (raytracer.ScalarTexture, error) {
	return raytracer.NewFbmTexture(d.Seed, d.Omega, d.Octaves), nil
}

type ConstantScalarTextureDefinition struct {
	raytracer.Scalar `yaml:"value"`
}

func (d ConstantScalarTextureDefinition) Build(env *Environment) (raytracer.ScalarTexture, error) {
	return d.Scalar, nil
}
