package scene

import (
	"archive/zip"
	"flag"
	"fmt"
	"gitlab.com/cguiney/raytracer/internal/resources"
	"io"
	"io/ioutil"
	"os"
)

type Writer struct {
	w   *zip.Writer
	err error
}

func NewWriter(w io.Writer) *Writer {
	return &Writer{w: zip.NewWriter(w)}
}

func (b *Writer) WriteScene(path string) {
	b.writeFile("scene.yml", path)
}

func (b *Writer) WriteResource(name, path string) {
	b.writeFile(name, path)
}

func (b *Writer) writeFile(name string, path string) {
	if b.err != nil {
		return
	}

	r, err := os.Open(path)
	if err != nil {
		b.err = fmt.Errorf("failed opening path %q: %s", path, err)
		return
	}

	b.write(name, r)
}

func (b *Writer) write(name string, r io.Reader) {
	if b.err != nil {
		return
	}

	w, err := b.w.Create(name)
	if err != nil {
		b.err = fmt.Errorf("failed adding resource %q: %s", name, err)
		return
	}

	b.copy(w, r)
}

func (b *Writer) Err() error {
	return b.err
}

func (b *Writer) Flush() {
	if b.err != nil {
		return
	}

	b.err = b.w.Flush()
}

func (b *Writer) Close() {
	if b.err != nil {
		return
	}
	b.err = b.w.Close()
}

func (b *Writer) copy(dst io.Writer, src io.Reader) {
	_, b.err = io.Copy(dst, src)
}

func Bundle(env *Environment, file string, out io.Writer) error {
	w := NewWriter(out)
	w.WriteScene(file)

	ast, err := ParseFile(file)
	if err != nil {
		return fmt.Errorf("failed parsing yaml: %w", err)
	}

	if err := ast.Bundle(env, w); err != nil {
		return fmt.Errorf("error bundling scene: %w", err)
	}

	w.WriteScene(flag.Arg(0))
	w.Close()

	if err := w.Err(); err != nil {
		return fmt.Errorf("error writing bundle: %w", err)
	}

	return nil
}

// LoadBundle loads the given file into the given scene
func LoadBundle(env *Environment, file string) (*Scene, error) {
	r, err := env.OpenSection(file)
	if err != nil {
		return nil, fmt.Errorf("failed opening bundle: %w", err)
	}

	zf, err := zip.NewReader(r, r.Size())
	if err != nil {
		return nil, fmt.Errorf("failed reading bundle: %w", err)
	}

	loader := &zipLoader{
		tmpdir: env.TmpDir,
		r:      zf,
	}

	desc, err := loader.Open("scene.yml")
	if err != nil {
		return nil, fmt.Errorf("failed reading scene description: %w", err)
	}
	defer desc.Close()

	ast, err := Parse(desc)
	if err != nil {
		return nil, fmt.Errorf("failed parsing scene description: %w", err)
	}

	bundleEnv := *env
	bundleEnv.Loader = loader

	scene := NewSceneWithEnv(&bundleEnv)
	if err := ast.Apply(scene); err != nil {
		return nil, fmt.Errorf("failed building scene: %w", err)
	}

	return scene, nil
}

type zipLoader struct {
	tmpdir string // used for when a readerat is needed
	r      *zip.Reader
}

func (z *zipLoader) ReadAll(path string) ([]byte, error) {
	r, err := z.Open(path)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("failed reading file %q from bundle: %w", path, err)
	}

	return b, nil
}

func (z *zipLoader) Open(path string) (io.ReadCloser, error) {
	f, err := z.findFile(path)
	if err != nil {
		return nil, err
	}

	r, err := f.Open()
	if err != nil {
		return nil, fmt.Errorf("failed opening file %q: %w", path, err)
	}

	return r, nil
}

func (z *zipLoader) OpenSection(path string) (resources.Section, error) {
	r, err := z.Open(path)
	if err != nil {
		return nil, err
	}

	tmpfile, err := ioutil.TempFile(z.tmpdir, "")
	if err != nil {
		return nil, fmt.Errorf("failed creating tmp file: %w", err)
	}

	n, err := io.Copy(tmpfile, r)
	if err != nil {
		return nil, nil
	}

	if _, err := tmpfile.Seek(0, io.SeekStart); err != nil {
		return nil, fmt.Errorf("failed resetting tmp file: %w", err)
	}

	sr := struct {
		*io.SectionReader
		io.Closer
	}{
		io.NewSectionReader(tmpfile, 0, n),
		tmpfile,
	}

	return sr, nil
}

func (z *zipLoader) findFile(name string) (*zip.File, error) {
	for _, file := range z.r.File {
		if file.Name == name {
			return file, nil
		}
	}
	return nil, fmt.Errorf("could not find file in bundle: %q", name)
}
