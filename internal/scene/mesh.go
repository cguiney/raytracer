package scene

import (
	"flag"
	"fmt"
	"gitlab.com/cguiney/raytracer/internal/wavefront"
	"log"
	"os"
	"path/filepath"
)

var (
	objImplementation = flag.String("p", "mesh", "object parser to use. values are 'mesh' or 'legacy'")
)

type FileDefinition struct {
	File      string                `yaml:"file"`
	Transform []TransformDefinition `yaml:"transform"`
}

func (f *FileDefinition) Bundle(env *Environment, w *Writer) error {
	w.WriteResource(f.File, filepath.Join(env.ResourceRoot, f.File))

	fh, err := os.Open(filepath.Join(env.ResourceRoot, f.File))
	if err != nil {
		return nil
	}
	defer fh.Close()

	p := wavefront.New()
	p.Loader = env.Loader

	if err := p.Parse(fh); err != nil {
		return fmt.Errorf("failed parsing object file %q: %s", f.File, err)
	}

	for _, file := range p.Resources() {
		w.WriteResource(file, filepath.Join(env.ResourceRoot, file))
	}

	return nil
}

func (f *FileDefinition) Build(env *Environment) (Object, error) {
	fh, err := env.Loader.Open(f.File)
	if err != nil {
		return nil, fmt.Errorf("failed opening object file %q: %s", f.File, err)
	}
	defer fh.Close()

	switch *objImplementation {
	case "mesh":
		if b, ok := env.Meshes[f.File]; ok {
			return b.Build(), nil
		}

		p := wavefront.New()
		p.Loader = env.Loader

		if err := p.Parse(fh); err != nil {
			return nil, fmt.Errorf("failed parsing object file %q: %s", f.File, err)
		}

		for _, warning := range p.Warnings() {
			log.Println(warning)
		}

		env.Meshes[f.File] = p.Builder()

		return p.Build(), nil
	case "legacy":
		p := wavefront.NewLegacy()
		p.ResourceRoot = env.ResourceRoot
		if err := p.Parse(fh); err != nil {
			return nil, fmt.Errorf("failed parsing object file %q: %s", f.File, err)
		}
		for _, warning := range p.Warnings() {
			log.Println(warning)
		}

		return p.Root, nil
	default:
		panic("unknown object parser")
	}
}

func (f *FileDefinition) Transforms(*Environment) []TransformDefinition {
	return f.Transform
}
