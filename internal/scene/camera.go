package scene

import "gitlab.com/cguiney/raytracer"

type CameraDefinition struct {
	Width  int              `yaml:"width"`
	Height int              `yaml:"height"`
	Fov    raytracer.Scalar `yaml:"field-of-view"`
	From   PointDefinition  `yaml:"from"`
	To     PointDefinition  `yaml:"to"`
	Up     VectorDefinition `yaml:"up"`
}

func (c *CameraDefinition) Bundle(*Environment, *Writer) error {

	return nil
}

func (c *CameraDefinition) Build() *raytracer.Camera {
	camera := raytracer.NewCamera(c.Width, c.Height, c.Fov)
	camera.ViewTransform(raytracer.NewViewTransform(c.From.Build(), c.To.Build(), c.Up.Build()))
	return camera
}

func (c *CameraDefinition) Apply(s *Scene) error {
	s.SetCamera(c.Build())
	return nil
}
