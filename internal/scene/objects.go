package scene

import "gitlab.com/cguiney/raytracer"

type Object interface {
	raytracer.Object
	Transformer
	SetMaterial(material raytracer.Material)
}

type Bound interface {
	TruncateLower(raytracer.Scalar)
	TruncateUpper(raytracer.Scalar)
	SetClosed(bool)
}

type Shadower interface {
	SetShadows(bool)
}
