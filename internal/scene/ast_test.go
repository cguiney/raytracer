package scene

import (
	"path/filepath"
	"testing"
)

func TestParseExamples(t *testing.T) {
	files, err := filepath.Glob("../../examples/*.yml")
	if err != nil {
		t.Fatal("failed listing example files:", err)
	}

	for _, file := range files {
		t.Run(filepath.Base(file), func(t *testing.T) {
			ast, err := ParseFile(file)
			if err != nil {
				t.Fatalf("failed parsing file: %s", err)
			}

			scene := NewSceneWithEnv(NewEnvironmentInDir("../../resources"))
			LoadPresets(scene.env)

			if err := ast.Apply(scene); err != nil {
				t.Fatalf("failed building scene: %s", err)
			}
		})
	}
}
