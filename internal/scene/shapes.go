package scene

import (
	"fmt"
	"gitlab.com/cguiney/raytracer"
)

type ObjectBuilder interface {
	// Build returns a new instance of the described instance
	// *without* any transformations applied.
	// Transformations cannot be applied first, because there may be
	// objects inheriting from it that depend on their transformations being applied
	// first.
	Build(*Environment) (Object, error)

	// Transforms returns a list of transform definitions
	Transforms(*Environment) []TransformDefinition

	Bundler
}

type BoundingDefinition struct {
	Min    *raytracer.Scalar `yaml:"min"`
	Max    *raytracer.Scalar `yaml:"max"`
	Closed *bool             `yaml:"closed"`
}

func (d *BoundingDefinition) Bound(object Object) {
	bound, ok := object.(Bound)
	if !ok {
		return
	}
	if d.Min != nil {
		bound.TruncateLower(*d.Min)
	}

	if d.Max != nil {
		bound.TruncateUpper(*d.Max)
	}

	if d.Closed != nil {
		bound.SetClosed(*d.Closed)
	}
}

type ShapeDefinition struct {
	Type string `yaml:"add"`

	// MaterialProperties could be a reference to a defined MaterialDefinition
	// or defining it inline
	Material        *MaterialDefinition   `yaml:"material"`
	Transformations []TransformDefinition `yaml:"transform"`

	// bounded objects
	BoundingDefinition `yaml:",inline"`

	Shadow *bool `yaml:"shadow"`
}

func (d *ShapeDefinition) Bundle(env *Environment, w *Writer) error {
	return d.Material.Bundle(env, w)
}

func (d *ShapeDefinition) Build(env *Environment) (Object, error) {
	var object Object

	switch d.Type {
	case "sphere":
		object = raytracer.NewSphere()
	case "cube":
		object = raytracer.NewCube()
	case "plane":
		object = raytracer.NewPlane()
	case "cylinder":
		object = raytracer.NewCylinder()
	case "disk":
		object = raytracer.NewDisk(1, 1.5, 0, 360)
	case "rectangle":
		object = raytracer.NewRectangle()
	default:
		return nil, fmt.Errorf("unknown shape: %s", d.Type)
	}

	if d.Material != nil {
		material, err := d.Material.Build(env)
		if err != nil {
			return nil, fmt.Errorf("failed building material for %s: %s", d.Type, err)
		}
		object.SetMaterial(material)
	}

	// allow the object to define shadowing as well, it should take priority
	if shadower, ok := object.(Shadower); ok {
		if d.Shadow != nil {
			shadower.SetShadows(*d.Shadow)
		}
	}

	d.BoundingDefinition.Bound(object)

	return object, nil
}

func (d *ShapeDefinition) Transforms(*Environment) []TransformDefinition {
	return d.Transformations
}

func (d *ShapeDefinition) Apply(s *Scene) error {
	object, err := d.Build(s.env)
	if err != nil {
		return fmt.Errorf("failed building object: %s", err)
	}

	ApplyTransforms(object, d.Transforms(s.env))

	s.AddObject(object)
	return nil
}

type ReferenceDefinition struct {
	Key                string                `yaml:"add"`
	Transformations    []TransformDefinition `yaml:"transform"`
	Material           *MaterialDefinition   `yaml:"material"`
	BoundingDefinition `yaml:",inline"`
}

func (r *ReferenceDefinition) Bundle(env *Environment, w *Writer) error {
	return r.Material.Bundle(env, w)
}

func (r *ReferenceDefinition) Build(env *Environment) (Object, error) {
	builder, ok := env.ObjectBuilders[r.Key]
	if !ok {
		return nil, fmt.Errorf("unknown parent %s: unknown definition", r.Key)
	}

	object, err := builder.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building object: %s", err)
	}

	if r.Material != nil {
		material, err := r.Material.Build(env)
		if err != nil {
			return nil, fmt.Errorf("failed building material for %s: %s", r.Key, err)
		}
		object.SetMaterial(material)
	}

	r.BoundingDefinition.Bound(object)

	return object, nil
}

func (r *ReferenceDefinition) Transforms(env *Environment) []TransformDefinition {
	builder, ok := env.ObjectBuilders[r.Key]
	if !ok {
		return r.Transformations
	}

	return append(builder.Transforms(env), r.Transformations...)
}

func (r *ReferenceDefinition) Apply(s *Scene) error {
	object, err := r.Build(s.env)
	if err != nil {
		return fmt.Errorf("failed building object type %s: %s", r.Key, err)
	}

	ApplyTransforms(object, r.Transforms(s.env))

	s.AddObject(object)

	return nil
}
