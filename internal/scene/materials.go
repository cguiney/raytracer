package scene

import (
	"errors"
	"fmt"
	"gitlab.com/cguiney/raytracer"
)

type MaterialBuilder interface {
	Build(*Environment) (raytracer.Material, error)
}

type ColorDefinition [3]raytracer.Scalar

func (c ColorDefinition) Build() raytracer.Color {
	return raytracer.NewColor(c[0], c[1], c[2])
}

func (c *ColorDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var raw interface{}

	if err := unmarshal(&raw); err != nil {
		return fmt.Errorf("failed parsing color definition: %w", err)
	}

	if v, ok := raw.(float64); ok {
		*c = [3]raytracer.Scalar{
			raytracer.Scalar(v),
			raytracer.Scalar(v),
			raytracer.Scalar(v),
		}
		return nil
	}

	var def [3]raytracer.Scalar
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed parsing color definition: %w", err)
	}

	*c = def
	return nil
}

type MaterialDefinition struct {
	Key  string
	Type string `yaml:"type"`

	EmissionDefinition `yaml:",inline"`

	PhysicalDefinition interface {
		Bundler
		Build(env *Environment) (raytracer.PhysicalMaterial, error)
	}
}

func (d *MaterialDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var raw interface{}
	if err := unmarshal(&raw); err != nil {
		return fmt.Errorf("failed decoding material: %s", err)
	}

	if key, ok := raw.(string); ok {
		d.Key = key
		return nil
	}

	type definition struct {
		Type string `yaml:"type"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding material: %s", err)
	}

	d.Type = def.Type

	switch d.Type {
	case "glass":
		var def GlassMaterialDefinition
		if err := unmarshal(&def); err != nil {
			return fmt.Errorf("failed decoding glass material: %s", err)
		}
		d.PhysicalDefinition = &def
	case "metal":
		var def MetalMaterialDefinition
		if err := unmarshal(&def); err != nil {
			return fmt.Errorf("failed decoding metal material: %w", err)
		}
		d.PhysicalDefinition = &def
	default:
		var def MaterialPropertiesDefinition
		if err := unmarshal(&def); err != nil {
			return fmt.Errorf("failed decoding material: %s", err)
		}
		d.PhysicalDefinition = &def
	}

	if err := unmarshal(&d.EmissionDefinition); err != nil {
		return fmt.Errorf("failed decoding emission: %w", err)
	}

	return nil
}

func (d *MaterialDefinition) Bundle(environment *Environment, w *Writer) error {
	return nil
}

func (d *MaterialDefinition) Build(env *Environment) (raytracer.Material, error) {
	if d.Key != "" {
		mb, ok := env.Materials[d.Key]
		if !ok {
			return nil, fmt.Errorf("material not found: %s", d.Key)
		}
		return mb.Build(env)
	}

	physical, err := d.PhysicalDefinition.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building physical material: %w", err)
	}

	emission, err := d.EmissionDefinition.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building emitter: %w", err)
	}

	return raytracer.EmissiveMaterial{
		Emitter:          emission,
		PhysicalMaterial: physical,
	}, nil
}

type GlassMaterialDefinition struct {
	Roughness    *raytracer.Scalar `yaml:"roughness"`
	IOR          *raytracer.Scalar `yaml:"ior"`
	Transparency TextureDefinition `yaml:"transparency"`
	Reflection   TextureDefinition `yaml:"reflection"`
}

func (d GlassMaterialDefinition) Build(env *Environment) (raytracer.PhysicalMaterial, error) {
	t, err := d.Transparency.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building transparency texture for glass: %s", err)
	}

	r, err := d.Reflection.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building reflection texture for glass: %s", err)
	}

	var (
		roughness raytracer.Scalar = 0.0001
		ior       raytracer.Scalar = 1.5
	)

	if d.Roughness != nil {
		roughness = *d.Roughness
	}

	if d.IOR != nil {
		ior = *d.IOR
	}

	return raytracer.NewGlassMaterial(roughness, ior, t, r), nil
}

func (g GlassMaterialDefinition) Bundle(env *Environment, w *Writer) error {
	panic("implement me")
}

type MetalMaterialDefinition struct {
	Roughness *ScalarTextureDefinition `yaml:"roughness"`
	Albedo    *TextureDefinition       `yaml:"albedo"`
}

func (d MetalMaterialDefinition) Bundle(environment *Environment, writer *Writer) error {
	panic("implement me")
}

func (d MetalMaterialDefinition) Build(env *Environment) (raytracer.PhysicalMaterial, error) {
	if d.Roughness == nil {
		return nil, errors.New("failed building metal: no roughness specified")
	}

	roughness, err := d.Roughness.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building metal roughness texture: %w", err)
	}

	if d.Albedo == nil {
		return nil, errors.New("failed building metal: no albedo specified")
	}

	albedo, err := d.Albedo.Build(env)
	if err != nil {
		return nil, fmt.Errorf("failed building metal albedo texture: %w", err)
	}

	return raytracer.NewMetalMaterial(roughness, albedo), nil
}

type MaterialPropertiesDefinition struct {
	Pattern *PatternDefinition `yaml:"pattern"`
	Color   *ColorDefinition   `yaml:"color"`

	Ambient         *raytracer.Scalar `yaml:"ambient"`
	Diffuse         *raytracer.Scalar `yaml:"diffuse"`
	Specular        *raytracer.Scalar `yaml:"specular"`
	Shininess       *raytracer.Scalar `yaml:"shininess"`
	Reflective      *raytracer.Scalar `yaml:"reflective"`
	Transparency    *raytracer.Scalar `yaml:"transparency"`
	RefractiveIndex *raytracer.Scalar `yaml:"refractive-index"`
	Emission        *raytracer.Scalar `yaml:"emission"`
	Metalness       *raytracer.Scalar `yaml:"metalness"`
	Roughness       *raytracer.Scalar `yaml:"roughness"`
}

func (d *MaterialPropertiesDefinition) Build(env *Environment) (raytracer.PhysicalMaterial, error) {
	m := raytracer.NewMaterialProperties()

	if d.Pattern != nil {
		pattern, err := d.Pattern.Build(env)
		if err != nil {
			return nil, fmt.Errorf("failed building pattern for material: %s", err)
		}
		m.SetPattern(pattern)
	}

	if d.Color != nil {
		m.SetColor(d.Color.Build())
	}

	if d.Diffuse != nil {
		m.SetDiffuse(raytracer.NewGray(*d.Diffuse))
	}

	if d.Ambient != nil {
		m.SetAmbient(raytracer.NewGray(*d.Ambient))
	}

	if d.Specular != nil {
		m.SetSpecular(raytracer.NewGray(*d.Specular))
	}

	if d.Shininess != nil {
		m.SetShininess(*d.Shininess)
	}

	if d.Reflective != nil {
		m.SetReflective(*d.Reflective)
	}

	if d.Transparency != nil {
		m.SetTransparency(*d.Transparency)
	}

	if d.RefractiveIndex != nil {
		m.SetRefractiveIndex(*d.RefractiveIndex)
	}

	if d.Emission != nil {
		m.SetEmission(raytracer.NewGray(*d.Emission))
	}

	if d.Metalness != nil {
		m.SetMetalness(*d.Metalness)
	}

	if d.Roughness != nil {
		m.SetRoughness(*d.Metalness)
	}

	return m, nil
}

func (d *MaterialPropertiesDefinition) Bundle(env *Environment, w *Writer) error {
	return nil
}

type EmissionDefinition struct {
	Emission *ColorDefinition `yaml:"emission"`
}

func (d *EmissionDefinition) Bundle(env *Environment, w *Writer) error {
	panic("implement me")
}

func (d *EmissionDefinition) Build(env *Environment) (raytracer.Emitter, error) {
	if d.Emission == nil {
		return raytracer.Emission(raytracer.Black), nil
	}
	return raytracer.Emission(d.Emission.Build()), nil
}

type PresetMaterial struct {
	raytracer.Material
}

func (p PresetMaterial) Build(*Environment) (raytracer.Material, error) {
	return p.Material, nil
}
