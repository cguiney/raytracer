package scene

import (
	"fmt"
	"gitlab.com/cguiney/raytracer"
)

var _ ObjectBuilder = &GroupDefinition{}

type GroupDefinition struct {
	Transform []TransformDefinition
	Children  []ObjectBuilder
}

func (g *GroupDefinition) UnmarshalYAML(unmarshal func(interface{}) error) error {
	type definition struct {
		Transform []TransformDefinition `yaml:"transform"`
		Children  []AddDefinition       `yaml:"children"`
	}

	var def definition
	if err := unmarshal(&def); err != nil {
		return fmt.Errorf("failed decoding group: %s", err)
	}

	g.Transform = def.Transform

	for i, child := range def.Children {
		if builder, ok := child.Definition.(ObjectBuilder); ok {
			g.Children = append(g.Children, builder)
		} else {
			return fmt.Errorf("child #%d: invalid definition. group children must define object types", i)
		}
	}

	return nil
}

func (g *GroupDefinition) Bundle(environment *Environment, w *Writer) error {
	for _, child := range g.Children {
		child.Bundle(environment, w)
	}
	return nil
}

func (g *GroupDefinition) Build(env *Environment) (Object, error) {
	group := raytracer.NewGroup()

	for i, child := range g.Children {
		object, err := child.Build(env)
		if err != nil {
			return nil, fmt.Errorf("failed building child #%d: %s", i, err)
		}

		ApplyTransforms(object, child.Transforms(env))

		group.AddObject(object)
	}

	return group, nil
}

func (g *GroupDefinition) Transforms(*Environment) []TransformDefinition {
	return g.Transform
}

func (g *GroupDefinition) Apply(s *Scene) error {
	obj, err := g.Build(s.env)
	if err != nil {
		return fmt.Errorf("failed building group: %s", err)
	}

	ApplyTransforms(obj, g.Transforms(s.env))

	s.AddObject(obj)
	return nil
}
