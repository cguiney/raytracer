package raytracer

import (
	"math"
	"testing"
)

func TestPoint(t *testing.T) {
	t.Run("Point construction", func(t *testing.T) {
		a := NewPoint(4.3, -4.2, 3.1)

		if a.X() != 4.3 {
			t.Errorf("incorrect X value. have=%f want=%f", a.X(), 4.3)
		}
		if a.Y() != -4.2 {
			t.Errorf("incorrect Y value. have=%f want=%f", a.Y(), -4.2)
		}
		if a.Z() != 3.1 {
			t.Errorf("incorrect Z value. have=%f want=%f", a.Z(), 3.1)
		}

		if a.w() != 1.0 {
			t.Errorf("incorrect w value. have=%f want=%f", a.w(), 1.0)
		}
	})

	t.Run("Point equality", func(t *testing.T) {
		type pointEqualityTest struct {
			a, b Point
			eq   bool
		}

		tests := []pointEqualityTest{
			{NewPoint(4.3, -4.2, 3.1), NewPoint(4.3, -4.2, 3.1), true},
			{NewPoint(4.2, -4.2, 3.1), NewPoint(4.3, -4.2, 3.1), false},
			{NewPoint(4.3, -4.1, 3.1), NewPoint(4.3, -4.2, 3.1), false},
			{NewPoint(4.3, -4.1, 3.0), NewPoint(4.3, -4.2, 3.1), false},
		}

		for _, tt := range tests {
			if eq := tt.a.Eq(tt.b); eq != tt.eq {
				t.Errorf("incorrect equality between point %v and point %v, have=%t want=%t",
					tt.a, tt.b, eq, tt.eq)
			}

			// test commutative equality
			if eq := tt.b.Eq(tt.a); eq != tt.eq {
				t.Errorf("incorrect equality between point %v and point %v, have=%t want=%t",
					tt.b, tt.a, eq, tt.eq)
			}
		}
	})

	t.Run("Travel towards", func(t *testing.T) {
		type pointTravelTest struct {
			src  Point
			path Vector
			dst  Point
		}

		tests := []pointTravelTest{
			{src: NewPoint(3, -2, 5), path: NewVector(-2, 3, 1), dst: NewPoint(1, 1, 6)},
			{src: NewPoint(0, 0, 0), path: NewVector(1, 1, 1), dst: NewPoint(1, 1, 1)},
			{src: NewPoint(0, 0, 0), path: NewVector(-1, -1, -1), dst: NewPoint(-1, -1, -1)},
		}

		// assert that a vector added to a point yields the correct point
		for _, tt := range tests {
			// Test traveling from the source to the destination
			if dst := tt.src.TravelTo(tt.path); !tt.dst.Eq(dst) {
				t.Errorf("incorrect destination for travel from point %v along vector %v. have=%v want=%v",
					tt.src, tt.path, dst, tt.dst)
			}
		}
	})

	t.Run("Path From Point", func(t *testing.T) {
		a, b := NewPoint(3, 2, 1), NewPoint(5, 6, 7)
		want := NewVector(-2, -4, -6)

		if have := a.PathFrom(b); !have.Eq(want) {
			t.Errorf("incorrect vector resulting from subtracting point %v from point %v. have=%v want=%v",
				a, b, have, want)
		}
	})

	t.Run("Travel away along path", func(t *testing.T) {
		p := NewPoint(3, 2, 1)
		v := NewVector(5, 6, 7)
		want := NewPoint(-2, -4, -6)

		if have := p.TravelAway(v); !have.Eq(want) {
			t.Errorf("incorrect point resulting from traveling away from point %v along vector %v. have=%v want=%v",
				p, v, have, want)
		}
	})
}

func TestVector(t *testing.T) {
	t.Run("Vector construction", func(t *testing.T) {
		a := NewVector(4.3, -4.2, 3.1)

		if a.X() != 4.3 {
			t.Errorf("incorrect X value. have=%f want=%f", a.X(), 4.3)
		}
		if a.Y() != -4.2 {
			t.Errorf("incorrect Y value. have=%f want=%f", a.Y(), -4.2)
		}
		if a.Z() != 3.1 {
			t.Errorf("incorrect Z value. have=%f want=%f", a.Z(), 3.1)
		}

		if a.w() != 0.0 {
			t.Errorf("incorrect w value. have=%f want=%f", a.w(), 0.0)
		}
	})

	t.Run("Vector equality", func(t *testing.T) {
		type vectorEqualityTest struct {
			a, b Vector
			eq   bool
		}

		tests := []vectorEqualityTest{
			{NewVector(4.3, -4.2, 3.1), NewVector(4.3, -4.2, 3.1), true},
			{NewVector(4.2, -4.2, 3.1), NewVector(4.3, -4.2, 3.1), false},
			{NewVector(4.3, -4.1, 3.1), NewVector(4.3, -4.2, 3.1), false},
			{NewVector(4.3, -4.1, 3.0), NewVector(4.3, -4.2, 3.1), false},
		}

		for _, tt := range tests {
			if eq := tt.a.Eq(tt.b); eq != tt.eq {
				t.Errorf("incorrect equality between vector %v and vector %v, have=%t want=%t",
					tt.a, tt.b, eq, tt.eq)
			}

			// test commutative equality
			if eq := tt.b.Eq(tt.a); eq != tt.eq {
				t.Errorf("incorrect equality between vector %v and vector %v, have=%t want=%t",
					tt.b, tt.a, eq, tt.eq)
			}
		}
	})

	type vectorOpTest struct {
		a    Vector
		b    Vector
		want Vector
	}

	t.Run("Vector addition", func(t *testing.T) {
		// assert that a vector added to a point yields the correct point
		tests := []vectorOpTest{
			{a: NewVector(3, -2, 5), b: NewVector(-2, 3, 1), want: NewVector(1, 1, 6)},
			{a: NewVector(0, 0, 0), b: NewVector(1, 1, 1), want: NewVector(1, 1, 1)},
			{a: NewVector(0, 0, 0), b: NewVector(-1, -1, -1), want: NewVector(-1, -1, -1)},
		}

		for _, tt := range tests {
			if have := tt.a.Add(tt.b); !tt.want.Eq(have) {
				t.Errorf("incorrect result of addition of vector %v and vector %v. have=%v want=%v",
					tt.a, tt.b, have, tt.want)
			}
		}
	})

	t.Run("Vector Subtraction", func(t *testing.T) {
		tests := []vectorOpTest{
			{a: NewVector(3, 2, 1), b: NewVector(5, 6, 7), want: NewVector(-2, -4, -6)},
		}

		for _, tt := range tests {
			if have := tt.a.Sub(tt.b); !tt.want.Eq(have) {
				t.Errorf("incorrect result of subtraction of vector %v and vector %v. have=%v want=%v",
					tt.a, tt.b, have, tt.want)
			}
		}
	})

	t.Run("Vector Negation", func(t *testing.T) {
		v := NewVector(1, -2, 3)
		want := NewVector(-1, 2, -3)
		if have := v.Negate(); !want.Eq(have) {
			t.Errorf("incorrect result of negating vector %v. have=%v want=%v",
				v, have, want)
		}
	})

	t.Run("Vector scaling", func(t *testing.T) {
		type vectorScalingTest struct {
			input  Vector
			factor Scalar
			want   Vector
		}

		tests := []vectorScalingTest{
			{NewVector(1, -2, 3), 3.5, NewVector(3.5, -7, 10.5)},
			{NewVector(1, -2, 3), 0.5, NewVector(0.5, -1, 1.5)},
		}

		for _, tt := range tests {
			if have := tt.input.Mul(tt.factor); !tt.want.Eq(have) {
				t.Errorf("incorrect result of scaling vector %v by scaling factor %f. have=%v want=%v",
					tt.input, tt.factor, have, tt.want)
			}
		}
	})

	t.Run("Vector magnitude", func(t *testing.T) {
		type vectorMagnitudeTest struct {
			input Vector
			want  Scalar
		}

		tests := []vectorMagnitudeTest{
			{NewVector(1, 0, 0), 1},
			{NewVector(0, 1, 0), 1},
			{NewVector(0, 0, 1), 1},
			{NewVector(1, 2, 3), Scalar(math.Sqrt(14))},
			{NewVector(-1, -2, -3), Scalar(math.Sqrt(14))},
		}

		for _, tt := range tests {
			if have := tt.input.Magnitude(); !have.Eq(tt.want) {
				t.Errorf("incorrect magnitude of vector %v. have=%f want=%f",
					tt.input, have, tt.want)
			}
		}
	})

	t.Run("Vector normalization", func(t *testing.T) {
		v := NewVector(1, 2, 3)
		want := NewVector(0.26726, 0.53452, 0.80178)
		if have := v.Normalize(); !have.Eq(want) {
			t.Errorf("incorrect result of normalization of vector %v. have=%v want=%v",
				v, have, want)
		}
	})

	t.Run("Magnitude of normalized vectors equals 1", func(t *testing.T) {
		type vectorNormalizationTest struct {
			input Vector
		}

		tests := []vectorNormalizationTest{
			{NewVector(4, 0, 0)},
			{NewVector(1, 2, 3)},
		}

		for _, tt := range tests {
			if have := tt.input.Normalize().Magnitude(); !have.Eq(1) {
				t.Errorf("expected magnitude of normalized vector %v to be 1. have=%f", tt.input, have)
			}
		}
	})

	t.Run("Vector dot product", func(t *testing.T) {
		a := NewVector(1, 2, 3)
		b := NewVector(2, 3, 4)
		want := Scalar(20.0)

		if have := a.Dot(b); !have.Eq(want) {
			t.Errorf("incorrect result of dot product between vectors %v and %v. have=%f want=%f",
				a, b, have, want)
		}
	})

	t.Run("Vector cross product", func(t *testing.T) {
		type vectorCrossProductTest struct {
			a    Vector
			b    Vector
			want Vector
		}

		tests := []vectorCrossProductTest{
			{NewVector(1, 2, 3), NewVector(2, 3, 4), NewVector(-1, 2, -1)},
			{NewVector(2, 3, 4), NewVector(1, 2, 3), NewVector(1, -2, 1)},
		}

		for _, tt := range tests {
			if have := tt.a.Cross(tt.b); !have.Eq(tt.want) {
				t.Errorf("incorrect result of cross product between vectors %v and %v. have=%v want=%v",
					tt.a, tt.b, have, tt.want)
			}
		}
	})

	t.Run("reflection", func(t *testing.T) {
		type reflectionTest struct {
			name           string
			in, norm, want Vector
		}

		var sq2 = Scalar(math.Sqrt2 / 2)
		tests := []reflectionTest{
			{
				name: "reflectiong vector approaching at 45 deg",
				in:   NewVector(1, -1, 0),
				norm: NewVector(0, 1, 0),
				want: NewVector(1, 1, 0),
			},
			{
				name: "reflecting vector off a slanted surface",
				in:   NewVector(0, -1, 0),
				norm: NewVector(sq2, sq2, 0),
				want: NewVector(1, 0, 0),
			},
		}

		for _, tt := range tests {
			tt := tt

			t.Run(tt.name, func(t *testing.T) {
				if have := tt.in.Reflect(tt.norm); !have.Eq(tt.want) {
					t.Errorf("incorrect result of reflecting.  have=%v want=%v", have, tt.want)
				}
			})
		}
	})
}
