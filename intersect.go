package raytracer

import "math"

type Surface struct {
	Geometry SurfaceGeometry
	Shading  SurfaceShading
}

type SurfaceGeometry struct {
	UV
	Normal Vector
}

type SurfaceShading struct {
	Normal Vector
	UV     UV
	DPDU   Vector
	DPDV   Vector

	DNDU Vector
	DNDV Vector
}

type Intersection struct {
	object Object
	time   Scalar
	uv     UV
}

func NewIntersection(object Object, time Scalar) Intersection {
	return Intersection{
		object: object,
		time:   time,
	}
}

func NewIntersectionWithUV(object Object, time, u, v Scalar) Intersection {
	x := NewIntersection(object, time)
	x.uv = UV{u, v}
	return x
}

func (i Intersection) Time() Scalar {
	return i.time
}

func (i Intersection) Object() Object {
	return i.object
}

func (i Intersection) U() Scalar {
	return i.uv.U
}

func (i Intersection) V() Scalar {
	return i.uv.V
}

type Intersections []Intersection

func (xs *Intersections) Append(intersections ...Intersection) {
	*xs = append(*xs, intersections...)
}

func (xs *Intersections) Reset() {
	*xs = (*xs)[:0]
}

func (xs *Intersections) Hit() (Intersection, bool) {
	var (
		hit   Intersection
		found bool
	)
	for _, x := range *xs {
		if x.Time() < 0 {
			continue
		}

		// hit hasn't been set yet (or multiple hits at 0)
		if hit.Time() == 0 {
			if !found {
				hit = x
				found = true
			}
			continue
		}

		// found a new closer hit
		if x.Time() < hit.Time() {
			hit = x
		}
	}

	return hit, found
}

func (xs *Intersections) Len() int {
	return len(*xs)
}

func (xs *Intersections) Less(i, j int) bool {
	return (*xs)[i].Time() < (*xs)[j].Time()
}

func (xs *Intersections) Swap(i, j int) {
	(*xs)[i], (*xs)[j] = (*xs)[j], (*xs)[i]
}

func Union(xs ...Intersections) Intersections {
	n := 0
	for _, x := range xs {
		n += len(x)
	}

	u := make(Intersections, 0, n)

	for _, x := range xs {
		u = append(u, x...)
	}

	return u
}

func findRefractiveIndices(hit Intersection, xs Intersections) (exit Scalar, enter Scalar) {
	var containers stackSet
	for _, x := range xs {
		if x == hit {
			if containers.empty() {
				exit = 1.0
			} else {
				exit = containers.peek().Material().(*MaterialProperties).RefractiveIndex()
			}
		}

		if containers.contains(x.object) {
			containers.remove(x.object)
		} else {
			containers.insert(x.object)
		}

		if x == hit {
			if containers.empty() {
				enter = 1.0
			} else {
				enter = containers.peek().Material().(*MaterialProperties).RefractiveIndex()
			}
		}
	}

	return
}

type SurfaceInteraction struct {
	Surface
	Interaction
}

type Interaction struct {
	hit     Intersection
	time    Scalar // time of intersection along ray
	object  Object // object collided with
	point   Point  // actual collision point
	over    Point  // point, but just a bit of fudge to prevent any "acne" during shading
	under   Point  // point, but just a bit of fudge to move it under the surface (used for refraction)
	eye     Vector // direction of the eye
	normal  Vector // normal of the object at the time of collision
	reflect Vector // direction of reflection
	inside  bool   // if the hit occurred inside the object

	// refractive indexes of material being exited and material being entered
	exit, enter Scalar

	uv UV
}

func NewInteraction(hit Intersection, r Ray, xs ...Intersection) Interaction {
	var (
		pos    = r.Position(hit.time)
		eye    = r.direction.Negate()
		normal = NormalAt(hit.object, pos, hit)
		inside = normal.Dot(eye) < 0
		exit, enter = findRefractiveIndices(hit, xs)
		// exit, enter Scalar = 1.0, 1.0
	)

	normal = normal.Forward(eye)

	return Interaction{
		hit:     hit,
		time:    hit.time,
		object:  hit.object,
		point:   pos,
		over:    pos.TravelTo(normal.Mul(epsilon)),
		under:   pos.TravelAway(normal.Mul(epsilon)),
		eye:     eye,
		normal:  normal,
		reflect: r.Direction().Reflect(normal),
		exit:    exit,
		enter:   enter,
		uv:      hit.uv,
		inside:  inside,
	}
}

func NewSurfaceInteraction(hit Intersection, r Ray, xs ...Intersection) SurfaceInteraction {
	var (
		interaction = NewInteraction(hit, r, xs...)
		surface     = SurfaceAt(hit.Object(), interaction)
	)

	surface.Geometry.Normal = surface.Geometry.Normal.Forward(interaction.eye)
	surface.Shading.Normal = surface.Shading.Normal.Forward(interaction.eye)
	interaction.uv = surface.Shading.UV

	return SurfaceInteraction{
		Surface:     surface,
		Interaction: interaction,
	}
}

// reflectance implements schlick's algorithm to determine how much light is reflected on the
// surface at the intersection.
// it returns a scalar between 0 and 1
func (x *Interaction) reflectance() Scalar {
	var (
		cos   = float64(x.eye.Dot(x.normal))
		ratio = float64(x.exit / x.enter)
		sin   = math.Pow(ratio, 2) * (1.0 - math.Pow(cos, 2))
	)

	if x.exit > x.enter {
		if sin > 1 {
			return 1
		}

		// when n1 > n2 use cos(theta_t)
		cos = math.Sqrt(1 - sin)
	}

	r := math.Pow(float64((x.exit-x.enter)/(x.exit+x.enter)), 2)
	return Scalar(r + (1-r)*math.Pow(1-cos, 5))
}

type item struct {
	Object
	next *item
	prev *item
}

type stackSet struct {
	head *item

	// map of object->index of object in stack
	set map[Object]*item
}

func (s *stackSet) insert(x Object) {
	if s.set == nil {
		s.set = make(map[Object]*item)
	}

	item := &item{
		Object: x,
		next:   s.head,
	}

	if s.head != nil { // no previous if this is the first item in the list
		s.head.prev = item
	}

	s.head = item

	s.set[x] = s.head
}

func (s *stackSet) contains(x Object) bool {
	_, ok := s.set[x]
	return ok
}

func (s *stackSet) pop() Object {
	x := s.head.Object
	s.head = s.head.next
	delete(s.set, x)
	return x
}

func (s *stackSet) remove(x Object) {
	item, ok := s.set[x]
	if !ok {
		return
	}

	// at the head of the list, need to update that reference
	if item == s.head {
		s.head = item.next
	}

	if item.prev != nil {
		item.prev.next = item.next
	}

	if item.next != nil {
		item.next.prev = item.prev
	}

	delete(s.set, x)
}

func (s *stackSet) peek() Object {
	if s.head == nil {
		return nil
	}
	return s.head.Object
}

func (s *stackSet) len() int {
	return len(s.set)
}

func (s *stackSet) empty() bool {
	return s.len() == 0
}
